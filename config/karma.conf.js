/**
 * @author: @MediatechSolutions
 */

module.exports = function (config) {
    var testWebpackConfig = require('./webpack.test.js')({ env: 'test' });

    var configuration = {

        // base path that will be used to resolve all patterns (e.g. files, exclude)
        basePath: '',

        /*
         * Frameworks to use
         *
         * available frameworks: https://npmjs.org/browse/keyword/karma-adapter
         */
        frameworks: ['jasmine'],

        // list of files to exclude
        exclude: [],

        client: {
            captureConsole: false
        },

        /*
         * list of files / patterns to load in the browser
         *
         * we are building the test environment in ./spec-bundle.js
         */
        files: [
            { pattern: './config/spec-bundle.js', watched: false },
            { pattern: './src/assets/**/*', watched: false, included: false, served: true, nocache: false }
        ],

        /*
         * By default all assets are served at http://localhost:[PORT]/base/
         */
        proxies: {
            "/assets/": "/base/src/assets/"
        },

        /*
         * preprocess matching files before serving them to the browser
         * available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
         */
        preprocessors: {
            './config/spec-bundle.js': ['webpack', 'sourcemap'],
            './src/app/index.ts': ['coverage'],
        },

        // Webpack Config at ./webpack.test.js
        webpack: testWebpackConfig,

        specReporter: {
            maxLogLines: 5,             // limit number of lines logged per test
            suppressErrorSummary: true, // do not print error summary
            suppressFailed: false,      // do not print information about failed tests
            suppressPassed: false,      // do not print information about passed tests
            suppressSkipped: true,      // do not print information about skipped tests
            showSpecTiming: true,       // print the time elapsed for each spec
            failFast: false,            // test would finish with error when a first fail occurs
        },

        coverageReporter: {
            reporters: [
                { type: 'cobertura', subdir: '.', file: 'cobertura.txt' },
                { type: 'in-memory' },
            ],
            instrumenterOptions: {
                istanbul: { noCompact: true },
            },
            includeAllSources: true,
        },

        remapCoverageReporter: {
            'text-summary': null,
            html: './coverage/html',
            cobertura: './coverage/cobertura.txt',
        },

        // Webpack please don't spam the console when running in karma!
        webpackMiddleware: {
            // webpack-dev-middleware configuration
            // i.e.
            noInfo: true,
            // and use stats to turn off verbose output
            stats: {
                // options i.e.
                chunks: false
            }
        },

        /*
         * test results reporter to use
         *
         * possible values: 'dots', 'progress'
         * available reporters: https://npmjs.org/browse/keyword/karma-reporter
         */
        reporters: ['spec', 'coverage', 'remap-coverage', 'junit'], //'progress', 'mocha'

        junitReporter: {
            outputDir: 'test_results', // results will be saved as $outputDir/$browserName.xml
            useBrowserName: true // add browser name to report and classes names
        },

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        /*
         * level of logging
         * possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
         */
        logLevel: config.LOG_WARN,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        /*
         * start these browsers
         * available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
         */
        browsers: ['ChromeNoSandbox'],

        customLaunchers: {
            ChromeNoSandbox: {
                base: 'Chrome',
                flags: ['--no-sandbox']
            }
        },

        /*
         * Continuous Integration mode
         * if true, Karma captures browsers, runs the tests and exits
         */
        singleRun: true
    };
    config.set(configuration);
};
