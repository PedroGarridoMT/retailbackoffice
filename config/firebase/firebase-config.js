var FireBaseConfig = {
    prod: {
        apiKey: "AIzaSyDovaPXLljVDBSNwrOtdBTtfd-Ev_01BxU",
        authDomain: "retailmonitoring-b7bdf.firebaseapp.com",
        databaseURL: "https://retailmonitoring-b7bdf.firebaseio.com",
        projectId: "retailmonitoring-b7bdf",
        storageBucket: "retailmonitoring-b7bdf.appspot.com",
        messagingSenderId: "842369267168"
    },

    pre: {
        apiKey: "AIzaSyBsJj2SBoPNvSivLaBVJyudPTCTBk_Ptgo",
        authDomain: "preretailmonitoring.firebaseapp.com",
        databaseURL: "https://preretailmonitoring.firebaseio.com",
        projectId: "preretailmonitoring",
        storageBucket: "preretailmonitoring.appspot.com",
        messagingSenderId: "103951360496"
    },

    dev: {
        apiKey: "AIzaSyAbG9-tRfo23HSQV7dmQWJsjU0Sh4fvtnQ",
        authDomain: "devmonitoring-f21cc.firebaseapp.com",
        databaseURL: "https://devmonitoring-f21cc.firebaseio.com",
        projectId: "devmonitoring-f21cc",
        storageBucket: "devmonitoring-f21cc.appspot.com",
        messagingSenderId: "663765618635"
    }
};

module.exports = FireBaseConfig;
