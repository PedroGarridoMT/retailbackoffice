FROM kyma/docker-nginx
EXPOSE 80
RUN rm /etc/nginx/sites-enabled/default
COPY nginx/ /etc/nginx/sites-enabled/
COPY dist/ /var/www
CMD 'nginx'
