/**
 * @author: @MediatechSolutions
 */
const oneSkyPublicKey = 'Qyludc3bpwLyKRsKdHhjipQmfhKGp4W5';
const oneSkySecretKey = 'ZJe4NP1I8nDJing3avpHfKhGe0BxAhtq';
const oneSkyProjectId = 87129;

var onesky = require('onesky-utils');
var fs = require('fs');

var englishFilename = 'iris_en.json';
var spanishFilename = 'iris_es.json';

var options_en = {
    language: 'en',
    secret: oneSkySecretKey,
    apiKey: oneSkyPublicKey,
    projectId: oneSkyProjectId,
    fileName: 'iris-bo.json'
};

var options_es = {
    language: 'es',
    secret: oneSkySecretKey,
    apiKey: oneSkyPublicKey,
    projectId: oneSkyProjectId,
    fileName: 'iris-bo.json'
};

function getOneSkyFile(options, filename) {
    onesky.getFile(options).then(function (content) {

        fs.writeFile('src/assets/i18n/' + filename, content, function (err) {
            err ? console.log('Error writing translations in file', err) : console.log('>> Contents saved in ', filename);
        });
    }).catch(function (error) {
        console.log('Error getting translations from onesky', error);
    });
}

getOneSkyFile(options_en, englishFilename);
getOneSkyFile(options_es, spanishFilename);

