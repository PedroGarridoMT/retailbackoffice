import { Injectable } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

/**
 *  Each call to the NgbModal's open method will return the same object as real method does, but
 *  now result property (promise) on that object can be resolved or rejected on demand using
 *  resolve and reject methods of FakeModalService instance
 *
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 *  1. Add "{ provide: NgbModal, useClass: FakeModalService }" in testBed providers
 *  2. Override componentInstance property or close method of modalRef object (one that "open" method returns)
 *
 *       fakeModalService.componentInstance = {
 *          componentInput: null,
 *          componentOutput: new Subject()
 *       };
 *
 *       fakeModalService.close = (result: any): void => { ... }
 *
 *  3. Call the component method that calls NgbModal's open method, and mock component inputs and outputs if needed
 *
 *      comp.openModal();
 *
 *      openModal method definition example:
 *
 *          public openModal(): void {
 *              ...
 *              const modalRef: NgbModalRef = this.ngbModalService.open(SomeModalComponent, options);
 *              modalRef.componentInstance.componentInput = "Some Input";
 *              modalRef.componentInstance.componentOutput.subscribe((event: Object) => this.someButtonClickHandler(data));
 *              modalRef.result.then(
 *                     (result) => this.someModalCloseEventHandler(result),
 *                     (error) => this.someModalCloseEventHandler(error),
 *              );
 *              ...
 *          }
 *
 *     comp.openModal() will set resolve and reject methods in FakeModalService instance
 *
 *  4. Resolve or reject promise:
 *
 *     By calling fakeModalService.resolve("resolution");
 *     By calling fakeModalService.reject("reason");
 *
 * //////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 *  EXAMPLE:
 *
 *  // { provide: NgbModal, useClass: FakeModalService }
 *
 *  beforeEach(() => {
 *       ...
 *       fakeModalService = TestBed.get(NgbModal);
 *       ...
 *  });
 *
 *  it("should ...", fakeAsync(() => {
 *       spyOn(comp, "someButtonClickHandler");
 *       spyOn(comp, "someModalCloseEventHandler");
 *
 *       fakeModalService.componentInstance = {
 *          componentInput: null,
 *          componentOutput: new Subject()
 *       };
 *       comp.openModal();
 *
 *       fakeModalService.componentInstance.componentOutput.next({some: "data"});
 *       fakeModalService.resolve({some: "result"});
 *       // fakeModalService.reject({some: "error"});
 *
 *       tick();
 *
 *       expect(comp.someButtonClickHandler).toHaveBeenCalledWith({some: "data"});
 *       expect(comp.someModalCloseEventHandler).toHaveBeenCalledWith({some: "result"});
 *       // expect(comp.someModalCloseEventHandler).toHaveBeenCalledWith({some: "error"});
 *  }));
 *
 * @export
 * @class FakeModalService
 */

@Injectable()
export class FakeModalService {

    public componentInstance: any = {};
    public resolve: Function;
    public reject: Function;
    public CONFIRM: string = "CONFIRM";

    public close(): void { }

    public open(): Object {
        return {
            componentInstance: this.componentInstance,
            result: new Promise((resolve, reject) => {
                this.resolve = resolve;
                this.reject = reject;
            }),
            close: this.close
        }
    }

    public closeAll(): void { }
}
