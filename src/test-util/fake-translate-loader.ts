import { TranslateLoader } from "@ngx-translate/core";
import { Observable } from "rxjs";

let translations: any = {"TEST_KEY": "This is a test"};

export class FakeTranslateLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return Observable.of(translations);
    }
}