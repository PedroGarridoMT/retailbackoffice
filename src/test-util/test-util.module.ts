import { NgModule } from "@angular/core";
import { DummyComponent } from "./dummy.component";
import { DummyMtPermissionsDirective } from "./dummy-mt-permissions.directive";

@NgModule({
    imports: [],
    declarations: [
        DummyComponent,
        DummyMtPermissionsDirective
    ],
    providers: [],
    exports: [
        DummyComponent,
        DummyMtPermissionsDirective
    ]
})
export class TestUtilModule {
}
