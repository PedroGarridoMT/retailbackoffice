import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[mtPermissions]'
})
export class DummyMtPermissionsDirective {
    @Input() public mtPermissions: string;
}
