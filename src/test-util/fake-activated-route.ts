import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ActivatedRouteStub {

    private _subjectParamMap = new BehaviorSubject<Object>({});
    private _subjectQueryParamMap = new BehaviorSubject<Object>({});
    private _subjectParentParamMap = new BehaviorSubject<Object>({});
    private _subjectParentQueryParamMap = new BehaviorSubject<Object>({});

    public params: Observable<Object> = this._subjectParamMap.asObservable();
    public paramMap: Observable<Object> = this._subjectParamMap.asObservable();
    public queryParams: Observable<Object> = this._subjectQueryParamMap.asObservable();
    public queryParamMap: Observable<Object> = this._subjectQueryParamMap.asObservable();

    public parent: {
        params: Observable<Object>,
        paramMap: Observable<Object>,
        queryParams: Observable<Object>,
        queryParamMap: Observable<Object>
    };

    constructor() {
        this.parent = {
            params: this._subjectParentParamMap.asObservable(),
            paramMap: this._subjectParentParamMap.asObservable(),
            queryParams: this._subjectParentQueryParamMap.asObservable(),
            queryParamMap: this._subjectParentQueryParamMap.asObservable()
        };
    }

    public setParams(mockedParams: Object): void {
        this._subjectParamMap.next(mockedParams);
    }
    public setQueryParams(mockedParams: Object): void {
        this._subjectQueryParamMap.next(mockedParams);
    }
    public setParentParams(mockedParams: Object): void {
        this._subjectParentParamMap.next(mockedParams);
    }
    public setParentQueryParams(mockedParams: Object): void {
        this._subjectParentQueryParamMap.next(mockedParams);
    }
}
