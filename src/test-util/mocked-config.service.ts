export let MockedConfigService = {
    load(){
    },

    getConfiguration(){
    },

    getEndpoint(endpoint: string){
    },

    getKey(key: string): any{
        switch (key.toLowerCase()) {
            case "languages":
                return [
                    {"value": "en", "selected": false, "name": "English", "flagClass": "flag flag-us"},
                    {"value": "es", "selected": false, "name": "Español", "flagClass": "flag flag-es"}
                ];

        }
        return null;
    }
};
