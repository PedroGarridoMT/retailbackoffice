import { Component, OnInit, ViewChild } from "@angular/core";
import { HomeTemplateDashboardItem, Language } from "mt-web-components";
import { TranslateService } from "@ngx-translate/core";
import { ConfigService } from "../core/config.service";

@Component({
    styleUrls: ['./settings-main.component.scss'],
    templateUrl: './settings-main.component.html'

})
export class SettingsMainComponent implements OnInit {
    appLanguages: Language[] = null;
    dashboardItems: HomeTemplateDashboardItem[];
    @ViewChild('languageSelectorTemplate') languageSelectorTemplate;

    constructor(private translate: TranslateService, private config: ConfigService) {
        this.appLanguages = config.getKey("languages") as Language[];
        this.appLanguages.forEach(lang => {
            lang.selected = this.translate.currentLang ? (this.translate.currentLang.substring(0, 2) == lang.value) : false;
        });
    }

    ngOnInit(): void {
        this.dashboardItems = [
            { template: this.languageSelectorTemplate, templateContext: null }
        ];
    }

    onLanguageChanged(selectedLang) {
        this.config.setLanguageAndLocale(selectedLang);
    }
}
