import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { BaseRequestOptions, ConnectionBackend, Http } from "@angular/http";
import { MockBackend } from "@angular/http/testing";
import { HmrStateService } from "../core/hmr/hmr.service";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { ConfigService } from "../core/config.service";
import { AuthenticationService } from "../account/shared/authentication.service";
import { MockedConfigService } from "../../test-util/mocked-config.service";
import { FakeTranslateLoader } from "../../test-util/fake-translate-loader";
import { NO_ERRORS_SCHEMA } from "@angular/core";
// Load the implementations that should be tested
import { SettingsMainComponent } from "./settings-main.component";
import { RouterTestingModule } from "@angular/router/testing";
import { DummyComponent } from "../../test-util/dummy.component";


describe(`Settings Home`, () => {
    let comp: SettingsMainComponent;
    let fixture: ComponentFixture<SettingsMainComponent>;
    let authenticationServiceStub = { login() {}, logout() {} };
    
    // async beforeEach
    beforeEach(async(() => {


        TestBed.configureTestingModule({
            declarations: [SettingsMainComponent, DummyComponent /*LangSelectorComponent*/],
            schemas: [NO_ERRORS_SCHEMA], // TODO Review why we get .ts warnings when we use our mt-components
            providers: [
                BaseRequestOptions,
                MockBackend,
                HmrStateService,
                {provide: AuthenticationService, useValue: authenticationServiceStub},
                {
                    provide: Http,
                    useFactory: (backend: ConnectionBackend, defaultOptions: BaseRequestOptions) => {
                        return new Http(backend, defaultOptions);
                    },
                    deps: [MockBackend, BaseRequestOptions]
                },
                {
                    provide: ConfigService,
                    useValue: MockedConfigService
                }
            ],
            imports: [
                RouterTestingModule.withRoutes([{path: 'login', component: DummyComponent}]),
                TranslateModule.forRoot({loader: {provide: TranslateLoader, useClass: FakeTranslateLoader}}),

            ]
        })
            .compileComponents(); // compile template and css
    }));

    // synchronous beforeEach
    beforeEach(() => {
        fixture = TestBed.createComponent(SettingsMainComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges(); // trigger initial data binding
    });

    it('should render', () => {
        expect(comp).toBeDefined;
    });

    it('should load the application languages', () => {
        expect(comp.appLanguages).toBeDefined;
        expect(comp.appLanguages.length).toBeGreaterThan(0);
    });

});
