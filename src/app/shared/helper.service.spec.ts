import { Observable } from 'rxjs';
import { RequestOptions, ResponseOptions } from "@angular/http";
import { HelperService } from "./helper.service";
import { BaseResultsCode, ApiResponse } from "./index";

describe("Helper service", () => {

    beforeEach(() => {
        localStorage.clear();
    });

    afterEach(() => {
        localStorage.clear();
    });

    it("should be initialized", () => {
        const helperService = new HelperService();
        expect(helperService).toBeTruthy();
    });

    it("Should return request options with multi part headers", () => {
        const headers = HelperService.addMultiPartHeader().headers;
        expect(headers.get("Content-Type")).toBe("multipart/form-data");
        expect(headers.get("Accept")).toBe("application/json");
    });

    it("Should clear local storage", () => {
        localStorage.setItem("permissions", "permissions");
        localStorage.setItem("user", "user");
        localStorage.setItem("last-player-search", "last-player-search");
        document.cookie = "XSRF-TOKEN=XSRF-TOKEN";

        HelperService.clearLocalStorage();

        expect(document.cookie.indexOf("XSRF-TOKEN")).toBe(-1);
        expect(localStorage.getItem("permissions")).toBe(null);
        expect(localStorage.getItem("user")).toBe(null);
        expect(localStorage.getItem("last-player-search")).toBe(null);
    });

    it("should return base result code", () => {
        for (let i = 200; i < 300; i++) {
            expect(HelperService.getBaseResultCode(i)).toBe(BaseResultsCode.Ok);
        }

        expect(HelperService.getBaseResultCode(400)).toBe(BaseResultsCode.BadRequest);
        expect(HelperService.getBaseResultCode(401)).toBe(BaseResultsCode.Unauthorized);
        expect(HelperService.getBaseResultCode(403)).toBe(BaseResultsCode.Forbidden);
        expect(HelperService.getBaseResultCode(404)).toBe(BaseResultsCode.NotFound);
        expect(HelperService.getBaseResultCode(409)).toBe(BaseResultsCode.AlreadyExists);
        expect(HelperService.getBaseResultCode(422)).toBe(BaseResultsCode.Unprocessable);

        expect(HelperService.getBaseResultCode(500)).toBe(BaseResultsCode.InternalServerError);
        expect(HelperService.getBaseResultCode(123)).toBe(BaseResultsCode.InternalServerError);
    });

    it("should handle error response with error body", () => {
        const body = { errorCode: "errorCode", errorDescription: "errorDescription" };
        const response = { status: 400, statusText: "statusText", json: () => body };
        const result: Observable<ApiResponse> = HelperService.handleError(response);

        result.subscribe(null, error => {
            expect(error.error).toEqual(body);
            expect(error.statusCode).toBe(BaseResultsCode.BadRequest);
        });
    });

    it("should handle error response without error body", () => {
        const response = { status: 500, statusText: "statusText", json: () => { throw Error("Error"); } };
        const result: Observable<ApiResponse> = HelperService.handleError(response);

        result.subscribe(null, error => {
            expect(error.error).toEqual(null);
            expect(error.statusCode).toBe(BaseResultsCode.InternalServerError);
        });
    });

    it("Should return return API response", () => {
        const body = { errorCode: "errorCode", errorDescription: "errorDescription" };
        const response = { status: 400, statusText: "statusText", json: () => body };
        const result: ApiResponse = HelperService.getApiResponse(response);

        expect(result.statusCode).toEqual(BaseResultsCode.BadRequest);
        expect(result.error).toEqual(null);
    });

});
