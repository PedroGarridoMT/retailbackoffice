import { Component, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { TablePagination } from "mt-web-components";
import { Subscription } from "rxjs";

@Component({
    template: ''
})
export class BaseSearchComponent implements OnDestroy {

    route: ActivatedRoute;
    queryParams: any = null;

    // mt-basic-filters configuration
    valuesToSearch: any = {};

    // mt-table configuration
    pagingData: TablePagination;


    protected _subscriptions: Subscription[] = [];


    constructor(activatedRoute: ActivatedRoute) {
        this.route = activatedRoute;

        this._subscriptions.push(
            this.route.queryParamMap.subscribe((params: Params) => {
                this.parseQueryParams(params);
            })
        );

        this.resetPagingParams();
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    parseQueryParams(params) {
        if (params && params.keys && params.keys.length > 0) {
            if (this.queryParams == null)
                this.queryParams = {};

            params.keys.forEach((key, index) => {
                this.queryParams[params.keys[index]] = params.params[key];
            });
        }
        //console.log('query params parsed:', this.queryParams);
    }

    thereAreQueryParams() {
        return this.queryParams && Object.keys(this.queryParams).length > 0;
    }

    prepareDataWithQueryParams() {
        this.valuesToSearch = this.queryParams;

        if (this.queryParams["pageNumber"] && this.queryParams["pageSize"]) {
            this.pagingData = {
                currentPage: Number(this.queryParams["pageNumber"]),
                pageSize: Number(this.queryParams["pageSize"]),
                totalPages: 0,
                totalItems: 0
            };
            delete this.valuesToSearch["pageNumber"];
            delete this.valuesToSearch["pageSize"];
            // also remove sorting params when implemented
        }
    }

    getValueFromParams(paramName) {
        if (this.queryParams && this.queryParams[paramName]) {
            return this.queryParams[paramName];
        }
        return null;
    }

    /**
     * Search query params matching the name passed.
     *
     * @param paramName.
     * @returns []
     */
    getMultipleSelectionFromParams(paramName) {

        if (this.queryParams && this.queryParams[paramName]) {
            return Array.isArray(this.queryParams[paramName]) ?
                this.queryParams[paramName] : this.queryParams[paramName].split(",");
        }
        return null;
    }

    /**
     * Search query params matching the name passed.
     * If there are some, the method returns a DateRange object that the basic-filter-bar understands.
     *
     * @param paramName. Since we're looking for ranges, the suffixes "To" and "From" will be added.
     * Example:  paramName = "someDate". The method will look for the parameters "someDateFrom" and "someDateTo"
     * @returns a date range object {from:{},to:{}}
     */
    getDateRangeFromParams(paramName) {
        let dateRange = {};
        let paramFrom = paramName + "From";
        let paramTo = paramName + "To";

        if (this.queryParams && (this.queryParams[paramFrom] || this.queryParams[paramTo])) {
            if (this.queryParams[paramFrom]) {
                let date = new Date(this.queryParams[paramFrom]);
                dateRange["from"] = {};
                dateRange["from"]["day"] = date.getDate();
                dateRange["from"]["month"] = date.getMonth();
                dateRange["from"]["year"] = date.getFullYear();
                dateRange["from"]["hour"] = date.getHours();
                dateRange["from"]["minute"] = date.getMinutes();
            }
            if (this.queryParams[paramTo]) {
                let date = new Date(this.queryParams[paramTo]);
                dateRange["to"] = {};
                dateRange["to"]["day"] = date.getDate();
                dateRange["to"]["month"] = date.getMonth();
                dateRange["to"]["year"] = date.getFullYear();
                dateRange["to"]["hour"] = date.getHours();
                dateRange["to"]["minute"] = date.getMinutes();
            }
            return dateRange;
        }
    }

    /**
     * Search query params matching the name passed.
     * If there are some, the method returns a NumberRange object that the basic-filter-bar understands.
     *
     * @param paramName. Since we're looking for ranges, the suffixes "To" and "From" will be added.
     * Example:  paramName = "amount". The method will look for the parameters "amountFrom" and "amountTo"
     * @returns a date range object {from:number,to:number}
     */
    getAmountRangeFromParams(paramName) {
        let amountRange = {};
        let paramFrom = paramName + "From";
        let paramTo = paramName + "To";
        if (this.queryParams && (this.queryParams[paramFrom] || this.queryParams[paramTo])) {
            if (this.queryParams[paramFrom])
                amountRange["from"] = Number(this.queryParams[paramFrom]);
            if (this.queryParams[paramTo])
                amountRange["to"] = Number(this.queryParams[paramTo]);

            return amountRange;
        }
        return null;
    }

    resetPagingParams() {
        this.pagingData = { currentPage: 1, pageSize: 10, totalPages: 0, totalItems: 0 };
    }

    getPaginationData() {
        return {
            pageNumber: this.pagingData.currentPage,
            pageSize: this.pagingData.pageSize,
        }
    }


}
