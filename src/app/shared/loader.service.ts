import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class LoaderService {
    // Observable string sources
    private loaderSource = new Subject<boolean>();
    private requests: number = 0;

    // Observable string streams
    loaderStatus$ = this.loaderSource.asObservable();

    // Service message commands
    show() {
        this.requests++;
        this.loaderSource.next(true);
    }

    hide() {
        // Async hide - to be able to pickup sync "show" requests
        Observable.of(null).delay(0).subscribe(() => {
            this.requests--;
            if(this.requests <= 0) {
                this.loaderSource.next(false);
            }
        });
    }
}
