export interface SystemCurrency {
    symbol: string,
    code: string,
    name: string,
    decimal_digits: number
}