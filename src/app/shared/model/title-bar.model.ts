import { ItemNavigatorLiterals, CustomNavigation } from "mt-web-components";

export class TitleBarActions {
    id: string;
    icon: string;
    label: string;
}

export class ItemNavigatorParams {
    links?: Array<string>;
    literals?: ItemNavigatorLiterals;
    currentIndex: number;
    customNavigation?: CustomNavigation;
    disabled?: boolean;
}

export class TitleBarPreviousRouteDetails {
    previousPagePath: string;
    previousPageLabel: string;
    previousPageQueryParams?: Object;
}

export class TitleBarParams extends TitleBarPreviousRouteDetails {
    pageTitle: string;
    pageActions: TitleBarActions[];
    itemNavigator?: ItemNavigatorParams;
}
