/**
 *
 */
export interface DefaultSearchRequest {
    /**
     * Page size. (Pagination)
     */
    pageSize?: number;

    /**
     * Page number / Current page number. (Pagination)
     */
    pageNumber?: number;

    /**
     * Sort by property.
     */
    sortBy?: string;

    /**
     * Sort direction. 'ASC' / 'DESC' possible values.
     */
    sortDirection?: string;

    /**
     * Account code. External and unique identifier representing a specific player.
     */
    accountCode?: string;

    /**
     * filter could be any other field to be used as filter in the a REST request.
     */
    [filter: string]: any;
}
