/**
 * Backoffice PAS 3 API
 * # Introduction  This API provides web applications with the necessary features to manage a casino operation. Destined applications are Back Office web applications.  # Authentication and Authorization  This API allows only authenticated Back Office users to access the different operations. Thus, in order to be able to call this API it is first necesary to open a user session via the `POST /session` operation. This operation will generate a session token, stored in cookies. Make sure your web application is able to use cookies, because otherwise the API will not be able to authenticate the Back Office users.  Session tokens stored in cookies expire after a configurable period of time. You will be notified about the expiration of your session in the response of the `POST /session` operation. In order to prevent a Back Office user from getting logged out of the Back Office while he/she is actively using the tool you are able to refresh the session token via the `PATCH /session`.  # Cross-Site Request Forgery  This API has active Cross-Site Request Forgery controls. When opening a user session with `POST /session`, a `XSRF-TOKEN` cookie is created. In order to perform successful requests to the API you need to read that cookie and provide its value in an `X-XSRF-TOKEN` HTTP header in every request. ## 4. Player Validations When registering a player, different validations are performed to the player details, according to the regulated market and configuration.    The following table summarizes the available checks and differences in different regulated markets:    | Check | Returned Validation Error | International | Spain | Madrid | Colombia | Notes |   | --- | --- | --- | --- | --- | --- | --- |   | User name validity | InvalidUserName | Checked. Regular expression: `[^\\p{L}\\p{N}_.-]` | Checked. Regular expression: `[^0-9A-Za-z]` | Checked. Regular expression: `[^0-9A-Za-z]` | Checked. Regular expression: `[^0-9A-Za-z]` |  |   | Email address validity | InvalidEmail | Checked. | Checked. | Checked. | Checked. | Regular expression:  `^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$` |   | Password strength | WeakPassword | Configurable | Configurable | Configurable | Configurable | If configured to be performed, the check  validates that the password contains at least three of the following four groups of caracters: lower-case letters, upper-case letters, numbers and symbols. |   | Document number format | WrongDocumentNumberFormat | Checked. Regular expression: `[^A-Za-z0-9 ]` and maximum length to 11 characters. | Checked. Valid Spanish DNI and NIE are checked (according to the specified document type). | Checked. Valid Spanish DNI and NIE are checked (according to the specified document type). | Not checked. |  |   | The document number has been provided | EmptyDocumentNumber | Checked | Checked | Checked | Checked |  |   | The document type has been provided | UnspecifiedDocumentType | Checked | Checked | Checked | Checked |  |   | Phone number format | InvalidPhone | Checked | Checked | Checked | Checked | Regular expression: ^[\\+0-9]{2,3}[1-9\\s]{1}[0-9]{3,14}$` ( Ej. +34 12345678901234 )|   | The person title has been provided | UnspecifiedPersonTitle | Checked | Checked | Checked | Checked | For colombian regulated market, this field is optional |   | The first name has been provided | EmptyFirstName | Checked | Checked | Checked | Checked |  |   | The last name has been provided | EmptyLastName | Checked | Checked | Checked | Checked |  |   | The family name has been provided | EmptyLastName | Checked | Checked | Checked | Checked |  |   | The second family name has been provided | EmptySecondLastName | Not checked | Checked | Checked | Checked |   | The gender has been provided | UnspecifiedGender | Checked | Checked | Checked | Checked | |   | The birth date has been provided | EmptyBirthDate | Checked | Checked | Checked | Checked | |   | The nationality has been provided | UnspecifiedNationality | Checked | Checked | Checked | Checked | |   | The fiscal residence has been provided | UnspecifiedFiscalResidence | Checked | Checked | Checked | Checked | If the system is configured to validate the player address. |   | The country of residence has been provided | UnspecifiedCountry | Checked | Checked | Checked | Checked | If the system is configured to validate the player address.  |   | The country of fiscal residence has been provided | UnspecifiedFiscalResidenceCountry | Checked | Checked | Checked | Checked | If the system is configured to validate the player address.  |   | The address has been provided. | EmptyAddress | Checked | Checked | Checked | Checked | If the system is configured to validate the player address. |   | The post code has been provided | EmptyAddrPostalCode | Checked | Checked | Checked | Checked | If the system is configured to validate the player address. |   | The name of the region of residence has been provided | EmptyRegionText | Checked | Checked | Checked | Checked | If the system is configured to validate the player address, except for colombian market, where this field is optional in any case|   | The name of the city of residence has been provided | EmptyCityText | Checked | Checked | Checked | Checked | Checked if the system is configured to validate the player address, except for colombian market, where this field is always optional  |   | The code of the city of residence has been provided | UnspecifiedCityCode | Checked | Checked | Checked | Checked | Checked if the system is configured to validate the player address. But not for colombian market, where this field y always mandatory |   | The code of the region of residence has been provided | UnspecifiedRegionCode | Checked | Checked | Checked | Checked | Checked if the system is configured to validate the player address, but not for colombian market where this field is mandatory in any case. |   | The player language of choice has been provided | UnspecifiedLanguage | Checked | Checked | Checked | Checked | |   | The mobile phone number of the player has been provided | EmptyMobile | Checked | Checked | Checked | Checked | |   | The player user name has been provided | EmptyUserName | Checked | Checked | Checked | Checked | |   | The e-mail adddress has been provided | EmptyEmail | Checked | Checked | Checked | Checked | |   | The player password has been provided | EmptyPassword | Checked | Checked | Checked | Checked | |   | The security question has been provided | EmptySecurityQuestion | Checked | Checked | Checked | Checked | |   | The answer to the security question | EmptySecurityAnswer | Checked | Checked | Checked | Checked | |   | The player currency has been provided | UnspecifiedCurrency | Checked | Checked | Checked | Checked | |   | The terms and conditions of the operator have been accepted | UnacceptedTerms | Checked | Checked | Checked | Checked | |   | The date of issuing of the personal id of the player is invalid | InvalidDateOfIssuing | Not checked | Not checked | Not checked | Checked | |   | The date of expiration of the personal id of the player is invalid | InvalidExpirationDate | Checked | Checked | Checked | Checked | This field is checked only in case that it was setted. The date must to have ISO 8601 format, including the timezone (yyyy-mm-ddT00:00:00Z) |   | The date of expiration of the personal id of the player is empty | EmptyDocumentExpirationDate | Not checked | Not checked | Not checked | Checked | This field should be mandatory only for players with DocumentType = RES, not for those whitch DocumentType = ID.|   | The provided city of birth is invalid. | InvalidPlaceOfBirthCity | Not checked | Not checked | Not checked | Checked | |   | The provided city of birth is empty. | EmptyPlaceOfBirthCity | Not checked | Not checked | Not checked | Checked | This field should be mandatory only for players with DocumentType = ID, not for those whitch DocumentType = RES. |   | The provided city of issuing of the personal id is invalid. | InvalidPlaceOfIssuing | Not checked | Not checked | Not checked | Checked | |   | Invalid country of birth | InvalidPlaceOfBirthCountry | Not checked | Not checked | Not checked | Checked | |   | Empty country of birth | EmptyPlaceOfBirthCountry | Not checked | Not checked | Not checked | Checked | This field should be mandatory only for players with DocumentType = RES, not for those whitch DocumentType = ID. |   | Format of the provided first or given name | InvalidFirstName | Checked. Regular expression: `[^\\p{L}]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` |  |   | Format of the provided second given name | InvalidFirstName2 | Checked. Regular expression: `[^\\p{L}]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | This field is optional, only will be checked in case which it is setted  |   | Format of the provided family name | InvalidLastName | Checked. Regular expression: `[^\\p{L}]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` |  |   | Format of the provided second family name | InvalidLastName2 | Checked. Regular expression: `[^\\p{L}]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Checked. Regular expression: `[1234567890$&Â¿?Â¡!&#124;\\(\\)@#Â¬+*\\{\\}<>%/\\\\]` | Only if the given document type is ID (citizenship of the country referred by the regulated market), except in colombian market, where this field is optional in any case |   | Format of the provided date of birth | InvalidBirthDate | Checked | Checked | Checked | Checked | ISO 8601 format, including the timezone (yyyy-mm-ddT00:00:00Z) |   | The player is old enough to register in the operator website according to the regulated market | PlayerUnderAge | Checked. | Checked. | Checked. | Checked. | The age is configurable. |   | Format of the provided post code | InvalidPostalCode | Not checked | Checked | Checked | Not checked | |   | The provided person title is an accepted value | InvalidPersonTitle | Checked | Checked | Checked | Checked | |   | The provided gender is an accepted value | InvalidGender | Checked | Checked | Checked | Checked | |   | The provided nationality is an accepted value | InvalidNationality | Checked | Checked | Checked | Checked | |   | The provided country of fiscal residence is an accepted value | InvalidCountryFiscalRegion | Checked | Checked | Checked | Checked | |   | The provided region of fiscal residence is an accepted value | InvalidFiscalRegion | Checked | Checked | Checked | Checked | |   | The provided country of fiscal residence is an accepted value | InvalidCountry | Checked | Checked | Checked | Checked | |   | The provided currency is an accepted value | InvalidCurrency | Checked | Checked | Checked | Checked | |   | The provided language is an accepted value | InvalidLanguage | Checked | Checked | Checked | Checked | |   | The provided document type is an accepted value | InvalidDocumentType | Checked | Checked | Checked | Checked | |   | The provided user name and password are equal | UserNameAndPasswordAreEqual | Configurable | Checked | Checked | Checked | No case sensitive |   | The provided password and security question are equal | PasswordAndSecurityQuestionAreEqual | Checked | Checked | Checked | Checked | No case sensitive |   | The provided password and security answer are equal | SecurityAnswerAndPasswordAreEqual | Checked | Checked | Checked | Checked | No case sensitive |   | The password contains the second family name | PasswordContainsTheSecondLastName | Checked | Checked | Checked | Checked | No case sensitive |   | The password contains the family name | PasswordContainsTheLastName | Checked | Checked | Checked | Checked | No case sensitive |   | The password contains the first name | PasswordContainsTheFirstName | Checked | Checked | Checked | Checked | No case sensitive|   | The password contains the birth date | PasswordContainsBirthDate | Checked | Checked | Checked | Checked | |    Please take into account that the validation of the format of some specific data is only performed in case that data is provided. If a piece of information is not mandatory, it is possible to register or edit a player without providing that data (in that case, the validation of format of that data does not apply). 
 *
 * OpenAPI spec version: 3.26.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface PlayerLimitChange {
    /**
     * Unique identifier representing a specific limit.
     */
    id?: number;

    /**
     * Username.
     */
    username?: string;

    /**
     * Account code.
     */
    accountCode?: string;

    /**
     * Full player name: name1 + name2.
     */
    name?: string;

    /**
     * Full player surname: surname1 + surname2.
     */
    surname?: string;

    /**
     * Object with daily / weekly / monthly limit.
     */
    requestedValues?: models.LimitValues;

    /**
     * Type of limit.
     */
    limitType?: models.LimitTypeEnum;

    /**
     * Limit change status.
     */
    status?: models.PlayerLimitChangeStatusEnum;

    /**
     * Request date.
     */
    requestDate?: string;

    /**
     * Date when operator accepts or rejects the limit.
     */
    closedDate?: string;

    /**
     * Date when the change will be applied.
     */
    validFromDate?: string;

    /**
     * List of notes written to the player auto-limit request.
     */
    notes?: Array<string>;

}
