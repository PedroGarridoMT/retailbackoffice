import { BaseResultsCode } from "./models";

export interface ApiResponse {
    /**
     * Status code BaseResultsCode enum
     */
    statusCode: BaseResultsCode;

    /**
     * ModelError object: errorCode + errorMessage
     */
    error?: any; // ModelError | Array<PlayerValidationError>;

    /**
     * Any value to be used
     */
    value?: any;
}
