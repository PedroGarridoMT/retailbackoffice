export class PermissionsCode {

    // Campaigns
    public static CampaignHome: Array<string> = ["CampaignHome"];
    public static CampaignsSearch: Array<string> = ["CampaignSearch"];
    public static PasswordChange: string = "PASSWORD_CHANGE";
    public static CampaignSearch: string = "CAMPAIGN_SEARCH";
    public static CampaignDetails: string = "CAMPAIGN_DETAILS";
    public static CampaignEdit: string = "CAMPAIGN_EDIT";
    public static CampaignCreate: Array<string> = ["CampaignAdd"];
    public static CampaignMassiveAssignation: Array<string> = ["MassiveAssignation"];

    // Players (Views)
    public static PlayerHome: Array<string> = ["PlayerHome"];
    public static PlayerSearch: Array<string> = ["PlayerSearch"];
    public static PlayerDetailData: Array<string> = ["PlayerDetailData", "PlayerBalance"];
    public static PlayerDetailTransactions: Array<string> = ["PlayerDetailData", "PlayerDetailTransactions"];

    // Players (Documentation)
    public static PlayerDetailDocumentation: Array<string> = ["PlayerDetailDocumentation"];
    public static PlayerDetailDocumentationRemove: Array<string> = ["PlayerDetailDocumentationRemove"];
    public static PlayerDetailDocumentationUpload: Array<string> = ["PlayerDetailDocumentationUpload"];

    //Players (Notes)
    public static PlayerNotes: Array<string> = ["PlayerNotes"];
    public static PlayerNotesAdd: Array<string> = ["PlayerNotesAdd"];
    public static PlayerNotesEditOwn: Array<string> = ["PlayerNotesEditOwn"];
    public static PlayerNotesEditAll: Array<string> = ["PlayerNotesEditAll"];
    public static PlayerNotesDeleteOwn: Array<string> = ["PlayerNotesDeleteOwn"];
    public static PlayerNotesDeleteAll: Array<string> = ["PlayerNotesDeleteAll"];

    // Players (actions)
    public static PlayerChangeStatus: Array<string> = ["PlayerChangeStatus"];
    public static PlayerEdit: Array<string> = ["PlayerEdit"];
    public static PlayerManualAdjustment: Array<string> = ["PlayerManualAdjustment"];
    public static PlayerRevokeSelfExclusion: Array<string> = ["PlayerRevokeSelfExclusion"];
    public static PlayerActiveSelfExclusion: Array<string> = ["PlayerActiveSelfExclusion"];
    public static PlayerVerifyIdentity: Array<string> = ["PlayerVerifyIdentity"];
    public static PlayerRejectIdentity: Array<string> = ["PlayerRejectIdentity"];

    // Player Limits
    public static PlayerAutoLimitsSearch: Array<string> = ["PlayerAutoLimitsSearch"]; // Search player auto-limit change requests
    public static PlayerAutoLimitsUpdate: Array<string> = ["PlayerAutoLimitsUpdate"]; // Approve or reject player auto-limits
    public static PlayerResponsibleGaming: Array<string> = ["PlayerAutoLimitsSearch", "OperatorLimits", "PlayerAutoLimitsHistorySearch"];

    // Player Bonuses
    public static PlayerBonuses: Array<string> = ["PlayerBonuses"];
    public static PlayerActiveAndPendingBonuses: Array<string> = ["PlayerActiveAndPendingBonuses"];
    public static PlayerBonusCancel: Array<string> = ["CancelActiveOrPendingPlayerBonus"];
    public static AddPlayerBonus: Array<string> = ["AddPlayerBonus"];

    // Player Blacklist
    public static PlayerBlacklistSearch: Array<string> = ["PlayerBlacklistSearch"];
    public static PlayerBlacklistAdd: Array<string> = ["PlayerBlacklistSearch", "PlayerBlacklistAdd"];

    // Operator Limits
    public static OperatorLimitsUpdate: Array<string> = ["OperatorLimitsUpdate"];

    // Transactions
    public static TransactionHome: Array<string> = ["TransactionHome"];
    public static TransactionSearch: Array<string> = ["TransactionSearch"];
    public static TransactionSearchPendingWithdrawals: Array<string> = ["TransactionSearchPendingWithdrawals"];
    public static TransactionWithdrawalApprovalLevel1: Array<string> = ["ApproveRejectWithdrawalLevel1"];
    public static TransactionWithdrawalApprovalLevel2: Array<string> = ["ApproveRejectWithdrawalLevel2"];
    public static TransactionWithdrawalFinalize: Array<string> = ["TransactionWithdrawalFinalize"];
    public static TransactionWithdrawalExportSepa: Array<string> = ["ExportSEPAFile"];

    // Settings
    public static SettingsHome: Array<string> = ["SettingsHome"];

    //Monitoring Functionality
    public static Monitoring: Array<string> = ["MonitoringFunction"];
}
