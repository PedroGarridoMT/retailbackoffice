export enum BaseResultsCode {
    Ok,
    AlreadyExists,
    InternalServerError,
    BadRequest,
    Unauthorized,
    Forbidden,
    Unprocessable,
    NotFound,
    CustomCode
}
