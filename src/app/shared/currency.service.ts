import { Injectable } from "@angular/core";
import { ConfigService } from "../core/config.service";
import { SystemCurrency } from "./model/models";
import * as moment from "moment";


@Injectable()
export class CurrencyService {
    systemCurrency: SystemCurrency;

    constructor(private configService: ConfigService) {
        this.systemCurrency = configService.getKey("currency");
    }

    /**
     * Returns the symbol of the system currency
     * @returns {string}
     */
    getSymbol(): string {
        return this.systemCurrency.symbol;
    }

    /**
     * Returns the code of the system currency
     * @returns {string}
     */
    getCode(): string {
        return this.systemCurrency.code;
    }

    /**
     * Returns the amount formatted with the current locale specifics.
     * This method uses the Internationalization API (toLocaleString)
     * @param amount
     * @returns {string}
     */
    formatAmount(amount: number): string {
        if (amount == null || amount == undefined || isNaN(amount)) {
            return '';
        }

        let locale = moment.locale();
        let props = {
            minimumFractionDigits: amount === 0 ? 0 : this.systemCurrency.decimal_digits
        };
        return amount.toLocaleString(locale, props);
    }

    /**
     * Returns the amount formatted with the current currency and locale specifics.
     * This method uses the Internationalization API (toLocaleString)
     * @param amount
     * @returns {string}
     */
    formatAmountWithCurrency(amount: number) {
        if (amount == null || amount === undefined || isNaN(amount)) {
            return '';
        }

        let locale = moment.locale();
        let props = {
            style: "currency",
            currency: this.systemCurrency.code,
            minimumFractionDigits: amount === 0 ? 0 : this.systemCurrency.decimal_digits
        };
        const formattedAmount = amount.toLocaleString(locale, props);
        if (locale == "en") {
            return formattedAmount.replace(this.systemCurrency.code, this.systemCurrency.code + " ");
        }
        return formattedAmount;
    }
}
