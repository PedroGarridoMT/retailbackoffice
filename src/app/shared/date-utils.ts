import * as moment from "moment";
import { DateInputModel, DateRangeValue } from "mt-web-components";

let timeZoneOffset = null;

export const DEFAULT_API_FORMAT: string = "YYYY-MM-DDTHH:mm:ssZ";

export const DATE_DEFAULT_FORMAT: string = "ll"; //E.g: Apr 12, 2017
export const DATE_LONG_FORMAT: string = "LL";
export const DATE_TIME_LONG_FORMAT: string = "LLL";
export const TIME_FORMAT: string = "HH:mm:ss";

export function setTimeZoneOffset(offset: number) {
    timeZoneOffset = offset;
}

export function getTimeZoneOffset() {
    if (timeZoneOffset != null) {
        return timeZoneOffset;
    } else {
        let d = new Date();
        return d.getTimezoneOffset();
    }
}

export function momentTimeZone(date: number | Date | string | any) {
    return moment.utc(date).utcOffset(-getTimeZoneOffset());
}

export function formatDate(date: number | Date | string | any, format?: string) {
    return momentTimeZone(date).format(format ? format : DEFAULT_API_FORMAT);
}

export function getDefaultDateFormat(date: string | Date) {
    return formatDate(date, DATE_DEFAULT_FORMAT);
}

export function getDefaultDateTimeFormat(date: number | string) {
    return formatDate(date, DATE_DEFAULT_FORMAT) + " " + formatDate(date, TIME_FORMAT);
}

export function getLongDateFormat(date: number | string) {
    return formatDate(date, DATE_LONG_FORMAT);
}

export function getLongDateTimeFormat(date: number | string) {
    return formatDate(date, DATE_TIME_LONG_FORMAT);
}

export function getYearMonthDayFormat(year, month, day) {
    if (year === '' || month === '' || day === '') {
        return null;
    }
    return formatDate(moment({ year: year, month: month, day: day }), DEFAULT_API_FORMAT);
}

/**
 * is Valid date
 * @param date
 * @return {boolean}
 */
export function isValidDate(date: DateInputModel | string) {
    return moment(date).isValid();
}

/**
 * is complete date regarding the DateInputModel values
 * @param date
 * @return {boolean}
 */
export function isCompleteDate(date: DateInputModel) {
    return date && date.year && date.month && date.day;
}

/**
 * Given a string date I get and object with DatePicker Format
 * @param date
 * @returns {{day: any, month: any, year: any}}
 */
export function getDateAsObject(date: string | Date): any {
    if (!date) {
        return null;
    }

    let momentDate = moment(date);
    return {
        day: momentDate.date(),
        month: momentDate.month(),
        year: momentDate.year()
    }
}

/**
 * @name  setTimeInfo
 *
 * @desc  Given a DateInputModel, we are able to set to 0, or any other time values ('hour', 'minute', 'second')
 * @returns {DateInputModel}
 * @param dateInput
 * @param hour
 * @param minute
 * @param second
 */
export function setTimeInfo(dateInput: DateInputModel, hour?: number, minute?: number, second?: number): DateInputModel {
    if (!dateInput) {
        return null;
    }

    dateInput.hour = hour ? hour : 0;
    dateInput.minute = minute ? minute : 0;
    dateInput.second = second ? second : 0;

    return dateInput;
}

/**
 * @name  getIsoString
 * @desc  Given a year, month and day, return a date as a ISO 8601 String. (UTC)
 * @returns {string}
 */
export function getISOString(dateInput: DateInputModel): string {
    if (!dateInput) {
        return null;
    }

    let momentDate = moment({
        year: dateInput.year,
        month: dateInput.month,
        day: dateInput.day,
        hour: dateInput.hour ? dateInput.hour : 0,
        minute: dateInput.minute ? dateInput.minute : 0,
        second: dateInput.second ? dateInput.second : 0
    });

    return momentDate.toISOString();
}

/**
 * Returns a DateRange object composed with the ISO string dates passed as arguments.
 * DateRange => {from:DateInputModel, to:DateInputModel}
 * DateInputModel => {day: number|string, month: number|string, year: number|string, hour, minute}
 * @param dateFrom. ISO string date, e.g.:1971-05-05T23:00:00.000Z
 * @param dateTo. ISO string date, e.g.:1971-05-05T23:00:00.000Z
 * @returns {DateRangeValue}
 */
export function getDateRangeValueFromISODate(dateFrom: string, dateTo: string): DateRangeValue {
    let dateRange: DateRangeValue = null;
    if (dateFrom) {
        if (!dateRange)
            dateRange = { from: null, to: null };
        dateRange.from = getDateAsObject(dateFrom);
    }
    if (dateTo) {
        if (!dateRange)
            dateRange = { from: null, to: null };
        dateRange.to = getDateAsObject(dateTo);
    }
    return dateRange;
}
