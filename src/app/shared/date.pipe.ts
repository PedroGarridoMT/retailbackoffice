import { Pipe, PipeTransform } from "@angular/core";
import { getDefaultDateTimeFormat, getLongDateFormat, getLongDateTimeFormat } from "./date-utils";

@Pipe({ name: 'longDatePipe' })
export class LongDatePipe implements PipeTransform {
    transform(date) {
        if (!date)
            return null;
        return getLongDateFormat(date)
    }
}

@Pipe({ name: 'longDateTimePipe' })
export class LongDateTimePipe implements PipeTransform {
    transform(date) {
        if (!date)
            return null;
        return getLongDateTimeFormat(date)
    }
}

@Pipe({ name: 'mediumDateTimePipe' })
export class MediumDateTimePipe implements PipeTransform {
    transform(date) {
        if (!date)
            return null;
        return getDefaultDateTimeFormat(date)
    }
}
