import { BaseRequestOptions, Http, ResponseOptions, Response, ResponseOptionsArgs } from "@angular/http";
import { TestBed } from "@angular/core/testing";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { Subscription } from "rxjs";
import { Observable } from 'rxjs/Observable';
import { PASConfigService } from "./pas-config.service";
import { ConfigService } from "../core/config.service";
import { Resource, OperationTypeForPaymentMethodEnum, StatusPermissions } from "./index";

describe("PASConfigService", () => {

    const baseUrl: string = "http://localhost:123";

    let subscriptions: Subscription[] = [];
    let service: PASConfigService;
    let configServiceStub: ConfigService;
    let backend: MockBackend;

    function setupConnections(backend: MockBackend, options: any): Subscription {
        const subscription = backend.connections.subscribe((connection: MockConnection) => {
            const responseOptions = new ResponseOptions(options);
            const response = new Response(responseOptions);
            connection.mockRespond(response);
        });
        subscriptions.push(subscription);
        return subscription;
    }

    afterEach(() => {
        subscriptions.forEach(s => s.unsubscribe());
        subscriptions = [];
    });

    beforeEach(() => {
        configServiceStub = <ConfigService>{
            getEndpoint(endpoint: string): string {
                return baseUrl;
            }
        };

        TestBed.configureTestingModule({
            providers: [
                PASConfigService,
                BaseRequestOptions,
                MockBackend,
                { provide: ConfigService, useValue: configServiceStub },
                {
                    provide: Http,
                    useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => new Http(backendInstance, defaultOptions),
                    deps: [MockBackend, BaseRequestOptions]
                }
            ]
        });

        service = TestBed.get(PASConfigService);
        backend = TestBed.get(MockBackend);
    });

    it("should return list of game types from cache", () => {
        const gameTypes: string[] = ["game type 1", "game type 2"];
        service.gameTypes = gameTypes;

        service.getGameTypes().subscribe((result) => {
            expect(result).toEqual(gameTypes);
        });
    });

    it("should return list of game types from backend if it's not cached", () => {
        const gameTypes: string[] = ["game type 1", "game type 2", "game type 3"];
        const options: ResponseOptionsArgs = { body: gameTypes, status: 200 };
        setupConnections(backend, options);
        service.gameTypes = null;

        service.getGameTypes().subscribe((result) => {
            expect(result).toEqual(gameTypes);
        });
    });

    it("should return list of payment methods for provided operation type", () => {
        const paymentMethods: string[] = ["payment method 1", "payment method", "payment method 3"];
        const options: ResponseOptionsArgs = { body: paymentMethods, status: 200 };
        setupConnections(backend, options);

        service.getPaymentMethods(OperationTypeForPaymentMethodEnum.Command).subscribe((result) => {
            expect(result).toEqual(paymentMethods);
        });
    });

    it("should return list of transaction types from cache", () => {
        const transactionTypes: string[] = ["transaction type 1", "transaction type 2"];
        service.transactionTypes = transactionTypes;

        service.getTransactionTypes().subscribe((result) => {
            expect(result).toEqual(transactionTypes);
        });
    });

    it("should return list of transaction types from backend if it's not cached", () => {
        const transactionTypes: string[] = ["transaction type 1", "transaction type 2", "transaction type 3"];
        const options: ResponseOptionsArgs = { body: transactionTypes, status: 200 };
        setupConnections(backend, options);
        service.transactionTypes = null;

        service.getTransactionTypes().subscribe((result) => {
            expect(result).toEqual(transactionTypes);
        });
    });

    it("should return status permissions types from cache", () => {
        const statusPermissions: StatusPermissions[] = [{ canBet: false }, { canLogin: false }];
        service.statusPermissions = statusPermissions;

        service.getStatusPermissions().subscribe((result) => {
            expect(result).toEqual(statusPermissions);
        });
    });

    it("should return status permissions from backend if it's not cached", () => {
        const statusPermissions: StatusPermissions[] = [{ canBet: false }, { canLogin: false }, { canChangePassword: false }];
        const options: ResponseOptionsArgs = { body: statusPermissions, status: 200 };
        setupConnections(backend, options);
        service.statusPermissions = null;

        service.getStatusPermissions().subscribe((result) => {
            expect(result).toEqual(statusPermissions);
        });
    });
});
