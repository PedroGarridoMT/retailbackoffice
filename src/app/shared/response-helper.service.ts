import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { BaseResultsCode } from "./model/models";
import { Notification, NotificationsService, NotificationTypeEnum } from "mt-web-components";

@Injectable()
export class ResponseHelperService {
    constructor(private translate: TranslateService, private notifications: NotificationsService) {
    }

    /**
     * It shows a friendly user message using NotificationsService.
     * If the responseCode or translationKey are unknown, a generic error is shown.
     * In order to add the custom translationKey the object could update the following translations:
     * "ok", "badRequest", "notFound", "alreadyExists", "unprocessable", "internalServerError"
     *
     * @param responseCode
     * @param translationKey (eg.: 'players.edit.response')
     * @param seconds - if specified, the note will remain in screen this number of seconds
     * @param type
     */
    responseHandler(responseCode: BaseResultsCode, translationKey?: string, type?: NotificationTypeEnum, seconds?: number) {
        let notification: Notification;
        switch (responseCode) {
            case BaseResultsCode.Ok:
                notification = {
                    type: NotificationTypeEnum.Success,
                    text: translationKey && this.translate.instant(translationKey + '.ok').toString().indexOf(translationKey) < 0 ?
                        this.translate.instant(translationKey + '.ok') :
                        this.translate.instant('common.response.ok')
                };
                break;
            case BaseResultsCode.BadRequest:
                notification = {
                    type: NotificationTypeEnum.Error,
                    text: translationKey && this.translate.instant(translationKey + '.badRequest').toString().indexOf(translationKey) < 0 ?
                        this.translate.instant(translationKey + '.badRequest') :
                        this.translate.instant('common.response.badRequest')
                };
                break;
            case BaseResultsCode.NotFound:
                notification = {
                    type: NotificationTypeEnum.Error,
                    text: translationKey && this.translate.instant(translationKey + '.notFound').toString().indexOf(translationKey) < 0 ?
                        this.translate.instant(translationKey + '.notFound') :
                        this.translate.instant('common.response.notFound')
                };
                break;
            case BaseResultsCode.AlreadyExists:
                notification = {
                    type: NotificationTypeEnum.Error,
                    text: translationKey && this.translate.instant(translationKey + '.alreadyExists').toString().indexOf(translationKey) < 0 ?
                        this.translate.instant(translationKey + '.alreadyExists') :
                        this.translate.instant('common.response.alreadyExists')
                };
                break;
            case BaseResultsCode.Unprocessable:
                notification = {
                    type: NotificationTypeEnum.Error,
                    text: translationKey && this.translate.instant(translationKey + '.unprocessable').toString().indexOf(translationKey) < 0 ?
                        this.translate.instant(translationKey + '.unprocessable') :
                        this.translate.instant('common.response.unprocessable')
                };
                break;
            case BaseResultsCode.CustomCode:
                notification = {
                    type: type,
                    text: this.translate.instant(translationKey)
                };
                break;
            default:
                notification = {
                    type: NotificationTypeEnum.Error,
                    text: translationKey && this.translate.instant(translationKey + '.internalServerError').toString().indexOf(translationKey) < 0 ?
                        this.translate.instant(translationKey + '.internalServerError') :
                        this.translate.instant('common.response.internalServerError')
                };
                break;
        }
        if(seconds){
            notification.time = seconds * 1000;
        }
        this.notifications.add(notification);
    }
}
