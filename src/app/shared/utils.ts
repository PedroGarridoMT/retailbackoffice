/**
 * sanitizeArrayParam
 * Mainly used to remove invalid spaces in Array type URLSearchParams
 * @param param
 * @return {any}
 */
export function sanitizeArrayParam(param): any {
    if (Array.isArray(param)) {
        param.forEach((element, index) => {
            param[index] = element.trim();
        });
    }
    return param;
}

/**
 * Remove duplicates into an array
 * @param array
 * @return {Array<any>}
 */
export function uniqueArray(array): Array<any> {
    return array.filter((elem, pos, arr) => {
        return arr.indexOf(elem) == pos;
    });
}

/**
 * Deletes a cookie by the given name
 * @param name
 */
export function deleteCookieByName(name): void {
    if (document.cookie && document.cookie.indexOf(name) >= 0) {
        document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }
}

/**
 * Given a known extension it returns a common MIME type.
 * Allowed extensions are jpg, jpeg, png, gif, svg and pdf.
 * Returned values can be: image/extension or application/pdf
 * @param {string} extension
 * @return {string}
 */
export function getMimeTypeByExtension(extension: string): string {
    if (!extension) return null;
    extension = extension.replace('.', '').toLowerCase();
    let mimeType: string = null;
    switch (extension) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            mimeType = 'image/' + extension;
            break;
        case 'svg':
            mimeType = 'image/svg+xml';
            break;
        case 'pdf':
            mimeType = 'application/pdf';
            break;
        case 'csv':
            mimeType = 'text/csv';
            break;
        case 'xml':
            mimeType = 'text/xml';
            break;
    }
    return mimeType;
}

/**
 * Given the dataURI it's able to download the file with a temporal anchor element.
 * @param {string} dataURI
 * @param {string} fileName
 * @return {any}
 */
export function downloadFileByDataURI(dataURI: string, fileName: string) {
    if (!dataURI) {
        return null;
    }

    let downloadLink = document.createElement("a");

    if (typeof downloadLink.download === 'string') {
        downloadLink.href = dataURI;
        downloadLink.download = fileName;

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    } else {
        window.open(dataURI);
    }
}

/**
 *
 * @param str
 * @return {string}
 */
export function b64EncodeUnicode(str) {
    //TODO Review UTF-8 VS Unicode chars
    return window.btoa(str);
}

/**
 *
 * @param num input value
 * @param size total string length, default is 2
 * @returns {string}
 */
export function zeroPadding(num: string | number, size: number = 2): string {
    var s: string = num + "";
    while (s.length < size)
        s = "0" + s;
    return s;
}
