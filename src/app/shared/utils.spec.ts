import { async } from '@angular/core/testing';
import * as Utils from "./utils";

describe("Utils", () => {

    it("should sanitize array param", () => {
        const param: Array<string> = ["param1 ", "param 2  ", "param3"];
        const sanitizedParam: Array<string> = ["param1", "param 2", "param3"];
        expect(Utils.sanitizeArrayParam(param)).toEqual(sanitizedParam);
    });

    it("should remove duplicates from array", () => {
        const arr: Array<string> = ["item", "item 2", "item 3", "item 2", "item 4", "item", "item 4", "item"];
        const uniqueArr: Array<string> = ["item", "item 2", "item 3", "item 4"];
        expect(Utils.uniqueArray(arr)).toEqual(uniqueArr);
    });

    it("should delete cookie by name", () => {
        document.cookie = "some_cookie=some data";
        Utils.deleteCookieByName("some_cookie");
        expect(document.cookie.indexOf("some_cookie")).toBe(-1);
    });

    it("should return proper MIME type for given extension", () => {
        ["jpeg", "jpg", "png", "gif"].forEach((ext: string) => expect(Utils.getMimeTypeByExtension(ext)).toBe("image/" + ext));
        expect(Utils.getMimeTypeByExtension("svg")).toBe("image/svg+xml");
        expect(Utils.getMimeTypeByExtension("pdf")).toBe("application/pdf");
        expect(Utils.getMimeTypeByExtension(null)).toBe(null);
    });

    it("should return base64 of binary", async(() => {
        const content: string = "some text";
        const blb: Blob = new Blob([content], {type: "text/plain"});
        const base64content: string = window.btoa(content);
        const reader: FileReader = new FileReader();

        reader.onload = () => expect(Utils.b64EncodeUnicode(reader.result)).toBe(base64content);
        reader.readAsBinaryString(blb);
    }));

    it("should download file by data URI", async(() => {
        const dataURI: string = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP";
        const filename: string = "filename.gif";

        // Save original method
        const removeChild = document.body.removeChild.bind(document.body);

        // Overide original method
        document.body.removeChild = (function(child){
            expect(child.download).toBe(filename);
            expect(child.href).toBe(dataURI);
            removeChild(child);

            // Restore original method
            document.body.removeChild = removeChild;
        }).bind(document.body);

        const handler = (e) => {
            e.preventDefault();
            e.stopPropagation();
        };

        document.addEventListener("click", handler, true);
        Utils.downloadFileByDataURI(dataURI, filename);
    }));
});
