import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { TitleBarParams } from "./index";

@Injectable()
export class TitleBarService {
    // Observable string sources
    private componentDestroyedSource = new Subject<void>();
    private componentInitializedSource = new Subject<TitleBarParams>();
    private actionRequestedSource = new Subject<string>();

    // Observable string streams
    componentDestroyed$ = this.componentDestroyedSource.asObservable();
    componentInitialized$ = this.componentInitializedSource.asObservable();
    actionRequested$ = this.actionRequestedSource.asObservable();

    // Service message commands
    componentInitialized(params: TitleBarParams){
        this.componentInitializedSource.next(params);
    }

    componentDestroyed(){
        this.componentDestroyedSource.next();
    }

    actionRequested(actionId: string){
        this.actionRequestedSource.next(actionId);
    }
}
