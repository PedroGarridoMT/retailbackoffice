import * as moment from "moment";
import * as DateUtils from "./date-utils";
import { DateInputModel, DateRangeValue } from 'mt-web-components';

describe(`Date Utils`, () => {

    const date = new Date(2042, 0, 1, 22, 22, 22);

    beforeEach(() => {
        DateUtils.setTimeZoneOffset(null);
    });

    afterEach(() => {
        DateUtils.setTimeZoneOffset(null);
    });

    describe('Time zone', () => {

        it("should return correct time zone offset if time zone offset not set", () => {
            const timeZoneOffset: number = (new Date).getTimezoneOffset();
            expect(DateUtils.getTimeZoneOffset()).toBe(timeZoneOffset);
        });

        it("should return correct time zone offset if time zone offset is set explicitly", () => {
            const timeZoneOffset: number = -111;
            DateUtils.setTimeZoneOffset(timeZoneOffset);
            expect(DateUtils.getTimeZoneOffset()).toBe(timeZoneOffset);
        });

        it(`should return "Invalid date" when invalid date argument is passed`, () => {
            expect(DateUtils.momentTimeZone("some string").format()).toBe("Invalid date");
        });

    });

    describe('Date formatting', () => {

        beforeEach(() => {
            DateUtils.setTimeZoneOffset(-moment(date).utcOffset());
        });

        it("should format date with default date format", () => {
            expect(DateUtils.formatDate(date)).toBe(moment(date).format(DateUtils.DEFAULT_API_FORMAT));
        });

        it("should format date with provided date format", () => {
            expect(DateUtils.formatDate(date, DateUtils.DATE_DEFAULT_FORMAT)).toBe(moment(date).format(DateUtils.DATE_DEFAULT_FORMAT));
        });

        it("should return default date format", () => {
            expect(DateUtils.getDefaultDateFormat(date)).toBe(moment(date).format(DateUtils.DATE_DEFAULT_FORMAT));
        });

        it("should return default date/time format", () => {
            expect(DateUtils.getDefaultDateTimeFormat(date.getTime())).toBe(moment(date).format(DateUtils.DATE_DEFAULT_FORMAT + " " + DateUtils.TIME_FORMAT));
        });

        it("should return long date format", () => {
            expect(DateUtils.getLongDateFormat(date.getTime())).toBe(moment(date).format(DateUtils.DATE_LONG_FORMAT));
        });

        it("should return long date/time format", () => {
            expect(DateUtils.getLongDateTimeFormat(date.getTime())).toBe(moment(date).format(DateUtils.DATE_TIME_LONG_FORMAT));
        });

        it(`should return default date format with time set to 0's`, () => {
            const y = date.getFullYear();
            const m = date.getMonth();
            const d = date.getDate();
            const customDate = {year: y, month: m, day: d};

            expect(DateUtils.getYearMonthDayFormat(y, m, d)).toBe(moment(customDate).format(DateUtils.DEFAULT_API_FORMAT));
        });

        it("should return null if any empty argument is passed", () => {
            expect(DateUtils.getYearMonthDayFormat('', 1, 1)).toBe(null);
        });

    });

    describe('Other date utils', () => {

        it("should return false for invalid string date", () => {
            expect(DateUtils.isValidDate('invalid date')).toBe(false);
        });

        it("should return true for valid string date", () => {
            expect(DateUtils.isValidDate(date.toISOString())).toBe(true);
        });

        it("should return date as object", () => {
            expect(typeof DateUtils.getDateAsObject(date)).toBe('object');
        });

        it("should return null when invalid argument is passed", () => {
            expect(DateUtils.getDateAsObject(null)).toBe(null);
        });

        it("should return ISO date string", () => {
            const customDate: DateInputModel = {
                year: date.getFullYear(),
                month: date.getMonth(),
                day: date.getDate(),
                hour: date.getHours(),
                minute: date.getMinutes(),
                second: date.getSeconds()
            };

            expect(DateUtils.getISOString(customDate)).toBe(date.toISOString());
        });

        it("should return null when invalid argument is passed", () => {
            expect(DateUtils.getISOString(null)).toBe(null);
        });

        it("should return ISO date string", () => {
            const customDate: DateInputModel = {
                year: date.getFullYear(),
                month: date.getMonth(),
                day: date.getDate()
            };

            const dateRange: DateRangeValue = {
                from: customDate,
                to: customDate
            };

            const dateString = moment(date).format();
            expect(DateUtils.getDateRangeValueFromISODate(dateString, dateString)).toEqual(dateRange);
        });

        it("should return null when invalid argument is passed", () => {
            expect(DateUtils.getDateRangeValueFromISODate(null, null)).toBe(null);
        });

        it("should return date input model with hour, minute and second property", () => {
            const customDateTime: DateInputModel = {
                year: date.getFullYear(),
                month: date.getMonth(),
                day: date.getDate(),
                hour: 0,
                minute: 0,
                second: 0
            };

            expect(DateUtils.setTimeInfo(customDateTime)).toEqual(customDateTime);
        });

        it("should return null when invalid argument is passed", () => {
            expect(DateUtils.setTimeInfo(null)).toBe(null);
        });

    });

});
