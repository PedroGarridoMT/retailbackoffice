import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MtComponentsModule } from "mt-web-components";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TitleBarHandlerComponent } from "./title-bar-handler.component";
import { TitleBarService } from "./title-bar.service";
import { HelperService } from "./helper.service";
import { PASEnumsService } from "./pas-enums.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LongDatePipe, LongDateTimePipe, MediumDateTimePipe } from "./date.pipe";
import { CurrencyService } from "./currency.service";
import { ResourcesService } from "./resources.service";
import { LoaderService } from "./loader.service";
import { NgbModule, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalConfirmationComponent } from "./components/modal-confirmation.component";
import { FeedbackComponent } from "./components/feedback.component";
import { PASConfigService } from "./pas-config.service";
import { ResponseHelperService } from "./response-helper.service";
import { PermissionsGuard } from "./permissions.guard";
import { BaseSearchComponent } from "./base-search.component";
import { EmitterService } from "./services/emmiter.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NumberValidatorDirective } from "./directives/number-validator.directive";
import { ModalService } from "./services/modal.service";

/**
 * AoT requires an exported function for factories
 */
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpClientModule,
        MtComponentsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        NgbModule.forRoot()
    ],
    declarations: [
        TitleBarHandlerComponent,
        LongDatePipe,
        LongDateTimePipe,
        MediumDateTimePipe,
        ModalConfirmationComponent,
        FeedbackComponent,
        BaseSearchComponent,
        NumberValidatorDirective
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpClientModule,
        TranslateModule,
        MtComponentsModule,
        LongDatePipe,
        LongDateTimePipe,
        MediumDateTimePipe,
        ModalConfirmationComponent,
        FeedbackComponent,
        BaseSearchComponent
    ],
    providers: [
        TitleBarService,
        HelperService,
        PASEnumsService,
        CurrencyService,
        ResourcesService,
        LoaderService,
        PASConfigService,
        ResponseHelperService,
        PermissionsGuard,
        EmitterService,
        {provide: NgbModal, useClass: ModalService},
        {provide: ModalService, useExisting: NgbModal}
    ]
})
export class SharedModule {
}
