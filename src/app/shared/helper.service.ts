import { Headers, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs";
import { ApiResponse, BaseResultsCode } from "./model/models";
import { deleteCookieByName } from "./utils";

export class HelperService {

    /**
     * Error handler
     * @param error
     * @returns {any}
     */
    static handleError(error: Response | any) {
        let errorApiResponse: ApiResponse;
        let body;
        try {
            body = error.json()
        } catch (err) {
            body = null;
        }

        if ((body && Array.isArray(body)) || (body && Object.keys(body)
                && Object.keys(body).some(item => item == 'errorCode')
                && Object.keys(body).some(item => item == 'errorDescription'))) {

            errorApiResponse = { statusCode: HelperService.getBaseResultCode(error.status), error: body };
        } else {
            errorApiResponse = { statusCode: HelperService.getBaseResultCode(error.status), error: null };
        }

        return Observable.throw(errorApiResponse);
    }

    /**
     * Add multipart header for files upload.
     * @return {RequestOptions}
     */
    static addMultiPartHeader(): RequestOptions {
        let headers = new Headers();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');

        return new RequestOptions({ headers: headers });
    }

    /**
     * Clear "permissions" in mt-web-components, and user objects, and also the XSRF-TOKEN
     */
    static clearLocalStorage() {
        // Used in PermissionsService (mt-web-components)
        localStorage.removeItem("permissions");
        // Used in PAS app
        localStorage.removeItem("user");
        localStorage.removeItem('last-player-search');

        deleteCookieByName('XSRF-TOKEN');
    }

    /**
     *
     * @param status
     * @returns {BaseResultsCode}
     */
    static getBaseResultCode(status: number) {
        if (status >= 200 && status < 300) {
            return BaseResultsCode.Ok;
        } else if (status == 400) {
            return BaseResultsCode.BadRequest;
        } else if (status == 401) {
            return BaseResultsCode.Unauthorized;
        } else if (status == 403) {
            return BaseResultsCode.Forbidden;
        } else if (status == 404) {
            return BaseResultsCode.NotFound;
        } else if (status == 409) {
            return BaseResultsCode.AlreadyExists;
        } else if (status == 422) {
            return BaseResultsCode.Unprocessable;
        } else {
            return BaseResultsCode.InternalServerError;
        }
    }

    /**
     *
     * @param response
     * @return {{statusCode: (BaseResultsCode|any), error: null}}
     */
    static getApiResponse(response: Response | any): ApiResponse {
        return {
            statusCode: this.getBaseResultCode(response.status),
            error: null
        }
    }
}
