import { Injectable } from "@angular/core";
import { NgbModal, NgbModalOptions, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export class ModalService extends NgbModal {

    private lastRef: NgbModalRef;

    public open(content: any, options?: NgbModalOptions): NgbModalRef {
        const ref: NgbModalRef = super.open(content, options);
        this.lastRef = ref;
        return ref;
    }

    public closeAll(): void {
        if(!this.lastRef) return;
        this.lastRef.close();
    }

}
