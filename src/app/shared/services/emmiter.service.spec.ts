import { TestBed } from "@angular/core/testing";
import { Subscription } from 'rxjs/Subscription';
import { EmitterService, EmitterServiceEventsEnum } from "./emmiter.service";

describe("ResourcesService", () => {

    let service: EmitterService;
    let subscriptions: Subscription[] = [];

    afterEach(() => {
        subscriptions.forEach(s => s.unsubscribe());
        subscriptions = [];
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [EmitterService]
        });

        service = TestBed.get(EmitterService);
    });

    it("should emit provided event", () => {
        const subscription: Subscription = service.eventBus$.subscribe((result) => expect(result).toBe(EmitterServiceEventsEnum.MENU_IS_EXPANDED));
        subscriptions.push(subscription);
        service.emitEvent(EmitterServiceEventsEnum.MENU_IS_EXPANDED);
    });

});
