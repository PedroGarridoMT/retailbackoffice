import { BaseSearchComponent } from "./base-search.component";
import { ComponentFixture } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { BehaviorSubject } from "rxjs";
import { Injectable, NO_ERRORS_SCHEMA } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ActivatedRouteStub } from '../../test-util/fake-activated-route';

describe('BaseSearchComponent', () => {
    let comp: BaseSearchComponent;
    let fixture: ComponentFixture<BaseSearchComponent>;

    let mockActivatedRoute: ActivatedRouteStub;

    beforeEach(() => {
        mockActivatedRoute = new ActivatedRouteStub();

        TestBed.configureTestingModule({
            declarations: [BaseSearchComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: ActivatedRoute, useValue: mockActivatedRoute }
            ]
        });

        //query params added here
        mockActivatedRoute.setQueryParams({
            keys: ["param1", "param2", "paramArray", "pageNumber", "pageSize", "dateFrom", "dateTo", "amountFrom", "amountTo"],
            params: {
                param1: 1,
                param2: 2,
                paramArray: "val1,val2",
                pageNumber: "4",
                pageSize: "10",
                dateFrom: "2017-10-01 06:00",
                dateTo: "2017-10-10 07:00",
                amountFrom: "2000",
                amountTo: "3000"
            }
        });

        fixture = TestBed.createComponent(BaseSearchComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should have query params", () => {
        expect(comp.thereAreQueryParams()).toBe(true);
    });

    it("should create paging data from query params", () => {
        const pagingData = {
            currentPage: 4,
            pageSize: 10,
            totalPages: 0,
            totalItems: 0
        };
        comp.prepareDataWithQueryParams();
        expect(comp.pagingData).toEqual(pagingData);
    });

    it("should return value for query param", () => {
        expect(comp.getValueFromParams("param1")).toEqual(1);
    });

    it("should return multiple values with comma for array query param", () => {
        const params = comp.getMultipleSelectionFromParams("paramArray");
        expect(params.length).toEqual(2);
    });

    it("should generate date object for date range query param", () => {
        const dateRage = comp.getDateRangeFromParams("date");
        expect(dateRage["from"]["day"]).toEqual(1);
        expect(dateRage["from"]["month"]).toEqual(9);
        expect(dateRage["from"]["year"]).toEqual(2017);
        expect(dateRage["from"]["hour"]).toEqual(6);
        expect(dateRage["from"]["minute"]).toEqual(0);
    });

    it("should generate amount object for amounte range query param", () => {
        const amountRage = comp.getAmountRangeFromParams("amount");
        expect(amountRage["from"]).toEqual(2000);
        expect(amountRage["to"]).toEqual(3000);
    });

    it("should return pagination data", () => {
        const pagingData = {
            pageNumber: 1,
            pageSize: 10
        };
        expect(comp.getPaginationData()).toEqual(pagingData);
    });

    it("should return null for non-existent parameter", () => {
        expect(comp.getValueFromParams("nonExistentParam")).toEqual(null);
    });

    it("should return null for non-existent multiple-selection parameter", () => {
        expect(comp.getMultipleSelectionFromParams("nonExistentParam")).toEqual(null);
    });

    it("should return null for non-existent amount range parameter", () => {
        expect(comp.getAmountRangeFromParams("nonExistentParam")).toEqual(null);
    });

});
