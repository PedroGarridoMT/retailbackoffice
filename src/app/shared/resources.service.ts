﻿import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { Http, Response, URLSearchParams } from "@angular/http";
import { ConfigService } from "../core/config.service";
import { Observable } from "rxjs/Observable";
import { Resource } from "./model/models";

@Injectable()
export class ResourcesService {
    APIResources;

    constructor(private http: Http, private config: ConfigService) {
        this.APIResources = {
            cities: this.config.getEndpoint('resources') + '/cities',
            regions: this.config.getEndpoint('resources') + '/regions',
            fiscalRegions: this.config.getEndpoint('resources') + '/fiscal_residence_regions'
        };
    }

    /**
     * Get the available cities for player residence in a given region.
     * Retrieves the list of available cities for player residence (e.g. Barcelona) in a given region.
     * These cities are used during player registration.
     * @param regionCode The resource code of the region for which the list of cities has to be retrieved.
     */
    public getCities(regionCode: string): Observable<Array<Resource>> {
        if (!regionCode) {
            throw new Error('Required parameter regionCode was null or undefined when calling getCities.');
        }
        let params: URLSearchParams = new URLSearchParams();
        params.set('regionCode', regionCode);

        return this.http.get(this.APIResources.cities, { search: params })
            .map((cities: Response) => {
                return cities.json();
            });
    }

    /**
     * Get the available regions for player residence in a given country.
     * Retrieves the list of available player regions for player residence (e.g. Catalonia) in a given country.
     * Each returned region is identified by its ISO 3166-2 alpha-2 code without the country code.
     * @param countryCode The ISO 3166-1 alpha-2 code of the country for which the list of regions has to be retrieved.
     * @param cityCode Optional parameter for getting a regionCode for a given cityCode.
     */
    public getRegions(countryCode: string, cityCode?: string): Observable<Array<Resource>> {
        if (!countryCode) {
            throw new Error('Required parameter countryCode was null or undefined when calling getRegions.');
        }

        let params: URLSearchParams = new URLSearchParams();
        params.set('countryCode', countryCode);
        params.set('cityCode', cityCode);

        return this.http.get(this.APIResources.regions, { search: params })
            .map((regions: Response) => {
                return regions.json();
            });
    }

    /**
     * Retrieves the list of available fiscal regions in a given country.
     */
    public getFiscalRegions(): Observable<Array<Resource>> {
        return this.http.get(this.APIResources.fiscalRegions)
            .map((regions: Response) => {
                return regions.json();
            });
    }
}
