import * as moment from "moment";
import { AbstractControl, ValidatorFn } from "@angular/forms";
import { PASConstants } from "./constant";

/**
 *
 * @return {(control:AbstractControl)=>{[p: string]: any}}
 */
export function basicDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let valueDate = control.value;
        if (!valueDate)
            return null;

        let momentDate = moment(valueDate);

        return momentDate.isValid() ? null : { 'invalidDateError': { momentDate } };
    };
}

/**
 *
 * @return {(control:AbstractControl)=>{[p: string]: any}}
 */
export function zipCodeColombiaValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let zipCodeValue = control.value;

        if (!zipCodeValue)
            return null;

        if (isNaN(zipCodeValue)) {
            return { 'invalidZipCodeError': { zipCodeValue } };
        }

        if (zipCodeValue.length !== PASConstants.COLOMBIA_ZIP_CODE_LENGTH) {
            return { 'invalidZipCodeError': { zipCodeValue } };
        }

        let availableZipCodePrefixes = PASConstants.COLOMBIA_ZIP_CODE_PREFIXES;
        let validPrefix = availableZipCodePrefixes.some((prefix) => {
            return prefix == zipCodeValue.substring(0, 2)
        });

        return validPrefix ? null : { 'invalidZipCodeError': { zipCodeValue } };
    };
}

/**
 *
 * @return {(control:AbstractControl)=>{[p: string]: any}}
 */
export function zipCodeSpainValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const zipCodeValue = control.value;

        if (!zipCodeValue)
            return null;

        if (isNaN(zipCodeValue)) {
            return { 'invalidZipCodeError': { zipCodeValue } };
        }

        if (zipCodeValue.length !== PASConstants.SPAIN_ZIP_CODE_LENGTH) {
            return { 'invalidZipCodeError': { zipCodeValue } };
        }

        let availableZipCodePrefixes = PASConstants.SPAIN_ZIP_CODE_PREFIXES;
        let validPrefix = availableZipCodePrefixes.some((prefix) => {
            return prefix == zipCodeValue.substring(0, 2)
        });

        return validPrefix ? null : { 'invalidZipCodeError': { zipCodeValue } };
    };
}

/**
 * Checks the maximum age, minimum age depending of the given country.
 *
 * @param {string} currentCountry
 * @return {ValidatorFn}
 */
export function ageValidator(currentCountry: string): ValidatorFn {
    function checkValidAge(date: string): boolean {
        let maximumAgedNotExceed = (moment().diff(moment((date)), 'years') < PASConstants.MAXIMUM_AGE);
        let birthDateBeforeToday = moment((date)).isBefore(moment());
        return maximumAgedNotExceed && birthDateBeforeToday;
    }

    function checkToBeUnderAge(date: string): boolean {
        let validAge = false;
        let minimumAge = currentCountry === PASConstants.COUNTRY_CODE_ESTONIA ? PASConstants.MINIMUM_AGE_ESTONIA : PASConstants.MINIMUM_AGE_EUROPE;
        let yearsDiff = moment().diff(moment((date)), 'years');
        if (yearsDiff >= minimumAge) {
            validAge = true;
        }

        return validAge;
    }

    function getErrorName(date: string): string {
        let errorName;

        if (!moment(date).isValid()) {
            errorName = 'invalidDateError';
        }
        else if (!checkValidAge(date)) {
            errorName = 'userInvalidAgeError';
        }
        else if (!checkToBeUnderAge(date)) {
            errorName = 'userUnderAgeError';
        }

        return errorName;
    }

    return (control: AbstractControl): { [key: string]: any } => {
        let dateValue = control.value;
        if (!dateValue) {
            return null;
        }

        let errorName = getErrorName(dateValue);

        return !errorName ? null : { [`${errorName}`]: { dateValue } };
    };
}

/**
 * numberValidator
 *
 * @return {ValidatorFn}
 */
export function numberValidator(): ValidatorFn {
    function isValidNumber(numberValue: number): boolean {
        return numberValue > 0 && !isNaN(numberValue)
    }

    return (control: AbstractControl): { [key: string]: any } => {
        const value = (control.value);
        return !value || isValidNumber(parseFloat(value)) ? null : { 'invalidNumberError': { value: control.value } };
    };
}
