import { CurrencyService } from "./currency.service";
import { ConfigService } from "../core/config.service";
import { TestBed } from "@angular/core/testing";
import * as moment from "moment";

describe("CurrencyService", () => {

    let service: CurrencyService;
    let configServiceSpy: jasmine.SpyObj<ConfigService> = jasmine.createSpyObj("ConfigService", ["getKey"]);

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                CurrencyService,
                { provide: ConfigService, useValue: configServiceSpy }
            ]
        });

        configServiceSpy.getKey.and.returnValue({
            "symbol": "$",
            "code": "COP",
            "name": "Colombian peso",
            "decimal_digits": 2
        });

        service = TestBed.get(CurrencyService);
    });

    it("should return received currency symbol", () => {
        expect(service.getSymbol()).toEqual("$");
    });

    it("should return received currency code", () => {
        expect(service.getCode()).toEqual("COP");
    });

    it("should format amount", () => {
        moment.locale("en");
        expect(service.formatAmount(5000)).toEqual("5,000.00");
    });

    it("should format currency in english", () => {
        moment.locale("en");
        expect(service.formatAmountWithCurrency(500)).toEqual("COP 500.00");
    });

    it("should format currency in spanish", () => {
        moment.locale("es");
        expect(service.formatAmountWithCurrency(500)).toEqual("500,00 COP");
    });

    it("should return empty if null is provided as amount", () => {
        expect(service.formatAmount(null)).toEqual("");
    });

    it("should return empty if null is provided as currency", () => {
        expect(service.formatAmountWithCurrency(null)).toEqual("");
    });
});
