import { QueryEncoder } from "@angular/http";

export class MTQueryEncoder extends QueryEncoder {

    public encodeKey(k: string): string {
        return this.encodeUrl(k);
    }

    public encodeValue(v: string): string {
        return this.encodeUrl(v);
    }

    // QueryEncoder that replaces "+" character, which is not replaced in encodeURI()
    private encodeUrl(value: string): string {
        return encodeURI(value).replace(/\+/g, "%2B");
    }
}
