import { TestBed } from "@angular/core/testing";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BaseRequestOptions, Http, ResponseOptions, Response, ResponseOptionsArgs } from "@angular/http";
import { Subscription } from "rxjs";
import { ResourcesService } from "./resources.service";
import { ConfigService } from "../core/config.service";
import { Resource } from "./index";

describe("ResourcesService", () => {

    const baseUrl: string = "http://localhost:123";
    const resourceResponse: Resource[] = [{ code: "1111", description: "Resource 1" }, { code: "2222", description: "Resource 2" }];

    let subscriptions: Subscription[] = [];
    let service: ResourcesService;
    let configServiceStub: ConfigService;
    let backend: MockBackend;

    function setupConnections(backend: MockBackend, options: any): Subscription {
        const subscription = backend.connections.subscribe((connection: MockConnection) => {
            const responseOptions = new ResponseOptions(options);
            const response = new Response(responseOptions);
            connection.mockRespond(response);
        });
        subscriptions.push(subscription);
        return subscription;
    }

    afterEach(() => {
        subscriptions.forEach(s => s.unsubscribe());
        subscriptions = [];
    });

    beforeEach(() => {
        configServiceStub = <ConfigService>{
            getEndpoint(endpoint: string): string {
                return baseUrl;
            }
        };

        TestBed.configureTestingModule({
            providers: [
                ResourcesService,
                BaseRequestOptions,
                MockBackend,
                { provide: ConfigService, useValue: configServiceStub },
                {
                    provide: Http,
                    useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => new Http(backendInstance, defaultOptions),
                    deps: [MockBackend, BaseRequestOptions]
                }
            ]
        });

        service = TestBed.get(ResourcesService);
        backend = TestBed.get(MockBackend);
    });

    it("should initialize API resources endpoints", () => {
        const APIResources = {
            cities: baseUrl + "/cities",
            regions: baseUrl + "/regions",
            fiscalRegions: baseUrl + "/fiscal_residence_regions"
        };

        expect(service.APIResources).toEqual(APIResources);
    });

    it("should throw error if region code is not provided", () => {
        spyOn(service, "getCities").and.callThrough();
        expect(service.getCities.bind(service, null)).toThrow();
    });

    it("should return list of cities for provided region code", () => {
        const options: ResponseOptionsArgs = { body: resourceResponse, status: 200 };
        setupConnections(backend, options);
        service.getCities("1111").subscribe((response) => {
            expect(response).toEqual(resourceResponse);
        });
    });

    it("should throw error if region code is not provided", () => {
        spyOn(service, "getRegions").and.callThrough();
        expect(service.getRegions.bind(service, null)).toThrow();
    });

    it("should return list of regions for provided region code", () => {
        const options: ResponseOptionsArgs = { body: resourceResponse, status: 200 };
        setupConnections(backend, options);
        service.getRegions("1111").subscribe((response) => {
            expect(response).toEqual(resourceResponse);
        });
    });

    it("should return list of fiscal regions", () => {
        const options: ResponseOptionsArgs = { body: resourceResponse, status: 200 };
        setupConnections(backend, options);
        service.getFiscalRegions().subscribe((response) => {
            expect(response).toEqual(resourceResponse);
        });
    });

});
