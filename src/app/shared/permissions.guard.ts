import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { PermissionsService } from "mt-web-components";
import { AppRoutes } from "../app-routes";

@Injectable()
export class PermissionsGuard implements CanActivate {

    constructor(private router: Router, private permissionsService: PermissionsService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let pathPermissions = route.data["permissions"] as Array<string>;

        let canActivate = this.permissionsService.hasAccess(pathPermissions);
        if (!canActivate) {
            this.router.navigate([AppRoutes.accessForbidden]);
        }
        return canActivate;
    }
}
