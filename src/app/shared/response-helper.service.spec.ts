import { ResponseHelperService } from "./response-helper.service";
import { TestBed } from "@angular/core/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { FakeTranslateLoader } from "../../test-util/fake-translate-loader";
import { NotificationsService, NotificationTypeEnum } from "mt-web-components";
import { BaseResultsCode } from "./index";

describe("ResponseHelperService", () => {

    let service: ResponseHelperService;
    let notificationsServiceSpy: jasmine.SpyObj<NotificationsService> = jasmine.createSpyObj("NotificationsService", ["add"]);

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                ResponseHelperService,
                { provide: NotificationsService, useValue: notificationsServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        service = TestBed.get(ResponseHelperService);
    });

    it("should show success notification on code Ok", () => {
        service.responseHandler(BaseResultsCode.Ok);

        const expectedParameter = {
            type: NotificationTypeEnum.Success,
            text: 'common.response.ok'
        };

        expect(notificationsServiceSpy.add).toHaveBeenCalledWith(expectedParameter);
    });

    it("should show error notification on code other than Ok", () => {
        service.responseHandler(BaseResultsCode.Unprocessable);

        const expectedParameter = {
            type: NotificationTypeEnum.Error,
            text: 'common.response.unprocessable'
        };

        expect(notificationsServiceSpy.add).toHaveBeenCalledWith(expectedParameter);
    });

    it("should show custom notification type on custom code", () => {
        service.responseHandler(BaseResultsCode.CustomCode, "message", NotificationTypeEnum.Info);

        const expectedParameter = {
            type: NotificationTypeEnum.Info,
            text: "message"
        };

        expect(notificationsServiceSpy.add).toHaveBeenCalledWith(expectedParameter);
    });

    it("should add time to notification if provided", () => {
        service.responseHandler(BaseResultsCode.BadRequest, null, null, 5);

        const expectedParameter = {
            type: NotificationTypeEnum.Error,
            text: "common.response.badRequest",
            time: 5000
        };

        expect(notificationsServiceSpy.add).toHaveBeenCalledWith(expectedParameter);
    });

});
