import * as validator from "./validators";
import { FormControl } from "@angular/forms";

describe("Validators", () => {
    describe("Date validator", () => {

        it("should return no errors if date is passed", () => {
            let control = new FormControl();
            control.validator = validator.basicDateValidator();
            control.setValue(new Date());
            expect(control.valid).toBe(true);
        });

        it("should return no errors if null is passed", () => {
            let control = new FormControl();
            control.validator = validator.basicDateValidator();
            control.setValue(null);
            expect(control.valid).toBe(true);
        });

        it("should return error if a string is passed", () => {
            let control = new FormControl();
            control.validator = validator.basicDateValidator();
            control.setValue("new date");
            expect(control.valid).toBe(false);
        });

    });

    describe("Zip code Colombia validator", () => {

        it("should return error if entered value is not a number", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeColombiaValidator();
            control.setValue("zip code");
            expect(control.valid).toBe(false);
        });

        it("should return error if entered value is longer than it should be", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeColombiaValidator();
            control.setValue("0511111");
            expect(control.valid).toBe(false);
        });

        it("should return error if entered value has non-existent prefix", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeColombiaValidator();
            control.setValue("002233");
            expect(control.valid).toBe(false);
        });

        it("should return no errors if null is passed", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeColombiaValidator();
            control.setValue(null);
            expect(control.valid).toBe(true);
        });

        it("should pass if entered zip code is correct", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeColombiaValidator();
            control.setValue("051111");
            expect(control.valid).toBe(true);
        })

    });

    describe("Zip code Spain validator", () => {

        it("should return error if entered value is not a number", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeSpainValidator();
            control.setValue("zip code");
            expect(control.valid).toBe(false);
        });

        it("should return error if entered value is longer than it should be", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeSpainValidator();
            control.setValue("0511111");
            expect(control.valid).toBe(false);
        });

        it("should return no errors if null is passed", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeSpainValidator();
            control.setValue(null);
            expect(control.valid).toBe(true);
        });

        it("should pass if entered zip code is correct", () => {
            let control = new FormControl();
            control.validator = validator.zipCodeSpainValidator();
            control.setValue("11111");
            expect(control.valid).toBe(true);
        });
    });

    describe("Age validator", () => {

        it("should return invalid age error if entered date is >200y ago", () => {
            let control = new FormControl();
            control.validator = validator.ageValidator("ES");
            control.setValue("1800-01-01");
            expect(control.hasError("userInvalidAgeError")).toBe(true);
        });

        it("should return invalid date error if entered value is not a date", () => {
            let control = new FormControl();
            control.validator = validator.ageValidator("ES");
            control.setValue("date");
            expect(control.hasError("invalidDateError")).toBe(true);
        });

        it("should return user under age error if entered date is <18y ago", () => {
            let control = new FormControl();
            control.validator = validator.ageValidator("ES");
            const year = new Date().getFullYear();
            control.setValue((year - 17) + "-01-01");
            expect(control.hasError("userUnderAgeError")).toBe(true);
        });

        it("should return no errors if null is passed", () => {
            let control = new FormControl();
            control.validator = validator.ageValidator("ES");
            control.setValue(null);
            expect(control.valid).toBe(true);
        });

        it("should return no error if entered date is okay", () => {
            let control = new FormControl();
            control.validator = validator.ageValidator("ES");
            control.setValue("1990-01-01");
            expect(control.valid).toBe(true);
        });
    });

    describe("Number validator", () => {

        it("should return an error if entered value is not a number", () => {
            let control = new FormControl();
            control.validator = validator.numberValidator();
            control.setValue("number");
            expect(control.valid).toBe(false);
        });

        it("should return no error if entered value is a number", () => {
            let control = new FormControl();
            control.validator = validator.numberValidator();
            control.setValue(12345678);
            expect(control.valid).toBe(true);
        });

    });
});
