import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { Http, Response, URLSearchParams } from "@angular/http";
import { ConfigService } from "../core/config.service";
import { Observable } from "rxjs/Observable";
import { OperationTypeForPaymentMethodEnum, StatusPermissions, TransactionTypeEnum } from "./model/models";

@Injectable()
export class PASConfigService {
    APIResources;
    gameTypes;
    transactionTypes;
    statusPermissions: Array<StatusPermissions>;

    constructor(private http: Http, private config: ConfigService) {
        this.APIResources = {
            gameTypes: this.config.getEndpoint('baseUrl') + '/config/game-types',
            paymentMethods: this.config.getEndpoint('baseUrl') + '/config/payment-methods',
            transactionTypes: this.config.getEndpoint('baseUrl') + '/config/transaction-types',
            permissionsByStatus: this.config.getEndpoint('baseUrl') + '/config/status-permissions'
        };
    }

    /**
     * Get the available game types.
     * Retrieves the list of available game Types. The possible values are: GameTypeEnum
     */
    public getGameTypes(): Observable<Array<string>> {
        if (this.gameTypes) {
            return Observable.from([this.gameTypes]);
        }
        return this.http.get(this.APIResources.gameTypes, { search: new URLSearchParams() })
            .map((response: Response) => {
                this.gameTypes = response.json();
                return this.gameTypes
            });
    }

    /**
     * Get the available payment methods by operation type and optionally by transaction type.
     * Retrieves the list of available payment methods. The possible values are: PaymentMethodEnum
     * @param operationType. Query or Command
     * @param transactionType. Deposit or Withdrawal
     * @returns Observable list of strings, corresponding to PaymentMethodEnum values
     */
    public getPaymentMethods(operationType: OperationTypeForPaymentMethodEnum, transactionType?: TransactionTypeEnum): Observable<Array<string>> {
        let queryParams: URLSearchParams = new URLSearchParams();
        queryParams.set('operationType', OperationTypeForPaymentMethodEnum[operationType]);
        transactionType ? queryParams.set('transactionType', TransactionTypeEnum[transactionType]) : null;

        return this.http.get(this.APIResources.paymentMethods, { search: queryParams })
            .map((response: Response) => {
                return response.json();
            });
    }

    /**
     * Get the available transaction types.
     * Retrieves the list of available transaction Types. The possible values are: TransactionTypeEnum
     */
    public getTransactionTypes(): Observable<Array<string>> {
        if (this.transactionTypes) {
            return Observable.from([this.transactionTypes]);
        }
        return this.http.get(this.APIResources.transactionTypes, { search: new URLSearchParams() })
            .map((response: Response) => {
                this.transactionTypes = response.json();
                return this.transactionTypes;
            });
    }

    /**
     * Returns the list of permissions by each player status entry
     * @returns {Observable<Array<StatusPermissions>>}
     */
    public getStatusPermissions(): Observable<Array<StatusPermissions>> {
        if (this.statusPermissions && this.statusPermissions.length > 0) {
            return Observable.from([this.statusPermissions]);
        }

        return this.http.get(this.APIResources.permissionsByStatus).map((statusPermissions: Response) => {
            this.statusPermissions = statusPermissions.json();
            return this.statusPermissions;
        });
    }

}
