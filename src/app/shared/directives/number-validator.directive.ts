import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { numberValidator } from "../validators";

@Directive({
    selector: '[pasNumberValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: NumberValidatorDirective, multi: true }]
})
export class NumberValidatorDirective implements Validator {
    validate(control: AbstractControl): { [key: string]: any } {
        return numberValidator()(control);
    }
}
