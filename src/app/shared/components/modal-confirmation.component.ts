import { Component, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";


@Component({
    template: `
        <div class="modal-header" [ngClass]="[cssSelector+'-header-selector']">
            <h4 *ngIf="title" class="modal-title">{{title}}</h4>
            <button *ngIf="showCloseIcon" type="button" class="close" aria-label="Close"
                    (click)="activeModal.close('CANCEL')">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" [ngClass]="[cssSelector+'-body-selector']">
            <p *ngIf="message">{{message}}</p>

            <div *ngIf="strongMessage" class="summary">
                <span><strong>{{strongMessage}}</strong></span>
            </div>
            <ng-container *ngTemplateOutlet="customTemplate"></ng-container>
        </div>
        <div class="modal-footer" [ngClass]="[cssSelector+'-footer-selector']">
            <button *ngIf="cancelText" class="btn btn-backward cancel-selector" type="button"
                    (click)="activeModal.close('CANCEL')">
                <span>{{cancelText}}</span>
            </button>

            <button *ngIf="confirmText" class="btn btn-forward confirm-selector" type="button"
                    (click)="activeModal.close('CONFIRM')">
                <span>{{confirmText}}</span>
            </button>
        </div>`,
    styleUrls: ['modal-confirmation.component.scss']

})
export class ModalConfirmationComponent {
    @Input() title: string;
    @Input() message: string;
    @Input() strongMessage: string;
    @Input() confirmText: string;
    @Input() cancelText: string;
    @Input() showCloseIcon: boolean;
    @Input() customTemplate: any;
    @Input() cssSelector: string;

    constructor(public activeModal: NgbActiveModal) {
    }
}
