import { Component, Input } from "@angular/core";

@Component({
    selector: 'pas-feedback',
    styleUrls: ['./feedback.component.scss'],
    templateUrl: './feedback.component.html'
})

export class FeedbackComponent {
    @Input() icon: string;
    @Input() title: string;
    @Input() message: string;
    @Input() customTemplate: any;
}
