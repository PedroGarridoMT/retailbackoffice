import "../scss/global.scss";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { APP_INITIALIZER, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { MtComponentsModule } from "mt-web-components";
import { AppRoutingModule } from "./app-routing.module";
import { CoreModule } from "./core/core.module";
import { SharedModule } from './shared/shared.module';
import { AccountModule } from "./account/account.module";
import { CampaignModule } from "./campaign/campaign.module";
import { HmrHelper } from "./core/hmr/hmr";
import { ENV_PROVIDERS } from "./environment";
import { HmrStateService } from "./core/hmr/hmr.service";
import { ConfigService } from "./core/config.service";
import { AppComponent } from "./app.component";
import { MainTemplateComponent } from "./main-template/main-template.component";
import { SettingsMainComponent } from "./settings/settings-main.component";
import { PlayerModule } from "./player/player.module";
import { TransactionModule } from "./transaction/transaction.module";
import { LoaderComponent } from "./main-template/loader.component";
import { StaticModule } from "./static/static.module";

import {MonitoringModule} from "./monitoring/monitoring.module";

//Translate
import {LOCALE_ID} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';


registerLocaleData(localeES);
/**
 * Init services before app loads. This factory needs to return a function that then returns a promise
 */
export function InitApplicationFactory(config: ConfigService) {
    return () => config.load();
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        HttpModule,
        AppRoutingModule,
        CoreModule,
        SharedModule,
        AccountModule,
        CampaignModule,
        PlayerModule,
        TransactionModule,
        MtComponentsModule,
        StaticModule,
        MonitoringModule
    ],
    declarations: [
        AppComponent,
        MainTemplateComponent,
        SettingsMainComponent,
        LoaderComponent
    ],
    providers: [
        ENV_PROVIDERS,
        HmrStateService,
        {
            'provide': APP_INITIALIZER,
            'useFactory': InitApplicationFactory,
            'deps': [ConfigService],
            'multi': true,
        },
        { provide: LOCALE_ID, useValue: 'es' },
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule extends HmrHelper {
    constructor(appRef: ApplicationRef, hmrState: HmrStateService) {
        super(appRef, hmrState)
    }

}
