import { Component, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs/Subscription';
import { LoaderService } from "../shared/loader.service";
@Component({
    selector:'pas-loader',
    styleUrls: ['./loader.component.scss'],
    template: `
        <div class="loader-wrapper" [hidden]="!showLoader">
            <div class="jumper">
                <div></div> <div></div> <div></div>
            </div>
        </div>`
})
export class LoaderComponent implements OnDestroy {
    private _subscriptions: Subscription[] = [];
    public showLoader: boolean = false;

    constructor(private loader: LoaderService) {
        //Subscribe to loader status changes
        this._subscriptions.push(
            loader.loaderStatus$.subscribe((state: boolean) => {
                this.showLoader = state;
            })
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }
}
