import { ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from "@angular/router";
import { MenuItem, PermissionsService, SearchInputComponent, SlideMenuComponent } from "mt-web-components";
import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "../account/shared/authentication.service";
import { TitleBarParams, TitleBarService } from "../shared/index";
import { PlayerService } from "../player/player.service";
import { PlayerSearchRequest } from "../player/model/models";
import { AppRoutes } from "../app-routes";
import { PermissionsCode } from "../shared/model/models";
import { LoaderService } from "../shared/loader.service";
import { Subscription } from "rxjs/Subscription";
import { PlayerLimitChangeStatusEnum, PlayerSearchResponse } from "../shared/model/models";
import { ChangePasswordModalComponent } from "../account/change-password/change-password-modal.component";
import { Title } from "@angular/platform-browser";
import { EmitterService, EmitterServiceEventsEnum } from "../shared/services/emmiter.service";

@Component({
    styleUrls: ['./main-template.component.scss'],
    templateUrl: './main-template.component.html'
})
export class MainTemplateComponent implements OnInit, OnDestroy {

    private _subscriptions: Subscription[] = [];

    menuCollapsed: boolean;
    activeState: string;
    menuItems: MenuItem[];
    menuLogoIcon: string = '/assets/img/onMix-icon.png';
    menuLogoText: string = '';
    userOptions: any[];
    quickSearch: any = null;
    titleBarContent: TitleBarParams;
    actionsComboExpanded: boolean = false;

    @ViewChild('playersQuickSearchInput') playersQuickSearchInput: SearchInputComponent;
    @ViewChild(SlideMenuComponent) mobileMenu: SlideMenuComponent;

    constructor(private router: Router, private route: ActivatedRoute, private translate: TranslateService,
        private authService: AuthenticationService, private titleBarService: TitleBarService,
        private playerService: PlayerService, private cd: ChangeDetectorRef,
        private permissionsService: PermissionsService, private loader: LoaderService,
        private titleService: Title, private modalService: NgbModal, private emitterService: EmitterService) {
    }

    public ngOnInit(): void {

        this.initializeComponents();
        this.getConfigDataFromRoutes();

        this._subscriptions.push(
            //Subscribe to default language change events. If any happens => re-initialize components in the new language
            this.translate.onDefaultLangChange.subscribe(() => {
                this.initializeComponents();
            }),
            //Subscribe to component initialized events so the title bar paints the ui elems according to parameters received.
            this.titleBarService.componentInitialized$.subscribe(data => {
                this.titleBarContent = data;
                this.setItemNavigatorLiterals();
                this.cd.detectChanges();
            }),
            //Subscribe to events launched when the components are destroyed so the title bar can be cleaned
            this.titleBarService.componentDestroyed$.subscribe(() => {
                this.titleBarContent = null;
            }),
            //
            this.emitterService.eventBus$.subscribe(event => {
                if (event == EmitterServiceEventsEnum.MENU_CURRENT_STATE_REQUEST) {
                    this.emitMenuStateEvent(this.menuCollapsed);
                }
            })
        );
    }

    ngOnDestroy() {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /*
    * Create the menu items to be passed to the mt-menu component.
    * First, it get the translation for the labels, and then creates the menu items containing the translated texts
    */
    initializeComponents() {
        this.createLateralMenu();
        this.initQuickSearch();
        this.initUserOptions();
    }

    /**
     * Everytime a NavigationEnd event occurs, this method get the data stored on the current route. (See app-routing.module.ts)
     * This data contains information about the currentStateName, which is used to mark the active section in the menu.
     * And title information, used to update the page title according to the current route.
     */
    getConfigDataFromRoutes() {
        this._subscriptions.push(
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {

                    //Store all params from the root route => To be used in the page title
                    let urlParams: any = {};
                    this.route.children.forEach(route => route.params.subscribe(params => {
                        Object.keys(params).forEach(key => urlParams[key] = params[key]);
                    }));

                    let currentRouteSnapshot = this.getRouteSnapshot();

                    // Store active state used in the menus
                    this.activeState = currentRouteSnapshot.data['stateName'] || null;

                    //Set page title
                    this.setPageTitle(currentRouteSnapshot, urlParams);

                    //Scroll top every time there is a route change.
                    window.scrollTo(0, 0);
                }
            })
        );
    }

    getRouteSnapshot(): ActivatedRouteSnapshot {
        // Traverse the active route tree to access the latest route
        let snapshot = this.route.snapshot;
        let activated = this.route.firstChild;
        if (activated != null) {
            while (activated != null) {
                snapshot = activated.snapshot;
                activated = activated.firstChild;
            }
        }
        return snapshot;
    }

    setPageTitle(routeSnapshot, urlParams) {
        let pageTitle = "OnMix";

        //Find title config from the data object stored in the route
        let titleConfig = routeSnapshot.data['title'] || null;
        if (titleConfig) {
            this.translate.get(titleConfig.text).subscribe(title => {
                if (titleConfig.param) {
                    title = title.replace("{{" + titleConfig.param + "}}", urlParams[titleConfig.param])
                }
                this.titleService.setTitle(title);
            });
        }
        else {
            this.titleService.setTitle(pageTitle);
        }
    }

    initQuickSearch() {
        if (this.permissionsService.hasAccess(PermissionsCode.PlayerSearch)) {
            this.quickSearch = {
                searching: false,
                message: null,
                placeholder: this.translate.instant('players.home.search'),
                placeholderActive: this.translate.instant('home.quickSearchActiveTooltip')
            }
        }
    }

    createLateralMenu() {
        this.menuItems = [];

        if (this.permissionsService.hasAccess(PermissionsCode.PlayerHome)) {
            this.menuItems.push(
                {
                    stateName: 'Players',
                    text: this.translate.instant("menu.players.main"),
                    targetRoute: '/players',
                    iconClasses: 'fa fa-users fa-fw',
                    cssSelector: "menu-players-selector",
                    children: []
                }
            );

            if (this.permissionsService.hasAccess(PermissionsCode.PlayerSearch) && this.menuItems.length) {
                this.menuItems.map((menuItem: MenuItem) => {
                    if (menuItem.stateName === 'Players' && menuItem.children) {
                        menuItem.children.push(
                            {
                                text: this.translate.instant("menu.players.search"),
                                targetRoute: '/players/search'
                            }
                        );
                    }
                });
            }

            if (this.permissionsService.hasAccess(PermissionsCode.PlayerAutoLimitsSearch) && this.menuItems.length) {
                this.menuItems.map((menuItem: MenuItem) => {
                    if (menuItem.stateName === 'Players' && menuItem.children) {
                        //By default, search limit changes with status = submitted. Add this param to the base URL
                        menuItem.children.push(
                            {
                                text: this.translate.instant("menu.players.limits"),
                                targetRoute: AppRoutes.searchPlayerAutoLimitChanges,
                                queryParams: { status: PlayerLimitChangeStatusEnum[PlayerLimitChangeStatusEnum.Submitted] }
                            }
                        );
                    }
                });
            }

            if (this.permissionsService.hasAccess(PermissionsCode.PlayerBlacklistSearch) && this.menuItems.length) {
                this.menuItems.map((menuItem: MenuItem) => {
                    if (menuItem.stateName === 'Players' && menuItem.children) {
                        menuItem.children.push(
                            {
                                text: this.translate.instant("menu.players.blacklist"),
                                targetRoute: AppRoutes.searchBlacklistedPlayers,
                            }
                        );
                    }
                });
            }
        }

        if (this.permissionsService.hasAccess(PermissionsCode.TransactionHome)) {
            this.menuItems.push(
                {
                    stateName: 'Transactions',
                    text: this.translate.instant("menu.transactions.main"),
                    targetRoute: '/transactions',
                    iconClasses: 'fa fa-exchange fa-fw',
                    cssSelector: "menu-transactions-selector",
                    children: []
                }
            );

            if (this.permissionsService.hasAccess(PermissionsCode.TransactionSearch)) {
                this.menuItems.map((menuItem: MenuItem) => {
                    if (menuItem.stateName === 'Transactions' && menuItem.children) {
                        menuItem.children.push(
                            {
                                text: this.translate.instant("menu.transactions.search"),
                                targetRoute: AppRoutes.transactionsSearch
                            }
                        );
                    }
                });
            }

            if (this.permissionsService.hasAccess(PermissionsCode.TransactionSearchPendingWithdrawals)) {
                let transactionsMenu = this.menuItems.find(item => item.stateName == 'Transactions');
                transactionsMenu.children.push(
                    {
                        text: this.translate.instant("menu.transactions.withdrawals"),
                        targetRoute: '/transactions/withdrawals'
                    });
            }
        }

        if (this.permissionsService.hasAccess(PermissionsCode.CampaignHome)) {
            this.menuItems.push(
                {
                    stateName: "Campaigns",
                    text: this.translate.instant("menu.campaigns.main"),
                    targetRoute: "/campaigns",
                    iconClasses: "fa fa-trophy fa-fw",
                    cssSelector: "menu-campaigns-selector",
                    children: [
                        {
                            text: this.translate.instant("menu.campaigns.search"),
                            targetRoute: "/campaigns/search",
                        }
                    ]
                }
            );
            if (this.permissionsService.hasAccess(PermissionsCode.CampaignMassiveAssignation)) {
                let campaignsMenu = this.menuItems.find(item => item.stateName == 'Campaigns');
                campaignsMenu.children.push(
                    {
                        text: this.translate.instant("menu.campaigns.massiveBonusAssignment"),
                        targetRoute: "/campaigns/manual-campaign-create",
                    });
            }
            if (this.permissionsService.hasAccess(PermissionsCode.CampaignCreate)) {
                let campaignsMenu = this.menuItems.find(item => item.stateName == 'Campaigns');
                campaignsMenu.children.push(
                    {
                        text: this.translate.instant("menu.campaigns.create"),
                        targetRoute: "/campaigns/create",
                    });
            }
        }

        if (this.permissionsService.hasAccess(PermissionsCode.Monitoring)) {
            this.menuItems.push(
                {
                    stateName: 'Monitoring',
                    text: this.translate.instant("menu.monitoring.main"),
                    targetRoute: '/monitoring',
                    iconClasses: 'fa fa-bar-chart',
                    cssSelector: "menu-monitoring",
                    children: [
                        {
                            text: this.translate.instant("menu.monitoring.search"),
                            targetRoute: '/monitoring/search'
                        }
                    ]
                }
            );
        }

        if (this.permissionsService.hasAccess(PermissionsCode.SettingsHome)) {
            this.menuItems.push(
                {
                    stateName: 'Settings',
                    text: this.translate.instant("menu.settings"),
                    targetRoute: '/settings',
                    iconClasses: 'fa fa-cog fa-fw',
                    cssSelector: "menu-settings-selector",
                    children: null
                }
            );
        }


    }

    /*
     * Create the user menu options to be passed to the user mt-dropdown component.
     */
    initUserOptions() {
        this.userOptions = [
            // { param: "change-password", text: this.translate.instant('account.changePassword'), cssSelector: 'change-password-selector'}, //TODO enabled after integration with PAS
            { param: "logout", text: this.translate.instant('account.logout'), cssSelector: 'logout-selector' }
        ];
    }

    onToggleMenu(collapsed) {
        this.menuCollapsed = collapsed;
        this.cd.detectChanges();
        this.emitMenuStateEvent(collapsed);
    }

    emitMenuStateEvent(isMenuCollapsed) {
        isMenuCollapsed ?
            this.emitterService.emitEvent(EmitterServiceEventsEnum.MENU_IS_COLLAPSED) :
            this.emitterService.emitEvent(EmitterServiceEventsEnum.MENU_IS_EXPANDED);
    }

    onToggleMobileMenu() {
        this.mobileMenu.toggleMenu();
    }

    onUserMenuClicked(param: string) {
        switch (param.toLowerCase()) {
            case "logout":
                this.loader.show();
                this.authService.logoutUser()
                    .finally(() => {
                        this.loader.hide();
                        this.router.navigate([AppRoutes.login]);
                    }).subscribe();

                break;

            case "change-password":
                this.showChangePasswordModal();
                break;
        }
    }

    onSearchRequested(searchString: string) {
        this.quickSearch.searching = true;
        this.quickSearch.message = null;

        let searchRequest: PlayerSearchRequest = {
            filterCriteria: { 'username': searchString }, pageNumber: 1, pageSize: 1, sortBy: null, sortDirection: null
        };

        this.playerService.getPlayers(searchRequest).subscribe(
            playerResponse => {
                if (playerResponse && playerResponse.items && playerResponse.items.length == 1) {
                    this.quickSearch.searching = false;
                    this.goToPlayerDetailPage(playerResponse.items[0].accountCode)
                }
                else {
                    //Try searching by AccountCode
                    searchRequest.filterCriteria = { 'accountCode': searchString };
                    this.playerService.getPlayers(searchRequest).subscribe(
                        response => this.parseSuccessResponse(response),
                        error => this.parseErrorResponse(error));
                }
            },
            error => {
                //Try searching by AccountCode
                searchRequest.filterCriteria = { 'accountCode': searchString };
                this.playerService.getPlayers(searchRequest).subscribe(
                    response => this.parseSuccessResponse(response),
                    err => this.parseErrorResponse(err));
                console.log(error);
            });
    }

    parseSuccessResponse(response: PlayerSearchResponse) {
        this.quickSearch.searching = false;
        if (response && response.items && response.items.length == 1) {
            this.goToPlayerDetailPage(response.items[0].accountCode)
        }
        else {
            this.quickSearch.message = this.translate.instant("players.search.playerNotFound");
        }
    }

    goToPlayerDetailPage(accountCode: string) {
        this.router.navigate(['players', accountCode, 'detail']).then((routeChanged) => {
            //When the route is changed, reset the quick search bar.
            if (routeChanged) {
                this.playersQuickSearchInput.clearInput();
            }
        });
    }

    parseErrorResponse(error) {
        this.quickSearch.message = this.translate.instant("players.search.playerNotFound");
        console.warn(error);
    }

    onTitleBarActionClick(actionId: string) {
        //Notify the title bar service so the owner of this action can handle it.
        this.titleBarService.actionRequested(actionId);
    }

    public getPreviousPagePath() {
        let path = this.titleBarContent.previousPagePath;
        return path.startsWith("/") ? [path] : ["/" + path];
    }

    public getPreviousPageQueryParams(): Object {
        return this.titleBarContent.previousPageQueryParams;
    }

    private showChangePasswordModal(): NgbModalRef {
        return this.modalService.open(ChangePasswordModalComponent, {
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            windowClass: 'xlg-modal'
        });
    }

    /**
     * If no literals are specified for the item-navigator component, get default ones.
     * mt-item-navigator literals
     */
    private setItemNavigatorLiterals() {
        if (this.titleBarContent.itemNavigator && !this.titleBarContent.itemNavigator.literals) {
            this.titleBarContent.itemNavigator.literals = {
                previous: this.translate.instant('common.previous'),
                next: this.translate.instant('common.next'),
                label: this.translate.instant('common.of'),
            };
        }
    }

    @HostListener('document:click', ['$event'])
    onDocumentClick(event: MouseEvent) {
        if (this.actionsComboExpanded &&
            event.srcElement.parentElement.id != 'actions-combo-panel' &&
            event.srcElement.parentElement.parentElement.id != 'actions-combo-panel') {
            this.actionsComboExpanded = false;
        }
    }
}
