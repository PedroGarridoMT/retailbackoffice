import { TestBed } from "@angular/core/testing";
import { BaseRequestOptions, Http, ResponseOptions, Response, ResponseOptionsArgs } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { PermissionsService } from "mt-web-components";
import { RouterTestingModule } from "@angular/router/testing";
import { ConfigService } from "../../core/config.service";
import { AuthenticationService } from "./authentication.service";
import { LoginRequest } from "../../shared/index";
import { AppRoutes } from "../../app-routes";
import { LoginResponseExtended } from "../model/LoginResponseExtended";
import { DummyComponent } from "../../../test-util/dummy.component";
import { ModalService } from "../../shared/services/modal.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

describe("AuthenticationService", () => {

    let service: AuthenticationService;
    let configServiceStub: ConfigService;
    let permissionsServiceStub: PermissionsService;
    let backend: MockBackend;
    let router: Router;
    let subscriptions: Subscription[] = [];
    let loginResponse: LoginResponseExtended;
    let modalServiceSpy: ModalService;

    beforeEach(() => {
        configServiceStub = <ConfigService>{ URLs: { account: { session: "/" } } };
        permissionsServiceStub = <PermissionsService>{};
        loginResponse = { username: "username", expiresIn: 600, permissions: ["Permission"] };
        modalServiceSpy = jasmine.createSpyObj<ModalService>("ModalService", ["closeAll"]);

        TestBed.configureTestingModule({
            providers: [
                AuthenticationService,
                BaseRequestOptions,
                MockBackend,
                { provide: ConfigService, useValue: configServiceStub },
                { provide: PermissionsService, useValue: permissionsServiceStub },
                {
                    provide: Http,
                    useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => new Http(backendInstance, defaultOptions),
                    deps: [MockBackend, BaseRequestOptions]
                },
                { provide: NgbModal, useValue: modalServiceSpy },
                { provide: ModalService, useExisting: NgbModal }
            ],
            declarations: [DummyComponent],
            imports: [RouterTestingModule.withRoutes([{ path: 'login', component: DummyComponent }]),]
        });

        service = TestBed.get(AuthenticationService);
        backend = TestBed.get(MockBackend);
        router = TestBed.get(Router);

        localStorage.clear();
    });

    afterEach(() => {
        subscriptions.forEach(s => s.unsubscribe());
        subscriptions = [];
        localStorage.clear();
    });

    function setupConnections(backend: MockBackend, options: any): Subscription {
        const subscription = backend.connections.subscribe((connection: MockConnection) => {
            const responseOptions = new ResponseOptions(options);
            const response = new Response(responseOptions);
            connection.mockRespond(response);
        });
        subscriptions.push(subscription);
        return subscription;
    }

    function getExpiresAtDate(expiresIn: number): Date {
        return new Date(Date.now() + expiresIn * 1000);
    }

    it("should login user, remove force-logout and set user in local storage", (done) => {
        localStorage.setItem("force-logout", "true");

        const body: LoginResponseExtended = loginResponse;
        const options: ResponseOptionsArgs = { body: body, status: 200 };
        setupConnections(backend, options);

        const request: LoginRequest = { username: "username", password: "password" };
        service.login(request)
            .finally(done)
            .subscribe((response: LoginResponseExtended) => {
                expect(response).toEqual(body);
                expect(localStorage.getItem("force-logout")).toBe(null);

                const expiresAt = getExpiresAtDate(body.expiresIn);
                const userFromLocalStorage = JSON.parse(localStorage.getItem("user"));

                expect(Math.abs(new Date(userFromLocalStorage.expiresAt).getTime() - expiresAt.getTime())).toBeLessThanOrEqual(500);
            });
    });

    it("should set and return current user from local storage if session token is valid", () => {
        const user: LoginResponseExtended = loginResponse;
        user.expiresAt = getExpiresAtDate(user.expiresIn);
        localStorage.setItem("user", JSON.stringify(user));

        expect(service.getCurrentUser()).toEqual(user);
        expect(service.isUserAuthenticated()).toBe(true);
    });

    it(`should logout user if there is no "user" in local storage`, () => {
        spyOn(service, "logoutUser");
        service.getCurrentUser();
        expect(service.logoutUser).toHaveBeenCalled();
        expect(service.isUserAuthenticated()).toBe(false);
    });

    it("should logout user if session token is expired", () => {
        const user: LoginResponseExtended = { username: "username", expiresIn: 0, permissions: ["Permission"] };
        user.expiresAt = getExpiresAtDate(user.expiresIn);
        localStorage.setItem("user", JSON.stringify(user));

        spyOn(service, "logoutUser");
        service.getCurrentUser();
        expect(service.logoutUser).toHaveBeenCalled();
        expect(service.isUserAuthenticated()).toBe(false);
    });

    it("should logout user if token has no expiration date", () => {
        const user: LoginResponseExtended = { username: "username", expiresIn: 0, permissions: ["Permission"] };
        localStorage.setItem("user", JSON.stringify(user));

        spyOn(service, "logoutUser");
        service.getCurrentUser();
        expect(service.logoutUser).toHaveBeenCalled();
        expect(service.isUserAuthenticated()).toBe(false);
    });

    it("should logout user and set force-logout in local storage", (done) => {
        const options: ResponseOptionsArgs = { body: null, status: 200 };
        setupConnections(backend, options);
        spyOn(router, "navigate").and.returnValue(Promise.resolve());
        service.logoutUser()
            .delay(1100)
            .finally(done)
            .subscribe((response: Response) => {
                expect(localStorage.getItem("force-logout")).toEqual("true");
                expect(router.navigate).toHaveBeenCalled();
            });
    });

    it("should check if user is authorized for auth code", () => {
        const user: LoginResponseExtended = loginResponse;
        user.expiresAt = getExpiresAtDate(user.expiresIn);
        localStorage.setItem("user", JSON.stringify(user));
        service.getCurrentUser();

        expect(service.isUserAuthorizedForAuthCode("Permission")).toBe(true);
        expect(service.isUserAuthorizedForAuthCode("NoPermission")).toBe(false);
    });

    it("should check if user needs to be logged out and do if needed", () => {
        spyOn(service, "logoutUser");
        spyOn(router, "navigate").and.returnValue(Promise.resolve());
        spyOn(service, "isLoginPage").and.returnValue(false);

        localStorage.setItem("force-logout", "true");
        service.checkForForceLogout();

        expect(service.logoutUser).toHaveBeenCalled();
        expect(router.navigate).toHaveBeenCalled();
    });

    it("should return true when login page is shown", () => {
        spyOnProperty(router, "url", "get").and.returnValue(AppRoutes.login + "?returnUrl=%2F");
        expect(service.isLoginPage()).toBe(true);
    });

    it("should return false when login page is not shown", () => {
        spyOnProperty(router, "url", "get").and.returnValue("/not-login");
        expect(service.isLoginPage()).toBe(false);
    });

});
