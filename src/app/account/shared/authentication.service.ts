﻿import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { HelperService } from "../../shared/helper.service";
import { ConfigService } from "../../core/config.service";
import { AppRoutes } from "../../app-routes";
import { PermissionsService } from "mt-web-components";
import { LoginRequest, LoginResponse, PatchDocument} from "../../shared";
import { LoginResponseExtended } from "./../model/LoginResponseExtended";
import { NgZone } from "@angular/core";
import { ModalService } from "../../shared/services/modal.service";
import {AuthenticationFirebaseService} from "../../monitoring/authentication/authentication-firebase.service";

@Injectable()
export class AuthenticationService {
    private currentUser: LoginResponseExtended;
    private logoutTimer: number;
    private loginCheckInterval: number;

    constructor(private http: Http, private config: ConfigService, private modalService: ModalService,
                private authFirebaseService: AuthenticationFirebaseService,
                private permissionsService: PermissionsService, private router: Router, private ngZone: NgZone) {
    }

    /**
     * Get user token and the allowed permissions for the user.
     * Retrieves an object with Access token information and a list of the available user permissions.
     * @param request The Login request, username and password.
     */
    public login(request: LoginRequest): Observable<LoginResponse> {
        return this.http.post(this.config.URLs.account.session, request)
            .map((response: Response) => {

                localStorage.removeItem("force-logout");
                window.clearInterval(this.loginCheckInterval);

                let loginResponse: LoginResponse = response.json();
                this.saveLoginResponseToLocalStorage(loginResponse);
                return loginResponse;
            }).catch(HelperService.handleError);
    }

    /**
     * Revokes user access token.
     * Revokes the access token information.
     */
    public logoutUser(): Observable<{}> {
        this.modalService.closeAll();
        this.clearStorage();
        this.authFirebaseService.logout();
        return this.http.delete(this.config.URLs.account.session)
            .map((response: Response) => {
                localStorage.setItem("force-logout", "true");
                this.waitForNextLogIn();
                return response;
            }).catch(HelperService.handleError);
    }

    /**
     * Applies partial modifications to a given token.
     * Used to partially modify a session. Currently, this operation is exclusively used to refreshToken:  | Operation | Path | Value | Description | | --- | --- | --- | --- | | \&quot;replace\&quot; | \&quot;/refreshToken\&quot; | \&quot;refreshToken\&quot; | Refresh user access token |
     * @param requestPatch Array of patch requests containing the set of changes to be done in the session.
     */
    public patchSession(requestPatch: Array<PatchDocument>): Observable<LoginResponse> {
        return this.http.patch(this.config.URLs.account.session, requestPatch)
            .map((response: Response) => {
                let loginResponse: LoginResponse = response.json();
                this.saveLoginResponseToLocalStorage(loginResponse);
                return loginResponse;
            }).catch(HelperService.handleError);
    }

    /**
     * getCurrentUser
     * @returns {LoginResponse}
     */
    public getCurrentUser(): LoginResponseExtended {
        if (!this.currentUser) {
            this.currentUser = JSON.parse(localStorage.getItem("user"));
        }
        if(!this.currentUser){
            this.currentUser = this.authFirebaseService.getCurrentUser();
        }

        if (!this.currentUser) {
            console.log("AuthService.getCurrentUser-> No user found!");
            this.logoutUser();
            return null;
        }
        else {
            if (this.currentUser.expiresAt) {
                this.currentUser.expiresAt = new Date(this.currentUser.expiresAt);
                if (new Date() >= this.currentUser.expiresAt) {
                    console.log("AuthService.getCurrentUser()-> Token has expired!");
                    this.logoutUser();
                    return null;
                }
            }
            else {
                console.log("AuthService.getCurrentUser()-> Token with no expiration date. Logout done for security!");
                this.logoutUser();
                return null;
            }
        }

        return this.currentUser;
    }

    /**
     * isUserAuthenticated
     * @returns {boolean}
     */
    public isUserAuthenticated(): boolean {
        return this.getCurrentUser() != null;
    }

    /**
     * isUserAuthorizedForAuthCode
     * @param authCode
     * @returns {boolean}
     */
    public isUserAuthorizedForAuthCode(authCode: string): boolean {
        return this.currentUser.permissions.some((permission: string) => permission === authCode);
    }

    /**
     * Logout and clear
     */
    private clearStorage(): void {
        HelperService.clearLocalStorage();
        this.currentUser = null;
    }

    /**
     * Save to local Storage and updates permissionsService.userPermissions
     * @param loginResponse
     * @returns {null}
     */
    private saveLoginResponseToLocalStorage(loginResponse: LoginResponse): void {
        if (loginResponse) {
            //Configure PermissionsService with current user permissions(mt-web-components)
            this.permissionsService.userPermissions = loginResponse.permissions;

            //Store user relevant data.
            localStorage.setItem("user", JSON.stringify({
                "username": loginResponse.username,
                "expiresIn": loginResponse.expiresIn,
                "expiresAt": new Date(Date.now() + loginResponse.expiresIn * 1000)
            }));

            // Kick of logout timer
            this.startLogoutTimer();
        }
    }

    /**
     * Trigger waitForNextLogIn interval after user session expires
     */
    public startLogoutTimer(): void {

        if (this.logoutTimer) {
            window.clearTimeout(this.logoutTimer);
        }

        const currentUser: LoginResponseExtended = this.getCurrentUser();
        const time: number = currentUser && currentUser.expiresAt ? currentUser.expiresAt.getTime() - Date.now() : 0;

        //Move timeouts/intervals outside the angular zone so protractor does not wait them forever.
        this.ngZone.runOutsideAngular(() => {
            this.logoutTimer = window.setTimeout(() => {
                // Changes here will not propagate into your view.
                this.ngZone.run(() => {
                    // Run inside the ngZone to trigger change detection.
                    if (!this.isUserAuthenticated()) {
                        this.waitForNextLogIn();
                    } else {
                        this.startLogoutTimer();
                    }
                });
            }, time);
        });
    }

    /**
     * Interval to handle login across all tabs
     */
    private waitForNextLogIn(): void {
        if (this.loginCheckInterval) {
            window.clearInterval(this.loginCheckInterval);
        }

        //Move timeouts/intervals outside the angular zone so protractor does not wait them forever.
        this.ngZone.runOutsideAngular(() => {

            // Constantly check for new login
            this.loginCheckInterval = window.setInterval(() => {

                // Run inside the ngZone to trigger change detection.
                this.ngZone.run(() => {

                    // If tab is visible, show login page
                    if (!document.hidden && !this.isLoginPage()) {
                        this.router.navigate([AppRoutes.login], { queryParams: { returnUrl: this.router.url } });
                    }

                    // Check for new login data
                    if (localStorage.getItem("user")) {
                        // Logged in, kill interval
                        window.clearInterval(this.loginCheckInterval);

                        if (this.isLoginPage()) {
                            // Login page shown - reload login page
                            window.location.reload();
                        } else {
                            // Not on login page - just start logout timer
                            this.startLogoutTimer();
                        }
                    }
                });
            }, 1000);
        });
    }

    /**
     * Helper to check if tab is on login page
     * @private
     * @returns {boolean}
     */
    public isLoginPage(): boolean {
        return this.router.url.startsWith(AppRoutes.login);
    }

    /**
     * Check if "force-logout" flag is set
     * if so, logout user and wait for the next login
     */
    public checkForForceLogout(): void {
        if (!localStorage.getItem("user") && localStorage.getItem("force-logout") && !this.isLoginPage()) {
            this.logoutUser();
            this.router.navigate(
                [AppRoutes.login],
                { queryParams: { returnUrl: this.router.url } }
            ).then(this.waitForNextLogIn.bind(this));
        }
    }
}
