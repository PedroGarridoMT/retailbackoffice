import { inject, TestBed, async } from "@angular/core/testing";
import { Observable } from "rxjs";
import { ErrorObservable } from "rxjs/observable/ErrorObservable";
import { LoginModel } from "mt-web-components";
import { ApiResponse } from "./../../shared/model/api-response.model";
import { LoginResponse } from "./../../shared/model/models";
import { LoginService } from "./login.service";
import { AuthenticationService } from "../shared/authentication.service";
import { ModelError } from "../../shared/index";

describe("LoginService", () => {

    let service: LoginService;
    let authServiceSpy: jasmine.SpyObj<AuthenticationService> = jasmine.createSpyObj("AuthenticationService", ["login"]);

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                LoginService,
                { provide: AuthenticationService, useValue: authServiceSpy }
            ]
        });

        service = TestBed.get(LoginService);
    });

    it("should return error and set loginResultCode on API error", async(() => {
        const errResponse: ApiResponse = {
            statusCode: 422,
            error: { errorCode: "ErrorCodeString" },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        authServiceSpy.login.and.returnValue(Observable.throw(errResponse));

        service.login(credentials).subscribe(null, (error) => {
            expect(error).toEqual(errResponse);
            expect(service.getLoginResultCode()).toBe("ErrorCodeString");
        });
    }));

    it("should not return error on successful login", async(() => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        authServiceSpy.login.and.returnValue(Observable.of(response));

        service.login(credentials).subscribe((result) => {
            expect(service.getLoginResultCode()).toBe(null);
        });
    }));

    it("should call AuthenticationService.login() with provided credentials", () => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        authServiceSpy.login.and.returnValue(Observable.of(response));
        service.login(credentials).subscribe((result) => {
            expect(authServiceSpy.login).toHaveBeenCalledWith(credentials);
            expect(result).toEqual(response);
        });
    });

    it("should be mandatory to change password on first login", async(() => {
        const errResponse: ApiResponse = {
            statusCode: 422,
            error: { errorCode: ModelError.ErrorCodeEnum.PasswordNotSetFirstLogin },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        authServiceSpy.login.and.returnValue(Observable.throw(errResponse));

        service.login(credentials).subscribe(null, (error) => {
            expect(error).toEqual(errResponse);
            expect(service.isMandatoryToChangePassword()).toBe(true);
        });
    }));

    it("should be mandatory to change password if password expired", async(() => {
        const errResponse: ApiResponse = {
            statusCode: 422,
            error: { errorCode: ModelError.ErrorCodeEnum.PasswordExpired },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        authServiceSpy.login.and.returnValue(Observable.throw(errResponse));

        service.login(credentials).subscribe(null, (error) => {
            expect(error).toEqual(errResponse);
            expect(service.isMandatoryToChangePassword()).toBe(true);
        });
    }));

    it("should not be mandatory to change password on other error codes", async(() => {
        const errResponse: ApiResponse = {
            statusCode: 422,
            error: { errorCode: "ErrorCodeString" },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        authServiceSpy.login.and.returnValue(Observable.throw(errResponse));

        service.login(credentials).subscribe(null, (error) => {
            expect(error).toEqual(errResponse);
            expect(service.isMandatoryToChangePassword()).toBe(false);
        });
    }));

});
