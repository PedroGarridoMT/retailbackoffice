import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { LoginLiterals, LoginModel } from "mt-web-components";
import { AppRoutes } from "../../app-routes";
import { LoginService } from "./login.service";
import { AuthenticationService } from "../shared/authentication.service";
import { ApiResponse, BaseResultsCode, ModelError } from "../../shared";

@Component({
    selector: 'pas-login',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    loginLiterals: LoginLiterals = {} as LoginLiterals;
    currentError: string = '';
    loading = false;
    logoIcon = '';
    returnUrl: string;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private loginService: LoginService,
                private translate: TranslateService,
                private authenticationService: AuthenticationService,
                private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("Login - OnMix");

        // get return url from route parameters or default to home page
        this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || AppRoutes.home;

        //If the user is already authenticated, redirect to the home page or to returnUrl
        if (this.authenticationService.isUserAuthenticated()) {
            this.router.navigateByUrl(this.returnUrl);
        }

        this.loginLiterals.login = this.translate.instant('login.button');
        this.loginLiterals.password = this.translate.instant('login.password');
        this.loginLiterals.username = this.translate.instant('login.username');

    }

    /**
     * (loginSubmitted) from mt-login handler
     * @param credentials
     */
    onLoginSubmitted(credentials: LoginModel): Subscription {
        this.loading = true;
        return this.loginService.loginFireBase(credentials)
            .finally(() => {
                this.loading = false;
            })
            .subscribe(() => {
                this.currentError = '';
                this.router.navigateByUrl(this.returnUrl);
            }, (error: ApiResponse) => {
                this.loginErrorHandler(error);
            });
    }

    /**
     * Error parser
     * @returns string
     * @param response
     */
    private loginErrorHandler(response: ApiResponse) {
        if (response.statusCode == BaseResultsCode.Unauthorized) {
            this.currentError = this.translate.instant('login.error.invalidCredentials');
        }
        else if (response.statusCode == BaseResultsCode.Unprocessable) {
            switch (response.error.errorCode) {
                case ModelError.ErrorCodeEnum.LoginAttemptsExceeded:
                    this.currentError = this.translate.instant('login.error.maximumAttemptsExceeded');
                    break;
                case ModelError.ErrorCodeEnum.PasswordNotSetFirstLogin:
                    this.router.navigate([AppRoutes.changePassword]);
                    break;
                case ModelError.ErrorCodeEnum.PasswordExpired:
                    this.router.navigate([AppRoutes.changePassword]);
                    break;
                default:
                    this.currentError = this.translate.instant('login.error.generic');
            }
        }
        else {
            this.currentError = this.translate.instant('login.error.generic');
        }
    }
}
