﻿import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { LoginModel } from "mt-web-components";
import { LoginResponse, ApiResponse, ModelError } from "../../shared";
import { AuthenticationService } from "../shared/authentication.service";
import {AuthenticationFirebaseService} from "../../monitoring/authentication/authentication-firebase.service";
import { Observable } from "rxjs/Observable";

@Injectable()
export class LoginService {
    private _loginResultCode: ApiResponse;
    constructor(
        private authService: AuthenticationService,
        private authFirebaseService: AuthenticationFirebaseService) {
    }

    /**
     * login
     * @param credentials
     * @returns {Observable<R>}
     */
    public login(credentials: LoginModel) {
        return this.authService.login(credentials)
            .map((loginResponse: LoginResponse) => {
                return loginResponse;
            })
            .catch((error: ApiResponse) => {
                this._loginResultCode = error;
                return Observable.throw(error);
            });
    }

    /**
     * login
     * @param credentials
     * @returns {Observable<R>}
     */
    public loginFireBase(credentials: LoginModel): Observable<any> {
        return Observable.fromPromise(this.authFirebaseService.login(credentials.username, credentials.password))
            .map((loginResponse: LoginResponse) => {
                return loginResponse;
            })
            .catch((error: ApiResponse) => {
                this._loginResultCode = error;
                // return this.login(credentials);
                return Observable.throw(error);
            });
    }


    /**
     * getLoginResultCode
     * @returns {string}
     */
    public getLoginResultCode(): string {
        //Return last login result code.
        if (this._loginResultCode && this._loginResultCode.error && this._loginResultCode.error.errorCode) {
            return this._loginResultCode.error.errorCode;
        }
        return null;
    }

    /**
     * isMandatoryToChangePassword
     * @returns {boolean}
     */
    public isMandatoryToChangePassword(): boolean {
        // Check if the login response is:
        // ModelError.ErrorCodeEnum.PasswordExpired OR ModelError.ErrorCodeEnum.PasswordNotSetFirstLogin
        // In that case, return true
        if (this._loginResultCode && this._loginResultCode.error && this._loginResultCode.error.errorCode &&
            (this._loginResultCode.error.errorCode == ModelError.ErrorCodeEnum.PasswordExpired
                || this._loginResultCode.error.errorCode == ModelError.ErrorCodeEnum.PasswordNotSetFirstLogin
            )) {
            return true;
        }
        else {
            return false;
        }
    }
}
