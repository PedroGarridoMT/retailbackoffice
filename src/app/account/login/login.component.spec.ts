import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import { BaseRequestOptions, ConnectionBackend, Http } from "@angular/http";
import { By } from "@angular/platform-browser";
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { async, fakeAsync, tick, ComponentFixture, TestBed, inject } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { Observable, Scheduler } from "rxjs";
import { LoginModel } from "mt-web-components";
import { FakeTranslateLoader } from "../../../test-util/fake-translate-loader";
import { LoginService } from "./login.service";
import { AuthenticationService } from "../shared/authentication.service";
import { DummyComponent } from "../../../test-util/dummy.component";
import { LoginComponent } from "./login.component";
import { LoginResponse, ApiResponse, ModelError, BaseResultsCode } from "../../shared/index";
import { AppRoutes } from "../../app-routes";

describe("LoginComponent", () => {
    let loginServiceSpy: jasmine.SpyObj<LoginService>;
    let authServiceSpy: jasmine.SpyObj<AuthenticationService>;
    let comp: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let de: DebugElement;

    beforeEach(async(() => {
        loginServiceSpy = jasmine.createSpyObj<LoginService>("LoginService", ["login"]);
        authServiceSpy = jasmine.createSpyObj<AuthenticationService>("AuthenticationService", ["isUserAuthenticated"]);

        TestBed.configureTestingModule({
            declarations: [LoginComponent, DummyComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: LoginService, useValue: loginServiceSpy },
                { provide: AuthenticationService, useValue: authServiceSpy },
            ],
            imports: [
                RouterTestingModule,
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;

        authServiceSpy.isUserAuthenticated.and.returnValue(false);

        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should render", () => {
        expect(de.query(By.css("mt-login"))).toBeTruthy();
    });

    it("should show loading animation on submit", () => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.of(response).delay(500));
        comp.onLoginSubmitted(credentials);
        expect(comp.loading).toBe(true);
    });

    it("should hide loading animation on failed login", async(() => {
        const errResponse: ApiResponse = {
            statusCode: 422,
            error: { errorCode: "ErrorCodeString" },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.throw(errResponse).delay(500));
        comp.onLoginSubmitted(credentials).add(() => {
            expect(comp.loading).toBe(false);
        });
    }));

    it("should hide loading animation on successful login", async(() => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.of(response).delay(500));
        comp.onLoginSubmitted(credentials).add(() => {
            expect(comp.loading).toBe(false);
        });
    }));

    it("should call LoginService.login() with provided credentials", () => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.of(response));
        comp.onLoginSubmitted(credentials);
        expect(loginServiceSpy.login).toHaveBeenCalledWith(credentials);
    });

    it("should not show error after successful login", async(() => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.of(response));
        comp.onLoginSubmitted(credentials);
        expect(comp.currentError).toBe("");
    }));

    it("should show error on login attempts exceeded", () => {
        const errResponse: ApiResponse = {
            statusCode: BaseResultsCode.Unprocessable,
            error: { errorCode: ModelError.ErrorCodeEnum.LoginAttemptsExceeded },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.throw(errResponse));
        comp.onLoginSubmitted(credentials);
        expect(comp.currentError).toBe("login.error.maximumAttemptsExceeded");
    });

    it("should show error if wrong credentials are provided", () => {
        const errResponse: ApiResponse = {
            statusCode: BaseResultsCode.Unauthorized,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        loginServiceSpy.login.and.returnValue(Observable.throw(errResponse));
        comp.onLoginSubmitted(credentials);
        expect(comp.currentError).toBe("login.error.invalidCredentials");
    });

    it("should redirect to change password page after first login", inject([Router], (router: Router) => {
        const errResponse: ApiResponse = {
            statusCode: BaseResultsCode.Unprocessable,
            error: { errorCode: ModelError.ErrorCodeEnum.PasswordNotSetFirstLogin },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        const routerNavigateSpy = spyOn(router, "navigate");
        loginServiceSpy.login.and.returnValue(Observable.throw(errResponse));
        comp.onLoginSubmitted(credentials);
        expect(routerNavigateSpy).toHaveBeenCalledWith([AppRoutes.changePassword]);
    }));

    it("should redirect to change password page after login when password is expired", inject([Router], (router: Router) => {
        const errResponse: ApiResponse = {
            statusCode: BaseResultsCode.Unprocessable,
            error: { errorCode: ModelError.ErrorCodeEnum.PasswordExpired },
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        const routerNavigateSpy = spyOn(router, "navigate");
        loginServiceSpy.login.and.returnValue(Observable.throw(errResponse));
        comp.onLoginSubmitted(credentials);
        expect(routerNavigateSpy).toHaveBeenCalledWith([AppRoutes.changePassword]);
    }));

    it("should redirect to home on load if user is authenticated", inject([Router], (router: Router) => {
        authServiceSpy.isUserAuthenticated.and.returnValue(true);
        const routerNavigateSpy = spyOn(router, "navigateByUrl");
        comp.ngOnInit();
        expect(routerNavigateSpy).toHaveBeenCalledWith(AppRoutes.home);
    }));

    it("should redirect to home page or returnUrl after successful login", inject([Router], (router: Router) => {
        const response: LoginResponse = {
            username: "username",
            permissions: [],
            expiresIn: 7200,
        };
        const credentials: LoginModel = {
            username: "username",
            password: "password",
        };
        const routerNavigateByUrlSpy = spyOn(router, "navigateByUrl");
        loginServiceSpy.login.and.returnValue(Observable.of(response));
        comp.onLoginSubmitted(credentials)
        expect(routerNavigateByUrlSpy).toHaveBeenCalledWith(AppRoutes.home);
    }));

});
