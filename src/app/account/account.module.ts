import { NgModule } from "@angular/core";
import { AppRoutingModule } from "../app-routing.module";
import { LoginComponent } from "./login/login.component";
import { ChangePasswordService } from "./change-password/change-password.service";
import { ChangePasswordResolver } from "./change-password/change-password.resolver";
import { ChangePasswordModalComponent } from "./change-password/change-password-modal.component";
import { LoginService } from "./login/login.service";
import { SharedModule } from "../shared/shared.module";
import {MonitoringModule} from "../monitoring/monitoring.module";

const MODULE_DIRECTIVES = [LoginComponent, ChangePasswordModalComponent];

@NgModule({
    imports: [
        AppRoutingModule,
        SharedModule,
        MonitoringModule
    ],
    declarations: MODULE_DIRECTIVES,
    providers: [
        LoginService,
        ChangePasswordService,
        ChangePasswordResolver
    ],
    entryComponents: [ChangePasswordModalComponent]
})
export class AccountModule {
}
