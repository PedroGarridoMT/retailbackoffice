import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from './../../core/config.service';
import { ChangePasswordService } from "./change-password.service";
import { BaseResultsCode } from "../../shared/model/base-results-code.enum";
import { ChangePasswordLiterals, ChangePasswordModel, NotificationTypeEnum } from "mt-web-components";
import { ChangePasswordRequest, RegionalEnvironment } from "../../shared/model/models";
import { ResponseHelperService } from "../../shared/response-helper.service";

@Component({
    templateUrl: "./change-password-modal.component.html"
})

export class ChangePasswordModalComponent implements OnInit, OnDestroy {
    public showCancelButton: boolean = true;
    literals: ChangePasswordLiterals;
    showRevealPassword: boolean = true;
    successMessage: string;
    warningMessage: string;
    currentError: string = '';
    loading: boolean = false;
    minPassLength: number;
    private _subscriptions: Subscription[] = [];

    constructor(private changePasswordService: ChangePasswordService,
                private config: ConfigService,
                private route: ActivatedRoute,
                private activeModal: NgbActiveModal,
                private responseHelper: ResponseHelperService) {
        this.minPassLength = config.getEnvironmentVariables().minPassWordLength;
    }

    ngOnInit(): void {
        this.changePasswordService.getLiterals().subscribe((res: ChangePasswordLiterals) => this.literals = res);
        this._subscriptions.push(
            this.route.data.subscribe((data: { loginResultCode: string }) => {
                this.warningMessage = this.changePasswordService.getWarningMessage(data.loginResultCode);
            })
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * (changePasswordSubmitted) from mt-change-password handler
     * @param changePasswordModel
     */
    onChangePasswordSubmitted(changePasswordModel: ChangePasswordModel) {
        this.loading = true;
        this.changePasswordService.changePasswordAndLogIn(<ChangePasswordRequest>{ newPassword: changePasswordModel.newPassword })
            .finally(() => {
                this.loading = false;
            })
            .subscribe(
                data => {
                    this.activeModal.close(BaseResultsCode.Ok);
                    this.responseHelper.responseHandler(BaseResultsCode.CustomCode, 'changePassword.message.success', NotificationTypeEnum.Success);
                },
                error => {
                    this.currentError = this.changePasswordService.parseChangePasswordError(error.json().errorCode);
                }
            )
        ;
    }

    public onCancelButtonClicked(event: any): void {
        this.activeModal.dismiss();
    }
}



