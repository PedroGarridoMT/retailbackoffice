﻿import { Injectable } from "@angular/core";
import { RouterStateSnapshot, CanActivate, Router, ActivatedRouteSnapshot } from "@angular/router";
import { AppRoutes } from "../../app-routes";
import { LoginService } from "../login/login.service";
import { AuthenticationService } from "../shared/authentication.service";

@Injectable()
export class ChangePasswordGuard implements CanActivate {

    constructor(private router: Router, private loginService: LoginService, private authService: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.loginService.isMandatoryToChangePassword()) {
            return true;
        }
        if (!this.authService.isUserAuthenticated()) {
            this.router.navigate([AppRoutes.login], { queryParams: { returnUrl: state.url } });
            return false;
        }
        else {
            return false;
        }
    }
}
