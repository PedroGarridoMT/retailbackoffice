﻿import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import "rxjs/add/observable/forkJoin";
import { Observable } from "rxjs/Observable";
import { LoginModel, ChangePasswordModel, ChangePasswordLiterals, ChangePasswordValidationErrors } from "mt-web-components";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "../shared/authentication.service";
import { ConfigService } from "../../core/config.service";
import { ChangePasswordRequest } from "../../shared";
import { ModelError } from "./../../shared/model/models";
import { LoginResponseExtended } from "../model/LoginResponseExtended";

@Injectable()
export class ChangePasswordService {

    constructor(private authService: AuthenticationService,
        private http: Http,
        private config: ConfigService,
        private translate: TranslateService) {
    }

    /**
     * Change password and do auto-login
     * @param changePasswordRequest
     */
    public changePasswordAndLogIn(changePasswordRequest: ChangePasswordRequest): Observable<void> {
        return this.http.put(this.config.URLs.account.changePassword, changePasswordRequest)
            .map((changePasswordResponse: Response) => {
                if (changePasswordResponse.ok) {
                    const currentUser: LoginResponseExtended = this.authService.getCurrentUser();
                    const newCredentials: LoginModel = {
                        username: currentUser.username,
                        password: changePasswordRequest.newPassword,
                    };

                    this.authService.login(newCredentials).subscribe((data) => {
                        console.log('Auto-login', data);
                    });
                }
            })
    }

    /**
     * Get translated ChangePasswordLiterals
     * @returns {Observable<ChangePasswordLiterals>}
     */
    public getLiterals(): Observable<ChangePasswordLiterals> {
        const literals: ChangePasswordLiterals = { validationErrors: {} as ChangePasswordValidationErrors } as ChangePasswordLiterals;
        return Observable.forkJoin([
            this.translate.get('changePassword.currentPassword').do((res: string) => literals.currentPassword = res),
            this.translate.get('changePassword.newPassword').do((res: string) => literals.newPassword = res),
            this.translate.get('changePassword.newPasswordRetyped').do((res: string) => literals.newPasswordRetyped = res),
            this.translate.get('changePassword.submitButton').do((res: string) => literals.submit = res),
            this.translate.get('changePassword.cancelButton').do((res: string) => literals.cancel = res),
            this.translate.get('changePassword.showCharacters').do((res: string) => literals.showCharacters = res),
            this.translate.get('changePassword.error.newPasswordMismatch').do((res: string) => literals.validationErrors.newPasswordMismatch = res),
            this.translate.get('forms.error.inputNotLongEnough').do((res: string) => literals.validationErrors.inputNotLongEnough = res),
            this.translate.get('forms.error.requiredField').do((res: string) => literals.validationErrors.requiredField = res),
            this.translate.get('forms.error.incorrectPasswordRegex').do((res: string) => literals.validationErrors.incorrectPasswordRegex = res)
        ]).map(() => literals);
    }

    /**
     * Get a warning message depending of login result
     * @param loginResultCode
     * @returns {string}
     */
    public getWarningMessage(loginResultCode: string): string {
        switch (loginResultCode) {
            case ModelError.ErrorCodeEnum.PasswordNotSetFirstLogin.toString():
                return this.translate.instant('changePassword.message.firstLoginWarning');
            case ModelError.ErrorCodeEnum.PasswordExpired.toString():
                return this.translate.instant('changePassword.message.passwordExpiredWarning');
        }
        return '';
    }

    /**
     * Get translated change password error description
     * @param error
     * @returns {string}
     */
    public parseChangePasswordError(error: string): string {
        switch (error) {
            case ModelError.ErrorCodeEnum.PasswordLength.toString():
                return this.translate.instant('changePassword.error.passwordLength');
            case ModelError.ErrorCodeEnum.PasswordWeak.toString():
                return this.translate.instant('changePassword.error.passwordWeak');
        }
        return this.translate.instant('changePassword.error.generic');
    }
}
