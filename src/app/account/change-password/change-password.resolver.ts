import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { LoginService } from "../login/login.service";

@Injectable()
export class ChangePasswordResolver implements Resolve<any> {
    constructor(private loginService: LoginService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string {
        return this.loginService.getLoginResultCode();
    }
}
