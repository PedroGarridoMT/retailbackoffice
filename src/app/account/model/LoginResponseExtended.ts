import { LoginResponse } from "../../shared/index";

/**
 * Internal Login Model (Non-swagger auto-generated)
 */
export interface LoginResponseExtended extends LoginResponse {
    /**
     *  Expiration date of the log-in session
     */
    expiresAt?: Date;
}
