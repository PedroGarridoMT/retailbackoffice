import { Http, RequestOptions, XHRBackend } from "@angular/http";
import { HttpInterceptor } from "./http-interceptor.service";

/**
 *
 * @type {{provide: Http; useFactory: ((xhrBackend:XHRBackend, requestOptions:RequestOptions)=>HttpInterceptor); deps: [XHRBackend,RequestOptions]}}
 */
export let HttpInterceptorProvider = {
    provide: Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions]
};

/**
 * DO NOT DELETE this exported function, it's required for AoT
 *
 * @param xhrBackend
 * @param requestOptions
 * @returns {HttpInterceptor}
 */
export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions) {
    return new HttpInterceptor(xhrBackend, requestOptions);
}
