import { ConnectionBackend, Headers, Http, Request, RequestOptions, RequestOptionsArgs, Response } from "@angular/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HelperService } from "../shared/helper.service";
import { AppRoutes } from "../app-routes";

@Injectable()
export class HttpInterceptor extends Http {
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);

    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        // It will add a "Credentials/Cookie" for every request except "resource"
        if ((typeof url === 'string') && url.toString().indexOf('resources') <= 0) {
            if (options) {
                options.withCredentials = true;
            }
        } else if (url instanceof Request) {
            if ((url as Request).url.indexOf('resources') <= 0) {
                (url as Request).withCredentials = true;
            }
        }
        return this.intercept(super.request(url, options));
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.get(url, options));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, options));
    }

    getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.headers.append('Content-Type', 'application/json');
        return options;
    }

    intercept(observable: Observable<Response>): Observable<Response> {
        return observable.catch((error, source) => {
            if (error.status === 401) {
                HelperService.clearLocalStorage();
                if (window.location.pathname.indexOf('login') <= 0) {
                    let returnUrl = window.location.pathname + window.location.search;
                    window.location.href = AppRoutes.login + '?returnUrl=' + encodeURIComponent(returnUrl);
                }
                return Observable.throw(error);
            } else if (error.status === 403) {
                if (window.location.pathname.indexOf('forbidden') <= 0)
                    window.location.href = AppRoutes.accessForbidden;
                return Observable.throw(error);
            } else {
                return Observable.throw(error);
            }
        });

    }
}
