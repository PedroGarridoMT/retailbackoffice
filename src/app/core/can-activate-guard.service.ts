﻿import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "../account/shared/authentication.service";
import { AppRoutes } from './../app-routes';
import {AuthenticationFirebaseService} from "../monitoring/authentication/authentication-firebase.service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthenticationService,
                private authFirebaseService: AuthenticationFirebaseService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let userAuthenticated = this.authFirebaseService.isUserAuthenticated() || this.authService.isUserAuthenticated();
        if (userAuthenticated) {
            return true;
        }
        this.router.navigate([AppRoutes.login], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
