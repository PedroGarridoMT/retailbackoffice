import { NgModule } from "@angular/core";
import { ConfigService } from "./config.service";
import { AuthGuard } from "./can-activate-guard.service";
import { AuthenticationService } from "../account/shared/authentication.service";
import { HttpInterceptorProvider } from "./http-interceptor.provider";
import { ChangePasswordGuard } from "../account/change-password/change-password.guard";
import { CanDeactivateGuard } from "./can-deactivate-guard.service";

@NgModule({
    imports: [],
    providers: [
        ConfigService,
        AuthGuard,
        ChangePasswordGuard,
        AuthenticationService,
        HttpInterceptorProvider,
        CanDeactivateGuard
    ]
})
export class CoreModule {
}
