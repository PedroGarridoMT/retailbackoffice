import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { ConfigModel } from "./config.model";
import { PlatformLocation } from "@angular/common";
import { Observable } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { Language } from "mt-web-components";
import * as moment from "moment";
import { RegionalEnvironment } from '../shared';

@Injectable()
export class ConfigService {
    private _appConfig: ConfigModel;
    private _propertiesFileUrl: string;
    private _keyForStoredLanguage = 'user-stored-lang';
    private _defaultLanguage = 'en';
    private _environmentVariables: RegionalEnvironment;
    private _selfExclusionStatuses;
    private _statusChanges;

    /**
     * API URL definition object
     * */
    public URLs = {
        // _configService will be set in ConfigService constructor. It is used as a pointer to ConfigService instance.
        _configService: undefined,

        get account() {
            return {
                session: this._configService.getEndpoint("baseUrl") + "/session",
                changePassword: this._configService.getEndpoint("baseUrl") + "/user/password",
            };
        },
        get campaign() {
            return {
                search: this._configService.getEndpoint("baseUrl") + "/campaigns/search",
                details(campaignId: string | number): string {
                    return this._configService.getEndpoint("baseUrl") + "/campaigns/" + campaignId;
                },
                detailsExport(campaignId: string | number): string {
                    return this._configService.getEndpoint("baseUrl") + "/campaigns/" + campaignId + "/export";
                },
                edit(campaignId: string | number): string {
                    return this._configService.getEndpoint("baseUrl") + "/campaigns/" + campaignId + "/edit";
                },
                gameTypes: this._configService.getEndpoint("baseUrl") + "/campaigns/creation/game-types",
                overlapCheck: this._configService.getEndpoint("baseUrl") + "/campaigns/mode-overlapping",
                creationValidatePartial: this._configService.getEndpoint("baseUrl") + "/campaigns/creation/validate-partial",
                creationSubmit: this._configService.getEndpoint("baseUrl") + "/campaigns/creation",
            };
        },
        get environment() {
            return this._configService.getEndpoint("baseUrl") + "/config/regional-environment";
        },
        get statusChanges() {
            return {
                selfExclusion: this._configService.getEndpoint("baseUrl") + "/config/self-exclusion",
                statuses: this._configService.getEndpoint("baseUrl") + "/config/status-changes"
            }
        },
        get player() {
            return {
                search: this._configService.getEndpoint("baseUrl") + "/players",
                playersReport: this._configService.getEndpoint("baseUrl") + "/players/report",
                details: this._configService.getEndpoint("baseUrl") + "/players/:accountCode",
                status: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/status",
                balance: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/wallets",
                activeAndPendingBonuses: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/bonuses",
                cancelActiveOrPendingBonus: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/bonuses/:bonusKey",
                applicableBonuses: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/applicable-bonuses",
                addBonus: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/bonuses",
                accountCodes: this._configService.getEndpoint("baseUrl") + "/players/accountCodes",
                massiveBonus: this._configService.getEndpoint("baseUrl") + "/players/massiveBonus",
                blacklisted: this._configService.getEndpoint("baseUrl") + "/players/black-listed"
            };
        },
        get bonuses(){
            return{
                active: this._configService.getEndpoint("campaignsApi") + "/bonuses/active",
            }
        },
        get transaction() {
            return {
                search: this._configService.getEndpoint("baseUrl") + "/transactions",
                transactionsReport: this._configService.getEndpoint("baseUrl") + "/transactions/report",
                playerTransactions: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/transactions",
                playerTransactionsReport: this._configService.getEndpoint("baseUrl") + "/players/:accountCode/transactions/report",
                withdrawals: this._configService.getEndpoint("baseUrl") + "/transactions/withdrawals",
                withdrawalDetails: this._configService.getEndpoint("baseUrl") + "/transactions/withdrawals/:transactionID",
                exportSepa: this._configService.getEndpoint("baseUrl") + "/transactions/sepa",
            };
        },
    };

    constructor(private http: Http,
        platformLocation: PlatformLocation,
        private translate: TranslateService) {
        this._propertiesFileUrl = `${platformLocation.getBaseHrefFromDOM()}assets/properties.json`;
        this.URLs._configService = this;
    }

    /**
     * properties.json loader
     * @returns {Promise<any>}
     */
    /*
     ** Original load of PassBackOffice
    load(): Promise<any> {
        return new Promise((resolve, reject) => {
            //Load application configuration from file
            this.http.get(this._propertiesFileUrl).flatMap(res => {
                this._appConfig = res.json();
                return this.http.get(this.URLs.environment).map((res) => res.json());
            }).subscribe((environment) => {
                //Load environment variables and store them
                this._environmentVariables = environment;

                //If there is a key "overrideEnv" in properties.json, then use values set in that object
                //Used for dev testing
                if (this._appConfig.keys["overrideEnv"]) {
                    this._environmentVariables = Object.assign(this._environmentVariables, this._appConfig.keys["overrideEnv"]);
                }

                //Load language files
                const baseLang = this.getLanguageToUse();
                const langWithCountry = baseLang + "-" + this._environmentVariables["countryCode"];

                // Set locale to moment so dates and currencies are formatted according to the current language
                moment.locale(baseLang);

                Observable
                    .forkJoin(this.translate.use(baseLang), this.translate.use(langWithCountry))
                    .subscribe(
                        results => {
                            this.translate.setDefaultLang(baseLang);
                            resolve(results);
                        },
                        translationError => {
                            console.error("Error loading translation files!!!", translationError);
                            reject(translationError);
                        });
            },
                error => {
                    console.error("Error loading configuration file!!!!!!", error);
                    reject(null);
                });
        });
    }
    */

    /*
    * Load without Pass Environment
    * */
    load(): Promise<any> {
        return new Promise((resolve, reject) => {
            //Load application configuration from file

            const environment= {
                "minPassWordLength":8,
                "currencyCode":"EUR",
                "countryCode":"ES",
                "showSecondName":false,
                "showFiscalData":true,
                "showSecurityQuestionAnswer":true,
                "showAdditionalDocumentData":false,
                "showAddressNumber":true,"zipCodeValidator":"ES"
            };

            this.http.get(this._propertiesFileUrl).subscribe(res => {
                this._appConfig = res.json();

                //Load environment variables and store them
                this._environmentVariables = environment;

                //If there is a key "overrideEnv" in properties.json, then use values set in that object
                //Used for dev testing
                if (this._appConfig.keys["overrideEnv"]) {
                    this._environmentVariables = Object.assign(this._environmentVariables, this._appConfig.keys["overrideEnv"]);
                }

                //Load language files
                const baseLang = this.getLanguageToUse();
                const langWithCountry = baseLang + "-" + this._environmentVariables["countryCode"];

                // Set locale to moment so dates and currencies are formatted according to the current language
                moment.locale(baseLang);

                Observable
                    .forkJoin(this.translate.use(baseLang), this.translate.use(langWithCountry))
                    .subscribe(
                        results => {
                            this.translate.setDefaultLang(baseLang);
                            resolve(results);
                        },
                        translationError => {
                            console.error("Error loading translation files!!!", translationError);
                            reject(translationError);
                        });
            });
        });
    }

    /**
     * get configuration Model
     * @returns {ConfigModel}
     */
    getConfiguration(): ConfigModel {
        return this._appConfig;
    }

    /**
     * get endpoint by param
     * @param endpoint
     * @returns {any}
     */
    getEndpoint(endpoint: string): string {
        return this._appConfig.endpoints[endpoint];
    }

    /**
     * get key by param
     * @param key
     * @returns {any}
     */
    getKey(key: string): any {
        return this._appConfig.keys[key];
    }

    /**
     * Get environments variables
     * @returns {any}
     */
    getEnvironmentVariables(): RegionalEnvironment {
        return this._environmentVariables;
    }

    /**
     * Get available player account status changes
     * @returns {Observable<any>}
     */
    getStatusChanges(): Observable<any> {
        if (this._statusChanges) {
            return Observable.from([this._statusChanges]);
        }

        return this.http.get(this.URLs.statusChanges.statuses).map((res) => {
            this._statusChanges = res.json();
            return this._statusChanges;
        });
    }

    /**
     * Get player statuses available for self-exclusion
     * @returns {Observable<any>}
     */
    getAllowedStatusChangesForSelfExclusion(): Observable<any> {
        if (this._selfExclusionStatuses) {
            return Observable.from([this._selfExclusionStatuses]);
        }

        return this.http.get(this.URLs.statusChanges.selfExclusion).map((res) => {
            this._selfExclusionStatuses = res.json().changes;
            return this._selfExclusionStatuses;
        });
    }

    /**
     * Translates the app according with i18n files
     * Updates moment locales
     * @param lang
     */
    setLanguageAndLocale(lang: string) {
        // Set locale to moment so dates and currencies are formatted according to the current language
        moment.locale(lang);

        //Store user language preference for future uses
        localStorage.setItem(this._keyForStoredLanguage, lang);

        //Use the language.
        this.translate.setDefaultLang(lang);
        this.translate.use(lang + "-" + this._environmentVariables["countryCode"]);
    }

    /**
     * Returns true when the passed lang is configured under the "languages" key in properties.json file
     * @param {string} lang
     * @returns {boolean}
     */
    private languageIsAvailable(lang: string): boolean {
        let applicationLangs = this.getKey("languages") as Language[];
        return lang && applicationLangs.some(item => item.value == lang);
    }

    /**
     * Get user preferences in localStorage
     * @return {null}
     */
    private getUserLanguagePreference(): string {
        let userLang = localStorage.getItem(this._keyForStoredLanguage);
        return userLang ? userLang : null;
    }

    /**
     * Returns the user base language:
     * 1º if there is a stored language from previous uses of the application, use it.
     * 2º else if the browser language is compatible with our application, use it.
     * 3º else, use default language
     * @returns {string}
     */
    private getLanguageToUse() {
        let storedLang = this.getUserLanguagePreference();
        let browserLang = this.translate.getBrowserLang();

        if (this.languageIsAvailable(storedLang)) {
            return storedLang;
        }
        else if (this.languageIsAvailable(browserLang)) {
            return browserLang;
        }
        else {
            return this._defaultLanguage;
        }
    }

}
