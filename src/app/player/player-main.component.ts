import { Component, OnInit } from "@angular/core";
import {
    HomeTemplateAction,
    HomeTemplateDashboardItem,
    HomeTemplateSavedSearch,
    PermissionsService
} from "mt-web-components";
import { TranslateService } from "@ngx-translate/core";
import { PermissionsCode } from "../shared/model/models";
import { AppRoutes } from "./../app-routes";
import { PlayerLimitChangeStatusEnum } from "./../shared/model/models";

@Component({
    selector: "pas-player-main",
    templateUrl: "./player-main.component.html",
    styleUrls: ["./player-main.component.scss"]
})
export class PlayersMainComponent implements OnInit {
    actions: HomeTemplateAction[] = [];
    searches: HomeTemplateSavedSearch[];
    dashboardItems: HomeTemplateDashboardItem[];

    constructor(private translate: TranslateService, private permissionsService: PermissionsService) {
    }

    ngOnInit(): void {
        this.translate
            .get(["players.home.search", "players.home.actionSearchDescription",
            "players.home.actionSearchLimits", "players.home.actionSearchLimitsDescription",
            "players.home.actionSearchBlacklist", "players.home.actionSearchBlacklistDescription"
            ])
            .subscribe((translations) => {
                if (this.permissionsService.hasAccess(PermissionsCode.PlayerSearch)) {
                    this.actions.push({
                        targetRoute: "/players/search",
                        text: translations["players.home.search"],
                        description: translations["players.home.actionSearchDescription"],
                        cssSelector: "players-home-search-players-selector"
                    })
                }

                if (this.permissionsService.hasAccess(PermissionsCode.PlayerAutoLimitsSearch)) {
                    this.actions.push({
                        targetRoute: AppRoutes.searchPlayerAutoLimitChanges,
                        text: translations["players.home.actionSearchLimits"],
                        queryParams: { status: PlayerLimitChangeStatusEnum[PlayerLimitChangeStatusEnum.Submitted] },
                        description: translations["players.home.actionSearchLimitsDescription"],
                        cssSelector: "players-home-search-limits-selector"
                    })
                }

                if (this.permissionsService.hasAccess(PermissionsCode.PlayerBlacklistSearch)) {
                    this.actions.push({
                        targetRoute: AppRoutes.searchBlacklistedPlayers,
                        text: translations["players.home.actionSearchBlacklist"],
                        description: translations["players.home.actionSearchBlacklistDescription"],
                        cssSelector: "players-home-search-blacklist-selector"
                    })
                }
            });
    }

    onSavedSearchClick(savedSearchId: string) {
        console.log("Click on saved search", savedSearchId);
    }
}
