import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { TitleBarParams, TitleBarService, BlacklistItem, BaseResultsCode, ApiResponse } from "../../shared";
import { AppRoutes } from "../../app-routes";
import { LoaderService } from "./../../shared/loader.service";
import { downloadFileByDataURI } from "../../shared/utils";
import { PlayerService } from "../player.service";
import { ResponseHelperService } from "../../shared/response-helper.service";
import { BaseSearchComponent } from "../../shared/base-search.component";

@Component({
    styleUrls: ['./blacklist-add.component.scss'],
    templateUrl: './blacklist-add.component.html'
})
export class PlayerBlacklistAddComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    public static PAGE_TITLE: string = "players.home.actionSearchBlacklist";

    public inputFileName: string = "";
    public errorMessage: string = "";
    public successMessage: string = "";
    public uploadFileOk: boolean = false;

    private blacklist: BlacklistItem[] = [];

    private titleBarParams: TitleBarParams = {
        previousPagePath: "",
        previousPageLabel: "",
        pageTitle: PlayerBlacklistAddComponent.PAGE_TITLE,
        pageActions: []
    };

    constructor(private titleBarService: TitleBarService,
                route: ActivatedRoute,
                private router: Router,
                private loader: LoaderService,
                private playerService: PlayerService,
                private translate: TranslateService,
                private responseHelper: ResponseHelperService)
    {
        super(route);
    }

    public ngOnInit(): void {
        this.titleBarService.componentInitialized(this.titleBarParams);
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this.titleBarService.componentDestroyed();
    }

    public onCancel(event: any): void {
        this.goToSearchBlacklist();
    }

    public onSave(event: any): void {
        this.loader.show();
        this.playerService
            .addBlacklistedPersons(this.blacklist)
            .finally(()=> this.loader.hide())
            .subscribe(
                (responseCode: BaseResultsCode) => {
                    if(responseCode === BaseResultsCode.Ok) {
                        this.goToSearchBlacklist();
                    }
                    this.responseHelper.responseHandler(responseCode, "players.blacklistAdd.response");
                },
                (error: ApiResponse) => {
                    this.responseHelper.responseHandler(error.statusCode, "players.blacklistAdd.response");
                }
            )
    }

    public onDownloadDocument(): void {
        downloadFileByDataURI("assets/samples/blacklist_example.csv", "blacklist_example.csv");
    }

    public onFileRemoveClick(): void {
        this.clearUpload();
    }

    public onUploadFileChange(event: any): void {

        this.clearUpload();

        if(!event || !event.target) return;

        const fileList: FileList = event.target.files;
        if (fileList && fileList.length > 0) {

            const reader: FileReader = new FileReader();
            const inputFile: File = fileList[0];

            if (inputFile) {
                this.inputFileName = inputFile.name;

                if(/.*\.csv$/.test(inputFile.name)) {
                    this.loader.show();
                    reader.readAsBinaryString(inputFile);
                } else {
                    this.errorMessage = this.translate.instant("players.blacklistAdd.invalidFiletype");
                }
            }

            reader.onloadend = () => {
                const result = this.parseAndValidateUploadedFile(reader.result);

                if(result.valid) {
                    this.successMessage = this.translate.instant("players.blacklistAdd.validCSVfile");
                    this.blacklist = result.blacklist;
                    this.uploadFileOk = true;
                } else {
                    this.errorMessage = this.translate.instant("players.blacklistAdd.invalidCSVformat");
                }

                this.loader.hide();
            };
        }
    }

    private clearUpload(): void {
        this.successMessage = "";
        this.errorMessage = "";
        this.inputFileName = "";
        this.uploadFileOk = false;
        this.blacklist = [];
    }

    private goToSearchBlacklist(): void {
        this.router.navigate([AppRoutes.searchBlacklistedPlayers], {queryParams: this.route.snapshot.queryParams});
    }

    private parseAndValidateUploadedFile(result: string): { valid: boolean, blacklist: BlacklistItem[] } {
        // Get rows
        const csvRows: string[] = result.split('\n');
        // Remove last empty row if exists
        const rows: any[] = !!csvRows[csvRows.length - 1] ? csvRows : csvRows.slice(0, -1);

        const blacklist: BlacklistItem[] = rows.map(row => {
            // Get columns and filter empty ones
            const columns = row.split(';').map(col => col.trim()).filter(col => !!col);

            // CSV must have 3 columns
            if(columns.length === 3) {
                const [name, documentType, documentNumber] = columns;
                return { name, documentType, documentNumber }
            } else {
                return null;
            }

        }).filter(blacklistItem => !!blacklistItem); // Remove those that didn't have 3 columns

        return {
            valid: rows.length === blacklist.length && blacklist.length > 0,
            blacklist
        }
    }

}
