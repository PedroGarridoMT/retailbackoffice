import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    ColumnVisibilityEvent,
    DateInputModel,
    FilterToValueMap,
    FilterType,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals,
    PermissionsService
} from "mt-web-components";
import { PlayerService } from "../player.service";
import { TranslateService } from "@ngx-translate/core";
import { TitleBarParams, TitleBarService } from "../../shared/index";
import { getDefaultDateTimeFormat, getISOString, setTimeInfo } from "../../shared/date-utils";
import { ActivatedRoute, Router } from "@angular/router";
import { PASEnumsService } from "../../shared/pas-enums.service";
import { LoaderService } from "../../shared/loader.service";
import {
    PlayerFilterCriteria,
    PlayerSearchRequest,
    PlayerSummary,
    PlayerSummaryExtended,
    DownloadableFile
} from "../model/models";
import { CurrencyService } from "../../shared/currency.service";
import { Observable } from "rxjs/Observable";
import { PermissionsCode } from "../../shared/model/permissions-code.model";
import { BaseSearchComponent } from "../../shared/base-search.component";
import { AppRoutes } from "../../app-routes";
import { getMimeTypeByExtension, downloadFileByDataURI } from "../../shared/utils";
import { PASConstants } from "../../shared/constant";
import { ResponseHelperService } from "../../shared/response-helper.service";

@Component({
    selector: 'pas-player-search',
    templateUrl: './player-search.component.html',
    styleUrls: ['./player-search.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PlayersSearchComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    public static PAGE_TITLE: string = "players.home.search";

    //Title bar attributes
    titleBarParams: TitleBarParams = {
        previousPagePath: 'players',
        previousPageLabel: 'players.home.title',
        pageTitle: PlayersSearchComponent.PAGE_TITLE,
        pageActions: null
    };

    playerDetailLinkParams: Object = { [PlayerService.PREVIOUS_ROUTE_KEY]: PlayersSearchComponent.PAGE_TITLE };

    //UI attributes
    loading: boolean = false;
    firstSearchDone: boolean = false;
    errorInSearch: boolean = false;
    previousUrl: string = null;

    // mt-table configuration
    table_columns: TableColumn[];
    table_rows: PlayerSummary[];
    tableLiterals: TableLiterals;
    showPlayerDetailLink: boolean = false;
    @ViewChild('playerDetailLinkTemplate') playerDetailLinkTemplate;

    // mt-basic-filters configuration
    filterBarLiterals: BasicFilterBarLiterals;
    filters: any[];
    countryList: any[];
    accountStatusList: any[];
    accountStatusReasonList: any[];
    documentTypeList: any[];
    genderList: any[];
    @ViewChild('filtersBar') filtersBar: BasicFilterBarComponent;

    constructor(private playerService: PlayerService,
        private translate: TranslateService,
        private titleBarService: TitleBarService,
        private enumsService: PASEnumsService,
        private router: Router,
        private currencyService: CurrencyService,
        private loader: LoaderService,
        private permissions: PermissionsService,
        route: ActivatedRoute,
        private responseHelper: ResponseHelperService, ) {
        super(route);
    }

    ngOnInit(): void {
        this.init();

        //Notify and pass params to the title bar service, so it can notify subscribers
        this.titleBarService.componentInitialized(this.titleBarParams)
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        // prevent memory leak when component destroyed
        this.titleBarService.componentDestroyed();
    }

    getPlayerDetailLinkParams(accountCode: string): object {
        const urlParams: object = this.playerService.getLastQueryParams(PlayersSearchComponent.PAGE_TITLE);
        const currentIndex: number = (this.pagingData.currentPage - 1) * this.pagingData.pageSize + this.table_rows.map(p => p.accountCode).indexOf(accountCode);

        const searchReq: object = Object.assign({}, urlParams, {
            totalItems: this.pagingData.totalItems,
            currentIndex
        });
        const linkParams = Object.assign({}, this.playerDetailLinkParams);
        linkParams[PASConstants.SEARCH_REQUEST_PARAM] = btoa(JSON.stringify(searchReq));

        return linkParams;
    }

    onColumnVisibilityChange(columnChanged: ColumnVisibilityEvent) {
        let colIndex = this.table_columns.findIndex(item => item.id == columnChanged.columnId);
        this.table_columns[colIndex].visibility = columnChanged.value ?
            TableColumnVisibilityEnum.VISIBLE : TableColumnVisibilityEnum.HIDDEN;
    }

    onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    onClearBasicFilter() {
        this.valuesToSearch = null;
        //Clean previous search results
        this.table_rows = [];
        this.resetPagingParams();
        //Reset UI
        this.errorInSearch = false;
        this.firstSearchDone = false;
        //Clear URL
        this.router.navigate([AppRoutes.playersSearch]);
    }

    onPlayerDetailClick(accountCode: string) {
        this.router.navigate(['players', accountCode]);
    }

    onExportClick(columnNames: string[]) {
        const columns: string[] = columnNames.map(c => c.replace("String", "").replace("Link", ""));
        const mimeType = getMimeTypeByExtension("csv");
        this.loader.show();
        this.loading = true;
        const filterCriteria: PlayerFilterCriteria = this.createPlayerFilterCriteria(this.valuesToSearch);
        this.playerService.getPlayersReport(filterCriteria, columns)
            .finally(() => {
                this.loader.hide();
                this.loading = false;
            })
            .subscribe((file: DownloadableFile) => {
                downloadFileByDataURI("data:" + mimeType + ";base64," + file.content, file.fileName);
            }, (error) => {
                this.responseHelper.responseHandler(error.statusCode, "common.search.error.exportErrorMessage");
            });
    }

    private init() {
        this.enumsService.getCountriesTranslatedList().subscribe(countries => {
            this.countryList = countries;
        });

        this.enumsService.getAccountStatusTranslatedList().subscribe(accountStatusList => {
            this.accountStatusList = accountStatusList
        });

        this.enumsService.getAccountStatusReasonTranslatedList().subscribe(accountStatusReasons => {
            this.accountStatusReasonList = accountStatusReasons;
        });

        this.enumsService.getGendersTranslatedList().subscribe(genders => {
            this.genderList = genders;
        });

        this.enumsService.getDocumentTypesTranslatedList().subscribe(documentTypes => {
            this.documentTypeList = documentTypes;
        });

        this.showPlayerDetailLink = this.permissions.hasAccess(PermissionsCode.PlayerDetailData);

        this.generateTable();
        this.generateFilterForm();
        this.manageUrlParams();
    }

    /**
     * If the URL contains a search, prepare the necessary objects to compose the search and then, launch it
     */
    private manageUrlParams() {
        if (this.thereAreQueryParams()) {
            this.prepareDataWithQueryParams();
            this.searchData();
        }
    }

    /**
     * Search Player Request
     */
    private searchData() {
        this.errorInSearch = false;
        this.loader.show();
        this.loading = true;
        let playerSearchRequest: PlayerSearchRequest = {
            filterCriteria: this.createPlayerFilterCriteria(this.valuesToSearch),
            pageNumber: this.pagingData.currentPage,
            pageSize: this.pagingData.pageSize
        };

        this.playerService
            .getPlayers(playerSearchRequest)
            .finally(() => {
                this.loader.hide();
                this.loading = false;
            })
            .subscribe((playerResponse) => {
                this.getPlayersWithExtendedInfo(playerResponse.items)
                    .finally(() => this.firstSearchDone = true)
                    .subscribe(playersExtended => {
                        this.table_rows = playersExtended;
                    });
                this.pagingData = {
                    currentPage: this.pagingData.currentPage,
                    pageSize: this.pagingData.pageSize,
                    totalPages: playerResponse.totalPages,
                    totalItems: playerResponse.totalItems
                };
            }, (error) => {
                this.errorInSearch = true;
                this.resetPagingParams();
            });

        //Flatten search request object.
        let urlParams = JSON.parse(JSON.stringify(playerSearchRequest.filterCriteria));
        urlParams["pageNumber"] = this.pagingData.currentPage;
        urlParams["pageSize"] = this.pagingData.pageSize;

        //Store last search
        this.playerService.setLastQueryParams(PlayersSearchComponent.PAGE_TITLE, urlParams);

        //Update URL with the new search criteria
        this.router.navigate([AppRoutes.playersSearch], { queryParams: urlParams });
    }

    /**
     * Mainly used for build request formatting dates
     * @param playerFilterMT
     * @return {PlayerFilterCriteria}
     */
    private createPlayerFilterCriteria(playerFilterMT: any): PlayerFilterCriteria {
        let playerFilterCriteria: PlayerFilterCriteria = playerFilterMT;
        if (playerFilterMT.accountStatus) {
            if (!Array.isArray(playerFilterMT.accountStatus)) {
                playerFilterCriteria.accountStatus = [playerFilterMT.accountStatus]
            }
        }
        if (playerFilterMT.accountStatusReason) {
            if (!Array.isArray(playerFilterMT.accountStatusReason)) {
                playerFilterCriteria.accountStatusReason = [playerFilterMT.accountStatusReason]
            }
        }
        if (playerFilterMT.creationDate) {
            let creationDateFrom: DateInputModel = setTimeInfo(playerFilterMT.creationDate.from);
            playerFilterCriteria.creationDateFrom = creationDateFrom ? getISOString(creationDateFrom) : null;

            let creationDateTo: DateInputModel = setTimeInfo(playerFilterMT.creationDate.to, 23, 59, 59);
            playerFilterCriteria.creationDateTo = creationDateTo ? getISOString(creationDateTo) : null;

            delete playerFilterCriteria['creationDate'];
        }

        if (playerFilterMT.birthDate) {
            let birthDateFrom: DateInputModel = setTimeInfo(playerFilterMT.birthDate.from);
            playerFilterCriteria.birthDateFrom = birthDateFrom ? getISOString(birthDateFrom) : null;

            let birthDateTo = setTimeInfo(playerFilterMT.birthDate.to, 23, 59, 59);
            playerFilterCriteria.birthDateTo = birthDateTo ? getISOString(birthDateTo) : null;

            delete playerFilterCriteria['birthDate'];
        }

        if (playerFilterMT.pageNumber)
            delete playerFilterCriteria["pageNumber"];

        if (playerFilterMT.pageSize)
            delete playerFilterCriteria["pageSize"];

        return playerFilterCriteria;
    }

    /**
     * It adds fullName and fullSurname, get translation for enumerations, and gets the balance for each player.
     * @param players. Array of PlayerSummaryExtended.
     * (Filled objects are PlayerSummary, but we cast it to the extended version
     * to have the extra fields to be displayed on the table).
     *
     * @return Observable <PlayerSummaryExtended[]>. Returns objects ready to be displayed on tables.
     */
    private getPlayersWithExtendedInfo(players: PlayerSummaryExtended[]): Observable<Array<PlayerSummaryExtended>> {
        if (players && players.length > 0) {
            this.loader.show();
            let playerBalanceObservables = [];
            players.forEach((player: PlayerSummaryExtended) => {
                player.fullName = (player.name1 ? player.name1 + ' ' : '') + (player.name2 ? player.name2 : '');
                player.fullSurname = (player.surname1 ? player.surname1 + ' ' : '') + (player.surname2 ? player.surname2 : '');
                player.usernameLink = this.playerDetailLinkTemplate;
                player.creationDate = player.creationDate ? getDefaultDateTimeFormat(player.creationDate) : null;
                player.accountStatus = player.accountStatus ? this.enumsService.translateEnumValue('accountStatus', player.accountStatus) : '';
                player.accountStatusReason = player.accountStatusReason ? this.enumsService.translateEnumValue('accountStatusReason', player.accountStatusReason) : '';
                player.receivePromotionsString = this.translateBoolean(player.receivePromotions);
                player.verifiedIdentityString = this.translateBoolean(player.verifiedIdentity);
                player.selfExcludedString = this.translateBoolean(player.selfExcluded);

                playerBalanceObservables.push(this.getPlayerBalance(player.accountCode));
            });

            return Observable
                .forkJoin(playerBalanceObservables)
                .finally(() => this.loader.hide())
                .map(balances => {
                    players.forEach((item, index) => {
                        players[index].balanceString = balances[index].toString();
                    });
                    return players;
                });
        }
        else {
            //No players => empty table rows.
            return Observable.of([]);
        }
    }

    /**
     * getPlayerBalance
     * @param accountCode
     * @return Observable{string}
     */
    private getPlayerBalance(accountCode: string): Observable<string> {
        return this.playerService.getPlayerWallets(accountCode).map(
            wallets => {
                let totalAmount: any = 0;
                if (wallets && wallets.length > 0) {
                    totalAmount = wallets.reduce((a, b) => {
                        return a + b.balance;
                    }, 0);
                }
                return this.currencyService.formatAmountWithCurrency(totalAmount);
            })
            .catch((error) => {
                console.log('getPlayerBalance catch error!: ', error);
                return Observable.from(['-']);
            });
    }

    private translateBoolean(value: boolean): string {
        return value ? this.translate.instant('common.yes') : this.translate.instant('common.no');
    }

    /**
     * Generates filter form component: mt-basic-filters
     */
    private generateFilterForm() {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "username",
                cssClass: "usernameFilter_selector",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.username"),
                shownByDefault: true,
                value: this.getValueFromParams("username")
            },
            {
                id: "accountCode",
                cssClass: "accountCodeFilter_selector",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.accountCode"),
                shownByDefault: true,
                value: this.getValueFromParams("accountCode")
            },
            {
                id: "accountStatus",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.accountStatus"),
                shownByDefault: true,
                options: this.accountStatusList,
                value: this.getMultipleSelectionFromParams("accountStatus")
            },
            {
                id: "accountStatusReason",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.accountStatusReason"),
                shownByDefault: false,
                options: this.accountStatusReasonList,
                value: this.getMultipleSelectionFromParams("accountStatusReason"),
                cssClass: "largePanel"
            },
            {
                id: "email",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.email"),
                shownByDefault: false,
                value: this.getValueFromParams("email")
            },
            {
                id: "playerId",
                type: FilterType.NUMBER,
                label: this.translate.instant("players.entity.playerId"),
                shownByDefault: false,
                value: this.getValueFromParams("playerId")
            },
            {
                id: "name1",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.name1"),
                shownByDefault: false,
                value: this.getValueFromParams("name1")
            },
            {
                id: "name2",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.name2"),
                shownByDefault: false,
                value: this.getValueFromParams("name2")
            },
            {
                id: "surname1",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.surname1"),
                shownByDefault: false,
                value: this.getValueFromParams("surname1")
            },
            {
                id: "surname2",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.surname2"),
                shownByDefault: false,
                value: this.getValueFromParams("surname2")
            },
            {
                id: "creationDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("players.entity.creationDate"),
                shownByDefault: false,
                value: this.getDateRangeFromParams("creationDate"),
                maxDate: new Date()
            },
            {
                id: "documentType",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.iDType"),
                shownByDefault: false,
                options: this.documentTypeList,
                value: this.getValueFromParams("documentType")
            },
            {
                id: "documentNumber",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.iDNumber"),
                shownByDefault: false,
                value: this.getValueFromParams("documentNumber")
            },
            {
                id: "birthDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("players.entity.birthDate"),
                shownByDefault: false,
                value: this.getDateRangeFromParams("birthDate"),
                maxDate: new Date()
            },
            {
                id: "gender",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.gender"),
                shownByDefault: false,
                options: this.genderList,
                value: this.getValueFromParams("gender")
            },
            {
                id: "nationality",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.nationality"),
                shownByDefault: false,
                options: this.countryList,
                value: this.getValueFromParams("nationality")
            },
            {
                id: "country",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.country"),
                shownByDefault: false,
                options: this.countryList,
                value: this.getValueFromParams("country")
            },
            {
                id: "phoneNumber",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.phoneNumber1"),
                shownByDefault: false,
                value: this.getValueFromParams("phoneNumber")
            },
            {
                id: "verifiedIdentity",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.verifiedIdentity"),
                shownByDefault: false,
                options: [
                    { value: "true", label: this.translate.instant("common.yes") },
                    { value: "false", label: this.translate.instant("common.no") }
                ],
                value: this.getValueFromParams("verifiedIdentity")
            },
            {
                id: "selfExcluded",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.selfExcluded"),
                shownByDefault: false,
                options: [
                    { value: "true", label: this.translate.instant("common.yes") },
                    { value: "false", label: this.translate.instant("common.no") }
                ],
                value: this.getValueFromParams("selfExcluded")
            },
            {
                id: "provenanceCode",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.provenanceCode"),
                shownByDefault: false,
                value: this.getValueFromParams("provenanceCode")
            },
            {
                id: "promotionalCode",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.promotionalCode"),
                shownByDefault: false,
                value: this.getValueFromParams("promotionalCode")
            },
            {
                id: "receivePromotions",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.entity.receivePromotions"),
                shownByDefault: false,
                options: [
                    { value: "true", label: this.translate.instant("common.yes") },
                    { value: "false", label: this.translate.instant("common.no") }
                ],
                value: this.getValueFromParams("receivePromotions")
            },
            {
                id: "referrer",
                type: FilterType.TEXT,
                label: this.translate.instant("players.entity.referrer"),
                shownByDefault: false,
                value: this.getValueFromParams("referrer")
            },
        ];
    }

    /**
     * Generates the table component with player columns: mt-table
     */
    private generateTable() {
        this.table_columns = [
            {
                type: TableColumnType.HTML,
                id: "usernameLink",
                label: this.translate.instant("players.entity.username"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "accountCode",
                label: this.translate.instant("players.entity.accountCode"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "fullName",
                label: this.translate.instant("players.entity.fullName"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "fullSurname",
                label: this.translate.instant("players.entity.fullSurname"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "creationDate",
                label: this.translate.instant("players.entity.creationDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "accountStatus",
                label: this.translate.instant("players.entity.accountStatus"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "balanceString",
                label: this.translate.instant("players.entity.balance"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "playerId",
                label: this.translate.instant("players.entity.playerId"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "accountStatusReason",
                label: this.translate.instant("players.entity.accountStatusReason"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "verifiedIdentityString",
                label: this.translate.instant("players.entity.verifiedIdentity"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "selfExcludedString",
                label: this.translate.instant("players.entity.selfExcluded"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "receivePromotionsString",
                label: this.translate.instant("players.entity.receivePromotions"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "provenanceCode",
                label: this.translate.instant("players.entity.provenanceCode"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "promotionalCode",
                label: this.translate.instant("players.entity.promotionalCode"),
                visibility: TableColumnVisibilityEnum.HIDDEN,
                sortable: false
            }
        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('table.noResultsMessage'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        };
    }

}
