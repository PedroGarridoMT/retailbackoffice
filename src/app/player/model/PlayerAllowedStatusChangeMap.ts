import { AccountStatusEnum } from "./models";

/**
 * Interface to map allowed status changes.  Source status -> Array of allowed target status
 * Note: The source status must be one of the elements of AccountStatusEnum, but Typescript doesn't allow to set an enum
 *       as a index signature parameter.
 */
export interface PlayerAllowedStatusChangeMap {
    [sourceStatus: string]: Array<AccountStatusEnum>;
}
