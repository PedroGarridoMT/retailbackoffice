import { PlayerSummary } from "./models";

/**
 * Internal Player entity (Non-swagger auto-generated)
 */

export interface PlayerSummaryExtended extends PlayerSummary {
    /**
     * fullName: First name + Second name.
     */
    fullName?: string;

    /**
     * fullSurname: First surname + second surname.
     */
    fullSurname?: string;

    /**
     * Internal field for storing link to player details.
     */
    usernameLink?: any;

    /**
     * receivePromotions translated as Yes/No
     */
    receivePromotionsString?: string;

    /**
     * verifiedIdentity translated as Yes/No
     */
    verifiedIdentityString?: string;

    /**
     * selfExcluded translated as Yes/No
     */
    selfExcludedString?: string;

    /**
     * balance as a formatted string
     */
    balanceString?: string;
}
