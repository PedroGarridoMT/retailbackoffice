/**
 * Internal Enum (Non-swagger auto-generated)
 */

export enum LimitActionOperationEnum {
    Accept = <any> 'Accept',
    Reject = <any> 'Reject',
}
