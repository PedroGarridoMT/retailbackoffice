/**
 * Internal Player entity (Non-swagger auto-generated)
 */
import * as models from './models';

export interface CurrentLimitExtended {

    /**
     * Type of limit
     */
    limitType?: models.LimitTypeEnum;

    limitTypeTranslated?: string;

    /**
     * Player daily limit
     */
    playerDailyLimit?: string;

    /**
     * Player weekly limit
     */
    playerWeeklyLimit?: string;

    /**
     * Player monthly limit
     */
    playerMonthlyLimit?: string;

    /**
     *Operator daily limit
     */
    operatorDailyLimit?: string;

    /**
     * Operator Weekly limit
     */
    operatorWeeklyLimit?: string;

    /**
     * Operator monthly limit
     */
    operatorMonthlyLimit?: string;

    /*
     * Operator limit Id
     */
    operatorLimitId?:number;

    /**
     * Daily, weekly and monthly values for this limit
     */
    operatorLimitValues?: models.LimitValues;
}
