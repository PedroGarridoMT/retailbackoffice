import { PlayerDetailsResponse } from "./models";

export interface PlayerDetail extends PlayerDetailsResponse {
    /**
     * Code of the region of expedition / issuing.
     */
    issueRegionCode?: string;

    /**
     * Code of the region of birth.
     */
    birthRegionCode?: string;
}
