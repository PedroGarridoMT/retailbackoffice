/**
 * Internal Player entity (Non-swagger auto-generated)
 */
import { LimitActionOperationEnum, PlayerLimitChange } from "./models";


export interface PlayerLimitChangeExtended extends PlayerLimitChange {

    /**
     * Amount of daily limit.
     */
    newDailyLimit?: string;

    /**
     * Amount of weekly limit.
     */
    newWeeklyLimit?: string;

    /**
     * Amount of monthly limit.
     */
    newMonthlyLimit?: string;

    /**
     * Amount of daily limit.
     */
    currentDailyLimit?: string;

    /**
     * Amount of weekly limit.
     */
    currentWeeklyLimit?: string;

    /**
     * Amount of monthly limit.
     */
    currentMonthlyLimit?: string;

    /**
     * Type of limit (Bet, Deposit, Loss)
     */
    translatedLimitType?: string;

    /**
     * Translated status
     */
    translatedStatus?: string;

    /**
     * daily Template to be rendered in table
     */
    dailyLimitTemplate?: any;

    /**
     * monthly Template to be rendered in table
     */
    monthlyLimitTemplate?: any;

    /**
     * weekly Template to be rendered in table
     */
    weeklyLimitTemplate?: any;

    /**
     * template to render a link to the player detail page
     */
    playerLinkTemplate?: any;
    /**
     * Operation
     */
    operation?: LimitActionOperationEnum;

    /**
     * Notes String
     */
    notesString?: string

}
