import { ViewChild } from '@angular/core';
import { PlayerBonus } from "./models";

/**
 * Internal Bonus Entity (Non-swagger auto-generated)
 */

export interface PlayerBonusExtended extends PlayerBonus {
    /**
     * Bonus status translated
     */
    translatedStatus: string;
    /**
     * Initial bonus amount with currency
     */
    formattedInitialBonusAmountAwarded: string;
    /**
     * Bonus balance with currency
     */
    formattedBonusBalance: string;
    /**
     * Full date/time format of date of awarding
     */
    formattedDateOfAwarding: string;
    /**
     * Full date/time format of date of activation
     */
    formattedDateOfActivation: string;
    /**
     * Full date/time format of date of closing
     */
    formattedDateOfClosing: string;
    /**
     * Currently achieved amount to rollover with currency
     */
    formattedMoneyRedemptionCurrentValue: string;
    /**
     * Target money redemption amount with currency
     */
    formattedMoneyRedemptionTargetValue: string;
    /**
     * Money redemption rollover percent
     */
    formattedMoneyRedemptionRolloverPercent: string;
    /**
     * Currently achieved number of bets to rollover
     */
    formattedNumberOfBetsRedemptionCurrentValue: string;
    /**
     * Formatted number of bets required for rollover.
     */
    formattedNumberOfBetsRedemptionTargetValue: string;
    /**
     * Bets redemption rollover percent
     */
    formattedBetsRedemptionRolloverPercent: string;
}
