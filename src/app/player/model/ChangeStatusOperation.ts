/**
 * Internal Change Status operation entity (Non-swagger auto-generated)
 */
import * as models from './models';
import { AccountStatusChangeReasonEnum } from '../../transaction/model/models';
import { AccountStatusEnum } from './models';

export interface ChangeStatusOperation {

    /**
     * Account code for selected player
     */
    accountCode: string;

    /**
     * Selected status
     */
    selectedStatus: AccountStatusEnum;

    /**
     * Selected reason for changing status
     */
    selectedChangeReason?: AccountStatusChangeReasonEnum;

    /**
     * If self-exclusion is selected
     */
    selfExclusion?: boolean;

    /**
     * End date for self-exclusion
     */
    selfExclusionEndDate?: Date;
}
