import { Documentation } from "./models";

/**
 * Internal Player entity (Non-swagger auto-generated)
 */

export interface DocumentationExtended extends Documentation {

    /**
     * Player account code related with the documentation.
     */
    accountCode: string;
}
