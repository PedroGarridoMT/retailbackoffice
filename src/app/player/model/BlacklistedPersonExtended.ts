import { BlacklistedPerson } from "./models";

/**
 * Internal Blacklisted Person Entity (Non-swagger auto-generated)
 */

export interface BlacklistedPersonExtended extends BlacklistedPerson {
    /**
     * Full date/time format of import date
     */
    formattedImportDate: string;
    /**
     * Translated document type
     */
    translatedDocumentType: string;
}
