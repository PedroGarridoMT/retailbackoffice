/**
 * Internal Player entity (Non-swagger auto-generated)
 */
import { LimitHistory } from "../../shared/model/models";
import { LimitActionOperationEnum } from "./LimitActionOperationEnum";

export interface LimitHistoryExtended extends LimitHistory {
    /**
     * Amount of daily limit.
     */
    dailyLimit?: string;

    /**
     * Amount of weekly limit.
     */
    weeklyLimit?: string;

    /**
     * Amount of monthly limit.
     */
    monthlyLimit?: string;

    /**
     * Translated status
     */
    statusTranslated?: string;

    /**
     * Type of limit (translated)
     */
    typeTranslated?: string;

    /**
     * Notes formatted
     */
    notesString?: string;

    /**
     * Operation (Accept or Reject)
     */
    operation?: LimitActionOperationEnum;
}
