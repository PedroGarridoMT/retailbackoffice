import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    FilterToValueMap,
    TableColumn,
    TableLiterals,
    FilterType,
    TableColumnType,
    TableColumnVisibilityEnum,
    PermissionsService
} from "mt-web-components";
import { TitleBarParams, TitleBarService, BlacklistedPerson, PASEnumsService, ListItem, PermissionsCode } from "../../shared";
import { getISOString, getDefaultDateTimeFormat } from "../../shared/date-utils";
import { AppRoutes } from "../../app-routes";
import { PlayerService } from "../player.service";
import { BaseSearchComponent } from "../../shared/base-search.component";
import { LoaderService } from "../../shared/loader.service";
import { BlacklistedPersonExtended } from "../model/BlacklistedPersonExtended";

@Component({
    templateUrl: "./blacklist-search.component.html"
})
export class PlayerBlacklistSearchComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    public static PAGE_TITLE: string = "players.home.actionSearchBlacklist";

    // UI attributes
    public firstSearchDone: boolean = false;
    public errorInSearch: boolean = false;
    public errorInExport: boolean = false;
    public filtersLoaded: boolean = false;

    // mt-basic-filters configuration
    public filterBarLiterals: BasicFilterBarLiterals;
    public filters: any[];
    @ViewChild("filtersBar") public filtersBar: BasicFilterBarComponent;

    // mt-table configuration
    public tableRows: BlacklistedPerson[];
    public tableColumns: TableColumn[];
    public tableLiterals: TableLiterals;

    // mt-basic-filters configuration
    private documentTypesList: ListItem[];

    //Title bar attributes
    private titleBarParams: TitleBarParams = {
        previousPagePath: "players",
        previousPageLabel: "players.home.title",
        pageTitle: PlayerBlacklistSearchComponent.PAGE_TITLE,
        pageActions: []
    };

    constructor(private translate: TranslateService,
        private router: Router,
        private titleBarService: TitleBarService,
        private enumsService: PASEnumsService,
        private loader: LoaderService,
        private playerService: PlayerService,
        private permissionService: PermissionsService,
        route: ActivatedRoute) {
        super(route);
    }

    public ngOnInit(): void {
        this.init();

        if(this.permissionService.hasAccess(PermissionsCode.PlayerBlacklistAdd)) {
            this.titleBarParams.pageActions.push({
                id: "add",
                icon: "fa-plus",
                label: this.translate.instant("players.blacklist.addToBlacklist")
            });
        }

        this.titleBarService.componentInitialized(this.titleBarParams);

        // Subscribe to action requests that happens on the title bar
        this._subscriptions.push(
            this.titleBarService.actionRequested$.subscribe(actionId => {
                this.handleTitleBarAction(actionId);
            })
        );
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this.titleBarService.componentDestroyed();
    }

    public onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    public onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    public onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    public onClearBasicFilter() {
        this.valuesToSearch = null;
        //Clean previous search results
        this.tableRows = [];
        this.resetPagingParams();
        //Reset UI
        this.errorInSearch = false;
        this.firstSearchDone = false;
        //Clear URL
        this.router.navigate([AppRoutes.searchBlacklistedPlayers]);
    }

    private init(): void {
        this.enumsService.getDocumentTypesTranslatedList().subscribe((documentTypesTranslated) => {
            this.documentTypesList = documentTypesTranslated;
        });

        this.generateTable();
        this.generateFilterForm();
        this.manageUrlParams();
        this.filtersLoaded = true;
    }

    private searchData(): void {
        this.errorInSearch = false;
        this.loader.show();

        let filterCriteria = this.createtBlacklistedPlayersFilterCriteria(this.valuesToSearch);

        this.playerService.getBlacklistedPlayers(filterCriteria)
            .finally(() => this.loader.hide())
            .subscribe((blacklistResponse) => {
                this.pagingData = {
                    currentPage: this.pagingData.currentPage,
                    pageSize: this.pagingData.pageSize,
                    totalPages: blacklistResponse.totalPages,
                    totalItems: blacklistResponse.totalItems
                };
                this.tableRows = this.updateBlacklistInfo(blacklistResponse.items);
                this.firstSearchDone = true;
            }, error => {
                this.errorInSearch = true;
                this.resetPagingParams();
                console.error(error);
            });

        //Update URL with the new search criteria
        this.router.navigate([AppRoutes.searchBlacklistedPlayers], { queryParams: filterCriteria });
    }

    /**
     * @return {Object}
     * @param blacklistedPlayersFilter
     */
    private createtBlacklistedPlayersFilterCriteria(blacklistedPlayersFilter: any): any {
        let blacklistedPlayersFilterCriteria: any = blacklistedPlayersFilter;

        if(!blacklistedPlayersFilter) return;

        if (blacklistedPlayersFilter.creationDate) {
            let creationDateFrom = blacklistedPlayersFilter.creationDate.from;
            blacklistedPlayersFilterCriteria.dateFrom = creationDateFrom ? getISOString(creationDateFrom) : null;

            let creationDateTo = blacklistedPlayersFilter.creationDate.to;
            blacklistedPlayersFilterCriteria.dateTo = creationDateTo ? getISOString(creationDateTo) : null;

            delete blacklistedPlayersFilterCriteria["creationDate"];
        }

        return Object.assign({}, blacklistedPlayersFilterCriteria, this.getPaginationData());
    }

    /**
     * If the URL contains a search, prepare the necessary objects to compose the search and then, launch it
     */
    private manageUrlParams(): void {
        if (this.thereAreQueryParams()) {
            this.prepareDataWithQueryParams();
            this.searchData();
        }
    }

    /**
     * Formatting
     */
    private updateBlacklistInfo(blacklist: BlacklistedPerson[]): BlacklistedPersonExtended[] {
        return blacklist ? blacklist.map((b: any) => {
            b.formattedImportDate = b.importDate ? getDefaultDateTimeFormat(b.importDate) : null;
            b.translatedDocumentType = b.documentType ? this.enumsService.translateEnumValue("documentType", b.documentType) : "";
            return b;
        }) : [];
    }

    private handleTitleBarAction(actionId: string) {
        switch (actionId) {
            case "add":
                this.router.navigate([AppRoutes.addBlacklistedPlayers], {queryParams: this.createtBlacklistedPlayersFilterCriteria(this.valuesToSearch)});
                break;
        }
    }

    /**
     * Generates filter form component: mt-basic-filters
     */
    private generateFilterForm(): void {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "documentNumber",
                type: FilterType.TEXT,
                label: this.translate.instant("players.blacklist.id"),
                shownByDefault: true,
                value: this.getValueFromParams("documentNumber")
            },
            {
                id: "creationDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("players.blacklist.date"),
                shownByDefault: true,
                value: this.getDateRangeFromParams("date"),
                maxDate: new Date()
            },
            {
                id: "name",
                type: FilterType.TEXT,
                label: this.translate.instant("players.blacklist.name"),
                shownByDefault: false,
                value: this.getValueFromParams("name")
            },
            {
                id: "username",
                type: FilterType.TEXT,
                label: this.translate.instant("players.blacklist.username"),
                shownByDefault: false,
                value: this.getValueFromParams("username")
            },
            {
                id: "documentType",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.blacklist.documentType"),
                shownByDefault: false,
                options: this.documentTypesList,
                value: this.getValueFromParams("documentType")
            }
        ];
    }

    /**
     * Generates the table component with blacklist columns: mt-table
     */
    private generateTable(): void {
        this.tableColumns = [
            {
                type: TableColumnType.DATA,
                id: "documentNumber",
                label: this.translate.instant("players.blacklist.id"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "translatedDocumentType",
                label: this.translate.instant("players.blacklist.documentType"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "username",
                label: this.translate.instant("players.blacklist.username"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "name",
                label: this.translate.instant("players.blacklist.name"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedImportDate",
                label: this.translate.instant("players.blacklist.creationDate"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            }

        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant("table.noResultsMessage"),
            columnsVisibilityLabel: this.translate.instant("table.columnsVisibilityLabel"),
            exportButtonLabel: this.translate.instant("table.exportButtonLabel"),
            totalItemsLabel: this.translate.instant("table.totalItemsLabel"),
            pageLabel: this.translate.instant("table.pageLabel"),
            actionsLabel: this.translate.instant("table.actionsLabel")
        };
    }

}
