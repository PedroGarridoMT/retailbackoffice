import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs/Subscription";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    FilterToValueMap,
    FilterType,
    PermissionsService,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals,
    TableActionButton,
    TableComponent
} from "mt-web-components";
import {
    BaseResultsCode,
    Limit,
    LimitActionOperationEnum,
    PermissionsCode,
    PlayerLimitChange,
    PlayerLimitChangeExtended,
    PlayerLimitChangeSearchResponse,
    PlayerLimitChangeStatusEnum
} from "../model/models";
import { TranslateService } from "@ngx-translate/core";
import { PASEnumsService } from "../../shared/pas-enums.service";
import { getDefaultDateTimeFormat, getISOString } from "../../shared/date-utils";
import { LimitService } from "../limit.service";
import { LoaderService } from "../../shared/loader.service";
import { CurrencyService } from "../../shared/currency.service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ApprovePlayerLimitComponent } from "./../detail/responsible-gaming/actions/approve-player-limit.component";
import { Observable } from "rxjs/Observable";
import { BaseSearchComponent } from "../../shared/base-search.component";
import { ActivatedRoute, Router } from "@angular/router";
import { AppRoutes } from "../../app-routes";
import { PlayerService } from "../../player/player.service";
import { TitleBarParams } from "./../../shared/model/title-bar.model";
import { TitleBarService } from "../../shared/index";

@Component({
    styleUrls: ['./autolimit-changes-search.component.scss'],
    templateUrl: './autolimit-changes-search.component.html'
})
export class AutoLimitChangesSearchComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    public static PAGE_TITLE: string = "players.home.actionSearchLimits";

    //Title bar attributes
    titleBarParams: TitleBarParams = {
        previousPagePath: 'players',
        previousPageLabel: 'players.home.title',
        pageTitle:AutoLimitChangesSearchComponent.PAGE_TITLE,
        pageActions: null
    };

    //UI attributes
    errorInSearch: boolean = false;
    limitActionsPermissions: Array<string> = PermissionsCode.PlayerAutoLimitsUpdate;
    showPlayerDetailLink: boolean = false;

    // mt-table configuration
    table_columns: TableColumn[];
    table_rows: PlayerLimitChange[];
    tableLiterals: TableLiterals;
    rowSelectionMode: string = "single";
    rowSelectionKey: string = "id";
    enableRowSelection: boolean = false;
    tableButtons: TableActionButton[] = [];
    @ViewChild("table") public table: TableComponent;

    selectedLimitChangesIds: number[] = [];

    playerDetailLinkParams: Object = { [PlayerService.PREVIOUS_ROUTE_KEY]: AutoLimitChangesSearchComponent.PAGE_TITLE };

    // mt-basic-filters configuration
    filterBarLiterals: BasicFilterBarLiterals;
    filters: any[];
    statusList: any[];
    limitTypeList: any[];
    @ViewChild('filtersBar') filtersBar: BasicFilterBarComponent;

    //Table html templates
    @ViewChild("dailyLimitTemplate") dailyLimitTemplate;
    @ViewChild("weeklyLimitTemplate") weeklyLimitTemplate;
    @ViewChild("monthlyLimitTemplate") monthlyLimitTemplate;
    @ViewChild("playerLinkTemplate") playerLinkTemplate;

    constructor(private translate: TranslateService,
        private enumsService: PASEnumsService,
        private limitsService: LimitService,
        private loader: LoaderService,
        private modalService: NgbModal,
        private currencyService: CurrencyService,
        private permissionsService: PermissionsService,
        private router: Router,
        private playerService: PlayerService,
        private titleBarService: TitleBarService,
        route: ActivatedRoute) {
        super(route);
    }

    ngOnInit(): void {
        this.showPlayerDetailLink = this.permissionsService.hasAccess(PermissionsCode.PlayerResponsibleGaming);

        if (this.permissionsService.hasAccess(PermissionsCode.PlayerAutoLimitsUpdate)) {
            this.enableRowSelection = true;
            this.tableButtons = [
                {
                    id: 1,
                    icon: "fa fa-check-circle",
                    text: this.translate.instant('players.limits.accept.subtitle'),
                    disabled: true
                },
                {
                    id: 2,
                    icon: "fa fa-times-circle",
                    text: this.translate.instant('players.limits.reject.subtitle'),
                    disabled: true
                }
            ];
        }

        this.enumsService.getPlayerLimitChangeStatusEnumTranslatedList().subscribe(statusList => {
            this.statusList = statusList
        });

        this.enumsService.getLimitTypeEnumTranslatedList().subscribe(limitTypeList => {
            this.limitTypeList = limitTypeList
        });

        this.generateTable();
        this.generateFilterForm();
        this.manageUrlParams();

        //Notify and pass params to the title bar service, so it can notify subscribers
        this.titleBarService.componentInitialized(this.titleBarParams);
    }

    ngOnDestroy(): void {
        this.titleBarService.componentDestroyed();
    }

    onSelectedRowsChange(event: any) {
        this.selectedLimitChangesIds = event;
        if(this.selectedLimitChangesIds && this.selectedLimitChangesIds.length === 1) {
            const selectedLimit = this.table_rows.find(r => r.id === this.selectedLimitChangesIds[0]);
            if (selectedLimit.status === PlayerLimitChangeStatusEnum.Submitted && this.hasSomeLimitToChange(selectedLimit)) {
                this.tableButtons.forEach(tb => tb.disabled = false);
            } else {
                this.tableButtons.forEach(tb => tb.disabled = true);
            }
        } else {
            this.tableButtons.forEach(tb => tb.disabled = true);
        }
    }

    onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    onClearBasicFilter() {
        this.valuesToSearch = null;
        //Clean previous search results
        this.table_rows = [];
        this.resetPagingParams();
        //Reset UI
        this.errorInSearch = false;
        //Clear URL
        this.router.navigate([AppRoutes.searchPlayerAutoLimitChanges]);
        this.clearSelectedIds();
    }

    onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    /**
     * Actions handler for each limit in table: Accept / Reject limit actions
     * @param {event} any
     */
    onActionButtonClick(event: any) {
        if(!this.selectedLimitChangesIds || this.selectedLimitChangesIds.length !== 1 || !event || !event.id) return;
        const selectedLimit = this.table_rows.find(r => r.id === this.selectedLimitChangesIds[0]);
        switch(event.id) {
            case 1:
                this.openUpdateLimitModal(Object.assign({}, selectedLimit, { operation: LimitActionOperationEnum.Accept }));
                break;
            case 2:
                this.openUpdateLimitModal(Object.assign({}, selectedLimit, { operation: LimitActionOperationEnum.Reject }));
                break;
        }
    }

    private clearSelectedIds(): void {
        this.selectedLimitChangesIds = [];
        this.tableButtons.forEach(tb => tb.disabled = true);
        this.table.clearSelectedRows();
    }

    /**
     * If the URL contains a search, prepare the necessary objects to compose the search and then, launch it
     */
    private manageUrlParams() {
        if (this.thereAreQueryParams()) {
            this.prepareDataWithQueryParams();
            this.searchData();
        }
    }

    /**
     * Search data
     */
    private searchData() {
        this.errorInSearch = false;
        this.loader.show();

        let filterCriteria = this.createFilterCriteria(this.valuesToSearch);
        this.limitsService
            .getPlayerLimitsChanges(filterCriteria)
            .flatMap(
                (response: PlayerLimitChangeSearchResponse) => {
                    this.pagingData = {
                        currentPage: this.pagingData.currentPage,
                        pageSize: this.pagingData.pageSize,
                        totalPages: response.totalPages,
                        totalItems: response.totalItems
                    };
                    this.clearSelectedIds();
                    return this.updatePlayerLimitChangeInfo(response.items);
                }
            )
            .finally(() => this.loader.hide())
            .subscribe(
                limits => this.table_rows = limits,
                error => {
                    this.errorInSearch = true;
                    this.resetPagingParams();
                }
            );

        //Store last search
        this.playerService.setLastQueryParams(AutoLimitChangesSearchComponent.PAGE_TITLE, filterCriteria);

        //Update URL with the new search criteria
        this.router.navigate([AppRoutes.searchPlayerAutoLimitChanges], { queryParams: filterCriteria });
    }

    /**
     * Mainly used for build request formatting dates
     * @return {} transactionResponse
     * @param searchFilter
     */
    private createFilterCriteria(searchFilter: any): any {
        let transactionFilterCriteria: any = searchFilter;

        if (searchFilter.requestDate) {
            transactionFilterCriteria.requestDateFrom = searchFilter.requestDate.from ? getISOString(searchFilter.requestDate.from) : null;
            transactionFilterCriteria.requestDateTo = searchFilter.requestDate.to ? getISOString(searchFilter.requestDate.to) : null;

            delete transactionFilterCriteria['requestDate'];
        }

        return Object.assign({}, transactionFilterCriteria, this.getPaginationData());
    }

    /**
     *
     * @param {Array<PlayerLimitChangeExtended>} limitChangeRequests
     * @return {Array<PlayerLimitChangeExtended>}
     */
    private updatePlayerLimitChangeInfo(limitChangeRequests: Array<PlayerLimitChangeExtended>): Observable<Array<PlayerLimitChangeExtended>> {
        if (limitChangeRequests && limitChangeRequests.length > 0) {
            let currentLimitsObservables = [];
            limitChangeRequests.map((changeRequest: PlayerLimitChangeExtended) => {
                //Format dates and translate enums
                changeRequest.requestDate = this.formatDateTime(changeRequest.requestDate);
                changeRequest.validFromDate = this.formatDateTime(changeRequest.validFromDate);
                changeRequest.closedDate = this.formatDateTime(changeRequest.closedDate);
                changeRequest.translatedStatus = this.formatEnum(changeRequest.status, 'playerLimitChangeStatus');
                changeRequest.translatedLimitType = this.formatEnum(changeRequest.limitType, 'limitType');
                changeRequest.notesString = this.formatNotes(changeRequest.notes);

                //Format amounts
                if (changeRequest.requestedValues) {
                    changeRequest.newDailyLimit = this.formatAmount(changeRequest.requestedValues.dailyLimit);
                    changeRequest.newWeeklyLimit = this.formatAmount(changeRequest.requestedValues.weeklyLimit);
                    changeRequest.newMonthlyLimit = this.formatAmount(changeRequest.requestedValues.monthlyLimit);
                }

                //Add html templates for the limits columns
                changeRequest.dailyLimitTemplate = this.dailyLimitTemplate;
                changeRequest.monthlyLimitTemplate = this.monthlyLimitTemplate;
                changeRequest.weeklyLimitTemplate = this.weeklyLimitTemplate;
                changeRequest.playerLinkTemplate = this.playerLinkTemplate;

                //Get current limits of the player so the table can print data in a friendly way.
                currentLimitsObservables.push(this.limitsService.getPlayerLimits(changeRequest.accountCode));
            });

            return Observable.forkJoin(currentLimitsObservables).map(currentLimits => {

                //currentLimits contains the current limits for each player, ordered in the same order of the array of limit changes
                limitChangeRequests.forEach((changeRequest: PlayerLimitChangeExtended, index: number) => {

                    //Find current limits for the current player
                    let playerLimits: Limit[] = currentLimits[index] as Limit[];
                    if (playerLimits) {
                        //Find the current limit value for the type of limit of the request change
                        let currentPlayerLimits = playerLimits.find(item => item.limitType == changeRequest.limitType);

                        if (currentPlayerLimits && currentPlayerLimits.values) {
                            changeRequest.currentDailyLimit = this.formatAmount(currentPlayerLimits.values.dailyLimit);
                            changeRequest.currentWeeklyLimit = this.formatAmount(currentPlayerLimits.values.weeklyLimit);
                            changeRequest.currentMonthlyLimit = this.formatAmount(currentPlayerLimits.values.monthlyLimit);
                        }
                    }
                });
                return limitChangeRequests;
            });
        }
        else {
            //No limit changes => empty table rows.
            return Observable.of([]);
        }
    }

    /**
     *
     * @param {PlayerLimitChangeExtended} limit
     * @return {boolean}
     */
    private hasSomeLimitToChange(limit: PlayerLimitChangeExtended): boolean {
        return (
            limit.currentDailyLimit !== limit.newDailyLimit ||
            limit.currentWeeklyLimit !== limit.newWeeklyLimit ||
            limit.currentMonthlyLimit !== limit.newMonthlyLimit
        );
    }

    private openUpdateLimitModal(limit: PlayerLimitChangeExtended): void {
        const modalRef: NgbModalRef = this.modalService.open(ApprovePlayerLimitComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.limit = limit;

        modalRef.result.then((result: BaseResultsCode) => {
            if (result == BaseResultsCode.Ok) {
                //Reload player limits search
                this.searchData();
            }
        });
    }

    /**
     * Generates filter form component: mt-basic-filters
     */
    private generateFilterForm() {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "status",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("players.limits.status"),
                shownByDefault: true,
                options: this.statusList,
                value: this.getMultipleSelectionFromParams("status")
            },
            {
                id: "username",
                type: FilterType.TEXT,
                label: this.translate.instant("players.limits.username"),
                shownByDefault: true,
                value: this.getValueFromParams("username")
            },
            {
                id: "accountCode",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.accountCode"),
                shownByDefault: true,
                value: this.getValueFromParams("accountCode")
            },
            {
                id: "requestDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("players.limits.requestDate"),
                shownByDefault: true,
                value: this.getDateRangeFromParams("requestDate")
            },
            {
                id: "type",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("players.limits.type"),
                shownByDefault: true,
                options: this.limitTypeList,
                value: this.getValueFromParams("type")
            }
        ];
    }

    /**
     * Generates the table component with player columns: mt-table
     */
    private generateTable() {
        this.table_columns = [
            {
                type: TableColumnType.HTML,
                id: "playerLinkTemplate",
                label: this.translate.instant("players.limits.username"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "name",
                label: this.translate.instant("players.entity.fullName"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "surname",
                label: this.translate.instant("players.entity.fullSurname"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "dailyLimitTemplate",
                label: this.translate.instant("players.limits.daily"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "weeklyLimitTemplate",
                label: this.translate.instant("players.limits.weekly"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "monthlyLimitTemplate",
                label: this.translate.instant("players.limits.monthly"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "requestDate",
                label: this.translate.instant("players.limits.requestDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "translatedLimitType",
                label: this.translate.instant("players.limits.type"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "translatedStatus",
                label: this.translate.instant("players.limits.status"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "closedDate",
                label: this.translate.instant("players.limits.closedDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "validFromDate",
                label: this.translate.instant("players.limits.validFromDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "notesString",
                label: this.translate.instant("players.limits.notes"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('table.noResultsMessage'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        };
    }

    private formatEnum(value, enumName) {
        return value ? this.enumsService.translateEnumValue(enumName, value) : '-';
    }

    private formatDateTime(date): string {
        return date ? getDefaultDateTimeFormat(date) : '-';
    }

    private formatAmount(value): string {
        return value ? this.currencyService.formatAmountWithCurrency(value) : null;
    }

    private formatNotes(notes: Array<string>): string {
        if (!notes || !notes.length) {
            return '-'
        }

        return notes.join(", ");
    }
}
