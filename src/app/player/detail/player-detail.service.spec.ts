import { TestBed, async } from "@angular/core/testing";
import { Subscription } from 'rxjs/Subscription';
import { PlayerDetail } from './../model/PlayerDetail';
import { PlayerDetailsResponse } from '../model/models';
import { PlayerDetailService, PlayerVisualizationModeEnum, PlayerViewEnum } from './player-detail.service';

describe("PlayerDetailService", () => {

    let service: PlayerDetailService;
    let subscriptions: Subscription[];

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                PlayerDetailService
            ]
        });

        service = TestBed.get(PlayerDetailService);
        subscriptions = [];
    });

    afterEach(() => subscriptions.forEach(s => s.unsubscribe()));

    it('should emit player detail and store copy of it in cache', async(() => {

        const playerDetailMock: PlayerDetailsResponse = {
            playerId: 1111,
            username: 'username'
        };

        subscriptions.push(
            service.playerDetail$.subscribe((playerDetail: PlayerDetailsResponse) => {
                expect(playerDetail).toBe(playerDetailMock);
                expect(service.getStoredPlayer()).not.toBe(playerDetailMock);  // Should be a copy of playerDetail
                expect(service.getStoredPlayer()).toEqual(playerDetailMock);
            })
        );

        service.setPlayer(playerDetailMock);
    }));

    it('should emit player visualization mode', async(() => {

        const modeMock: PlayerVisualizationModeEnum = PlayerVisualizationModeEnum.Edition;

        subscriptions.push(
            service.visualizationMode$.subscribe((mode: PlayerVisualizationModeEnum) => {
                expect(mode).toBe(modeMock);
            })
        );

        service.setVisualizationMode(modeMock);
    }));

    it('should emit active view', async(() => {

        const viewMock: PlayerViewEnum = PlayerViewEnum.PLAYER_DATA;

        subscriptions.push(
            service.activeView$.subscribe((view: PlayerViewEnum) => {
                expect(view).toBe(viewMock);
            })
        );

        service.setActiveView(viewMock);
    }));

    it('should emit reload player event with account code', async(() => {

        const accountCodeMock: string = '1111';

        subscriptions.push(
            service.reloadPlayer$.subscribe((accountCode: string) => {
                expect(accountCode).toBe(accountCodeMock);
            })
        );

        service.reloadPlayer(accountCodeMock);
    }));

});
