import { PlayerService } from "../../player.service";
import { PlayerDetailService, PlayerVisualizationModeEnum } from "./../player-detail.service";
import { LoaderService } from "../../../shared/loader.service";
import { ResponseHelperService } from "../../../shared/response-helper.service";
import { PlayerPersonalDataComponent } from "./player-personal-data.component";
import { ComponentFixture, tick, fakeAsync } from "@angular/core/testing";
import { DebugElement, NO_ERRORS_SCHEMA, Pipe, PipeTransform } from "@angular/core";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { async } from "@angular/core/testing";
import { TestBed } from "@angular/core/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { Observable } from "rxjs";
import { FormBuilder, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ResourcesService } from "../../../shared/resources.service";
import { RouterTestingModule } from "@angular/router/testing";
import { ConfigService } from "../../../core/config.service";
import { FakeModalService } from "../../../../test-util/fake-modal.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NotificationsService } from "mt-web-components";
import { BaseResultsCode, ApiResponse } from "../../model/models";
import { ModalService } from "../../../shared/services/modal.service";

@Pipe({ name: "longDatePipe" })
export class FakeLongDatePipe implements PipeTransform {
    transform(date) {
        return date;
    }
}
@Pipe({ name: "longDateTimePipe" })
export class FakeLongDateTimePipe implements PipeTransform {
    transform(date) {
        return date;
    }
}

describe("PlayerPersonalDataComponent", () => {

    let resourcesServiceSpy: jasmine.SpyObj<ResourcesService>;
    let playerServiceSpy: jasmine.SpyObj<PlayerService>;
    let playerDetailServiceSpy: jasmine.SpyObj<PlayerDetailService>;
    let loaderServiceSpy: jasmine.SpyObj<LoaderService>;
    let responseHelperServiceSpy: jasmine.SpyObj<ResponseHelperService>;
    let enumsServiceSpy: jasmine.SpyObj<PASEnumsService>;
    let configServiceSpy: jasmine.SpyObj<ConfigService>;
    let notificationServiceSpy: jasmine.SpyObj<NotificationsService>;

    let fixture: ComponentFixture<PlayerPersonalDataComponent>;
    let comp: PlayerPersonalDataComponent;
    let de: DebugElement;

    let fakeModalService: FakeModalService;
    let playerDetailService: PlayerDetailService;

    const fakePlayerWithRegion = {
        "playerId": 0,
        "accountCode": "0000",
        "username": "username",
        "name1": "name1",
        "name2": "name2",
        "surname1": "surname1",
        "surname2": "surname2",
        "identityDocument": {
            "type": "NationalId",
            "number": "0000",
            "issueDate": "2017-10-04T00:00:00.0000000",
            "issueCityCode": "0000",
            "expirationDate": "2017-10-04T00:00:00.0000000",
            "birthCityCode": "0000",
            "birthCountryCode": null
        },
        "creationDate": "2017-10-04T00:00:00.0000000Z",
        "birthDate": "1999-01-01T00:00:00.0000000Z",
        "gender": "gender",
        "nationality": "CO",
        "fiscalResidence": "CO",
        "fiscalRegion": "00",
        "residence": {
            "countryCode": "CO",
            "regionCode": "BY",
            "zipCode": "000000",
            "cityCode": "00000",
            "address": "address",
            "addressNumber": "0"
        },
        "mobilePhoneNumber": "00000000",
        "homePhoneNumber": null,
        "language": "ES",
        "accountStatus": "Activated",
        "accountStatusReason": "reason",
        "verifiedIdentity": true,
        "selfExcluded": false,
        "selfExclusionInfo": null,
        "balance": 0.0,
        "email": "0@0.0",
        "securityQuestionAnswer": "0000",
        "receivePromotions": true,
        "provenanceCode": null,
        "promotionalCode": null,
        "registrationBonusCode": "",
        "referrer": null,
        "paymentAccounts": [{ type: "Paypal", identifier: "PayPal Identifier" }]
    };

    const fakePlayerWithoutRegion = {
        "playerId": 0,
        "accountCode": "0000",
        "username": "username",
        "name1": "name1",
        "name2": "name2",
        "surname1": "surname1",
        "surname2": "surname2",
        "identityDocument": null,
        "creationDate": "2017-10-04T00:00:00.0000000Z",
        "birthDate": "1999-01-01T00:00:00.0000000Z",
        "gender": "gender",
        "nationality": "CO",
        "fiscalResidence": "CO",
        "fiscalRegion": "00",
        "residence": {
            "countryCode": "CO",
            "regionCode": null,
            "zipCode": "000000",
            "cityCode": "00000",
            "address": "address",
            "addressNumber": "0"
        },
        "mobilePhoneNumber": "00000000",
        "homePhoneNumber": null,
        "language": "ES",
        "accountStatus": "Activated",
        "accountStatusReason": "reason",
        "verifiedIdentity": true,
        "selfExcluded": false,
        "selfExclusionInfo": null,
        "balance": 0.0,
        "email": "0@0.0",
        "securityQuestionAnswer": "0000",
        "receivePromotions": true,
        "provenanceCode": null,
        "promotionalCode": null,
        "registrationBonusCode": "",
        "referrer": null,
        "paymentAccounts": []
    };

    beforeEach(async(() => {

        const observableStreams = ["playerDetail$", "visualizationMode$", "activeView$", "reloadPlayer$"];

        resourcesServiceSpy = jasmine.createSpyObj<ResourcesService>("ResourcesService", ["getCities", "getRegions", "getFiscalRegions"]);
        enumsServiceSpy = jasmine.createSpyObj<PASEnumsService>("PASEnumsService", ["translateEnumValue", "getAccountStatusTranslatedList", "getCountriesTranslatedList", "getLanguagesTranslatedList", "getDocumentTypesTranslatedList", "getGendersTranslatedList"]);
        playerServiceSpy = jasmine.createSpyObj<PlayerService>("PlayerService", ["getAllowedStatusChanges", "getPlayerWallets", "patchPlayer"]);
        playerDetailServiceSpy = jasmine.createSpyObj<PlayerDetailService>("PlayerDetailService", ["setVisualizationMode", "reloadPlayer", "setActiveView", "getStoredPlayer"].concat(observableStreams));
        loaderServiceSpy = jasmine.createSpyObj<LoaderService>("LoaderService", ["show", "hide"]);
        responseHelperServiceSpy = jasmine.createSpyObj<ResponseHelperService>("ResponseHelperService", ["responseHandler"]);
        configServiceSpy = jasmine.createSpyObj<ConfigService>("ConfigService", ["getKey", "getEnvironmentVariables"]);
        notificationServiceSpy = jasmine.createSpyObj<NotificationsService>("NotificationsService", ["add"]);

        // HACK to set public service fields as observables in order to allow subscription in components using it
        observableStreams.forEach((property) => {
            Object.defineProperty(playerDetailServiceSpy, property, { get: () => Observable.of(null) });
        });

        TestBed.configureTestingModule({
            declarations: [PlayerPersonalDataComponent, FakeLongDatePipe, FakeLongDateTimePipe],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                FormBuilder,
                { provide: ResourcesService, useValue: resourcesServiceSpy },
                { provide: PASEnumsService, useValue: enumsServiceSpy },
                { provide: PlayerService, useValue: playerServiceSpy },
                { provide: PlayerDetailService, useValue: playerDetailServiceSpy },
                { provide: LoaderService, useValue: loaderServiceSpy },
                { provide: ResponseHelperService, useValue: responseHelperServiceSpy },
                { provide: ConfigService, useValue: configServiceSpy },
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal },
                { provide: NotificationsService, useValue: notificationServiceSpy },
            ],
            imports: [
                FormsModule, ReactiveFormsModule, RouterTestingModule,
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        })
            .compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerPersonalDataComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;

        fakeModalService = TestBed.get(NgbModal);

        enumsServiceSpy.getCountriesTranslatedList.and.callFake(() => { return Observable.of(["countries"]) });
        enumsServiceSpy.getAccountStatusTranslatedList.and.callFake(() => { return Observable.of(["accountStatus"]) });
        enumsServiceSpy.getLanguagesTranslatedList.and.callFake(() => { return Observable.of(["language"]) });
        enumsServiceSpy.getDocumentTypesTranslatedList.and.callFake(() => { return Observable.of(["documentType"]) });
        enumsServiceSpy.getGendersTranslatedList.and.callFake(() => { return Observable.of(["genders"]) });

        playerDetailServiceSpy.getStoredPlayer.and.returnValue(null);

        resourcesServiceSpy.getFiscalRegions.and.callFake(() => { return Observable.of(["region"]) });
        resourcesServiceSpy.getRegions.and.callFake(() => { return Observable.of([{ code: "region", description: "region" }]) });
        resourcesServiceSpy.getCities.and.callFake(() => { return Observable.of(["city"]) });

        configServiceSpy.getEnvironmentVariables.and.returnValue(Observable.of({
            "showSecondName": true,
            "zipCodeValidator": "col",
            "showFiscalData": false,
            "showSecurityQuestionAnswer": true
        }));

        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy;
    });

    it("should populate input lists", async () => {
        expect(comp.countryList.length).toBeGreaterThan(0);
        expect(comp.fiscalRegionList.length).toBeGreaterThan(0);
        expect(comp.accountStatusList.length).toBeGreaterThan(0);
        expect(comp.languageList.length).toBeGreaterThan(0);
        expect(comp.documentTypeList.length).toBeGreaterThan(0);
        expect(comp.genderList.length).toBeGreaterThan(0);
    });

    it("should create player dates as objects", async () => {
        let formBuilder = TestBed.get(FormBuilder);
        // comp.environment = { zipCodeValidator: 'zipCode' };
        spyOn(formBuilder, "group").and.returnValue(null);


        fixture.detectChanges();
        comp.originalPlayer = JSON.parse(JSON.stringify(fakePlayerWithRegion));
        comp.player = JSON.parse(JSON.stringify(fakePlayerWithRegion));

        expect(comp.birthDate.day).toBeTruthy;
        expect(comp.birthDate.month).toBeTruthy;
        expect(comp.birthDate.year).toBeTruthy;

        expect(comp.iDExpiryDate.day).toBeTruthy;
        expect(comp.iDExpiryDate.month).toBeTruthy;
        expect(comp.iDExpiryDate.year).toBeTruthy;

        expect(comp.iDIssueDate.day).toBeTruthy;
        expect(comp.iDIssueDate.month).toBeTruthy;
        expect(comp.iDIssueDate.year).toBeTruthy;
    });

    it("should discard changes to the current player", async () => {
        let formBuilder = TestBed.get(FormBuilder);
        comp.environment = { zipCodeValidator: 'zipCode' };
        spyOn(formBuilder, "group").and.returnValue(null);

        fixture.detectChanges();
        comp.originalPlayer = JSON.parse(JSON.stringify(fakePlayerWithoutRegion));

        comp.cancelEdition();

        expect(JSON.parse(JSON.stringify(comp.originalPlayer))).toEqual(JSON.parse(JSON.stringify(comp.player)));
    });

    it("should set visualization mode and reload player on successful change", fakeAsync(() => {
        comp.environment = { zipCodeValidator: 'zipCode' };

        comp.player = JSON.parse(JSON.stringify(fakePlayerWithoutRegion));

        comp.playerFormGroup.controls["mobilePhoneNumber"].setValue("2222");
        comp.playerFormGroup.controls["mobilePhoneNumber"].markAsDirty();

        playerServiceSpy.patchPlayer.and.returnValue(Observable.of(BaseResultsCode.Ok));

        comp.onSubmit();
        fakeModalService.resolve(fakeModalService.CONFIRM);
        tick();

        expect(playerDetailServiceSpy.setVisualizationMode).toHaveBeenCalledWith(PlayerVisualizationModeEnum.Visualization);
        expect(playerDetailServiceSpy.reloadPlayer).toHaveBeenCalledWith(comp.player.accountCode);
    }));

    it("should show error on API patch error", fakeAsync(() => {
        comp.environment = { zipCodeValidator: 'zipCode' };

        comp.player = JSON.parse(JSON.stringify(fakePlayerWithoutRegion));

        comp.playerFormGroup.controls["mobilePhoneNumber"].setValue("2222");
        comp.playerFormGroup.controls["mobilePhoneNumber"].markAsDirty();

        playerServiceSpy.patchPlayer.and.returnValue(Observable.throw(<ApiResponse>{ statusCode: BaseResultsCode.Unprocessable, error: ["error"] }));

        comp.onSubmit();
        fakeModalService.resolve(fakeModalService.CONFIRM);
        tick();

        expect(notificationServiceSpy.add).toHaveBeenCalled();
    }));

});
