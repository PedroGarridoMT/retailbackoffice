import { Subscription } from 'rxjs/Subscription';
import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, AbstractControl } from "@angular/forms";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { formatDate, getDateAsObject, getLongDateFormat } from "../../../shared/date-utils";
import { ResourcesService } from "../../../shared/resources.service";
import { PlayerDetailService, PlayerViewEnum, PlayerVisualizationModeEnum } from "./../player-detail.service";
import { PlayerService } from "../../player.service";
import { Observable } from "rxjs/Observable";
import { TranslateService } from "@ngx-translate/core";
import { LoaderService } from "../../../shared/loader.service";
import { ConfigService } from "../../../core/config.service";
import { AccountStatusEnum, DocumentTypeEnum, PlayerDetail, Resource, PaymentMethodEnum } from "../../model/models";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ModalConfirmationComponent } from "../../../shared/components/modal-confirmation.component";
import { ResponseHelperService } from "../../../shared/response-helper.service";
import { PatchDocument, RegionalEnvironment } from "../../../shared/model/models";
import { BaseResultsCode } from "../../../shared/model/base-results-code.enum";
import { NotificationsService, NotificationTypeEnum } from "mt-web-components";
import { ApiResponse } from "../../../shared/model/api-response.model";
import { PASConstants } from "../../../shared/constant";
import { ageValidator, zipCodeColombiaValidator, zipCodeSpainValidator } from "../../../shared/validators";


@Component({
    templateUrl: './player-personal-data.component.html'
})

export class PlayerPersonalDataComponent implements OnInit, OnDestroy {
    // Player detail attributes
    private _player: PlayerDetail = null;
    private _birthDate: { day: number, month: number, year: number };
    private _iDIssueDate: { day: number, month: number, year: number };
    private _iDExpiryDate: { day: number, month: number, year: number };
    private specialFieldSet = ['name1', 'name2', 'surname1', 'surname2', 'birthDate', 'verifiedIdentity', 'nationality', 'identityDocument.type', 'identityDocument.number'];

    private _subscriptions: Subscription[] = [];

    originalPlayer: PlayerDetail = null;
    visualizationModeEnum = PlayerVisualizationModeEnum; // The enum is made usable from the template
    visualizationMode: PlayerVisualizationModeEnum;
    playerFormGroup: FormGroup;
    currentCountryCode: string; // Colombia by properties
    countryList: any[];
    accountStatusList: any[];
    languageList: any[];
    documentTypeList: any[];
    genderList: any[];
    regionList: any[];
    residenceCityList: any[];
    birthCityList: any[];
    issueCityList: any[];
    fiscalRegionList: any[];
    environment: RegionalEnvironment = {};

    constructor(private formBuilder: FormBuilder, private playerService: PlayerService,
        private enumsService: PASEnumsService, private resourcesService: ResourcesService,
        private playerDetailService: PlayerDetailService, private route: ActivatedRoute,
        private translate: TranslateService, private loader: LoaderService, private configService: ConfigService,
        private responseHelper: ResponseHelperService, private modalService: NgbModal,
        private notifications: NotificationsService) {
    }


    public ngOnInit(): void {
        this.playerDetailService.setActiveView(PlayerViewEnum.PLAYER_DATA);
        this.environment = this.configService.getEnvironmentVariables();
        this.currentCountryCode = this.environment.countryCode;
        this._subscriptions.push(
            // Subscribe to player data and visualization mode change events
            this.playerDetailService.playerDetail$.subscribe(playerData => this.player = JSON.parse(JSON.stringify(playerData))),

            this.playerDetailService.visualizationMode$.subscribe((mode) => {
                if (mode == PlayerVisualizationModeEnum.Edition) {
                    this.setVisualizationMode(true);
                }
                else if (mode == PlayerVisualizationModeEnum.Visualization) {
                    this.setVisualizationMode(false);
                }
            })
        );

        //Load resources data
        this.loadInputsData();

        // When navigating back to this child view, the parent will not launch an player data change event. But the
        // current player is stored in the shared player detail service -> Get it.
        if (!this.player) {
            this._subscriptions.push(
                this.route.params.subscribe((params: Params) => {
                    let storedPlayer = this.playerDetailService.getStoredPlayer();
                    if (storedPlayer) {
                        this.player = storedPlayer;
                    }
                })
            )
        }
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * See CanDeactivateGuard in Core module.
     * If the player data is being edited, request the user a confirmation to leave the edition.
     */
    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        return this.visualizationMode == PlayerVisualizationModeEnum.Edition ? this.askForConfirmation() : true;
    }

    get player(): PlayerDetail {
        return this._player;
    }

    @Input()
    set player(value: PlayerDetail) {
        if (value && Object.keys(value).length > 0) {
            this._player = value;

            if (!this.environment.showAdditionalDocumentData && value.paymentAccounts.length == 0) {
                this.player.paymentAccounts.push({
                    type: PaymentMethodEnum.Paypal,
                    identifier: null
                });
            }

            this.birthDate = getDateAsObject(this._player.birthDate);
            this.iDExpiryDate = getDateAsObject(this._player.identityDocument ? this._player.identityDocument.expirationDate : null);
            this.iDIssueDate = getDateAsObject(this._player.identityDocument ? this._player.identityDocument.issueDate : null);

            this.buildFormGroup(this._player);
            this.loadAsyncInputsData(this._player);
            this.originalPlayer = JSON.parse(JSON.stringify(this._player));

            //Fields are disabled by default
            this.setVisualizationMode(false);
            this.formatAllFieldsWithPipes();
        }
    }

    askForConfirmation(): Promise<boolean> {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant('players.detail.messageUnsavedChangesTitle');
        modalRef.componentInstance.message = this.translate.instant('players.detail.messageUnsavedChanges');
        modalRef.componentInstance.confirmText = this.translate.instant('players.detail.messageUnsavedChangesLeave');
        modalRef.componentInstance.cancelText = this.translate.instant('players.detail.messageUnsavedChangesStay');


        return modalRef.result.then(
            result => {
                if (result === "CONFIRM") {
                    this.cancelEdition();
                    return true; //Can deactivate => proceed with navigation
                }
                return false;
            }, reason => {
                //This window can not be dismissed. Return false as it is the safest option
                return false;
            });
    }

    get birthDate(): { day: number; month: number; year: number } {
        if (!this._birthDate && this._player.birthDate) {
            this._birthDate = getDateAsObject(this._player.birthDate);
        }
        return this._birthDate;
    }

    set birthDate(value: { day: number; month: number; year: number }) {
        this._birthDate = value;
    }

    get iDExpiryDate(): { day: number; month: number; year: number } {
        if (!this._iDExpiryDate && this._player.identityDocument && this._player.identityDocument.expirationDate) {
            this._iDExpiryDate = getDateAsObject(this._player.identityDocument.expirationDate);
        }
        return this._iDExpiryDate;
    }

    set iDExpiryDate(value: { day: number; month: number; year: number }) {
        this._iDExpiryDate = value;
    }

    get iDIssueDate(): { day: number; month: number; year: number } {
        if (!this._iDIssueDate && this._player.identityDocument && this._player.identityDocument.issueDate) {
            this._iDIssueDate = getDateAsObject(this._player.identityDocument.issueDate);
        }
        return this._iDIssueDate;
    }

    set iDIssueDate(value: { day: number; month: number; year: number }) {
        this._iDIssueDate = value;
    }

    /**
     * Updates any date property in player entity
     * For example: player.birthDate = _birthDate.year + _birthDate.month + _birthDate.day input values
     */
    onDateInputModelUpdate(date: any, controlKey: string, groupKey?: string) {
        let composedControlKey = groupKey ? this.playerFormGroup.controls[groupKey]['controls'][controlKey] : this.playerFormGroup.controls[controlKey];
        composedControlKey.markAsDirty();

        if (this.player && date) {
            groupKey ? (this.player[groupKey][controlKey] = formatDate(date)) : (this.player[controlKey] = formatDate(date));

            composedControlKey.setValue(groupKey ? this.player[groupKey][controlKey] : this.player[controlKey]);
        } else {
            composedControlKey.setErrors({ "required": true })
        }
    }


    /**
     * Load translated data into selects, inputs, etc
     */
    private loadInputsData() {
        this.enumsService.getCountriesTranslatedList().subscribe(countries => {
            this.countryList = countries;
        });

        this.resourcesService.getFiscalRegions()
            .do(() => this.loader.show())
            .finally(() => this.loader.hide())
            .subscribe(regions => {
                this.fiscalRegionList = regions;
            });

        this.enumsService.getAccountStatusTranslatedList().subscribe(accountStatuses => {
            this.accountStatusList = accountStatuses;
        });

        this.enumsService.getLanguagesTranslatedList().subscribe(languages => {
            this.languageList = languages;
        });

        this.enumsService.getDocumentTypesTranslatedList().subscribe(documentTypes => {
            this.documentTypeList = documentTypes;
        });

        this.enumsService.getGendersTranslatedList().subscribe(genders => {
            this.genderList = genders;
        });
    }

    /**
     * It will load the needed "resources" API
     * Used in "cities" and "regions" selects
     */
    private loadAsyncInputsData(player: PlayerDetail) {
        if (!this.regionList && player && player.residence && player.residence.countryCode) {
            this.loader.show();
            this.resourcesService.getRegions(player.residence.countryCode)
                .finally(() => this.loader.hide())
                .subscribe(regions => {
                    this.regionList = regions;
                })
        }

        if (!this.residenceCityList && player && player.residence && player.residence.regionCode) {
            this.loadResidenceCityList(player.residence.regionCode);
        }

        if (!this.player.birthRegionCode && player && player.nationality && player.identityDocument && player.identityDocument.birthCityCode) {
            this.loader.show();
            this.resourcesService.getRegions(player.nationality, player.identityDocument.birthCityCode)
                .finally(() => this.loader.hide())
                .subscribe(regions => {
                    if (regions && regions.length > 0) {
                        let regionCode = (regions[0] as Resource).code;
                        this.player.birthRegionCode = regionCode;
                        this.originalPlayer.birthRegionCode = regionCode; // Needed because it's not stored in DB
                        if (!this.birthCityList)
                            this.loadBirthCityList(regionCode);
                    }
                });
        }

        if (!this.player.issueRegionCode && player && player.nationality && player.identityDocument && player.identityDocument.issueCityCode) {
            this.loader.show();
            this.resourcesService.getRegions(player.nationality, player.identityDocument.issueCityCode)
                .finally(() => this.loader.hide())
                .subscribe(regions => {
                    if (regions && regions.length > 0) {
                        let regionCode = (regions[0] as Resource).code;
                        this.player.issueRegionCode = regionCode;
                        this.originalPlayer.issueRegionCode = regionCode; // Needed because it's not stored in DB
                        if (!this.issueCityList)
                            this.loadIssueCityList(regionCode);
                    }
                });
        }
    }

    private loadResidenceCityList(regionCode) {
        if (regionCode) {
            this.loader.show();
            this.resourcesService.getCities(regionCode)
                .finally(() => this.loader.hide())
                .subscribe(cities => {
                    this.residenceCityList = cities;
                })
        } else {
            this.residenceCityList = [];
        }
    }

    private loadBirthCityList(regionCode) {
        if (regionCode) {
            this.loader.show();
            this.resourcesService.getCities(regionCode)
                .finally(() => this.loader.hide())
                .subscribe(cities => {
                    this.birthCityList = cities;
                })
        } else {
            this.birthCityList = [];
        }
    }

    private loadIssueCityList(regionCode) {
        if (regionCode) {
            this.loader.show();
            this.resourcesService.getCities(regionCode)
                .finally(() => this.loader.hide())
                .subscribe(cities => {
                    this.issueCityList = cities;
                })
        } else {
            this.issueCityList = [];
        }
    }

    /**
     * Create FormBuilder with Player entity fields
     */
    private buildFormGroup(player: PlayerDetail) {
        //dynamically create FormGroup for multiple payment accounts
        const paymentAccounts: FormGroup = new FormGroup({});
        for (let account of this.player.paymentAccounts) {
            const identifier: FormControl = new FormControl(account.identifier);
            const reason: FormControl = new FormControl(null);

            identifier.validator = this.validatorPaymentAccountIdentifier(account.type.toString(), reason);
            reason.validator = this.validatorPaymentAccountChangeReason(account.type.toString(), identifier);

            const accountGroup: FormGroup = new FormGroup({});

            accountGroup.addControl("identifier", identifier);
            accountGroup.addControl("reason", reason);
            paymentAccounts.addControl(account.type.toString(), accountGroup);
        }

        //choose validator depending on the environment
        let zipCodeValidator: ValidatorFn;
        if (this.environment && this.environment.zipCodeValidator == PASConstants.COUNTRY_CODE_COLOMBIA) {
            zipCodeValidator = zipCodeColombiaValidator();
        }
        else if (this.environment && this.environment.zipCodeValidator == PASConstants.COUNTRY_CODE_SPAIN) {
            zipCodeValidator = zipCodeSpainValidator();
        }
        else {
            zipCodeValidator = zipCodeSpainValidator();
        }

        this.playerFormGroup = this.formBuilder.group({
            // Personal Data
            name1: [player ? player.name1 : '', [Validators.required, Validators.pattern(PASConstants.VALIDATION_NAME_REGEX)]],
            name2: [player ? player.name2 : '', [Validators.pattern(PASConstants.VALIDATION_NAME_REGEX)]],
            surname1: [player ? player.surname1 : '', [Validators.required, Validators.pattern(PASConstants.VALIDATION_NAME_REGEX)]],
            surname2: [player ? player.surname2 : '', [Validators.pattern(PASConstants.VALIDATION_NAME_REGEX)]],
            nationality: [player ? player.nationality : '', Validators.required],
            identityDocument: this.formBuilder.group({
                type: [player && player.identityDocument ? player.identityDocument.type : '', Validators.required],
                number: [player && player.identityDocument ? player.identityDocument.number : '', Validators.required],
                birthCountryCode: [player && player.identityDocument ? player.identityDocument.birthCountryCode : ''],
                birthRegionCode: [player ? player.birthRegionCode : ''],
                birthCityCode: [player && player.identityDocument ? player.identityDocument.birthCityCode : '', (player.nationality == this.currentCountryCode && this.environment.showAdditionalDocumentData) ? Validators.required : ''],
                issueRegionCode: [player ? player.issueRegionCode : ''],
                issueCityCode: [player && player.identityDocument ? player.identityDocument.issueCityCode : '', (player.nationality == this.currentCountryCode && this.environment.showAdditionalDocumentData) ? Validators.required : ''],
                issueDate: [player && player.identityDocument ? player.identityDocument.issueDate : '', (this.environment.showAdditionalDocumentData) ? Validators.required : ''],
                expirationDate: [player && player.identityDocument ? player.identityDocument.expirationDate : '', player.nationality !== this.currentCountryCode ? Validators.required : ''],
            }),
            fiscalResidence: [player && player.fiscalResidence, (this.environment.showFiscalData) ? Validators.required : ''],
            fiscalRegion: [player && player.fiscalRegion, (this.environment.showFiscalData) ? Validators.required : ''],
            gender: [player ? player.gender : '', Validators.required],
            birthDate: [player ? player.birthDate : '', [Validators.required, ageValidator(this.currentCountryCode)]],

            // Contact data
            email: [player ? player.email : '', [Validators.required, Validators.email]],
            mobilePhoneNumber: [player ? player.mobilePhoneNumber : '', [Validators.required, Validators.pattern(PASConstants.VALIDATION_PHONE_REGEX)]],
            homePhoneNumber: [player ? player.homePhoneNumber : '', [Validators.pattern(PASConstants.VALIDATION_PHONE_REGEX)]],
            residence: this.formBuilder.group({
                countryCode: [player && player.residence ? player.residence.countryCode : '', Validators.required],
                regionCode: [player && player.residence ? player.residence.regionCode : '', Validators.required],
                cityCode: [player && player.residence ? player.residence.cityCode : '', Validators.required],
                zipCode: [player && player.residence ? player.residence.zipCode : '', [Validators.required, zipCodeValidator]],
                address: [player && player.residence ? player.residence.address : '', Validators.required],
                addressNumber: [player && player.residence ? player.residence.addressNumber : '', (this.environment.showAddressNumber) ? Validators.required : ''],
            }),
            language: [player ? player.language : '', Validators.required],

            //Account data
            provenanceCode: [player ? player.provenanceCode : ''],
            paymentAccounts: paymentAccounts,
            verifiedIdentity: [player ? player.verifiedIdentity : '', Validators.required], // Documentation approved
            receivePromotions: [player ? player.receivePromotions : '', Validators.required] // Newsletter subscription
        });
    }

    validatorPaymentAccountIdentifier(accountType: string, reason: FormControl): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (this.hasPaymentAccountIdentifierChanged(accountType, control.value) && (!reason.value || reason.value.length == 0)) {
                reason.setErrors({ "required": true });
                return null;
            }
            if (!this.hasPaymentAccountIdentifierChanged(accountType, control.value) && (reason.value && reason.value.length != 0)) {
                reason.reset();
                return null;
            }
            reason.setErrors(null);
            return null;
        };
    }

    validatorPaymentAccountChangeReason(accountType: string, identifier: FormControl): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!this.hasPaymentAccountIdentifierChanged(accountType, identifier.value) && (control.value && control.value.length != 0)) {
                control.reset();
            }
            if (this.hasPaymentAccountIdentifierChanged(accountType, identifier.value) && (!control.value || control.value.length == 0)) {
                return { "required": true };
            }
            return null;
        };
    }

    private hasPaymentAccountIdentifierChanged(accountType: string, currentValue: string): boolean {
        const identifier = this.originalPlayer.paymentAccounts.find((account) => account.type.toString() == accountType).identifier;
        return identifier !== currentValue && (!!identifier || !!currentValue);
    }

    /**
     * Sets the player detail mode (Edition or Visualization)
     * @param editable
     * @see PlayerVisualizationModeEnum
     */
    private setVisualizationMode(editable?: boolean) {
        if (editable) {
            this.visualizationMode = PlayerVisualizationModeEnum.Edition;
            this.enableFormInputs(true);
            this.checkEditionModeByStatus();
        } else {
            this.visualizationMode = PlayerVisualizationModeEnum.Visualization;
            this.enableFormInputs(false);
        }
    }

    /**
     * Enable / disable all form inputs (if no "controls" specified)
     * @param enable
     * @param controls
     */
    private enableFormInputs(enable: boolean, controls?: string[]) {
        if ((!this.playerFormGroup || !this.playerFormGroup.controls) && !controls) {
            return;
        }

        if (enable) {
            (controls ? controls : Object.keys(this.playerFormGroup.controls)).forEach((key) => {
                let composedKey = key.split(".");
                composedKey[1] ? this.playerFormGroup.controls[composedKey[0]]['controls'][composedKey[1]].enable() : this.playerFormGroup.controls[composedKey[0]].enable();
            })
        } else {
            (controls ? controls : Object.keys(this.playerFormGroup.controls)).forEach((key) => {
                let composedKey = key.split(".");
                composedKey[1] ? this.playerFormGroup.controls[composedKey[0]]['controls'][composedKey[1]].disable() : this.playerFormGroup.controls[composedKey[0]].disable();
            })
        }
        this.disableFieldsByDefault();
    }

    /**
     * These fields should be always disabled
     */
    private disableFieldsByDefault() {
        if (this.playerFormGroup && this.playerFormGroup.controls && this.playerFormGroup.controls['identityDocument'] && this.playerFormGroup.controls['identityDocument']['controls'] && this.playerFormGroup.controls['identityDocument']['controls']['type'])
            this.playerFormGroup.controls['identityDocument']['controls']['type'].disable();
    }

    /**
     * Check the ability to edit for some fields depending of the player status
     */
    private checkEditionModeByStatus() {
        if (this.player) {
            switch (this.player.accountStatus) {
                case AccountStatusEnum.Confirmed:
                    this.enableFormInputs(true);
                    break;
                case AccountStatusEnum.Blacklisted:
                case AccountStatusEnum.New:
                case AccountStatusEnum.Closed:
                    this.enableFormInputs(false);
                    break;
                case AccountStatusEnum.Activated:
                case AccountStatusEnum.SecurityBlocked:
                case AccountStatusEnum.Frozen:
                case AccountStatusEnum.Deactivated:
                case AccountStatusEnum.Blocked:
                    this.enableFormInputs(false, this.specialFieldSet);
                    break;
            }
        }
    }

    /**
     * Disable edition mode
     * Restore player data
     */
    cancelEdition() {
        this.playerDetailService.setVisualizationMode(PlayerVisualizationModeEnum.Visualization);
        this.player = JSON.parse(JSON.stringify(this.originalPlayer));
        this.formatAllFieldsWithPipes();
    }

    /**
     * Apply corresponding pipes to fields
     *
     */
    formatAllFieldsWithPipes() {
        if (this.playerFormGroup && this.playerFormGroup.get("birthDate"))
            this.playerFormGroup.get("birthDate").setValue(getLongDateFormat(this.player.birthDate));
        if (this.playerFormGroup && this.playerFormGroup.get("identityDocument") && this.playerFormGroup.get('identityDocument').get('issueDate') && this.player.identityDocument)
            this.playerFormGroup.get('identityDocument').get('issueDate').setValue(getLongDateFormat(this.player.identityDocument.issueDate));
        if (this.playerFormGroup && this.playerFormGroup.get("identityDocument") && this.playerFormGroup.get('identityDocument').get('expirationDate') && this.player.identityDocument)
            this.playerFormGroup.get('identityDocument').get('expirationDate').setValue(getLongDateFormat(this.player.identityDocument.expirationDate));

    }

    /**
     * Submit form action
     */
    onSubmit() {
        let patchDocuments: Array<PatchDocument> = [];
        let path: string = '';
        this.getPatchDocumentArray(this.getChangedProperties(this.playerFormGroup), patchDocuments, path);
        console.log('patchDocuments', patchDocuments);

        if (patchDocuments && patchDocuments.length > 0) {
            //If there are some changes open confirmation modal
            const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
                backdrop: 'static',
                keyboard: false
            });
            modalRef.componentInstance.title = this.translate.instant('players.detail.messageConfirmChangesTitle');
            modalRef.componentInstance.message = this.translate.instant('players.detail.messageConfirmChanges');
            modalRef.componentInstance.confirmText = this.translate.instant('common.yes');
            modalRef.componentInstance.cancelText = this.translate.instant('common.cancel');

            modalRef.result.then(
                result => {
                    if (result === "CONFIRM") {
                        this.loader.show();
                        this.playerService.patchPlayer(this.player.accountCode, patchDocuments)
                            .finally(() => {
                                this.loader.hide();
                            })
                            .subscribe(response => {
                                this.responseHelper.responseHandler(response, 'players.edit.response');
                                if (response == BaseResultsCode.Ok) {
                                    this.playerDetailService.setVisualizationMode(PlayerVisualizationModeEnum.Visualization);

                                    // Reload player data to get updated data from database
                                    this.playerDetailService.reloadPlayer(this.player.accountCode);
                                }
                            },
                            (error: ApiResponse) => {
                                this.editionErrorHandler(error);
                            });
                    }
                    return false;
                }, reason => {
                    //This window can not be dismissed. Return false as it is the safest option
                    return false;
                });
        } else {
            this.notifications.add({
                type: NotificationTypeEnum.Warning,
                text: this.translate.instant('players.edit.warning.noChanges')
            });
        }
    }

    /**
     *
     * @param values
     * @param patchDocuments
     * @param path
     */
    private getPatchDocumentArray(values, patchDocuments, path) {
        for (let key in values) {
            let value = values[key];
            if (typeof value == "object" && value !== null) {
                path = path + '/' + key;
                this.getPatchDocumentArray(value, patchDocuments, path);
                path = '';
            }
            else {
                patchDocuments.push(this.getPatchDocument(path + '/' + key, value));
            }
        }
    }

    /**
     *
     * @param {string} path
     * @param {string} value
     * @return {PatchDocument}
     */
    private getPatchDocument(path: string, value: string): PatchDocument {
        return {
            op: PatchDocument.OpEnum.Replace,
            path: path,
            value: value
        } as PatchDocument;
    }

    /**
     *
     * @param form
     * @return {any}
     */
    private getChangedProperties(form: any): any {
        let dirtyValues = {};

        Object.keys(form.controls)
            .forEach(key => {
                let currentControl = form.controls[key];

                if (currentControl.dirty) {
                    if (currentControl.controls)
                        dirtyValues[key] = this.getChangedProperties(currentControl);
                    else
                        dirtyValues[key] = currentControl.value;
                }

                if (currentControl.invalid) {
                    console.log('Invalid control: ', key, '. Value: ', currentControl.value);
                }
            });

        return dirtyValues;
    }

    /**
     * It will show a notification with a translated list of the possible player validation errors
     * @returns string
     * @param response
     */
    private editionErrorHandler(response: ApiResponse) {
        if (response.statusCode == BaseResultsCode.Unprocessable && response.error && Array.isArray(response.error)) {
            let validationErrors: string = '';
            response.error.map((validationError) => {
                validationErrors += "\n - " + this.translate.instant(`enum.playerValidationError.${validationError}`)
            });

            this.notifications.add({
                type: NotificationTypeEnum.Error,
                text: this.translate.instant('players.edit.response.reviewFields') + " \n" + validationErrors,
                time: -1
            });

        } else {
            this.responseHelper.responseHandler(response.statusCode, 'players.edit.response');
        }
    }


    translateStatusReasonEnum(accountStatusReason) {
        return this.enumsService.translateEnumValue('accountStatusReason', accountStatusReason);
    }

    /**
     * For any nationality change,
     * ID Type and any dependent field will be updated
     *
     * @param countryCode
     */
    onNationalityChange(countryCode) {
        if (this.currentCountryCode && countryCode === this.currentCountryCode) {
            if (this.playerFormGroup.controls['identityDocument'])
                this.playerFormGroup.controls['identityDocument']['controls']['type'].setValue(DocumentTypeEnum.NationalId);
        } else {
            if (this.playerFormGroup.controls['identityDocument'])
                this.playerFormGroup.controls['identityDocument']['controls']['type'].setValue(DocumentTypeEnum.Resident);
        }
    }

    /**
     * For any change of Department of residence,
     * cities will be reloaded
     */
    onResidenceDepartmentChange(regionCode) {
        if (this.playerFormGroup.controls['residence']) {
            this.playerFormGroup.controls['residence']['controls']['cityCode'].setValue(null);
        }
        this.loadResidenceCityList(regionCode);
    }

    /**
     * For any change of Department of birth,
     * place of birth cities will be reloaded
     */
    onBirthDepartmentChange(regionCode: string) {
        if (this.playerFormGroup.controls['identityDocument']) {
            this.playerFormGroup.controls['identityDocument']['controls']['birthRegionCode'].markAsPristine(); // Not available in PATCH operation
            this.playerFormGroup.controls['identityDocument']['controls']['birthCityCode'].setValue(null);
        }
        this.loadBirthCityList(regionCode);
    }

    /**
     * For any change of Department of expedition,
     * place of expedition cities will be reloaded
     */
    onIssueDepartmentChange(regionCode: string) {
        if (this.playerFormGroup.controls['identityDocument']) {
            this.playerFormGroup.controls['identityDocument']['controls']['issueRegionCode'].markAsPristine(); // Not available in PATCH operation
            this.playerFormGroup.controls['identityDocument']['controls']['issueCityCode'].setValue(null);
        }
        this.loadIssueCityList(regionCode);
    }
}
