import { Component, OnDestroy, OnInit } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import {
    AccountStatusEnum,
    BaseResultsCode,
    MoneyTypeEnum,
    PatchDocument,
    PermissionsCode,
    PlayerDetailsResponse,
    PlayerWallet,
    ApplicableBonus,
    AddPlayerBonusRequest,
    StatusPermissions,
    ChangeStatusOperation,
    ChangeStatusEnum,
    RegionalEnvironment
} from "../../model/models";
import { PlayerService } from "../../player.service";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { CurrencyService } from "../../../shared/currency.service";
import {
    PlayerActionsEnum,
    PlayerDetailService,
    PlayerViewEnum,
    PlayerVisualizationModeEnum
} from "./../player-detail.service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { PlayerStatusChangeComponent } from "./../actions/player-status-change.component";
import { ManualAdjustmentComponent } from "./../actions/manual-adjustment.component";
import { ModalConfirmationComponent } from "../../../shared/components/modal-confirmation.component";
import { TranslateService } from "@ngx-translate/core";
import { LoaderService } from "../../../shared/loader.service";
import { ResponseHelperService } from "../../../shared/response-helper.service";
import { PermissionsService, NotificationTypeEnum } from "mt-web-components";
import { ApiResponse } from "../../../shared/model/api-response.model";
import { ConfigService } from "../../../core/config.service";
import { PlayerAddBonusComponent } from "./../actions/add-bonus.component";
import { PASConfigService } from "../../../shared/pas-config.service";
import { BonusService } from "../player-bonus.service";
import { formatDate } from '../../../shared/date-utils';

interface PlayerAction {
    translationKey: string;
    actionId: PlayerActionsEnum;
    cssClasses: string;
    cssSelector?: string;
}

@Component({
    selector: 'pas-player-header',
    templateUrl: './player-header.component.html',
    styleUrls: ['./player-header.component.scss']
})
export class PlayerDetailHeaderComponent implements OnInit, OnDestroy {
    private _subscriptions: Subscription[] = [];
    private _player: PlayerDetailsResponse = {};
    private _statusPermissionsList: Array<StatusPermissions> = [];
    private _revokeAvailable: boolean = false;
    private _selfExclusionAvailable: boolean = false;
    private _allowedStatusesForSelfExclusion: Array<AccountStatusEnum> = [];
    private _allowedStatusChanges: any;

    public walletType = MoneyTypeEnum;
    public playerWallets: PlayerWallet[];
    public visualizationMode: PlayerVisualizationModeEnum;
    public visualizationModeEnum = PlayerVisualizationModeEnum;
    public activeView: PlayerViewEnum;
    public availableActions: PlayerAction[] = [];
    public environment: RegionalEnvironment = {};
    public playerNameHeader: string = "";
    public applicableBonuses: ApplicableBonus[];

    constructor(private playerService: PlayerService, private enumsService: PASEnumsService,
        private currencyService: CurrencyService, private playerDetailService: PlayerDetailService, private bonusService: BonusService,
        private modalService: NgbModal, private translate: TranslateService, private loader: LoaderService, private pasConfigService: PASConfigService,
        private responseHelper: ResponseHelperService, private permissionsService: PermissionsService, private configService: ConfigService) {
    }

    public ngOnInit(): void {
        this.environment = this.configService.getEnvironmentVariables();

        this._subscriptions.push(
            this.playerDetailService.playerDetail$.subscribe(playerData => this.player = playerData),
            this.playerDetailService.visualizationMode$.subscribe(mode => this.visualizationMode = mode),
            this.playerDetailService.activeView$.subscribe(view => this.activeView = view),

            Observable.forkJoin(
                [this.pasConfigService.getStatusPermissions(),
                this.configService.getAllowedStatusChangesForSelfExclusion(),
                this.configService.getStatusChanges()]
            ).finally(this.setAvailableActions.bind(this))
                .subscribe((res) => {
                    this._statusPermissionsList = res[0];
                    this._allowedStatusesForSelfExclusion = res[1];
                    this._allowedStatusChanges = res[2];
                }),

            // Refresh available actions on any data change
            Observable.merge(
                this.playerDetailService.playerDetail$,
                this.playerDetailService.visualizationMode$,
                this.playerDetailService.activeView$,
            ).subscribe(this.setAvailableActions.bind(this))
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    set player(player: PlayerDetailsResponse) {
        if (player) {
            this._player = player;
            let firstName = this.player.name1 ? this.player.name1 + ' ' : '';
            let secondName = this.environment.showSecondName ? (this.player.name2 ? this.player.name2 + ' ' : '') : '';
            let surname1 = this.player.surname1 ? this.player.surname1 + ' ' : '';
            let surname2 = this.player.surname2 ? this.player.surname2 : '';

            this.playerNameHeader = firstName + secondName + surname1 + surname2;
            this.loadPlayerBalance(player.accountCode);
            this.getApplicableBonuses(player.accountCode);
        }
    }

    get player(): PlayerDetailsResponse {
        return this._player;
    }

    public getAvailableActions(): PlayerAction[] {
        let actions: PlayerAction[] = [];
        if (!this.isEmptyPlayer() && this._player.accountStatus != AccountStatusEnum.Closed) {

            //Edit
            if (this.permissionsService.hasAccess(PermissionsCode.PlayerEdit) &&
                this.activeView === PlayerViewEnum.PLAYER_DATA && this.visualizationMode === PlayerVisualizationModeEnum.Visualization) {
                actions.push({
                    translationKey: "common.edit",
                    actionId: PlayerActionsEnum.EDIT,
                    cssClasses: "fa fa-pencil fa-fw",
                    cssSelector: "edit-selector"
                });
            }

            // Change status
            // Visible if user has permission to change status and player is not self-excluded
            // or if the player is self-excluded and the user has permission to both change the status and revoke self-exclusion
            if ((this.permissionsService.hasAccess(PermissionsCode.PlayerChangeStatus) && this.someStatusChangeAllowed(this.player) && !this.player.selfExcluded) ||
                (this.permissionsService.hasAccess(PermissionsCode.PlayerChangeStatus) && this.permissionsService.hasAccess(PermissionsCode.PlayerRevokeSelfExclusion) && this.player.selfExcluded)) {
                actions.push({
                    translationKey: "players.detail.changeStatus.changeStatus",
                    actionId: PlayerActionsEnum.CHANGE_STATUS,
                    cssClasses: "fa fa-refresh fa-fw",
                    cssSelector: "change-status-selector"
                });
                this._revokeAvailable = this.permissionsService.hasAccess(PermissionsCode.PlayerRevokeSelfExclusion) && this.player.selfExcluded;
            }

            // Manual adjustment
            if (this.permissionsService.hasAccess(PermissionsCode.PlayerManualAdjustment) && this.player.accountStatus != AccountStatusEnum.Closed) {
                actions.push({
                    translationKey: "players.detail.manualAdjustment.manualAdjustment",
                    actionId: PlayerActionsEnum.MANUAL_ADJUSTMENT,
                    cssClasses: "fa fa-usd fa-fw",
                    cssSelector: "manual-adjustment-selector"
                });
            }

            // Self-exclude a player
            if (this.permissionsService.hasAccess(PermissionsCode.PlayerActiveSelfExclusion) && !this.player.selfExcluded &&
                (this._allowedStatusesForSelfExclusion.some((status: AccountStatusEnum) => status === this.player.accountStatus))) {
                this._selfExclusionAvailable = true;
            }
            else {
                this._selfExclusionAvailable = false;
            }

            // Verify Identity / Approve documentation
            if (this.permissionsService.hasAccess(PermissionsCode.PlayerVerifyIdentity) && this.isVerifyIdentityAllowed(this.player)) {
                actions.push({
                    translationKey: "players.verifyIdentity.approveDocumentation",
                    actionId: PlayerActionsEnum.VERIFY_IDENTITY,
                    cssClasses: "fa fa-id-card fa-fw",
                    cssSelector: "approve-doc-selector"
                });
            }

            // Do not Verify Identity / reject documentation
            if (this.permissionsService.hasAccess(PermissionsCode.PlayerRejectIdentity) && this.isRejectIdentityAllowed(this.player)) {
                actions.push({
                    translationKey: "players.rejectDocumentation.rejectTitle",
                    actionId: PlayerActionsEnum.REJECT_IDENTITY,
                    cssClasses: "fa fa-id-card fa-fw",
                    cssSelector: "reject-doc-selector"
                });
            }

            // Add bonus to a player
            const accountStatus: StatusPermissions = this._statusPermissionsList.find(s => s.status === this.player.accountStatus);
            if (this.permissionsService.hasAccess(PermissionsCode.AddPlayerBonus) && accountStatus && accountStatus.canBet) {
                actions.push({
                    translationKey: "players.bonuses.addBonus.title",
                    actionId: PlayerActionsEnum.ADD_BONUS,
                    cssClasses: "fa fa-btc fa-fw",
                    cssSelector: "add-bonus-selector"
                });
            }
        }
        return actions;
    }

    public getStatusTranslated(): string {
        if (this.player && this.player.accountStatus) {
            return this.enumsService.translateEnumValue('accountStatus', this.player.accountStatus);
        }
        return '';
    }

    public hasBonusWallets(): boolean {
        if (this.playerWallets && this.playerWallets.length > 0) {
            return this.playerWallets.some(item => item.moneyType == MoneyTypeEnum.Bonus);
        }
        return false;
    }

    public getCurrencySymbol(): string {
        return this.currencyService.getSymbol();
    }

    public getRealWalletBalance(): string {
        let amount: number = 0;
        if (this.playerWallets && this.playerWallets.length > 0) {
            let realWallet = this.playerWallets.find(item => item.moneyType == MoneyTypeEnum.Real);
            if (realWallet) {
                return this.getFormattedAmount(realWallet.balance);
            }
        }
        return this.getFormattedAmount(amount);
    }

    public getBonusWalletsBalance(): string {
        let totalAmount: any = 0;
        if (this.playerWallets && this.playerWallets.length > 0) {
            totalAmount = this.playerWallets.reduce((a, b) => {
                return (b.moneyType == MoneyTypeEnum.Bonus) ? a + b.balance : a;
            }, 0);
        }
        return this.getFormattedAmount(totalAmount);
    }

    public getFormattedAmount(amount: number): string {
        return this.currencyService.formatAmount(amount);
    }

    public getFormattedAmountWithCurrency(amount: number): string {
        return this.currencyService.formatAmountWithCurrency(amount);
    }

    // public onWalletClick(wallet: PlayerWallet) {
    //     // TODO: when campaign details page is ready, whe should navigate to it.
    //     console.info(wallet.walletId + " clicked. Bonus campaing detail available soon...");
    // }

    /**
     * Player detail actions handler
     * @param actionId
     */
    public onActionClick(actionId: PlayerActionsEnum): void {
        switch (actionId) {
            case PlayerActionsEnum.EDIT:
                this.playerDetailService.setVisualizationMode(PlayerVisualizationModeEnum.Edition);
                break;
            case PlayerActionsEnum.CHANGE_STATUS:
                this.openChangeStatusModal();
                break;
            case PlayerActionsEnum.MANUAL_ADJUSTMENT:
                this.openManualAdjustmentModal();
                break;
            case PlayerActionsEnum.VERIFY_IDENTITY:
                this.askForVerifyIdentityConfirmation();
                break;
            case PlayerActionsEnum.REJECT_IDENTITY:
                this.askForRejectIdentityConfirmation();
                break;
            case PlayerActionsEnum.ADD_BONUS:
                this.openAddBonusModal();
                break;
        }
    }

    public openChangeStatusModal(): void {
        const modalRef: NgbModalRef = this.modalService.open(PlayerStatusChangeComponent, {
            backdrop: 'static',
            keyboard: false
        });

        let targetStates: AccountStatusEnum[] = [];
        if (this._revokeAvailable) {
            targetStates.push(this._player.accountStatus);
        } else {
            targetStates = this.getAllowedStatusChanges(this.player.accountStatus);
        }
        modalRef.componentInstance.selfExclusionAvailable = this._selfExclusionAvailable;
        modalRef.componentInstance.hasMoney = this.hasMoney();
        modalRef.componentInstance.statusPermissionsList = this._statusPermissionsList;
        modalRef.componentInstance.targetStates = targetStates;
        modalRef.componentInstance.player = this.player;
        modalRef.componentInstance.confirmationClick.subscribe((changeStatusOperation: ChangeStatusOperation) => {
            //New status is the same as old status only if it's "revoke self-exclusion"
            if (changeStatusOperation.selectedStatus == this.player.accountStatus) {
                this.revokeSelfExclusion();
                modalRef.close();
            }
            else if (changeStatusOperation.selfExclusion) {
                this.activateSelfExclusion(changeStatusOperation.selfExclusionEndDate);
                modalRef.close();
            }
            else {
                this.playerService.updatePlayerStatus(changeStatusOperation.accountCode, changeStatusOperation.selectedStatus, changeStatusOperation.selectedChangeReason)
                    .finally(() => {
                        this.loader.hide();
                        modalRef.close();
                        //Reload player data to get update status from database
                        this.playerDetailService.reloadPlayer(this.player.accountCode);
                    })
                    .subscribe(response => {
                        if (response.value === ChangeStatusEnum.ActivationEmailSent) {
                            this.responseHelper.responseHandler(BaseResultsCode.CustomCode, 'players.detail.changeStatus.response.activationEmailSent', NotificationTypeEnum.Info);
                        } else {
                            this.responseHelper.responseHandler(response.statusCode, 'players.detail.changeStatus.response');
                        }
                    });
            }
        });
    }

    public openManualAdjustmentModal(): void {
        const modalRef: NgbModalRef = this.modalService.open(ManualAdjustmentComponent, {
            backdrop: 'static',
            keyboard: false
        });
        let realMoneyWallet = this.playerWallets.find(item => item.moneyType == MoneyTypeEnum.Real);
        modalRef.componentInstance.player = this.player;
        modalRef.componentInstance.playerBalance = realMoneyWallet ? realMoneyWallet.balance : 0;

        modalRef.result.then((result: BaseResultsCode) => {
            if (result == BaseResultsCode.Ok) {
                //Reload player balance from database
                this.loadPlayerBalance(this.player.accountCode);
            }
        });
    }

    public openAddBonusModal(): void {
        const modalRef: NgbModalRef = this.modalService.open(PlayerAddBonusComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.applicableBonuses = this.applicableBonuses;
        modalRef.componentInstance.confirmationClick.subscribe((cancelBonusRequest: AddPlayerBonusRequest) => {
            this.loader.show();
            this.bonusService.addPlayerBonus(this._player.accountCode, cancelBonusRequest)
                .finally(() => {
                    this.loader.hide();
                    modalRef.close();
                })
                .subscribe(
                    (responseCode: BaseResultsCode) => {
                        if (responseCode === BaseResultsCode.Ok) {
                            this.loadPlayerBalance(this.player.accountCode);
                            this.playerDetailService.reloadBonuses(this.player.accountCode);
                        }
                        this.responseHelper.responseHandler(responseCode, "players.bonuses.addBonus.response");
                    },
                    (error: ApiResponse) => {
                        this.responseHelper.responseHandler(error.statusCode, "players.bonuses.addBonus.response");
                    }
                );
        });
    }

    public askForVerifyIdentityConfirmation(): void {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant('players.verifyIdentity.approveDocumentation');
        modalRef.componentInstance.message = this.translate.instant('players.verifyIdentity.approveMessage');
        modalRef.componentInstance.confirmText = this.translate.instant('players.verifyIdentity.approveConfirmButton');
        modalRef.componentInstance.cancelText = this.translate.instant('common.cancel');
        modalRef.componentInstance.cssSelector = "modal-approve-doc";
        modalRef.result.then(result => (result === "CONFIRM") ? this.verifyIdentity(true) : null);
    }

    public askForRejectIdentityConfirmation(): void {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant('players.rejectDocumentation.rejectTitle');
        modalRef.componentInstance.message = this.translate.instant('players.rejectDocumentation.rejectMessage');
        modalRef.componentInstance.confirmText = this.translate.instant('players.rejectDocumentation.rejectConfirmButton');
        modalRef.componentInstance.cancelText = this.translate.instant('common.cancel');
        modalRef.componentInstance.cssSelector = "modal-reject-doc";
        modalRef.result.then(result => (result === "CONFIRM") ? this.verifyIdentity(false) : null);
    }

    private setAvailableActions(): void {
        this.availableActions.splice(0, this.availableActions.length);
        Array.prototype.push.apply(this.availableActions, this.getAvailableActions());
    }

    private loadPlayerBalance(accountCode: string): void {
        this.playerService
            .getPlayerWallets(accountCode)
            .subscribe(
                (wallets) => {
                    this.playerWallets = wallets;
                },
                (error) => {
                    this.playerWallets = null;
                    console.error(error);
                }
            );
    }

    private getApplicableBonuses(accountCode: string): void {
        if (this.permissionsService.hasAccess(PermissionsCode.AddPlayerBonus)) {
            this.bonusService.getApplicableBonuses(accountCode).subscribe((bonuses: ApplicableBonus[]) => {
                this.applicableBonuses = bonuses;
            });
        } else {
            this.applicableBonuses = [];
        }
    }

    private isEmptyPlayer(): boolean {
        return Object.keys(this.player).length == 0;
    }

    /**
     * Revoke Self exclusion Request
     */
    private revokeSelfExclusion(): void {
        const patch: PatchDocument = {
            op: PatchDocument.OpEnum.Replace,
            path: "/selfExcluded",
            value: {
                active: false
            }
        };

        this.patchPlayer(patch, 'players.selfExclusion.response');
    }

    /**
     * Activate Self exclusion
     */
    private activateSelfExclusion(date: Date): void {
        const patch: PatchDocument = {
            op: PatchDocument.OpEnum.Replace,
            path: "/selfExcluded",
            value: {
                active: true,
                endDate: formatDate(date)
            }
        };

        this.patchPlayer(patch, 'players.selfExclusion.response');
    }

    /**
     * Verify Identity Request
     * Approve documentation / Reject documentation depending of approve param
     *
     * @param approve
     */
    private verifyIdentity(approve: boolean): void {
        const patch: PatchDocument = {
            op: PatchDocument.OpEnum.Replace,
            path: "/verifiedIdentity",
            value: approve
        };

        this.patchPlayer(patch, approve ? 'players.verifyIdentity.response' : 'players.rejectDocumentation.response');
    }

    /**
     * Patch request
     * @param patchDocument
     * @param translationKey
     */
    private patchPlayer(patchDocument: PatchDocument, translationKey): void {
        this.loader.show();
        this.playerService
            .patchPlayer(this.player.accountCode, [patchDocument])
            .finally(() => this.loader.hide())
            .subscribe(
                (responseCode) => {
                    if (responseCode == BaseResultsCode.Ok) {
                        //Reload player data to get updated status from database
                        this.playerDetailService.reloadPlayer(this.player.accountCode);
                    }
                    this.responseHelper.responseHandler(responseCode, translationKey);
                },
                (error: ApiResponse) => {
                    this.responseHelper.responseHandler(error.statusCode, translationKey);
                }
            );
    }

    /**
     * Get available status changes for provided status
     * @param status
     * @return {Array<AccountStatusEnum>}
     */
    private getAllowedStatusChanges(status: AccountStatusEnum): Array<AccountStatusEnum> {
        let availableStatuses = [];
        const statusChanges = this._allowedStatusChanges ? this._allowedStatusChanges.find((statusChange) => statusChange.status == status) : null;
        if (statusChanges)
            availableStatuses = statusChanges.changes;

        return availableStatuses;
    }

    /**
     * If true the player is allowed to change his status
     * @param player
     * @return {boolean}
     */
    private someStatusChangeAllowed(player: PlayerDetailsResponse): boolean {
        if (player) {
            let statuses = this.getAllowedStatusChanges(player.accountStatus);
            if (statuses && statuses.length > 0) {
                // When the only target status is "Closed" and the user balance is 0 then the player cannot be closed.
                if (statuses.length == 1 && statuses[0] == AccountStatusEnum.Closed && this.hasMoney()) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check player balance
     * @return {boolean}
     */
    private hasMoney(): boolean {
        if (this.playerWallets && this.playerWallets.length) {
            return this.playerWallets.some(item => item.balance > 0);
        }
        return false;
    }


    /**
     * If true Verify Identity is allowed
     * @param player
     * @return {boolean}
     */
    private isVerifyIdentityAllowed(player: PlayerDetailsResponse): boolean {
        return (player.accountStatus === AccountStatusEnum.Confirmed ||
            player.accountStatus === AccountStatusEnum.Activated)
            && !player.verifiedIdentity;
    }

    /**
     * If true Reject Identity is allowed
     * @param player
     * @return {boolean}
     */
    private isRejectIdentityAllowed(player: PlayerDetailsResponse): boolean {
        return (player.accountStatus === AccountStatusEnum.Confirmed ||
            player.accountStatus === AccountStatusEnum.Activated ||
            player.accountStatus === AccountStatusEnum.SecurityBlocked ||
            player.accountStatus === AccountStatusEnum.Frozen ||
            player.accountStatus === AccountStatusEnum.Deactivated ||
            player.accountStatus === AccountStatusEnum.Blocked)
            && player.verifiedIdentity;
    }
}
