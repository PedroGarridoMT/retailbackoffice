import { async, ComponentFixture, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, Pipe, PipeTransform } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PermissionsService } from "mt-web-components";
import { PlayerDetailsResponse, PlayerWallet } from "./../../../shared/model/models";
import { FakeModalService } from "./../../../../test-util/fake-modal.service";
import { ResponseHelperService } from "./../../../shared/response-helper.service";
import { LoaderService } from "./../../../shared/loader.service";
import {
    PlayerActionsEnum,
    PlayerDetailService,
    PlayerViewEnum,
    PlayerVisualizationModeEnum
} from "./../player-detail.service";
import { CurrencyService } from "./../../../shared/currency.service";
import { PASEnumsService } from "./../../../shared/pas-enums.service";
import { PlayerService } from "./../../player.service";
import { BonusService } from "../player-bonus.service";
import { FakeTranslateLoader } from "./../../../../test-util/fake-translate-loader";
import { PlayerDetailHeaderComponent } from "./../header/player-header.component";
import { AccountStatusEnum, ApiResponse, BaseResultsCode, MoneyTypeEnum, AddPlayerBonusRequest, StatusPermissions, ChangeStatusOperation, AccountStatusChangeReasonEnum, PatchDocument } from "../../model/models";
import { By } from "@angular/platform-browser";
import { ConfigService } from "../../../core/config.service";
import { PASConfigService } from "../../../shared/pas-config.service";
import { ModalService } from "../../../shared/services/modal.service";

@Pipe({ name: "mediumDateTimePipe" })
export class FakeMediumDateTimePipe implements PipeTransform {
    transform(date) {
        return date;
    }
}

describe("PlayerDetailHeaderComponent", () => {

    const player: PlayerDetailsResponse = {
        accountCode: "accountCode",
        username: "Username",
        name1: "Name1",
        name2: "Name2",
        surname1: "Surname1",
        surname2: "Surname2",
        playerId: 123,
        accountStatus: AccountStatusEnum.Activated,
        selfExcluded: false
    };

    const selfExcludedPlayer: PlayerDetailsResponse = {
        accountCode: "accountCode",
        username: "Username",
        name1: "Name1",
        name2: "Name2",
        surname1: "Surname1",
        surname2: "Surname2",
        playerId: 123,
        accountStatus: AccountStatusEnum.Activated,
        selfExcluded: true
    };

    const statusPermissions: StatusPermissions[] = [
        {
            status: AccountStatusEnum.Activated,
            canLogin: true,
            canDeposit: true,
            canWithdraw: true,
            canBet: true,
            canChangePassword: true
        },
        {
            status: AccountStatusEnum.New,
            canLogin: false,
            canDeposit: false,
            canWithdraw: false,
            canBet: false,
            canChangePassword: false
        },
        {
            status: AccountStatusEnum.Blocked,
            canLogin: false,
            canDeposit: false,
            canWithdraw: false,
            canBet: true,
            canChangePassword: false
        }];

    let playerServiceSpy: jasmine.SpyObj<PlayerService>;
    let bonusServiceSpy: jasmine.SpyObj<BonusService>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;
    let playerDetailServiceSpy: jasmine.SpyObj<PlayerDetailService>;
    let loaderServiceSpy: jasmine.SpyObj<LoaderService>;
    let responseHelperServiceSpy: jasmine.SpyObj<ResponseHelperService>;
    let permissionsServiceSpy: jasmine.SpyObj<PermissionsService>;
    let enumsServiceSpy: jasmine.SpyObj<PASEnumsService>;
    let configServiceSpy: jasmine.SpyObj<ConfigService>;
    let pasConfigServiceSpy: jasmine.SpyObj<PASConfigService>;

    let fixture: ComponentFixture<PlayerDetailHeaderComponent>;
    let comp: PlayerDetailHeaderComponent;
    let de: DebugElement;

    let fakeModalService: FakeModalService;
    let playerDetailService: PlayerDetailService;

    beforeEach(async(() => {

        const observableStreams = ["playerDetail$", "visualizationMode$", "activeView$", "reloadPlayer$"];

        enumsServiceSpy = jasmine.createSpyObj<PASEnumsService>("PASEnumsService", ["translateEnumValue"]);
        playerServiceSpy = jasmine.createSpyObj<PlayerService>("PlayerService", ["getPlayerWallets", "patchPlayer", "updatePlayerStatus"]);
        bonusServiceSpy = jasmine.createSpyObj<BonusService>("BonusService", ["getApplicableBonuses", "addPlayerBonus"]);
        currencyServiceSpy = jasmine.createSpyObj<CurrencyService>("CurrencyService", ["formatAmount", "formatAmountWithCurrency", "getSymbol"]);
        playerDetailServiceSpy = jasmine.createSpyObj<PlayerDetailService>("PlayerDetailService", ["setVisualizationMode", "reloadPlayer", "reloadBonuses"].concat(observableStreams));
        loaderServiceSpy = jasmine.createSpyObj<LoaderService>("LoaderService", ["show", "hide"]);
        responseHelperServiceSpy = jasmine.createSpyObj<ResponseHelperService>("ResponseHelperService", ["responseHandler"]);
        permissionsServiceSpy = jasmine.createSpyObj<PermissionsService>("PermissionsService", ["hasAccess"]);
        configServiceSpy = jasmine.createSpyObj<ConfigService>("ConfigService", ["getKey", "getEnvironmentVariables","getStatusChanges", "getAllowedStatusChangesForSelfExclusion"]);
        pasConfigServiceSpy = jasmine.createSpyObj<PASConfigService>("PASConfigService", ["getStatusPermissions"]);

        // HACK to set public service fields as observables in order to allow subscription in components using it
        observableStreams.forEach((property) => {
            Object.defineProperty(playerDetailServiceSpy, property, { get: () => Observable.of(null) });
        });

        TestBed.configureTestingModule({
            declarations: [PlayerDetailHeaderComponent, FakeMediumDateTimePipe],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal },
                { provide: PASEnumsService, useValue: enumsServiceSpy },
                { provide: PlayerService, useValue: playerServiceSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy },
                { provide: PlayerDetailService, useValue: playerDetailServiceSpy },
                { provide: LoaderService, useValue: loaderServiceSpy },
                { provide: ResponseHelperService, useValue: responseHelperServiceSpy },
                { provide: PermissionsService, useValue: permissionsServiceSpy },
                { provide: ConfigService, useValue: configServiceSpy },
                { provide: PASConfigService, useValue: pasConfigServiceSpy },
                { provide: BonusService, useValue: bonusServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        })
            .compileComponents();

    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerDetailHeaderComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;

        configServiceSpy.getAllowedStatusChangesForSelfExclusion.and.returnValue(Observable.of(["Activated", "Frozen", "Blocked"]));
        configServiceSpy.getStatusChanges.and.returnValue(Observable.of([{
            status: AccountStatusEnum.Blocked,
            changes: ["Activated"]
        }]));

        configServiceSpy.getEnvironmentVariables.and.returnValue({
            "showSecondName": true,
            "zipCodeValidator": "col",
            "showFiscalData": false,
            "showSecurityQuestionAnswer": true
        });

        fakeModalService = TestBed.get(NgbModal);
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        bonusServiceSpy.getApplicableBonuses.and.returnValue(Observable.of(null));
        pasConfigServiceSpy.getStatusPermissions.and.returnValue(Observable.of(statusPermissions));
        Object.defineProperty(playerDetailServiceSpy, "reloadBonusesSource$", { get: () => Observable.of(player) });
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should add player bonus and then reload player balance and bonuses on confirmation in add bonus modal", () => {
        comp.player = player;
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        bonusServiceSpy.addPlayerBonus.and.returnValue(Observable.of(BaseResultsCode.Ok));
        spyOn(fakeModalService, "close").and.returnValue(null);

        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            applicableBonuses: []
        };
        comp.openAddBonusModal();

        const addPlayerBonusRequest: AddPlayerBonusRequest = { description: "Description", bonusKey: "bonusKey", amount: 10 };
        fakeModalService.componentInstance.confirmationClick.next(addPlayerBonusRequest);

        expect(loaderServiceSpy.show).toHaveBeenCalled();
        // Finally
        expect(loaderServiceSpy.hide).toHaveBeenCalled();
        expect(fakeModalService.close).toHaveBeenCalled();

        // addPlayerBonus Request
        expect(bonusServiceSpy.addPlayerBonus).toHaveBeenCalledWith(player.accountCode, addPlayerBonusRequest);
        // addPlayerBonus Success
        expect(playerServiceSpy.getPlayerWallets).toHaveBeenCalledWith(player.accountCode);
        expect(playerDetailServiceSpy.reloadBonuses).toHaveBeenCalledWith(player.accountCode);
    });

    it("should try to add bonus to a player on confirmation click in modal and show notification if it fails", () => {
        comp.player = player;
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        const apiResponse: ApiResponse = { statusCode: BaseResultsCode.InternalServerError, error: null };
        bonusServiceSpy.addPlayerBonus.and.returnValue(Observable.throw(apiResponse));
        spyOn(fakeModalService, "close").and.returnValue(null);

        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            applicableBonuses: []
        };
        comp.openAddBonusModal();

        const addPlayerBonusRequest: AddPlayerBonusRequest = { description: "Description", bonusKey: "bonusKey", amount: 10 };
        fakeModalService.componentInstance.confirmationClick.next(addPlayerBonusRequest);

        expect(loaderServiceSpy.show).toHaveBeenCalled();
        // Finally
        expect(loaderServiceSpy.hide).toHaveBeenCalled();
        expect(fakeModalService.close).toHaveBeenCalled();

        // addPlayerBonus Request
        expect(bonusServiceSpy.addPlayerBonus).toHaveBeenCalledWith(player.accountCode, addPlayerBonusRequest);
        // addPlayerBonus Error
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalledWith(BaseResultsCode.InternalServerError, "players.bonuses.addBonus.response");
    });

    it("should reload player on confirmation in change status modal", fakeAsync(() => {
        playerDetailServiceSpy.reloadPlayer.and.returnValue(null);
        playerServiceSpy.updatePlayerStatus.and.returnValue(Observable.of({ value: "Response" }));
        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        comp.openChangeStatusModal();

        const changeStatusOperation: ChangeStatusOperation = { accountCode: "1111", selectedStatus: AccountStatusEnum.Blocked, selectedChangeReason: AccountStatusChangeReasonEnum.BonusAbuse };
        fakeModalService.componentInstance.confirmationClick.next(changeStatusOperation);

        tick();
        expect(playerDetailServiceSpy.reloadPlayer).toHaveBeenCalled();
    }));

    it("should load player balance on confirmation in manual adjustment modal", fakeAsync(() => {
        comp.playerWallets = [];

        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));

        comp.openManualAdjustmentModal();
        fakeModalService.resolve(BaseResultsCode.Ok);
        tick();
        expect(playerServiceSpy.getPlayerWallets).toHaveBeenCalled();
    }));

    it("should revoke self execution on confirmation in ask for revoke self execution modal", fakeAsync(() => {
        comp.player = selfExcludedPlayer;
        playerServiceSpy.patchPlayer.and.returnValue(Observable.of(BaseResultsCode.Ok));
        responseHelperServiceSpy.responseHandler.and.returnValue(null);
        playerDetailServiceSpy.reloadPlayer.and.returnValue(Observable.of(null));


        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        comp.openChangeStatusModal();

        const changeStatusOperation: ChangeStatusOperation = { accountCode: "1111", selectedStatus: AccountStatusEnum.Activated, selectedChangeReason: AccountStatusChangeReasonEnum.BonusAbuse };
        fakeModalService.componentInstance.confirmationClick.next(changeStatusOperation);

        tick();
        const fakePatchDocument = {
            op: PatchDocument.OpEnum.Replace,
            path: "/selfExcluded",
            value: {
                active: false
            }
        }
        expect(playerServiceSpy.patchPlayer).toHaveBeenCalledWith(selfExcludedPlayer.accountCode, [fakePatchDocument]);
        expect(playerDetailServiceSpy.reloadPlayer).toHaveBeenCalled();
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalled();
    }));

    it("should verify identity on confirmation in ask for verify identity modal", fakeAsync(() => {
        playerServiceSpy.patchPlayer.and.returnValue(Observable.of(BaseResultsCode.Ok));
        responseHelperServiceSpy.responseHandler.and.returnValue(null);
        playerDetailServiceSpy.reloadPlayer.and.returnValue(Observable.of(null));

        comp.askForVerifyIdentityConfirmation();
        fakeModalService.resolve(fakeModalService.CONFIRM);
        tick();
        expect(playerServiceSpy.patchPlayer).toHaveBeenCalled();
        expect(playerDetailServiceSpy.reloadPlayer).toHaveBeenCalled();
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalled();
    }));

    it("should verify identity on confirmation in ask for reject identity modal", fakeAsync(() => {
        playerServiceSpy.patchPlayer.and.returnValue(Observable.of(BaseResultsCode.Ok));
        responseHelperServiceSpy.responseHandler.and.returnValue(null);
        playerDetailServiceSpy.reloadPlayer.and.returnValue(Observable.of(null));

        comp.askForRejectIdentityConfirmation();
        fakeModalService.resolve(fakeModalService.CONFIRM);
        tick();
        expect(playerServiceSpy.patchPlayer).toHaveBeenCalled();
        expect(playerDetailServiceSpy.reloadPlayer).toHaveBeenCalled();
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalled();
    }));

    it("should handle patch player error response", fakeAsync(() => {

        ["askForVerifyIdentityConfirmation", "askForRejectIdentityConfirmation"].forEach(methodName => {
            const method = comp[methodName].bind(comp);

            responseHelperServiceSpy.responseHandler.and.returnValue(null);
            playerDetailServiceSpy.reloadPlayer.and.returnValue(Observable.of(null));
            playerServiceSpy.patchPlayer.and.returnValue(Observable.of(BaseResultsCode.Ok));

            method();
            fakeModalService.resolve(fakeModalService.CONFIRM);
            tick();
            expect(playerDetailServiceSpy.reloadPlayer).toHaveBeenCalled();
            expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalled();

            playerServiceSpy.patchPlayer.and.returnValue(Observable.throw(<ApiResponse>{ statusCode: BaseResultsCode.InternalServerError }));
            method();
            fakeModalService.resolve(fakeModalService.CONFIRM);
            tick();
            expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalled();
        });

    }));

    it("should load player balance when player is set (setter)", () => {
        const playerWallets: PlayerWallet[] = [<PlayerWallet>{ moneyType: MoneyTypeEnum.Real, balance: 10 }];

        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(playerWallets));
        comp.player = {};
        expect(playerServiceSpy.getPlayerWallets).toHaveBeenCalled();
        expect(comp.playerWallets).toBe(playerWallets);

        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.throw("Error"));
        comp.player = {};
        expect(comp.playerWallets).toBe(null);
    });

    it("should return true if has bonus wallets with", () => {
        comp.playerWallets = [<PlayerWallet>{ moneyType: MoneyTypeEnum.Bonus }];
        expect(comp.hasBonusWallets()).toBe(true);
    });

    it("should return true if has no bonus wallets", () => {
        comp.playerWallets = [];
        expect(comp.hasBonusWallets()).toBe(false);

        comp.playerWallets = [<PlayerWallet>{ moneyType: MoneyTypeEnum.Real }];
        expect(comp.hasBonusWallets()).toBe(false);
    });

    it("should return real wallet balance", () => {
        currencyServiceSpy.formatAmount.and.callFake((val) => "$" + val);
        comp.playerWallets = [
            <PlayerWallet>{ moneyType: MoneyTypeEnum.Real, balance: 10 },
            <PlayerWallet>{ moneyType: MoneyTypeEnum.Real, balance: 20 },
            <PlayerWallet>{ moneyType: MoneyTypeEnum.Bonus, balance: 30 }
        ];
        expect(comp.getRealWalletBalance()).toBe("$10");
    });

    it("should return bonus wallets balance", () => {
        currencyServiceSpy.formatAmount.and.callFake((val) => "$" + val);
        comp.playerWallets = [
            <PlayerWallet>{ moneyType: MoneyTypeEnum.Bonus, balance: 10 },
            <PlayerWallet>{ moneyType: MoneyTypeEnum.Real, balance: 20 },
            <PlayerWallet>{ moneyType: MoneyTypeEnum.Bonus, balance: 30 }
        ];
        expect(comp.getBonusWalletsBalance()).toBe("$40");
    });

    it("should return currency symbol", () => {
        currencyServiceSpy.getSymbol.and.returnValue("$");
        expect(comp.getCurrencySymbol()).toBe("$");
    });

    it("should format ammount", () => {
        currencyServiceSpy.formatAmount.and.callFake((val) => val + ".00");
        expect(comp.getFormattedAmount(100)).toBe("100.00");
    });

    it("should format ammount with currency", () => {
        currencyServiceSpy.formatAmountWithCurrency.and.callFake((val) => "$" + val + ".00");
        expect(comp.getFormattedAmountWithCurrency(100)).toBe("$100.00");
    });

    it("should get translated status", () => {
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        enumsServiceSpy.translateEnumValue.and.returnValue("accountStatus");

        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.Activated };
        expect(comp.getStatusTranslated()).toBe("accountStatus");

        comp.player = {};
        expect(comp.getStatusTranslated()).toBe("");
    });

    it("should handle action click", () => {
        let spy;

        playerDetailServiceSpy.setVisualizationMode.and.returnValue(null);
        comp.onActionClick(PlayerActionsEnum.EDIT);
        expect(playerDetailServiceSpy.setVisualizationMode).toHaveBeenCalled();

        spy = spyOn(comp, "openChangeStatusModal");
        comp.onActionClick(PlayerActionsEnum.CHANGE_STATUS);
        expect(spy).toHaveBeenCalled();

        spy = spyOn(comp, "openManualAdjustmentModal");
        comp.onActionClick(PlayerActionsEnum.MANUAL_ADJUSTMENT);
        expect(spy).toHaveBeenCalled();

        spy = spyOn(comp, "askForVerifyIdentityConfirmation");
        comp.onActionClick(PlayerActionsEnum.VERIFY_IDENTITY);
        expect(spy).toHaveBeenCalled();

        spy = spyOn(comp, "askForRejectIdentityConfirmation");
        comp.onActionClick(PlayerActionsEnum.REJECT_IDENTITY);
        expect(spy).toHaveBeenCalled();

        spy = spyOn(comp, "openAddBonusModal");
        comp.onActionClick(PlayerActionsEnum.ADD_BONUS);
        expect(spy).toHaveBeenCalled();
    });

    it("should not return any actions if player status is Closed", () => {
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        comp.player = <PlayerDetailsResponse>{ accountCode: '1111', accountStatus: AccountStatusEnum.Closed };

        expect(comp.getAvailableActions().length).toBe(0);
    });

    it("should disable actions button if there are no actions available", () => {
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        comp.player = <PlayerDetailsResponse>{ accountCode: '1111', accountStatus: AccountStatusEnum.Closed };

        let actionsButton = de.query(By.css('.actions-button-selector')).nativeElement;
        expect(actionsButton.disabled).toBe(true);
    });

    it("should not return any actions if player is empty", () => {
        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        comp.player = <PlayerDetailsResponse>{};

        expect(comp.getAvailableActions().length).toBe(0);
    });

    xit("should return available actions", () => {

        /**
         * @param {PlayerActionsEnum} actionId
         * @returns {boolean}
         */
        function containsAction(actionId: PlayerActionsEnum): boolean {
            return !!comp.getAvailableActions().filter(action => action.actionId === actionId).length;
        }

        playerServiceSpy.getPlayerWallets.and.returnValue(Observable.of(null));
        comp.player = <PlayerDetailsResponse>{ accountCode: '1111' };

        // Remove all permissions
        permissionsServiceSpy.hasAccess.and.returnValue(false);
        expect(comp.getAvailableActions().length).toBe(0);

        // Give all permissions
        permissionsServiceSpy.hasAccess.and.returnValue(true);

        // EDIT ACTION

        comp.visualizationMode = PlayerVisualizationModeEnum.Visualization;

        comp.activeView = PlayerViewEnum.PLAYER_DATA;
        expect(containsAction(PlayerActionsEnum.EDIT)).toBe(true);

        comp.activeView = PlayerViewEnum.PLAYER_DOCUMENTATION;
        expect(containsAction(PlayerActionsEnum.EDIT)).toBe(false);

        comp.activeView = PlayerViewEnum.PLAYER_RESPONSIBLE_GAMING;
        expect(containsAction(PlayerActionsEnum.EDIT)).toBe(false);

        comp.activeView = PlayerViewEnum.PLAYER_TRANSACTIONS;
        expect(containsAction(PlayerActionsEnum.EDIT)).toBe(false);

        // CHANGE_STATUS ACTION

        someStatusChangeAllowed => false
        playerServiceSpy.getAllowedStatusChanges.and.returnValue(<AccountStatusEnum[]>[AccountStatusEnum.Closed]);
        comp.playerWallets = [<PlayerWallet>{ moneyType: MoneyTypeEnum.Real, balance: 20 }];
        expect(containsAction(PlayerActionsEnum.CHANGE_STATUS)).toBe(false);

        someStatusChangeAllowed => true
        comp.playerWallets = [];
        expect(containsAction(PlayerActionsEnum.CHANGE_STATUS)).toBe(true);

        // MANUAL_ADJUSTMENT ACTION

        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.Closed };
        expect(containsAction(PlayerActionsEnum.MANUAL_ADJUSTMENT)).toBe(false);

        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.Activated };
        expect(containsAction(PlayerActionsEnum.MANUAL_ADJUSTMENT)).toBe(true);

        // VERIFY_IDENTITY ACTION

        // isVerifyIdentityAllowed - false
        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.Closed };
        expect(containsAction(PlayerActionsEnum.VERIFY_IDENTITY)).toBe(false);
        comp.player = <PlayerDetailsResponse>{ verifiedIdentity: true };
        expect(containsAction(PlayerActionsEnum.VERIFY_IDENTITY)).toBe(false);

        // isVerifyIdentityAllowed - true
        comp.player = <PlayerDetailsResponse>{ verifiedIdentity: false, accountStatus: AccountStatusEnum.Activated };
        expect(containsAction(PlayerActionsEnum.VERIFY_IDENTITY)).toBe(true);
        comp.player = <PlayerDetailsResponse>{ verifiedIdentity: false, accountStatus: AccountStatusEnum.Confirmed };
        expect(containsAction(PlayerActionsEnum.VERIFY_IDENTITY)).toBe(true);

        // REJECT_IDENTITY ACTION

        // isRejectIdentityAllowed - false
        comp.player = <PlayerDetailsResponse>{ verifiedIdentity: false };
        expect(containsAction(PlayerActionsEnum.REJECT_IDENTITY)).toBe(false);

        [AccountStatusEnum.New, AccountStatusEnum.Closed, AccountStatusEnum.Blacklisted, AccountStatusEnum.Undefined].forEach(status => {
            comp.player = <PlayerDetailsResponse>{ verifiedIdentity: true, accountStatus: status };
            expect(containsAction(PlayerActionsEnum.REJECT_IDENTITY)).toBe(false);
        });

        // isRejectIdentityAllowed - true
        [
            AccountStatusEnum.Confirmed, AccountStatusEnum.Activated, AccountStatusEnum.SecurityBlocked,
            AccountStatusEnum.Frozen, AccountStatusEnum.Deactivated, AccountStatusEnum.Blocked
        ].forEach(status => {
            comp.player = <PlayerDetailsResponse>{ verifiedIdentity: true, accountStatus: status };
            expect(containsAction(PlayerActionsEnum.REJECT_IDENTITY)).toBe(true);
        });

        // ADD BONUS

        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.Activated };
        expect(containsAction(PlayerActionsEnum.ADD_BONUS)).toBe(true);

        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.New };
        expect(containsAction(PlayerActionsEnum.ADD_BONUS)).toBe(false);

        // Has canBet set to true
        comp.player = <PlayerDetailsResponse>{ accountStatus: AccountStatusEnum.Blocked };
        expect(containsAction(PlayerActionsEnum.ADD_BONUS)).toBe(true);

    });
});
