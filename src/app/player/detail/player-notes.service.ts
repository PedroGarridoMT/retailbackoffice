import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { ConfigService } from "../../core/config.service";
import { PlayerNote, NewPlayerNoteRequest, PatchDocument, ApiResponse } from "../model/models";
import 'rxjs/add/operator/map';
import { HelperService } from "../../shared/helper.service";


@Injectable()
export class PlayerNotesService {
    APIResources;

    constructor(private http: Http, private config: ConfigService) {
        this.APIResources = {
            playerNote: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/notes/{noteId}',
            playerNotes: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/notes'
        };
    }

    /**
     * Get a list of notes associated to a player, given his account code.
     * @param {string} accountCode
     * @returns {Observable<PlayerDocument>}
     */
    public getPlayerNotes(accountCode: string): Observable<Array<PlayerNote>> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerNotes.');
        }

        return this.http
            .get(this.APIResources.playerNotes.replace('{accountCode}', accountCode))
            .map((response: Response) => response.json());
    }


    /**
     *
     * @param {string} accountCode
     * @param {string} noteText
     */
    public createPlayerNote(accountCode: string, noteText: string) {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling createPlayerNote.');
        }

        let addNoteRequest: NewPlayerNoteRequest = { text: noteText };

        return this.http
            .post(this.APIResources.playerNotes.replace('{accountCode}', accountCode), addNoteRequest)
            .map(response => HelperService.getApiResponse(response))
            .catch(HelperService.handleError)
    }

    /**
     * Deletes an existing note
     * @param {string} accountCode
     * @param {number} noteId
     * @returns {Observable<any | any>}
     */
    public deletePlayerNote(accountCode: string, noteId: number) {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling deletePlayerNote.');
        }
        if (!noteId) {
            throw new Error('Required parameter noteId was null or undefined when calling deletePlayerNote.');
        }
        let deleteNoteUrl = this.APIResources.playerNote.replace('{accountCode}', accountCode).replace('{noteId}', noteId);

        return this.http.delete(deleteNoteUrl)
            .map(response => HelperService.getApiResponse(response))
            .catch(HelperService.handleError)
    }

    /**
     * Edit an existing note.
     * @param {string} accountCode
     * @param {number} noteId
     * @param {string} noteText
     * @returns {Observable<ApiResponse>}
     */
    public editPlayerNote(accountCode: string, noteId: number, noteText: string): Observable<ApiResponse> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling editPlayerNote.');
        }
        if (!noteId) {
            throw new Error('Required parameter noteId was null or undefined when calling editPlayerNote.');
        }

        let noteUrl = this.APIResources.playerNote.replace('{accountCode}', accountCode).replace('{noteId}', noteId);
        let patchParams: Array<PatchDocument> = [{
            op: PatchDocument.OpEnum.Replace,
            path: "/text",
            value: noteText
        }];

        return this.http.patch(noteUrl, patchParams)
            .map(response => HelperService.getApiResponse(response))
            .catch(HelperService.handleError)
    }

}
