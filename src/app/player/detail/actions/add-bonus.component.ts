import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { CurrencyService } from "./../../../shared/currency.service";
import { ApplicableBonus, AddPlayerBonusRequest } from "../../model/models";

export enum PlayerAddBonusStepEnum { Select, Confirm }

@Component({
    templateUrl: "./add-bonus.component.html",
    styleUrls: ["./add-bonus.component.scss"]
})
export class PlayerAddBonusComponent implements OnInit {

    @Input() public applicableBonuses: ApplicableBonus[];
    @Output() public confirmationClick = new EventEmitter<AddPlayerBonusRequest>();

    // NgModels
    public selectedBonus: string;
    public selectedBonusAmount: number;
    public selectedReason: string;

    // Enums
    public playerAddBonusStepEnum = PlayerAddBonusStepEnum;
    public currentStep: PlayerAddBonusStepEnum = PlayerAddBonusStepEnum.Select;

    public currencyCode: string;
    public confirmationMsg: string;

    constructor(public activeModal: NgbActiveModal, private currencyService: CurrencyService, private translate: TranslateService) { }

    public ngOnInit(): void {
        this.applicableBonuses = this.applicableBonuses || [];
        this.currencyCode = this.currencyService.getCode();
    }

    public nextStep(): void {
        const bonusName: string = this.applicableBonuses.find(b => b.bonusKey === this.selectedBonus).displayName;
        const amount: string = this.currencyService.formatAmountWithCurrency(this.selectedBonusAmount);
        this.confirmationMsg = this.translate.instant("players.bonuses.addBonus.confirmationMsg", {amount, bonusName});
        this.currentStep = PlayerAddBonusStepEnum.Confirm;
    }

    public prevStep(): void {
        this.currentStep = PlayerAddBonusStepEnum.Select;
        this.confirmationMsg = null;
    }

    public onConfirmationClick(): void {
        const addPlayerBonusRequest: AddPlayerBonusRequest = {
            description: this.selectedReason,
            amount: this.selectedBonusAmount,
            bonusKey: this.selectedBonus
        };
        this.confirmationClick.next(addPlayerBonusRequest);
    }

    public canDoAssignment(): boolean {
        return !!this.selectedBonus && this.selectedBonusAmount > 0 && !!this.selectedReason && this.selectedReason.trim().length >= 10;
    }
}
