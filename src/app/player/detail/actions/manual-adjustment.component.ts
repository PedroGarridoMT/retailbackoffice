import { Component, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { TranslateService } from "@ngx-translate/core";
import { LoaderService } from "../../../shared/loader.service";
import { TransactionService } from "../../../transaction/transaction.service";
import {
    ApiResponse,
    BaseResultsCode,
    ManualTransactionRequest,
    ModelError,
    OperationTypeForPaymentMethodEnum,
    PaymentMethodEnum,
    PlayerDetailsResponse,
    TransactionTypeEnum
} from "../../model/models";
import { PASConfigService } from "../../../shared/pas-config.service";
import { CurrencyService } from "../../../shared/currency.service";
import { ResponseHelperService } from "../../../shared/response-helper.service";

export enum ManualAdjustmentStepEnum{SELECT, CONFIRM}

@Component({
    styleUrls: ["./manual-adjustment.component.scss"],
    templateUrl: "./manual-adjustment.component.html"
})
export class ManualAdjustmentComponent {
    private _player: PlayerDetailsResponse;
    private _balance: number;
    private _transactionTypesAllowed = [TransactionTypeEnum.Deposit, TransactionTypeEnum.Withdrawal];

    //Vars to populate UI elements
    transactionTypeList: Array<{ label, value }>;
    paymentMethodList: Array<{ label, value }>;
    currencyCode: string;

    //Vars binded to the UI elems
    selectedTransactionType: TransactionTypeEnum;
    txTypeEnum = TransactionTypeEnum;
    selectedPaymentMethod: PaymentMethodEnum = null;
    selectedAmount: number;
    selectedReason: string;
    invalidAmountMsg: string;
    amountTouched: boolean = false;
    currentStep: ManualAdjustmentStepEnum = ManualAdjustmentStepEnum.SELECT;
    stepEnum = ManualAdjustmentStepEnum;
    summaryMessageOperation: any = null;
    summaryMessageResult: any = null;
    errorMessage: string = null;


    @Input()
    set playerBalance(value: number) {
        this._balance = value;
        if (value == 0) {
            this.selectedTransactionType = TransactionTypeEnum.Deposit;
        }
    }

    get playerBalance(): number {
        return this._balance;
    }

    @Input()
    set player(value: PlayerDetailsResponse) {
        this._player = value;
    }

    get player(): PlayerDetailsResponse {
        return this._player;
    }


    constructor(public activeModal: NgbActiveModal,
                private enumsService: PASEnumsService,
                private transactionService: TransactionService,
                private pasConfigService: PASConfigService,
                private responseHelper: ResponseHelperService,
                private translate: TranslateService,
                private loader: LoaderService,
                private currencyService: CurrencyService) {


        this.currencyCode = this.currencyService.getCode();

        this.updatePaymentMethodsList();

        //Get transaction types string names to translate them
        let txTypesAllowed = this._transactionTypesAllowed.map(item => TransactionTypeEnum[item]);
        this.enumsService.getTranslatedList('enum.transactionType.', txTypesAllowed).subscribe(txTypesTranslated => {
            this.transactionTypeList = txTypesTranslated;
        });
    }

    updatePaymentMethodsList() {
        this.pasConfigService
            .getPaymentMethods(OperationTypeForPaymentMethodEnum.Command, this.selectedTransactionType)
            .subscribe(paymentMethods => {
                this.enumsService
                    .getTranslatedList('enum.paymentMethod.', paymentMethods)
                    .subscribe(paymentMethodsTranslated => {
                        this.paymentMethodList = paymentMethodsTranslated;

                        //if the current selected payment method does not exist for the current transaction type, reset it.
                        if (!this.isPaymentMethodAllowed()) {
                            this.selectedPaymentMethod = null;
                        }
                    });
            });
    }

    onAmountKeyPressed() {
        this.amountTouched = true;
    }

    isInvalidAmount(): boolean {
        let intAmount = parseInt(this.selectedAmount + "", 10);

        if (this.amountTouched && (isNaN(intAmount) || intAmount < 0)) {
            this.invalidAmountMsg = this.translate.instant('players.detail.manualAdjustment.msgInvalidAmount');
            return true;
        }
        if (this.selectedTransactionType == TransactionTypeEnum.Withdrawal && this.selectedAmount > this.playerBalance) {
            this.invalidAmountMsg = this.translate.instant('players.detail.manualAdjustment.msgMaxAmountForWithdrawals',
                { amount: this.currencyService.formatAmountWithCurrency(this.playerBalance) });
            return true;
        }

        this.invalidAmountMsg = null;
        return false;
    }

    canDoManualAdjustment(): boolean {
        return this.selectedTransactionType && this.selectedPaymentMethod && this.selectedAmount
            && this.selectedReason && this.selectedReason.trim().length >= 10 && !this.invalidAmountMsg
    }

    nextStep() {
        this.currentStep = ManualAdjustmentStepEnum.CONFIRM;

        let translatedVerb = this.translate.instant('players.detail.manualAdjustment.deposit');
        let resultingBalance = this.playerBalance + this.selectedAmount;

        if (this.selectedTransactionType == TransactionTypeEnum.Withdrawal) {
            translatedVerb = this.translate.instant('players.detail.manualAdjustment.withdraw');
            resultingBalance = this.playerBalance - this.selectedAmount;
        }
        this.summaryMessageOperation = {
            verb: translatedVerb,
            amount: this.currencyService.formatAmountWithCurrency(this.selectedAmount),
            paymentMethod: this.paymentMethodList.find(item => item.value == this.selectedPaymentMethod).label
        };
        this.summaryMessageResult = { newAmount: this.currencyService.formatAmountWithCurrency(resultingBalance) }
    }

    previousStep() {
        this.currentStep = ManualAdjustmentStepEnum.SELECT;
        this.errorMessage = null;
    }

    onConfirmManualAdjustment() {
        this.loader.show();
        let request: ManualTransactionRequest = {
            transactionType: this.selectedTransactionType,
            paymentMethod: this.selectedPaymentMethod,
            amount: this.selectedAmount,
            reason: this.selectedReason.trim()
        };
        this.transactionService
            .manualAdjustment(this.player.accountCode, request)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe((response: ApiResponse) => {
                    if (response.statusCode == BaseResultsCode.Ok) {
                        this.activeModal.close(response.statusCode);
                        this.responseHelper.responseHandler(response.statusCode, 'players.detail.manualAdjustment.response');
                    }
                },
                (error: ApiResponse) => {
                    this.handleErrorResponse(error);
                });
    }

    private handleErrorResponse(response: ApiResponse) {
        if (response.statusCode == BaseResultsCode.Unprocessable) {
            switch (response.error.errorCode) {
                case ModelError.ErrorCodeEnum.SelfExcluded:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.selfExcluded');
                    break;

                case ModelError.ErrorCodeEnum.AccountStatusForbidsDeposits:
                case ModelError.ErrorCodeEnum.AccountStatusForbidsWithdrawals:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.accountStatus');
                    break;

                case ModelError.ErrorCodeEnum.WithdrawalsNotAllowedWithUnverifiedDocuments:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.unverifiedDocumentation');
                    break;

                case ModelError.ErrorCodeEnum.NotEnoughFunds:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.funds');
                    break;

                case ModelError.ErrorCodeEnum.DailyDepositLicenseeLimitExceeded:
                case ModelError.ErrorCodeEnum.WeeklyDepositLicenseeLimitExceeded:
                case ModelError.ErrorCodeEnum.MonthlyDepositLicenseeLimitExceeded:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.playerDepositLimitExceeded');
                    break;

                case ModelError.ErrorCodeEnum.DailyWithdrawalLicenseeLimitExceeded:
                case ModelError.ErrorCodeEnum.WeeklyWithdrawalLicenseeLimitExceeded:
                case ModelError.ErrorCodeEnum.MonthlyWithdrawalLicenseeLimitExceeded:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.playerWithdrawalLimitExceeded');
                    break;

                case ModelError.ErrorCodeEnum.DailyDepositPlayerAutoLimitExceeded:
                case ModelError.ErrorCodeEnum.WeeklyDepositPlayerAutoLimitExceeded:
                case ModelError.ErrorCodeEnum.MonthlyDepositPlayerAutoLimitExceeded:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.operatorDepositLimitExceeded');
                    break;

                case ModelError.ErrorCodeEnum.DailyWithdrawalPlayerAutoLimitExceeded:
                case ModelError.ErrorCodeEnum.WeeklyWithdrawalPlayerAutoLimitExceeded:
                case ModelError.ErrorCodeEnum.MonthlyWithdrawalPlayerAutoLimitExceeded:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.operatorWithdrawalLimitExceeded');
                    break;

                case ModelError.ErrorCodeEnum.AmountBelowTransactionTypeLimit:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.amountBelowTransactionTypeLimit');
                    break;

                case ModelError.ErrorCodeEnum.AmountOverTransactionTypeLimit:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.amountOverTransactionTypeLimit');
                    break;

                default:
                    this.errorMessage = this.translate.instant('players.detail.manualAdjustment.response.badRequest');
                    break;
            }
        }
        else {
            this.errorMessage = this.translate.instant('common.response.internalServerError');
        }
    }

    private isPaymentMethodAllowed(): boolean {
        if (this.selectedPaymentMethod) {
            let isAllowed = this.paymentMethodList.some(item => {
                return item.value == this.selectedPaymentMethod
            });
            return isAllowed;
        }
        return true;
    }
}
