import { Component, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import {
    AccountStatusChangeReasonEnum,
    AccountStatusEnum,
    PlayerDetailsResponse,
    StatusPermissions,
    ChangeStatusOperation
} from "../../model/models";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { TranslateService } from "@ngx-translate/core";
import { LoaderService } from "../../../shared/loader.service";
import { ResponseHelperService } from "../../../shared/response-helper.service";
import { ChangeStatusEnum } from "../../../shared/model/models";
import {
    NotificationTypeEnum,
    TableColumn,
    TableRow,
    TableColumnType,
    TableColumnVisibilityEnum
} from "mt-web-components";
import { BaseResultsCode } from "../../../shared/model/base-results-code.enum";
import { getDefaultDateFormat } from '../../../shared/date-utils';

export enum ChangeStatusStepEnum { SELECT, CONFIRM }

@Component({
    styleUrls: ["./player-status-change.component.scss"],
    templateUrl: "./player-status-change.component.html"
})
export class PlayerStatusChangeComponent {
    private _player: PlayerDetailsResponse;
    //Vars to populate UI elements
    statusChangeReasons: Array<{ label: string, value: string }>;

    //Vars binded to the UI elems
    selectedStatus: AccountStatusEnum;
    selectedChangeReason: AccountStatusChangeReasonEnum;
    reasonFieldVisible: boolean;
    selfExclusionSelected: boolean = false;
    selfExclusionDate: Date;
    selfExclusionMinDate: Date = new Date();
    tableRows: any[] = [];
    tableColumns = [
        {
            type: TableColumnType.HTML,
            id: "selfExclusionStatusTemplate",
            label: this.translate.instant("players.entity.accountStatus"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }, {
            type: TableColumnType.DATA,
            id: "date",
            label: this.translate.instant("players.selfExclusion.selfExclusionDateEnd"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }
    ];

    //Vars to handle the current step of the status change
    currentStep: ChangeStatusStepEnum = ChangeStatusStepEnum.SELECT;
    changeStatusStepEnum = ChangeStatusStepEnum;

    //Vars to show the proper message to the user.
    confirmationMessageTemplate: any = null;
    targetStatusTranslated: any;
    targetStatusPermissions: any;

    @Input()
    set player(value: PlayerDetailsResponse) {
        this._player = value;
    }

    get player(): PlayerDetailsResponse {
        return this._player;
    }

    @Input() selfExclusionAvailable: boolean;
    @Input() hasMoney: boolean;
    @Input() statusPermissionsList: Array<StatusPermissions>;
    @Input() targetStates: Array<AccountStatusEnum> = [];
    //Vars to return data to the main component
    @Output() public confirmationClick = new EventEmitter<ChangeStatusOperation>();

    //Vars for templates defined in the html file to show different messages to the user according to the target status selected.
    @ViewChild("confirmationMessageClose") confirmationMessageClose;
    @ViewChild("confirmationMessageDeactivate") confirmationMessageDeactivate;
    @ViewChild("confirmationMessageBlock") confirmationMessageBlock;
    @ViewChild("confirmationMessageActivate") confirmationMessageActivate;
    @ViewChild("confirmationMessageRevoke") confirmationMessageRevoke;
    @ViewChild("confirmationMessageSelfExclude") confirmationMessageSelfExclude;
    @ViewChild("confirmationMessageGeneric") confirmationMessageGeneric;
    @ViewChild('selfExclusionStatusTemplate') selfExclusionStatusTemplate;

    constructor(public activeModal: NgbActiveModal,
                private enumsService: PASEnumsService,
                private responseHelper: ResponseHelperService,
                private translate: TranslateService,
                private loader: LoaderService) {

        this.enumsService.getAccountStatusReasonTranslatedList().subscribe(accountStatusReasons => {
            this.statusChangeReasons = accountStatusReasons;
        });
    }

    translateStatus(status) {
        return this.enumsService.translateEnumValue('accountStatus', status);
    }

    onSelfExcludedSelection() {
        this.selfExclusionSelected = true;
        this.selectedStatus = null;
        this.confirmationMessageTemplate = this.confirmationMessageSelfExclude;
    }


    /**
     *
     * @param status
     */
    onStatusSelected(status) {
        let currentStatusPermissions = this.statusPermissionsList.find(item => item.status == status);
        this.targetStatusTranslated = { status: this.translateStatus(status) };
        this.selfExclusionSelected = false;
        if (this._player.selfExcluded && status == this._player.accountStatus) {
            this.confirmationMessageTemplate = this.confirmationMessageRevoke;
            this.targetStatusPermissions = this.getStatusPermissionsMessage(currentStatusPermissions, true);
        }
        else {
            switch (status) {
                case AccountStatusEnum.Closed:
                    this.confirmationMessageTemplate = this.confirmationMessageClose;
                    break;
                case AccountStatusEnum.Blocked:
                    this.confirmationMessageTemplate = this.confirmationMessageBlock;
                    this.targetStatusPermissions = this.getStatusPermissionsMessage(currentStatusPermissions, false);
                    break;
                case AccountStatusEnum.Deactivated:
                    this.confirmationMessageTemplate = this.confirmationMessageDeactivate;
                    this.targetStatusPermissions = this.getStatusPermissionsMessage(currentStatusPermissions, false);
                    break;
                case AccountStatusEnum.Activated:
                    this.confirmationMessageTemplate = this.confirmationMessageActivate;
                    this.targetStatusPermissions = this.getStatusPermissionsMessage(currentStatusPermissions, true);
                    break;
                default:
                    this.confirmationMessageTemplate = this.confirmationMessageGeneric;
                    break;
            }
        }
    }

    /**
     * onFirstStepConfirmed
     */
    onFirstStepConfirmed() {
        this.currentStep = ChangeStatusStepEnum.CONFIRM;
        //Reason is hidden when the player is self-excluded or when it is changing status from "In process"(Confirmed) to "Activated"
        this.reasonFieldVisible = !this._player.selfExcluded && !(this._player.accountStatus == AccountStatusEnum.Confirmed && this.selectedStatus == AccountStatusEnum.Activated)
            && !this.selfExclusionSelected;

        if (this.selfExclusionSelected && this.selfExclusionDate) {
            this.tableRows.splice(0);
            this.tableRows.push({
                selfExclusionStatusTemplate: this.selfExclusionStatusTemplate,
                date: getDefaultDateFormat(this.selfExclusionDate)
            });
        }
    }

    /**
     * onConfirmStatusChange
     */
    onConfirmStatusChange() {
        this.confirmationClick.next({
            accountCode: this.player.accountCode,
            selectedStatus: this.selectedStatus,
            selectedChangeReason: this.selectedChangeReason,
            selfExclusion: this.selfExclusionSelected,
            selfExclusionEndDate: this.selfExclusionDate
        });
    }

    /**
     * Returns a translated, comma separated list of permissions
     * @param statusPerm
     * @param allowedPermissions. When this value is true, the permissions taken will be those who have a truth value.
     *        Otherwise, the permissions taken are those with a false value.
     * @returns {{permissionsMsg: string}}
     */
    private getStatusPermissionsMessage(statusPerm: StatusPermissions, allowedPermissions: boolean) {
        let msg: Array<string> = [];

        if (allowedPermissions) {
            if (statusPerm.canLogin) {
                msg.push(this.translate.instant('players.permissions.canLogin'));
            }
            if (statusPerm.canBet) {
                msg.push(this.translate.instant('players.permissions.canBet'));
            }
            if (statusPerm.canDeposit) {
                msg.push(this.translate.instant('players.permissions.canDeposit'));
            }
            if (statusPerm.canWithdraw) {
                msg.push(this.translate.instant('players.permissions.canWithdraw'));
            }
            if (statusPerm.canChangePassword) {
                msg.push(this.translate.instant('players.permissions.canChangePassword'));
            }
        }
        else {
            if (!statusPerm.canLogin) {
                msg.push(this.translate.instant('players.permissions.canLogin'));
            }
            if (!statusPerm.canBet) {
                msg.push(this.translate.instant('players.permissions.canBet'));
            }
            if (!statusPerm.canDeposit) {
                msg.push(this.translate.instant('players.permissions.canDeposit'));
            }
            if (!statusPerm.canWithdraw) {
                msg.push(this.translate.instant('players.permissions.canWithdraw'));
            }
            if (!statusPerm.canChangePassword) {
                msg.push(this.translate.instant('players.permissions.canChangePassword'));
            }
        }

        return { permissionsMsg: msg.join(', ') };
    }

    canShowTargetStatus(status: AccountStatusEnum) {
        return !(status == AccountStatusEnum.Closed && this.hasMoney);
    }
}
