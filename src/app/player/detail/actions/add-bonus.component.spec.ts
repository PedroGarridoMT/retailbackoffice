import { NO_ERRORS_SCHEMA, DebugElement } from "@angular/core";
import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { CurrencyService } from "../../../shared/currency.service";
import { getDefaultDateTimeFormat } from "../../../shared/date-utils";
import { PlayerAddBonusComponent, PlayerAddBonusStepEnum } from "./add-bonus.component";
import { ApplicableBonus, AddPlayerBonusRequest } from "../../model/models";

describe("PlayerAddBonusComponent", () => {

    const currencyCode: string = "COP";

    let ngbActiveModalSpy: jasmine.SpyObj<NgbActiveModal>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;

    let fixture: ComponentFixture<PlayerAddBonusComponent>;
    let comp: PlayerAddBonusComponent;
    let de: DebugElement;

    beforeEach(async(() => {

        ngbActiveModalSpy = jasmine.createSpyObj<NgbActiveModal>("NgbActiveModal", ["close"]);
        currencyServiceSpy = jasmine.createSpyObj<CurrencyService>("CurrencyService", ["formatAmountWithCurrency", "getCode"]);

        TestBed.configureTestingModule({
            declarations: [PlayerAddBonusComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: NgbActiveModal, useValue: ngbActiveModalSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerAddBonusComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;

        currencyServiceSpy.getCode.and.returnValue(currencyCode);
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should have currency code set up", () => {
        expect(comp.currencyCode).toBe(currencyCode);
    });

    it("should take user to the next step", () => {
        comp.applicableBonuses = [{ bonusKey: "bonusKey", displayName: "displayName" }] as ApplicableBonus[];
        comp.selectedBonus = "bonusKey";
        comp.nextStep();
        expect(comp.currentStep).toBe(PlayerAddBonusStepEnum.Confirm);
        expect(comp.confirmationMsg).toBe("players.bonuses.addBonus.confirmationMsg");
    });

    it("should take user to the previous step", () => {
        comp.prevStep();
        expect(comp.currentStep).toBe(PlayerAddBonusStepEnum.Select);
        expect(comp.confirmationMsg).toBe(null);
    });

    it("should emit add player bonus request on confirmation click", () => {
        const reason: string = "Reason";
        const amount: number = 10;
        const bonusKey: string = "bonusKey";
        comp.selectedBonus = bonusKey;
        comp.selectedBonusAmount = amount;
        comp.selectedReason = reason;

        const addPlayerBonusRequest: AddPlayerBonusRequest = { description: reason, bonusKey, amount };

        comp.confirmationClick.subscribe((event: AddPlayerBonusRequest) => {
            expect(event).toEqual(addPlayerBonusRequest);
        });

        comp.onConfirmationClick();
    });

    it("should return true user can assign bonus to a player", () => {
        comp.selectedBonusAmount = 10;
        comp.selectedReason = "An informed, data-driven, relationship reenergizes the clients."
        comp.selectedBonus = "bonusKey";
        expect(comp.canDoAssignment()).toBe(true);
    });

    it("should return false user can not assign bonus to a player", () => {
        comp.selectedBonusAmount = 10;
        comp.selectedReason = "a                    ";
        comp.selectedBonus = "bonusKey";
        expect(comp.canDoAssignment()).toBe(false);
    });

});
