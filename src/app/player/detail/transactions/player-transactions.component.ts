import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Subscription } from "rxjs";
import { Observable } from "rxjs/Observable";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    FilterToValueMap,
    FilterType,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals,
} from "mt-web-components";
import { getDefaultDateTimeFormat, getISOString } from "../../../shared/date-utils";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { CurrencyService } from "../../../shared/currency.service";
import { PASConstants } from "../../../shared/constant";
import { PlayerService } from "../../player.service";
import { LoaderService } from "../../../shared/loader.service";
import { TransactionService } from "../../../transaction/transaction.service";
import { PlayerDetailService, PlayerViewEnum } from "./../player-detail.service";
import { PASConfigService } from "../../../shared/pas-config.service";
import { OperationTypeForPaymentMethodEnum, TransactionSummary, DefaultSearchRequest, DownloadableFile } from "../../model/models";
import { TransactionSummaryExtended } from "../../../transaction/model/models";
import { AppRoutes } from "../../../app-routes";
import { BaseSearchComponent } from "../../../shared/base-search.component";
import { downloadFileByDataURI, getMimeTypeByExtension } from "../../../shared/utils";
import { ResponseHelperService } from "../../../shared/response-helper.service";

@Component({
    templateUrl: "./player-transactions.component.html",
    styleUrls: ["./player-transactions.component.scss"]
})
export class PlayerTransactionsComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    playerAccountCode: string;

    // UI attributes
    firstSearchDone: boolean = false;
    errorInSearch: boolean = false;
    filtersLoaded: boolean = false;

    // mt-table configuration
    tableColumns: TableColumn[];
    tableRows: TransactionSummary[];
    tableLiterals: TableLiterals;
    tableMaxColumnWidth = 250;

    // mt-basic-filters configuration
    filterBarLiterals: BasicFilterBarLiterals;
    filters: any[];
    transactionTypeList: any[];
    paymentMethodList: any[];
    gameTypeList: any[];
    moneyTypeList: any[];
    @ViewChild("filtersBar") filtersBar: BasicFilterBarComponent;

    constructor(private transactionService: TransactionService,
        private playerDetailService: PlayerDetailService,
        private pasConfigService: PASConfigService,
        private translate: TranslateService,
        private enumsService: PASEnumsService,
        private currencyService: CurrencyService,
        private loader: LoaderService,
        private router: Router,
        private responseHelper: ResponseHelperService,
        public route: ActivatedRoute) {

        super(route);
    }

    ngOnInit(): void {
        this.playerDetailService.setActiveView(PlayerViewEnum.PLAYER_TRANSACTIONS);
        this.resetPagingParams();

        this._subscriptions.push(
            this.route.parent.params.subscribe((params: Params) => {
                this.playerAccountCode = params["accountCode"];
                this.firstSearchDone = false;
                this.init();
            })
        );
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    private init() {
        this.loader.show();
        return Observable
            .forkJoin(
                this.pasConfigService.getGameTypes(),
                this.pasConfigService.getPaymentMethods(OperationTypeForPaymentMethodEnum.Query),
                this.pasConfigService.getTransactionTypes())
            .finally(() => this.loader.hide())
            .subscribe((res) => {
                this.enumsService.getTranslatedList("enum.gameType.", res[0]).subscribe(gameTypesTranslated => {
                    this.gameTypeList = gameTypesTranslated;
                });

                this.enumsService.getTranslatedList("enum.paymentMethod.", res[1]).subscribe(paymentMethodsTranslated => {
                    this.paymentMethodList = paymentMethodsTranslated;
                });

                this.enumsService.getTranslatedList("enum.transactionType.", res[2]).subscribe(transactionTypeListTranslated => {
                    this.transactionTypeList = transactionTypeListTranslated;
                });

                this.enumsService.getMoneyTypeTranslatedList().subscribe(moneyTypes => {
                    this.moneyTypeList = moneyTypes;
                });

                this.generateTable();
                this.generateFilterForm();
                this.manageUrlParams();
                this.filtersLoaded = true;
            });
    }

    /**
     * If the URL contains a search, prepare the necessary objects to compose the search and then, launch it
     */
    private manageUrlParams() {
        if (this.thereAreQueryParams()) {
            this.prepareDataWithQueryParams();
            this.searchData();
        }
    }

    /**
     * Generates filter form component: mt-basic-filters
     */
    private generateFilterForm() {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "transactionDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("transactions.entity.transactionDate"),
                shownByDefault: true,
                showTimeRange: true,
                value: this.getDateRangeFromParams("transactionDate"),
                maxDate: new Date()
            },
            {
                id: "transactionId",
                type: FilterType.NUMBER,
                label: this.translate.instant("transactions.entity.transactionId"),
                shownByDefault: true,
                value: this.getValueFromParams("transactionId")
            },
            {
                id: "amount",
                type: FilterType.NUMBER_RANGE,
                label: this.translate.instant("transactions.entity.amount"),
                shownByDefault: true,
                value: this.getAmountRangeFromParams("amount")
            },
            {
                id: "moneyType",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.moneyType"),
                shownByDefault: true,
                options: this.moneyTypeList,
                value: this.getValueFromParams("moneyType")
            },
            {
                id: "transactionType",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.transactionType"),
                shownByDefault: true,
                options: this.transactionTypeList,
                value: this.getMultipleSelectionFromParams("transactionType")
            },
            {
                id: "paymentMethod",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.paymentMethod"),
                shownByDefault: true,
                options: this.paymentMethodList,
                value: this.getMultipleSelectionFromParams("paymentMethod")
            },
            {
                id: "gameType",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.gameType"),
                shownByDefault: true,
                options: this.gameTypeList,
                value: this.getMultipleSelectionFromParams("gameType")
            },
            {
                id: "providerTransactionId",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.providerTransactionId"),
                shownByDefault: true,
                value: this.getValueFromParams("providerTransactionId")
            },
            {
                id: "couponId",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.couponId"),
                shownByDefault: true,
                value: this.getValueFromParams("couponId")
            }
        ];
    }

    /**
     * Generates the table component with transactions columns: mt-table
     */
    private generateTable() {
        this.tableColumns = [
            {
                type: TableColumnType.DATA,
                id: "transactionId",
                label: this.translate.instant("transactions.entity.transactionId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "transactionDate",
                label: this.translate.instant("transactions.entity.transactionDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "amountString",
                label: this.translate.instant("transactions.entity.amount"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "moneyType",
                label: this.translate.instant("transactions.entity.moneyType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "transactionType",
                label: this.translate.instant("transactions.entity.transactionType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "paymentMethod",
                label: this.translate.instant("transactions.entity.paymentMethod"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "gameType",
                label: this.translate.instant("transactions.entity.gameType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "providerTransactionId",
                label: this.translate.instant("transactions.entity.providerTransactionId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "couponId",
                label: this.translate.instant("transactions.entity.couponId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('table.noResultsMessage'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        };
    }

    /**
     * Search Transactions Request
     */
    private searchData() {
        this.errorInSearch = false;

        this.loader.show();

        // valuesToSearch will contain extra params we don't need to send to backend
        // due to itemNavigatorComponent
        const valuesToSearch = Object.assign({}, this.valuesToSearch);
        delete valuesToSearch[PASConstants.SEARCH_REQUEST_PARAM];
        delete valuesToSearch[PlayerService.PREVIOUS_ROUTE_KEY];

        const filterCriteria = this.createTransactionFilterCriteria(valuesToSearch, true);

        this.transactionService.getPlayerTransactions(this.playerAccountCode, filterCriteria)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe(transactionResponse => {
                this.updateTransactionInfo(transactionResponse.items);
                this.pagingData = {
                    currentPage: this.pagingData.currentPage,
                    pageSize: this.pagingData.pageSize,
                    totalPages: transactionResponse.totalPages,
                    totalItems: transactionResponse.totalItems
                };
            }, (error) => {
                this.errorInSearch = true;
                this.resetPagingParams();
                console.error(error);
            });

        //Update URL with the new search criteria
        const queryParams: object = Object.assign({}, filterCriteria);
        this.queryParams = {};

        // But we need those extra params in url for itemNavigatorComponent, add them before router navigate action
        queryParams[PASConstants.SEARCH_REQUEST_PARAM] = this.route.snapshot.queryParamMap.get(PASConstants.SEARCH_REQUEST_PARAM);
        queryParams[PlayerService.PREVIOUS_ROUTE_KEY] = this.route.snapshot.queryParamMap.get(PlayerService.PREVIOUS_ROUTE_KEY);

        this.router.navigate(AppRoutes.playerTransactions(this.playerAccountCode), { queryParams });
    }

    /**
     * Mainly used for build request formatting dates
     * @param transactionSearchFilter
     * @param addPagingAndSortingData
     * @returns DefaultSearchRequest
     */
    private createTransactionFilterCriteria(transactionSearchFilter: any, addPagingAndSortingData: boolean): DefaultSearchRequest {
        let transactionFilterCriteria: any = transactionSearchFilter;

        if (transactionSearchFilter.transactionDate) {
            let transactionDateFrom = transactionSearchFilter.transactionDate.from;
            transactionFilterCriteria.transactionDateFrom = transactionDateFrom ? getISOString(transactionDateFrom) : null;

            let transactionDateTo = transactionSearchFilter.transactionDate.to;
            transactionFilterCriteria.transactionDateTo = transactionDateTo ? getISOString(transactionDateTo) : null;

            delete transactionFilterCriteria["transactionDate"];
        }

        if (transactionSearchFilter.amount) {
            transactionFilterCriteria.amountFrom = transactionSearchFilter.amount.from;
            transactionFilterCriteria.amountTo = transactionSearchFilter.amount.to;

            delete transactionFilterCriteria["amount"];
        }

        if (addPagingAndSortingData) {
            // Sorting is not available in backend.
            // return Object.assign({}, transactionFilterCriteria, this.getPaginationData(), this.getSortingData());
            return Object.assign({}, transactionFilterCriteria, this.getPaginationData());
        } else {
            return transactionFilterCriteria;
        }
    }

    // private getSortingData() {
    //     return {
    //         sortBy: "transactionDate",
    //         sortDirection: "DESC",
    //     }
    // }

    /**
     * It formats dates, amounts, enums, etc.
     */
    public updateTransactionInfo(transactions: any[]): void {
        transactions.map((transaction: TransactionSummaryExtended) => {
            transaction.transactionDate = transaction.transactionDate ? getDefaultDateTimeFormat(transaction.transactionDate) : null;
            transaction.transactionType = transaction.transactionType ? this.enumsService.translateEnumValue("transactionType", transaction.transactionType) : "";
            transaction.gameType = transaction.gameType ? this.enumsService.translateEnumValue("gameType", transaction.gameType) : "";
            transaction.paymentMethod = transaction.paymentMethod ? this.enumsService.translateEnumValue("paymentMethod", transaction.paymentMethod) : "";
            transaction.moneyType = transaction.moneyType ? this.enumsService.translateEnumValue("moneyType", transaction.moneyType) : "";
            transaction.amountString = this.currencyService.formatAmountWithCurrency(transaction.amount);
        });
        this.hideEmptyColumns(transactions);
    }

    public onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    public onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    public onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    public onClearBasicFilter() {
        this.valuesToSearch = null;
        this.queryParams = {};
        //Clean previous search results
        this.tableRows = [];
        this.resetPagingParams();
        //Reset UI
        this.errorInSearch = false;
        this.firstSearchDone = false;

        // Preserve those two params required by itemNavigatorComponent
        const queryParams: object = {
            [PASConstants.SEARCH_REQUEST_PARAM]: this.route.snapshot.queryParamMap.get(PASConstants.SEARCH_REQUEST_PARAM),
            [PlayerService.PREVIOUS_ROUTE_KEY]: this.route.snapshot.queryParamMap.get(PlayerService.PREVIOUS_ROUTE_KEY)
        };

        //Clear URL
        this.router.navigate(AppRoutes.playerTransactions(this.playerAccountCode), { queryParams });
    }

    onExportClick(columnNames: string[]) {
        this.loader.show();

        // valuesToSearch will contain extra params we don't need to send to backend
        // due to itemNavigatorComponent
        const valuesToSearch = Object.assign({}, this.valuesToSearch);
        delete valuesToSearch[PASConstants.SEARCH_REQUEST_PARAM];
        delete valuesToSearch[PlayerService.PREVIOUS_ROUTE_KEY];
        const filterCriteria = this.createTransactionFilterCriteria(valuesToSearch, false);

        const columns: string[] = columnNames.map(c => c.replace("String", "").replace("Link", ""));

        this.transactionService.getPlayerTransactionsReport(this.playerAccountCode, filterCriteria, columns)
            .finally(() => this.loader.hide())
            .subscribe((file: DownloadableFile) => {
                downloadFileByDataURI("data:" + getMimeTypeByExtension("csv") + ";base64," + file.content, file.fileName);
            }, (error) => {
                this.responseHelper.responseHandler(error.statusCode, "common.search.error.exportErrorMessage");
            });
    }

    /**
     * Checks if there are some columns with every null value and hides them
     * @param transactions
     */
    private hideEmptyColumns(transactions: TransactionSummary[]): void {
        const cols = this.tableColumns.splice(0);
        cols.forEach((column) => {
            if (transactions.every((row) => row[column.id] == null || row[column.id] == '-')) {
                column.visibility = TableColumnVisibilityEnum.HIDDEN;
            }
            else {
                column.visibility = TableColumnVisibilityEnum.VISIBLE;
            }
        });
        this.tableColumns = cols;
        this.tableRows = transactions;
        this.firstSearchDone = true;
    }
}
