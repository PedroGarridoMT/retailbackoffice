import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from "rxjs/Subscription";
import { TableColumn, TableColumnType, TableColumnVisibilityEnum, TableLiterals, TableActionButton, TableComponent, PermissionsService } from "mt-web-components";
import { BaseSearchComponent } from "../../../shared/base-search.component";
import { LoaderService } from "../../../shared/loader.service";
import { BonusService } from "./../player-bonus.service";
import { CurrencyService } from "./../../../shared/currency.service";
import { ApiResponse, BaseResultsCode, CancelBonusRequest, PermissionsCode, PlayerBonus } from "../../model/models";
import { PlayerBonusExtended } from "../../model/PlayerBonusExtended";
import { PlayerCancelBonusComponent } from "./actions/cancel-bonus.component";
import { getDefaultDateTimeFormat } from "../../../shared/date-utils";
import { ResponseHelperService } from "./../../../shared/response-helper.service";
import { PASEnumsService } from "../../../shared/index";
import { PlayerDetailService } from "../player-detail.service";

@Component({
    selector: "pas-player-active-and-pending-bonuses",
    templateUrl: "./active-and-pending-bonuses.component.html"
})
export class PlayerActiveAndPendingBonusesComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    public accountCode: string;
    public activeAndPendingBonuses: PlayerBonus[];

    // UI attributes
    public errorInSearch: boolean = false;

    // mt-table configuration
    public tableColumns: TableColumn[];
    public tableRows: PlayerBonusExtended[];
    public tableLiterals: TableLiterals;
    public rowSelectionMode: string = "single";
    public rowSelectionKey: string = "bonusKey";
    public enableRowSelection: boolean = false;
    public tableButtons: TableActionButton[] = [];
    @ViewChild("table") public table: TableComponent;

    private selectedBonusKeys: Array<string>;

    constructor(
        public route: ActivatedRoute,
        private loader: LoaderService,
        private translate: TranslateService,
        private bonusService: BonusService,
        private playerDetailService: PlayerDetailService,
        private currencyService: CurrencyService,
        private modalService: NgbModal,
        private responseHelper: ResponseHelperService,
        private enumsService: PASEnumsService,
        private permissionsService: PermissionsService
    ) {
        super(route);
    }

    public ngOnInit(): void {
        this._subscriptions.push(
            this.route.parent.params.subscribe((params: Params) => {
                this.accountCode = params["accountCode"];
                this.searchData(this.accountCode);
            }),
            this.playerDetailService.reloadBonusesSource$.subscribe((accountCode: string) => {
                this.searchData(accountCode);
            })
        );

        if(this.permissionsService.hasAccess(PermissionsCode.PlayerBonusCancel)) {
            this.tableButtons = [{
                id: 1,
                icon: "fa fa-trash",
                text: this.translate.instant("players.bonuses.cancelPlayerBonus.title"),
                disabled: true
            }];
            this.enableRowSelection = true;
        }
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    public openCancelBonusModal(bonusKey: string): void {
        const bonus: PlayerBonusExtended = this.tableRows.find(bonus => bonus.bonusKey === bonusKey);
        const accountCode: string = this.accountCode;
        const modalRef: NgbModalRef = this.modalService.open(PlayerCancelBonusComponent, {
            backdrop: "static",
            keyboard: false
        });

        modalRef.componentInstance.accountCode = this.accountCode;
        modalRef.componentInstance.bonus = bonus;
        modalRef.componentInstance.confirmationClick.subscribe((cancelBonusRequest: CancelBonusRequest) => {
            this.loader.show();
            this.bonusService.cancelPlayerActiveOrPendingBonus(accountCode, bonusKey, cancelBonusRequest)
                .finally(() => {
                    this.loader.hide();
                    modalRef.close();
                })
                .subscribe(
                    (responseCode: BaseResultsCode) => {
                        if(responseCode === BaseResultsCode.Ok) {
                            this.tableColumns.splice(0);
                            this.searchData(this.accountCode);
                        }
                        this.responseHelper.responseHandler(responseCode, "players.bonuses.cancelPlayerBonus.response");
                    },
                    (error: ApiResponse) => {
                        this.responseHelper.responseHandler(error.statusCode, "players.bonuses.cancelPlayerBonus.response");
                    }
                );
        });
    }

    public onSelectedRowsChange(event: any) {
        this.selectedBonusKeys = event;

        if(event && event.length === 1) {
            const bonus = this.tableRows.find(r => r.bonusKey === event[0]);
            this.tableButtons[0].disabled = !bonus || bonus.bonusBalance <= 0;
        } else {
            this.tableButtons[0].disabled = true;
        };
    }

    public onActionButtonClick(event: any) {
        if(!event || event.id !== 1 || !this.selectedBonusKeys || this.selectedBonusKeys.length !== 1) return;
        this.openCancelBonusModal(this.selectedBonusKeys[0]);
    }

    private clearSelectedBonusKeys(): void {
        this.tableButtons.forEach(tb => tb.disabled = true);
        this.selectedBonusKeys = [];
        this.table.clearSelectedRows();
    }

    /**
     * Search Player"s Active And Pending Bonuses
     */
    private searchData(accountCode: string) {
        this.errorInSearch = false;
        this.loader.show();

        this.bonusService
            .getPlayerActiveAndPendingBonuses(accountCode)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe(
                (playerBonuses: PlayerBonus[]) => {
                    this.tableRows = this.getFormattedPlayerBonuses(playerBonuses);
                    this.generateTable(this.tableRows);
                    this.clearSelectedBonusKeys();
                }, () => {
                    this.errorInSearch = true;
                }
            );
    }

    /**
      * Generates the table component with bonus columns: mt-table
      */
    private generateTable(playerBonuses: PlayerBonus[]) {
        this.tableColumns = [
            // General
            {
                type: TableColumnType.DATA,
                id: "displayName",
                label: this.translate.instant("players.bonuses.entity.name"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "translatedStatus",
                label: this.translate.instant("players.bonuses.entity.status"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "formattedInitialBonusAmountAwarded",
                label: this.translate.instant("players.bonuses.entity.initialBonusAmountAwarded"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "formattedBonusBalance",
                label: this.translate.instant("players.bonuses.entity.bonusBalance"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedDateOfAwarding",
                label: this.translate.instant("players.bonuses.entity.dateOfAwarding"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedDateOfActivation",
                label: this.translate.instant("players.bonuses.entity.dateOfActivation"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedDateOfClosing",
                label: this.translate.instant("players.bonuses.entity.dateOfClosing"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "daysToExpiration",
                label: this.translate.instant("players.bonuses.entity.daysToExpiration"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            // Money redemption
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "formattedMoneyRedemptionCurrentValue",
                label: this.translate.instant("players.bonuses.entity.moneyRedemptionCurrentValue"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedMoneyRedemptionRolloverPercent",
                label: this.translate.instant("players.bonuses.entity.moneyRedemptionRolloverPercent"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "formattedMoneyRedemptionTargetValue",
                label: this.translate.instant("players.bonuses.entity.moneyRedemptionTargetValue"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            // Bets redemption
            {
                type: TableColumnType.DATA,
                id: "formattedNumberOfBetsRedemptionCurrentValue",
                label: this.translate.instant("players.bonuses.entity.numberOfBetsRedemptionCurrentValue"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedBetsRedemptionRolloverPercent",
                label: this.translate.instant("players.bonuses.entity.betsRedemptionRolloverPercent"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedNumberOfBetsRedemptionTargetValue",
                label: this.translate.instant("players.bonuses.entity.numberOfBetsRedemptionTargetValue"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ].map((column) => {
            if (playerBonuses.every((row) => row[column.id] == null || row[column.id] == "-")) {
                column.visibility = TableColumnVisibilityEnum.HIDDEN;
            } else {
                column.visibility = TableColumnVisibilityEnum.VISIBLE;
            }
            return column;
        });

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('players.bonuses.activeAndPendingBonuses.noResults'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        }
    }

    private getFormattedPlayerBonuses(playerBonuses: PlayerBonus[]): PlayerBonusExtended[] {
        return playerBonuses.map((bonus: PlayerBonus) => {
            const formattedBonus: PlayerBonusExtended = Object.assign(<PlayerBonusExtended>{}, bonus);
            // General
            formattedBonus.formattedBonusBalance = this.currencyService.formatAmountWithCurrency(bonus.bonusBalance);
            formattedBonus.formattedInitialBonusAmountAwarded = this.currencyService.formatAmountWithCurrency(bonus.initialBonusAmountAwarded);
            formattedBonus.formattedDateOfAwarding = this.formatColumnDate(bonus.dateAwarded);
            formattedBonus.formattedDateOfActivation = this.formatColumnDate(bonus.dateActivated);
            formattedBonus.formattedDateOfClosing = this.formatColumnDate(bonus.dateClosed);
            // Money redemption
            formattedBonus.formattedMoneyRedemptionCurrentValue = this.formatAmount(bonus.moneyRedemptionCurrentValue);
            formattedBonus.formattedMoneyRedemptionTargetValue = this.formatAmount(bonus.moneyRedemptionTargetValue);
            const hasMoneyBonus: boolean = bonus.moneyRedemptionCurrentValue !== null && bonus.moneyRedemptionTargetValue > 0;
            const moneyRolloverPercent: string = Math.round(bonus.moneyRedemptionCurrentValue / bonus.moneyRedemptionTargetValue * 100) + "%";
            formattedBonus.formattedMoneyRedemptionRolloverPercent = hasMoneyBonus ? moneyRolloverPercent : "-";
            // Bets redemption
            formattedBonus.formattedNumberOfBetsRedemptionCurrentValue = this.formatColumnValue(bonus.numberOfBetsRedemptionCurrentValue);
            formattedBonus.formattedNumberOfBetsRedemptionTargetValue = this.formatColumnValue(bonus.numberOfBetsRedemptionTargetValue);
            const hasBetsBonus: boolean = bonus.numberOfBetsRedemptionCurrentValue !== null && bonus.numberOfBetsRedemptionTargetValue > 0;
            const betsRolloverPercent: string = Math.round(bonus.numberOfBetsRedemptionCurrentValue / bonus.numberOfBetsRedemptionTargetValue * 100) + "%";
            formattedBonus.formattedBetsRedemptionRolloverPercent = hasBetsBonus ? betsRolloverPercent : "-";
            // Additional
            formattedBonus.translatedStatus = bonus.status ? this.enumsService.translateEnumValue("bonusStatuses", bonus.status) : "-";
            return formattedBonus;
        });
    }

    private formatAmount(value: any): string {
        return value ? this.currencyService.formatAmountWithCurrency(value) : "-";
    }

    private formatColumnValue(value: any): string {
        return value !== null ? value : "-";
    }

    private formatColumnDate(date: string): string {
        return date !== null ? getDefaultDateTimeFormat(date) : "-";
    }
}
