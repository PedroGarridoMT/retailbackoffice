import { CUSTOM_ELEMENTS_SCHEMA, Component, DebugElement } from "@angular/core";
import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { DummyMtPermissionsDirective } from "../../../../test-util/dummy-mt-permissions.directive";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { PlayerBonusesComponent } from "./player-bonuses.component";
import { PlayerDetailService, PlayerViewEnum } from "../player-detail.service";
import { PermissionsCode } from "../../model/models";

describe("PlayerBonusesComponent", () => {

    let fixture: ComponentFixture<PlayerBonusesComponent>;
    let comp: PlayerBonusesComponent;
    let de: DebugElement;

    let playerDetailServiceSpy: jasmine.SpyObj<PlayerDetailService>;

    beforeEach(async(() => {

        playerDetailServiceSpy = jasmine.createSpyObj<PlayerDetailService>("PlayerDetailService", ["setActiveView"]);
        Object.defineProperty(playerDetailServiceSpy, "activeView$", { get: () => Observable.of(null) });

        TestBed.configureTestingModule({
            declarations: [PlayerBonusesComponent, DummyMtPermissionsDirective],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: PlayerDetailService, useValue: playerDetailServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerBonusesComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should have bonus permissions set up", () => {
        expect(comp.viewActiveAndPendingBonusesPermission).toEqual(PermissionsCode.PlayerActiveAndPendingBonuses)
    })

    it("should set correct active view", () => {
        expect(playerDetailServiceSpy.setActiveView).toHaveBeenCalledWith(PlayerViewEnum.PLAYER_BONUSES);
    })

});
