import { ActivatedRoute } from "@angular/router";
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { PlayerActiveAndPendingBonusesComponent } from "./active-and-pending-bonuses.component";
import { DummyMtPermissionsDirective } from "../../../../test-util/dummy-mt-permissions.directive";
import { PlayerDetailService } from "../player-detail.service";
import { BonusService } from "./../player-bonus.service";
import { CurrencyService } from "../../../shared/currency.service";
import { FakeModalService } from "../../../../test-util/fake-modal.service";
import { ResponseHelperService } from "../../../shared/response-helper.service";
import { PASEnumsService } from "../../../shared/index";
import { ActivatedRouteStub } from "../../../../test-util/fake-activated-route";
import { BaseResultsCode, CancelBonusRequest, PlayerBonus, ApiResponse } from "../../model/models";
import { TableColumnVisibilityEnum, TableComponent, PermissionsService } from "mt-web-components";
import { LoaderService } from "./../../../shared/loader.service";
import { ModalService } from "../../../shared/services/modal.service";

describe("PlayerActiveAndPendingBonusesComponent", () => {

    const accountCode: string = "e6313fd8c85be0bb";

    const reloadBonusesSource: Subject<string> = new Subject<string>();
    const reloadBonuses$: Observable<string> = reloadBonusesSource.asObservable();

    let playerDetailServiceSpy: jasmine.SpyObj<PlayerDetailService>;
    let loaderServiceSpy: jasmine.SpyObj<LoaderService>;
    let bonusServiceSpy: jasmine.SpyObj<BonusService>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;
    let responseHelperServiceSpy: jasmine.SpyObj<ResponseHelperService>;
    let enumsServiceSpy: jasmine.SpyObj<PASEnumsService>;
    let permissionsSerivceSpy: jasmine.SpyObj<PermissionsService>;

    let tableComponentSpy: jasmine.SpyObj<TableComponent>;

    let fixture: ComponentFixture<PlayerActiveAndPendingBonusesComponent>;
    let comp: PlayerActiveAndPendingBonusesComponent;
    let de: DebugElement;

    let fakeModalService: FakeModalService;
    let mockActivatedRoute: ActivatedRouteStub;
    let playerBonuses: PlayerBonus[];

    playerDetailServiceSpy = jasmine.createSpyObj<PlayerDetailService>("PlayerDetailService", ["setActiveView", "reloadBonuses", "reloadBonusesSource$"]);
    bonusServiceSpy = jasmine.createSpyObj<BonusService>("BonusService", ["cancelPlayerActiveOrPendingBonus", "getPlayerActiveAndPendingBonuses"]);
    loaderServiceSpy = jasmine.createSpyObj<LoaderService>("LoaderService", ["show", "hide"]);
    currencyServiceSpy = jasmine.createSpyObj<CurrencyService>("CurrencyService", ["formatAmountWithCurrency"]);
    responseHelperServiceSpy = jasmine.createSpyObj<ResponseHelperService>("ResponseHelperService", ["responseHandler"]);
    enumsServiceSpy = jasmine.createSpyObj<PASEnumsService>("PASEnumsService", ["translateEnumValue"]);
    permissionsSerivceSpy = jasmine.createSpyObj<PermissionsService>("PermissionsService", ["hasAccess"]);
    tableComponentSpy = jasmine.createSpyObj<TableComponent>("TableComponent", ["clearSelectedRows"]);
    mockActivatedRoute = new ActivatedRouteStub();

    Object.defineProperty(playerDetailServiceSpy, "reloadBonusesSource$", { get: () => reloadBonuses$ });

    beforeEach(async(() => {

        mockActivatedRoute.setParentParams({ accountCode });

        TestBed.configureTestingModule({
            declarations: [PlayerActiveAndPendingBonusesComponent, DummyMtPermissionsDirective],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            providers: [
                { provide: PlayerDetailService, useValue: playerDetailServiceSpy },
                { provide: LoaderService, useValue: loaderServiceSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy },
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal },
                { provide: PASEnumsService, useValue: enumsServiceSpy },
                { provide: BonusService, useValue: bonusServiceSpy },
                { provide: ResponseHelperService, useValue: responseHelperServiceSpy },
                { provide: ActivatedRoute, useValue: mockActivatedRoute },
                { provide: PermissionsService, useValue: permissionsSerivceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of([]));
        playerBonuses = [{
            bonusKey: "BONUSKEY",
            displayName: "Bonus Display Name",
            status: PlayerBonus.StatusEnum.Active,
            initialBonusAmountAwarded: 0,
            bonusBalance: 80,
            dateAwarded: "2016-12-31T23:00:00.000Z",
            dateActivated: "2016-12-31T23:00:00.000Z",
            dateClosed: "2016-12-31T23:00:00.000Z",
            daysToExpiration: 10,
            moneyRedemptionCurrentValue: 50,
            moneyRedemptionTargetValue: 400,
            numberOfBetsRedemptionCurrentValue: 2,
            numberOfBetsRedemptionTargetValue: 4
        }];

        fakeModalService = TestBed.get(NgbModal);

        fixture = TestBed.createComponent(PlayerActiveAndPendingBonusesComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;

        comp.table = tableComponentSpy;
        permissionsSerivceSpy.hasAccess.and.returnValue(true);
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should have cancel bonus action button and row selection enabled if user has permission", () => {
        permissionsSerivceSpy.hasAccess.and.returnValue(true);
        comp.ngOnInit();
        expect(comp.tableButtons.length).toEqual(1);
        expect(comp.enableRowSelection).toEqual(true);
    });

    it("should not have cancel bonus action button and row selection enabled if user has no permission", () => {
        permissionsSerivceSpy.hasAccess.and.returnValue(false);
        comp.ngOnInit();
        expect(comp.tableButtons.length).toEqual(0);
        expect(comp.enableRowSelection).toEqual(false);
    });

    it("should read account code from route params and search for player's active and pending bonuses", () => {
        comp.ngOnInit();
        expect(comp.accountCode).toBe(accountCode);
        expect(bonusServiceSpy.getPlayerActiveAndPendingBonuses).toHaveBeenCalledWith(accountCode);
    });

    it("should clear errors and show loader on search start", (() => {
        comp.ngOnInit();
        expect(comp.errorInSearch).toBe(false);
        expect(loaderServiceSpy.show).toHaveBeenCalled();
    }));

    it("should set table rows, columns, clear errors and hide loader on search success", () => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        comp.ngOnInit();
        expect(comp.errorInSearch).toBe(false);
        expect(loaderServiceSpy.hide).toHaveBeenCalled();
        expect(comp.tableRows.length).toBeGreaterThan(0);
    });

    it("should show error on search failure", () => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.throw("Error"));
        comp.ngOnInit();
        expect(comp.errorInSearch).toBe(true);
        expect(loaderServiceSpy.hide).toHaveBeenCalled();
    });

    it("should hide table columns with no values", async(() => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        comp.ngOnInit();
        let visibleColumns = comp.tableColumns.filter(c => c.visibility !== TableColumnVisibilityEnum.HIDDEN);
        expect(visibleColumns.length).toBe(9);
        const playerBonusesWithNullValue: PlayerBonus[] = playerBonuses.map(x => { x.dateClosed = null; return x; });
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonusesWithNullValue));
        comp.ngOnInit();
        visibleColumns = comp.tableColumns.filter(c => c.visibility !== TableColumnVisibilityEnum.HIDDEN);
        expect(visibleColumns.length).toBe(8);
    }));

    it("should show cancel player's bonus modal", (() => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        comp.ngOnInit();
        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            bonus: null,
            accountCode: null
        };
        comp.openCancelBonusModal(playerBonuses[0].bonusKey);
        expect(fakeModalService.componentInstance.bonus).toBe(comp.tableRows[0]);
        expect(fakeModalService.componentInstance.accountCode).toBe(accountCode);
    }));

    it("should cancel player's bonus on confirmation click in modal and show success notification", () => {
        tableComponentSpy.clearSelectedRows.calls.reset();
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.calls.reset();
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        bonusServiceSpy.cancelPlayerActiveOrPendingBonus.and.returnValue(Observable.of(BaseResultsCode.Ok));
        spyOn(fakeModalService, "close").and.returnValue(null);

        comp.ngOnInit();
        expect(bonusServiceSpy.getPlayerActiveAndPendingBonuses).toHaveBeenCalledWith(accountCode);
        comp.tableColumns = comp.tableColumns || [];

        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            bonus: null,
            accountCode: null
        };
        comp.openCancelBonusModal(playerBonuses[0].bonusKey);
        const cancelBonusRequest: CancelBonusRequest = { description: "Description", amountToDeduct: 10 };
        fakeModalService.componentInstance.confirmationClick.next(cancelBonusRequest);

        expect(loaderServiceSpy.show).toHaveBeenCalled();
        // Finally
        expect(loaderServiceSpy.hide).toHaveBeenCalled();
        expect(fakeModalService.close).toHaveBeenCalled();
        // cancelPlayerActiveOrPendingBonus Request
        expect(bonusServiceSpy.cancelPlayerActiveOrPendingBonus).toHaveBeenCalledWith(accountCode, playerBonuses[0].bonusKey, cancelBonusRequest);
        // cancelPlayerActiveOrPendingBonus Success
        expect(bonusServiceSpy.getPlayerActiveAndPendingBonuses).toHaveBeenCalledTimes(2);
        expect(comp.table.clearSelectedRows).toHaveBeenCalled();
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalledWith(BaseResultsCode.Ok, "players.bonuses.cancelPlayerBonus.response");
    });

    it("should try to cancel player's bonus on confirmation click in modal and show error notification if it fails", () => {
        tableComponentSpy.clearSelectedRows.calls.reset();
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.calls.reset();
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        const apiResponse: ApiResponse = { statusCode: BaseResultsCode.InternalServerError, error: null };
        bonusServiceSpy.cancelPlayerActiveOrPendingBonus.and.returnValue(Observable.throw(apiResponse));
        spyOn(fakeModalService, "close").and.returnValue(null);

        comp.ngOnInit();
        comp.tableColumns = comp.tableColumns || [];

        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            bonus: null,
            accountCode: null
        };
        comp.openCancelBonusModal(playerBonuses[0].bonusKey);
        const cancelBonusRequest: CancelBonusRequest = { description: "Description", amountToDeduct: 10 };
        fakeModalService.componentInstance.confirmationClick.next(cancelBonusRequest);

        expect(loaderServiceSpy.show).toHaveBeenCalled();
        // Finally
        expect(loaderServiceSpy.hide).toHaveBeenCalled();
        expect(fakeModalService.close).toHaveBeenCalled();
        // cancelPlayerActiveOrPendingBonus Request
        expect(bonusServiceSpy.cancelPlayerActiveOrPendingBonus).toHaveBeenCalledWith(accountCode, playerBonuses[0].bonusKey, cancelBonusRequest);
        // cancelPlayerActiveOrPendingBonus Error
        expect(bonusServiceSpy.getPlayerActiveAndPendingBonuses).toHaveBeenCalledTimes(1);
        expect(comp.table.clearSelectedRows).toHaveBeenCalledTimes(1);
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalledWith(BaseResultsCode.InternalServerError, "players.bonuses.cancelPlayerBonus.response");
    });

    it("should reload player bonuses on reloadBonuses event", () => {
        comp.ngOnInit();
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.calls.reset();
        reloadBonusesSource.next(accountCode);
        expect(bonusServiceSpy.getPlayerActiveAndPendingBonuses).toHaveBeenCalledWith(accountCode);
    });

    it("should set selected bonus keys on selected rows change event", () => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        comp.ngOnInit();

        expect(comp.tableButtons[0].disabled).toBe(true);

        comp.tableButtons[0].disabled = false;
        comp.onSelectedRowsChange([]);
        expect(comp.tableButtons[0].disabled).toBe(true);

        comp.tableButtons[0].disabled = false;
        comp.onSelectedRowsChange(null);
        expect(comp.tableButtons[0].disabled).toBe(true);

        comp.tableButtons[0].disabled = true;
        comp.onSelectedRowsChange(['BONUSKEY']);
        expect(comp.tableButtons[0].disabled).toBe(false);
    });

    it("should open cancel bonus modal on table action click", () => {
        bonusServiceSpy.getPlayerActiveAndPendingBonuses.and.returnValue(Observable.of(playerBonuses));
        comp.ngOnInit();
        comp.onSelectedRowsChange(['BONUSKEY']);

        spyOn(comp, "openCancelBonusModal").and.returnValue(null);

        comp.onActionButtonClick(null);
        expect(comp.openCancelBonusModal).not.toHaveBeenCalled();

        comp.onActionButtonClick({id: 1});
        expect(comp.openCancelBonusModal).toHaveBeenCalledWith('BONUSKEY');
    });

});
