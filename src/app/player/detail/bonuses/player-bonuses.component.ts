import { Component, OnInit } from "@angular/core";
import { PlayerDetailService, PlayerViewEnum } from "../player-detail.service";
import { PermissionsCode } from "../../model/models";

@Component({
    templateUrl: './player-bonuses.component.html',
    styleUrls: ['./player-bonuses.component.scss']
})
export class PlayerBonusesComponent implements OnInit {

    public viewActiveAndPendingBonusesPermission: Array<string>;

    constructor(private playerDetailService: PlayerDetailService) {}

    public ngOnInit(): void {
        this.playerDetailService.setActiveView(PlayerViewEnum.PLAYER_BONUSES);
        this.viewActiveAndPendingBonusesPermission = PermissionsCode.PlayerActiveAndPendingBonuses;
    }
}
