import { NO_ERRORS_SCHEMA, DebugElement } from "@angular/core";
import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FakeTranslateLoader } from "../../../../../test-util/fake-translate-loader";
import { PlayerCancelBonusComponent, CancelTypeEnum, PlayerCancelBonusStepEnum } from "./cancel-bonus.component";
import { CurrencyService } from "../../../../shared/currency.service";
import { PlayerBonus, CancelBonusRequest } from "../../../model/models";
import { getDefaultDateTimeFormat } from "../../../../shared/date-utils";

describe("PlayerCancelBonusComponent", () => {

    const currencyCode: string = "COP";

    let ngbActiveModalSpy: jasmine.SpyObj<NgbActiveModal>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;

    let fixture: ComponentFixture<PlayerCancelBonusComponent>;
    let comp: PlayerCancelBonusComponent;
    let de: DebugElement;

    beforeEach(async(() => {

        ngbActiveModalSpy = jasmine.createSpyObj<NgbActiveModal>("NgbActiveModal", ["close"]);
        currencyServiceSpy = jasmine.createSpyObj<CurrencyService>("CurrencyService", ["formatAmountWithCurrency", "getCode"]);

        TestBed.configureTestingModule({
            declarations: [PlayerCancelBonusComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: NgbActiveModal, useValue: ngbActiveModalSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PlayerCancelBonusComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement;

        comp.accountCode = "e6313fd8c85be0bb";
        comp.bonus = {
            bonusKey: "BONUSKEY",
            displayName: "Bonus Display Name",
            status: PlayerBonus.StatusEnum.Active,
            initialBonusAmountAwarded: 0,
            bonusBalance: 80,
            dateAwarded: "2016-12-31T23:00:00.000Z",
            dateActivated: "2016-12-31T23:00:00.000Z",
            dateClosed: "2016-12-31T23:00:00.000Z",
            daysToExpiration: 10,
            moneyRedemptionCurrentValue: 50,
            moneyRedemptionTargetValue: 400,
            numberOfBetsRedemptionCurrentValue: 2,
            numberOfBetsRedemptionTargetValue: 4,
            translatedStatus: "Activo",
            formattedInitialBonusAmountAwarded: "COP 0.00",
            formattedBonusBalance: "COP 80.00",
            formattedDateOfAwarding: "January 1 2017 12:00 AM",
            formattedDateOfActivation: "January 1 2017 12:00 AM",
            formattedDateOfClosing: "January 1 2017 12:00 AM",
            formattedMoneyRedemptionCurrentValue: "COP 50.00",
            formattedMoneyRedemptionTargetValue: "COP 400",
            formattedMoneyRedemptionRolloverPercent: "13%",
            formattedNumberOfBetsRedemptionCurrentValue: "2",
            formattedNumberOfBetsRedemptionTargetValue: "4",
            formattedBetsRedemptionRolloverPercent: "50%"
        };

        currencyServiceSpy.getCode.and.returnValue(currencyCode);
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(comp).toBeTruthy();
    });

    it("should have currency code set up", () => {
        expect(comp.currencyCode).toBe(currencyCode);
    });

    it("should calculate and format date of bonus expiration", () => {
        const formattedDateOfExpiration: string = getDefaultDateTimeFormat("2017-01-10T23:00:00.000Z");
        expect(comp.formattedDateOfExpiration).toBe(formattedDateOfExpiration);
    });

    it("should return true if invalid cancel amount is entered and cancel type is partial", () => {
        comp.selectedCancelType = CancelTypeEnum.Partial;
        comp.invalidCancelAmountMsg = "Previous error message.";

        comp.selectedCancelAmount = 0;
        expect(comp.isInvalidCancelAmount()).toBe(true);

        comp.selectedCancelAmount = comp.bonus.bonusBalance + 1;
        expect(comp.isInvalidCancelAmount()).toBe(true);

        comp.selectedCancelAmount = -1;
        expect(comp.isInvalidCancelAmount()).toBe(true);

        expect(comp.invalidCancelAmountMsg).toBe("players.bonuses.cancelPlayerBonus.invalidAmountMsg");
    });

    it("should return false if valid cancel amount is entered or cancel type is total", () => {
        comp.selectedCancelType = CancelTypeEnum.Partial;
        comp.invalidCancelAmountMsg = "Previous error message.";
        comp.selectedCancelAmount = 1;

        expect(comp.isInvalidCancelAmount()).toBe(false);
        comp.selectedCancelType = CancelTypeEnum.Total;
        expect(comp.isInvalidCancelAmount()).toBe(false);
        expect(comp.invalidCancelAmountMsg).toBe("");
    });

    it("should adjust cancel amount when cancel type changes", () => {
        expect(comp.selectedCancelAmount).toBe(undefined);

        comp.cancelTypeChanged();
        expect(comp.selectedCancelAmount).toBe(undefined);

        comp.selectedCancelType = CancelTypeEnum.Total;
        comp.cancelTypeChanged();
        expect(comp.selectedCancelAmount).toBe(comp.bonus.bonusBalance);

        comp.selectedCancelType = CancelTypeEnum.Partial;
        comp.cancelTypeChanged();
        expect(comp.selectedCancelAmount).toBe(null);
    });

    it("should return true user can cancel palyer's bonus", () => {
        comp.selectedCancelAmount = 1;
        comp.selectedCancelType = CancelTypeEnum.Partial;
        comp.selectedCancelReason = "Selected reason for bonus cancel";
        expect(comp.canDoCancel()).toBe(true);
    });

    it("should return false user can not cancel palyer's bonus", () => {
        comp.selectedCancelType = CancelTypeEnum.Total;
        comp.selectedCancelReason = "a                    b                    c";
        expect(comp.canDoCancel()).toBe(false);
    });

    it("should take user to the next step", () => {
        comp.selectedCancelType = CancelTypeEnum.Partial;
        comp.nextStep();
        expect(comp.currentStep).toBe(PlayerCancelBonusStepEnum.Confirm);
        expect(comp.confirmationMsg).toBe("players.bonuses.cancelPlayerBonus.cancelTypes.partial.confirmationMsg");

        comp.selectedCancelType = CancelTypeEnum.Total;
        comp.nextStep();
        expect(comp.currentStep).toBe(PlayerCancelBonusStepEnum.Confirm);
        expect(comp.confirmationMsg).toBe("players.bonuses.cancelPlayerBonus.cancelTypes.total.confirmationMsg");
    });

    it("should take user to the previous step", () => {
        comp.prevStep();
        expect(comp.currentStep).toBe(PlayerCancelBonusStepEnum.Select);
        expect(comp.confirmationMsg).toBe(null);
    });

    it("should emit cancel bonus request on confirmation click", () => {
        const reason: string = "Selected reason for bonus cancel";
        const amount: number = 1;
        const cancelBonusRequest: CancelBonusRequest = { description: reason, amountToDeduct: amount };

        comp.selectedCancelType = CancelTypeEnum.Partial;
        comp.selectedCancelAmount = amount;
        comp.selectedCancelReason = reason;

        comp.confirmationClick.subscribe((event: CancelBonusRequest) => {
            expect(event).toEqual(cancelBonusRequest);
        });

        comp.onConfirmationClick();
    });

});
