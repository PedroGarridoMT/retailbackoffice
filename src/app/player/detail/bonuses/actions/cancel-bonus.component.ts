import * as moment from "moment";
import { Component, Input, ViewChild, Output, EventEmitter, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { PlayerBonusExtended } from "../../../model/PlayerBonusExtended";
import { CurrencyService } from "./../../../../shared/currency.service";
import { CancelBonusRequest, PlayerBonus } from "../../../model/models";
import { getDefaultDateTimeFormat } from "../../../../shared/date-utils";

export enum CancelTypeEnum { Total, Partial }
export enum PlayerCancelBonusStepEnum { Select, Confirm }

@Component({
    styleUrls: ["./cancel-bonus.component.scss"],
    templateUrl: "./cancel-bonus.component.html"
})
export class PlayerCancelBonusComponent implements OnInit {

    @Input() public accountCode: string;
    @Input() public bonus: PlayerBonusExtended;

    @Output() public confirmationClick = new EventEmitter<CancelBonusRequest>();

    // NgModels
    public selectedCancelType: CancelTypeEnum;
    public selectedCancelAmount: number;
    public selectedCancelReason: string;

    // Enums
    public cancelTypeEnum = CancelTypeEnum;
    public playerCancelBonusStepEnum = PlayerCancelBonusStepEnum;
    public playerBonusStatusEnum = PlayerBonus.StatusEnum;
    public currentStep: PlayerCancelBonusStepEnum = PlayerCancelBonusStepEnum.Select;

    public invalidCancelAmountMsg: string;
    public confirmationMsg: string;
    public formattedDateOfExpiration: string;
    public currencyCode: string;

    constructor(public activeModal: NgbActiveModal, private translate: TranslateService, private currencyService: CurrencyService) { }

    public ngOnInit(): void {
        if(this.bonus && this.bonus.dateActivated !== null) {
            this.formattedDateOfExpiration = getDefaultDateTimeFormat(moment(this.bonus.dateActivated).add(this.bonus.daysToExpiration, "days").toISOString());
        }
        this.currencyCode = this.currencyService.getCode();
    }

    public isInvalidCancelAmount(): boolean {
        this.invalidCancelAmountMsg = "";
        if (this.selectedCancelAmount !== null && this.selectedCancelType !== CancelTypeEnum.Total && (this.selectedCancelAmount <= 0 || this.selectedCancelAmount > this.bonus.bonusBalance)) {
            this.invalidCancelAmountMsg = this.translate.instant("players.bonuses.cancelPlayerBonus.invalidAmountMsg");
            return true;
        }
        return false;
    }

    public cancelTypeChanged(): void {
        if (this.selectedCancelType === CancelTypeEnum.Total) {
            this.selectedCancelAmount = this.bonus.bonusBalance;
        } else {
            if (this.selectedCancelAmount === this.bonus.bonusBalance) {
                this.selectedCancelAmount = null;
            }
        }
    }

    public canDoCancel(): boolean {
        return !this.isInvalidCancelAmount() &&
            !!this.selectedCancelReason && this.selectedCancelReason.trim().length >= 10 &&
            this.selectedCancelAmount > 0 &&
            (this.selectedCancelType === CancelTypeEnum.Total || this.selectedCancelType === CancelTypeEnum.Partial);
    }

    public nextStep(): void {
        this.currentStep = PlayerCancelBonusStepEnum.Confirm;

        if (this.selectedCancelType === CancelTypeEnum.Total) {
            this.confirmationMsg = this.translate.instant("players.bonuses.cancelPlayerBonus.cancelTypes.total.confirmationMsg");
        } else {
            const amount = this.currencyService.formatAmountWithCurrency(this.bonus.bonusBalance - this.selectedCancelAmount);
            this.confirmationMsg = this.translate.instant("players.bonuses.cancelPlayerBonus.cancelTypes.partial.confirmationMsg", { amount });
        }
    }

    public prevStep(): void {
        this.currentStep = PlayerCancelBonusStepEnum.Select;
        this.confirmationMsg = null;
    }

    public onConfirmationClick(): void {
        const cancelBonusRequest: CancelBonusRequest = { description: this.selectedCancelReason };
        if (this.selectedCancelType === CancelTypeEnum.Partial) {
            cancelBonusRequest.amountToDeduct = this.selectedCancelAmount;
        }
        this.confirmationClick.emit(cancelBonusRequest);
    }

}
