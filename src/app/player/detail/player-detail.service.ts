import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { PlayerDetailsResponse } from "../model/models";

export enum PlayerVisualizationModeEnum { Visualization, Edition }

export enum PlayerActionsEnum { EDIT, CHANGE_STATUS, MANUAL_ADJUSTMENT, REVOKE_SELF_EXCLUSION, VERIFY_IDENTITY, REJECT_IDENTITY, ADD_BONUS }

export enum PlayerViewEnum { PLAYER_DATA, PLAYER_TRANSACTIONS, PLAYER_DOCUMENTATION, PLAYER_RESPONSIBLE_GAMING, PLAYER_BONUSES }

@Injectable()
export class PlayerDetailService {

    //Private var, to act as a player cache.
    private _player = {};

    // Observable sources
    private playerDetailSource = new Subject<PlayerDetailsResponse>();
    private visualizationModeSource = new Subject<PlayerVisualizationModeEnum>();
    private activeViewSource = new Subject<PlayerViewEnum>();
    private reloadPlayerSource = new Subject<string>();
    private reloadBonusesSource = new Subject<string>();

    // Observable streams
    public playerDetail$ = this.playerDetailSource.asObservable();
    public visualizationMode$ = this.visualizationModeSource.asObservable();
    public activeView$ = this.activeViewSource.asObservable();
    public reloadPlayer$ = this.reloadPlayerSource.asObservable();
    public reloadBonusesSource$ = this.reloadBonusesSource.asObservable();

    // Service message commands
    public setPlayer(playerDetail: PlayerDetailsResponse): void {
        //Store a copy of the player in cache
        this._player = JSON.parse(JSON.stringify(playerDetail));
        //And notify subscribers
        this.playerDetailSource.next(playerDetail);
    }

    public setVisualizationMode(mode: PlayerVisualizationModeEnum): void {
        this.visualizationModeSource.next(mode);
    }

    public setActiveView(view: PlayerViewEnum): void {
        this.activeViewSource.next(view);
    }

    public reloadPlayer(accountCode: string): void {
        this.reloadPlayerSource.next(accountCode);
    }

    public reloadBonuses(accountCode: string): void {
        this.reloadBonusesSource.next(accountCode);
    }

    //Helper methods
    public getStoredPlayer(): PlayerDetailsResponse {
        return this._player ? this._player : null;
    }
}
