﻿import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ConfigService } from "../../core/config.service";
import { Documentation, PlayerDocument } from "../model/models";
import { HelperService } from "../../shared/helper.service";


@Injectable()
export class DocumentationService {
    APIResources;

    constructor(private http: Http, private config: ConfigService) {
        this.APIResources = {
            playerDocumentation: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/documentation',
            playerDocument: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/documentation/{documentId}',
        };
    }

    /**
     * Remove some existing player personal document, given the player account code and the document ID.  This operation requires the user to have the `PlayerDetailDocumentationRemove` permission assigned. If the user does not have the permission a `Forbidden (403)` response will be returned.
     * @summary Remove some existing player personal document, given the player account code and the document ID.
     * @param accountCode Account code.
     * @param documentId The identifier of player document.
     */
    public deletePlayerDocument(accountCode: string, documentId: number): Observable<any> {
        return this.http.delete(this.APIResources.playerDocument
            .replace('{accountCode}', accountCode)
            .replace('{documentId}', documentId))
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch((error) => {
                return Observable.from([HelperService.getBaseResultCode(error.status)]);
            });
    }


    /**
     * Get the document file, given the player account code and the document ID.  This operation requires the user to have the `PlayerDetailDocumentation` permission assigned. If the user does not have the permission a `Forbidden (403)` response will be returned.
     * @summary Get the document file, given the player account code and the document ID.
     * @param accountCode Account code.
     * @param documentId The identifier of player document.
     */
    public getPlayerDocument(accountCode: string, documentId: number): Observable<PlayerDocument> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerDocument.');
        }
        if (documentId === null || documentId === undefined) {
            throw new Error('Required parameter documentId was null or undefined when calling getPlayerDocument.');
        }

        return this.http.get(this.APIResources.playerDocument
            .replace('{accountCode}', accountCode)
            .replace('{documentId}', documentId))
            .map((response: Response) => {
                return response.json();
            });
    }

    /**
     * Get a list of the uploaded documents, given the account code of the player.  This operation requires the user to have the `PlayerDetailDocumentation` permission assigned. If the user does not have the permission a `Forbidden (403)` response will be returned.
     * @summary Get a list of the uploaded documents, given the account code of the player.
     * @param accountCode Account code.
     */
    public getPlayerDocumentation(accountCode: string): Observable<Array<Documentation>> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerDocumentation.');
        }

        return this.http.get(this.APIResources.playerDocumentation.replace('{accountCode}', accountCode))
            .map((response: Response) => {
                return response.json();
            });
    }

    /**
     * Upload a player document.
     * @summary Post a new player document.
     * @param accountCode Account code of the player.
     * @param request The player document file to upload. The corresponding HTTP request will include multipart/form-data payload.
     */
    public uploadPlayerDocument(accountCode: string, request: Array<PlayerDocument>) {
        return this.http.post(this.APIResources.playerDocumentation.replace('{accountCode}', accountCode), request)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch((error) => {
                return Observable.from([HelperService.getBaseResultCode(error.status)]);
            });
    }
}
