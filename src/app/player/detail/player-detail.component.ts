import { PlayerSearchResponse } from "./../../shared/model/api-generated/PlayerSearchResponse";
import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { TitleBarHandlerComponent } from "../../shared/title-bar-handler.component";
import { TitleBarParams } from "../../shared/model/title-bar.model";
import { TitleBarService } from "../../shared/title-bar.service";
import { PlayerService } from "../player.service";
import { PlayerDetailService, PlayerVisualizationModeEnum } from "./player-detail.service";
import { LoaderService } from "../../shared/loader.service";
import { PermissionsCode } from "../../shared/model/permissions-code.model";
import { PlayerNotesService } from "./player-notes.service";
import { PlayerNote } from "../../shared/model/models";
import {
    Note,
    NotesBarComponent,
    NotesBarIconClasses,
    NotesBarLiterals,
    NotesBarUserActions,
    NotesModeEnum,
    NotesTimelineLiterals,
    NotificationTypeEnum,
    PermissionsService
} from "mt-web-components";
import { EmitterService, EmitterServiceEventsEnum } from "../../shared/services/emmiter.service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ModalConfirmationComponent } from "../../shared/components/modal-confirmation.component";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "../../account/shared/authentication.service";
import { ResponseHelperService } from "../../shared/response-helper.service";
import { BaseResultsCode } from "../../shared/model/base-results-code.enum";
import { getDefaultDateTimeFormat } from "../../shared/date-utils";
import { Observable } from "rxjs/Observable";
import { TitleBarPreviousRouteDetails, ItemNavigatorParams, PlayerSearchRequest, PlayerFilterCriteria } from "../model/models";
import { PASConstants } from "../../shared/constant";
import { NavigationEnd } from "@angular/router";

export interface SearchRequest {
    pageNumber: number;
    pageSize: number;
    currentIndex: number;
    totalItems: number;
    filterCriteria: PlayerFilterCriteria;
    currentAccountCode: string;
};

@Component({
    selector: 'pas-player-detail',
    templateUrl: './player-detail.component.html',
    styleUrls: ['./player-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PlayerDetailComponent extends TitleBarHandlerComponent implements OnInit, OnDestroy {
    private _subscriptions: Subscription[] = [];

    playerAccountCode: string;
    errorLoadingPlayer: boolean = false;

    // Title bar attributes
    titleBarParams: TitleBarParams;
    playerDetailLinkParams: Object;

    // Item navigator
    itemNavigator = <ItemNavigatorParams>{ links: [], currentIndex: 0, disabled: true };
    accountCodes: string[] = [];

    //Note bar inputs
    playerNotes: Note[];
    currentUser: string;
    notesBarClass: string; //Class to adjust the notes width depending on the menu state(collapsed/expanded)
    notesBarLiterals: NotesBarLiterals;
    notesBarIcons: NotesBarIconClasses;
    notesActionsAllowed: NotesBarUserActions;
    notesTimelineLiterals: NotesTimelineLiterals;
    @ViewChild("notesViewer") notesViewer;
    @ViewChild("notesBar") notesBar: NotesBarComponent;

    notesResponses = {
        add: 'players.notes.add.response',
        delete: 'players.notes.delete.response',
        edit: 'players.notes.edit.response'
    };

    //Permission vars
    playerDetailTransactionsPermissions: Array<string>;
    playerDocumentationPermissions: Array<string>;
    responsibleGamingPermissions: Array<string>;
    playerBonusesPermissions: Array<string>;
    notesPermissions: {
        view: Array<string>,
        add: Array<string>,
        edit_own: Array<string>,
        edit_all: Array<string>,
        delete_own: Array<string>,
        delete_all: Array<string>
    };


    constructor(titleBarService: TitleBarService,
        private route: ActivatedRoute,
        private router: Router,
        private playerService: PlayerService,
        private playerDetailService: PlayerDetailService,
        private loader: LoaderService,
        private notesService: PlayerNotesService,
        private emitterService: EmitterService,
        private permissionsService: PermissionsService,
        private modalService: NgbModal,
        private translate: TranslateService,
        private authServive: AuthenticationService,
        private responseHelper: ResponseHelperService) {
        super(titleBarService);
    }

    public ngOnInit(): void {

        const searchRequest: any = this.parseSearchRequest();
        if (searchRequest) {

            // Fill in links so it can show total and current item
            this.itemNavigator.links = Array(searchRequest.totalItems).fill("");
            // Fill in total items and current index immediately
            this.itemNavigator.currentIndex = searchRequest.currentIndex;

            const playerSearchRequest: PlayerSearchRequest = {
                filterCriteria: searchRequest.filterCriteria,
                pageNumber: searchRequest.pageNumber,
                pageSize: searchRequest.pageSize
            };

            this.playerService.getAccountCodes(playerSearchRequest).subscribe((accountCodes: string[]) => {
                this.accountCodes = accountCodes;
                // Update item navigation with real data
                this.updateItemNavigation(this.accountCodes, searchRequest);
                this.titleBarParams.itemNavigator.disabled = false;
            });
        }

        this.initVars();

        this._subscriptions.push(
            this.route.params.subscribe((params: Params) => {
                this.playerAccountCode = params['accountCode'];
                this.loadPlayerDetail(this.playerAccountCode);

                if (this.permissionsService.hasAccess(this.notesPermissions.view)) {
                    this.listenToChangesOnMenuState();
                    this.askForCurrentMenuState();
                    this.loadNotes(false);
                }
            }),
            this.router.events.filter(e => e instanceof NavigationEnd).subscribe(() => {
                if (this.accountCodes && this.accountCodes.length) {
                    this.updateItemNavigation(this.accountCodes, this.parseSearchRequest());
                }
            })
        );


        const previousRouteDetails: TitleBarPreviousRouteDetails = this.playerService.getpreviousRouteDetails();
        this.titleBarParams = {
            previousPagePath: previousRouteDetails.previousPagePath,
            previousPageLabel: previousRouteDetails.previousPageLabel,
            previousPageQueryParams: previousRouteDetails.previousPageQueryParams,
            pageTitle: 'players.home.detail',
            pageActions: null,
            itemNavigator: this.itemNavigator
        };

        if(this.route.snapshot.queryParamMap.keys.length) {
            this.playerDetailLinkParams = {
                [PlayerService.PREVIOUS_ROUTE_KEY]: previousRouteDetails.previousPageLabel,
                [PASConstants.SEARCH_REQUEST_PARAM]: this.route.snapshot.queryParamMap.get(PASConstants.SEARCH_REQUEST_PARAM)
            };
        } else {
            this.playerDetailLinkParams = null;
        }


        //Notify and pass params to the title bar service, so it can notify subscribers
        this.notifyComponentInitialized(this.titleBarParams);

        //Subscribe to reload player requests.
        //Requests may come from the children components, when the user edits a player, changes his status...
        this._subscriptions.push(
            this.playerDetailService.reloadPlayer$.subscribe(accountCode => this.loadPlayerDetail(accountCode))
        );
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    public isRouteActive(first: string, second: string): string {
        if(new RegExp("\/" + first + "\/.*\/" + second, "g").test(this.router.url)) {
            return "section-tab--active";
        } else {
            return "";
        }
    }

    /**
     * Parse search request base64 query param
     * @returns {SearchRequest}
      */
    private parseSearchRequest(): SearchRequest {
        try {
            const currentAccountCode: string = this.route.snapshot.paramMap.get("accountCode");
            const searchRequest: string = this.route.snapshot.queryParamMap.get(PASConstants.SEARCH_REQUEST_PARAM);

            if (searchRequest && searchRequest.length) {
                const { pageNumber, pageSize, currentIndex, totalItems, ...filterCriteria } = JSON.parse(atob(searchRequest));

                return {
                    pageNumber,
                    pageSize,
                    currentIndex,
                    totalItems,
                    filterCriteria,
                    currentAccountCode
                };
            } else {
                console.log(`No "${PASConstants.SEARCH_REQUEST_PARAM}" query param.`);
            }
        } catch (e) {
            console.error(`Couldn't parse "${PASConstants.SEARCH_REQUEST_PARAM}" query param.\n`, e);
        }

        return null;
    }

    /**
     * Update item navigator with new links
     * @param {string[]} accountCodes
     * @param {SearchRequest} searchRequest
      */
    private updateItemNavigation(accountCodes: string[], searchRequest: SearchRequest): void {

        if (searchRequest) {
            const { pageNumber, pageSize, currentIndex, totalItems, filterCriteria } = searchRequest;
            const newSearchRequestParamValue = Object.assign({}, filterCriteria, { pageNumber, pageSize, currentIndex, totalItems });

            this.playerDetailLinkParams[PASConstants.SEARCH_REQUEST_PARAM] = btoa(JSON.stringify(newSearchRequestParamValue));

            this.itemNavigator.links = this.accountCodes.map((accountCode, index) => {
                // Get current base64 encoded query param
                const currentSearchRequestParam: string = encodeURIComponent(this.route.snapshot.queryParamMap.get(PASConstants.SEARCH_REQUEST_PARAM));
                // Update accountCode url param and set pageNumber param to 1 so next or previous page will load transactions from the begining
                const newAccountCodeURI: string = this.router.url.replace(searchRequest.currentAccountCode, accountCode).replace(/(pageNumber=\d+)/g, "pageNumber=1");
                // Create new base64 search request param
                const base64SearchRequestParam: string = btoa(JSON.stringify(Object.assign({}, newSearchRequestParamValue, { currentIndex: index })));

                return newAccountCodeURI.replace(currentSearchRequestParam, base64SearchRequestParam);
            });

            this.itemNavigator.currentIndex = searchRequest.currentIndex;

            // Update previous page link to match table pagination with current item
            if (this.titleBarParams && this.titleBarParams.previousPageQueryParams && this.titleBarParams.previousPageQueryParams["pageNumber"]) {
                this.titleBarParams.previousPageQueryParams["pageNumber"] = Math.floor(searchRequest.currentIndex / searchRequest.pageSize) + 1;
            }
        }
    }

    /**
     * See CanDeactivateGuard in Core module.
     * If there is a note being edited, request the user a confirmation before leaving the page.
     */
    canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        const noteIsBeingEdited = this.notesBar.mode == NotesModeEnum.EDIT;
        const noteIsBeingAdded = this.notesBar.mode == NotesModeEnum.ADD || this.notesBar.mode == NotesModeEnum.ADD_QUICKLY;
        return (noteIsBeingAdded || noteIsBeingEdited) ? this.askForExitWithoutSavingNoteConfirmation() : true;
    }

    /**
     * Handle clicks on the notes bar counter button.
     * Shows the notes timeline component
     */
    onCounterClick() {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
        });
        modalRef.componentInstance.title = this.playerDetailService.getStoredPlayer().username + " - " +
            this.playerNotes.length + " " + this.translate.instant("players.notes.notes");
        modalRef.componentInstance.showCloseIcon = true;
        modalRef.componentInstance.customTemplate = this.notesViewer;
        modalRef.componentInstance.confirmText = this.translate.instant('common.ok');
    }

    /**
     * Handles the addNote event received from the notes bar.
     * Gets the text of the new note and call the service to create a new note with it.
     * @param {string} noteText
     */
    onAddNote(noteText: string) {
        this.loader.show();
        this.notesService
            .createPlayerNote(this.playerAccountCode, noteText)
            .finally(() => this.loader.hide())
            .subscribe(
            resp => {
                this.responseHelper.responseHandler(resp.statusCode, this.notesResponses.add, null, 3);
                this.loadNotes(true);

            }, error => {
                this.parseChangeNoteError(error.statusCode, this.notesResponses.add, 3);
            });
    }

    onDeleteNote(noteId: number) {
        this.askForDeleteNoteConfirmation(noteId);
    }

    onConfirmDeleteNote(noteId: number) {
        this.loader.show();
        this.notesService
            .deletePlayerNote(this.playerAccountCode, noteId)
            .finally(() => this.loader.hide())
            .subscribe(
            resp => {
                this.responseHelper.responseHandler(resp.statusCode, this.notesResponses.delete, null, 3);
                this.loadNotes(true);

            }, error => {
                this.parseChangeNoteError(error.statusCode, this.notesResponses.delete, 3);
            });
    }

    /**
     * Hanldes the editNote event received from the notes bar.
     * Sends a request to edit the note.
     * @param noteChanges
     */
    onEditNote(noteChanges: { id: number, text: string }) {
        this.loader.show();
        this.notesService
            .editPlayerNote(this.playerAccountCode, noteChanges.id, noteChanges.text)
            .finally(() => this.loader.hide())
            .subscribe(
            resp => {
                this.responseHelper.responseHandler(resp.statusCode, this.notesResponses.edit, null, 3);
                this.loadNotes(true);

            }, error => {
                this.parseChangeNoteError(error.statusCode, this.notesResponses.edit, 3);
            });
    }

    private askForDeleteNoteConfirmation(noteId: number) {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: "static",
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant("players.notes.delete.title");
        modalRef.componentInstance.message = this.translate.instant("players.notes.delete.message");
        modalRef.componentInstance.confirmText = this.translate.instant("players.notes.delete.confirmDelete");
        modalRef.componentInstance.cancelText = this.translate.instant("players.notes.delete.cancelDelete");

        modalRef.result.then(
            result => {
                if (result === "CONFIRM") this.onConfirmDeleteNote(noteId);
            },
            () => false);
    }

    private askForExitWithoutSavingNoteConfirmation() {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant('players.notes.edit.unsavedNoteTitle');
        modalRef.componentInstance.message = this.translate.instant('players.notes.edit.unsavedNoteMessage');
        modalRef.componentInstance.confirmText = this.translate.instant('players.notes.edit.unsavedNoteCancelButton');
        modalRef.componentInstance.cancelText = this.translate.instant('players.notes.edit.unsavedNoteStayButton');

        return modalRef.result.then(result => result === "CONFIRM" ? true : false, () => false);
    }


    /**
     * Executes Player detail server request
     * @param accountCode
     */
    private loadPlayerDetail(accountCode: string) {
        this.loader.show();
        this.errorLoadingPlayer = false;

        //console.log('Load player detail of: ', accountCode);
        return this.playerService.getPlayerDetail(accountCode)
            .finally(() => {
                this.loader.hide();
                this.playerDetailService.setVisualizationMode(PlayerVisualizationModeEnum.Visualization);
            })
            .subscribe(
            player => {
                //Pass data to the player detail service, so subscribers can get it.
                this.playerDetailService.setPlayer(player);
            },
            error => {
                this.errorLoadingPlayer = true;
                console.warn(error);
            });
    }

    private loadNotes(afterSomeNoteAction: boolean) {
        this.loader.show();
        this.notesService.getPlayerNotes(this.playerAccountCode)
            .finally(() => this.loader.hide())
            .subscribe(
            this.parsePlayerNotes.bind(this),
            () => {
                if (afterSomeNoteAction) {
                    this.parsePlayerNotesError(true);
                } else {
                    this.parsePlayerNotesError(false);
                }
            });
    }

    /**
     * Convert PlayerNote[] to Note[](from mt-web-components)
     * @param {PlayerNote[]} newNotes
     */
    private parsePlayerNotes(newNotes: PlayerNote[]) {
        let aux: Note[] = [];
        newNotes.forEach(note => aux.push({
            id: note.id,
            text: note.text,
            author: note.author,
            date: getDefaultDateTimeFormat(note.creationDate)
        }));

        this.playerNotes = aux;
    }

    /**
     * Handles error when the operation to fetch player notes fails
     * @param {boolean} afterSomeNoteAction
     */
    private parsePlayerNotesError(afterSomeNoteAction: boolean) {
        if (afterSomeNoteAction) {
            this.playerNotes = JSON.parse(JSON.stringify(this.playerNotes));
            this.responseHelper.responseHandler(BaseResultsCode.CustomCode, 'players.notes.error.errorAfterUpdate', NotificationTypeEnum.Warning, 3);
        }
        else {
            this.playerNotes = [];
            this.responseHelper.responseHandler(BaseResultsCode.CustomCode, 'players.notes.error.errorLoadingNotes', NotificationTypeEnum.Warning, 3);
        }
        console.warn("Error getting notes for player ", this.playerAccountCode);
    }

    /**
     * Handles errors when a operation to modify a note (add/edit/delete) fails
     * @param statusCode
     * @param translationKey
     * @param seconds
     */
    private parseChangeNoteError(statusCode, translationKey, seconds) {
        this.responseHelper.responseHandler(statusCode, translationKey, null, seconds);
        this.playerNotes = JSON.parse(JSON.stringify(this.playerNotes));
    }

    /**
     * The main-template component emit events when the menu state is changed.
     * Subscribe to these events and update notes bar width accordingly.
     */
    private listenToChangesOnMenuState() {
        this._subscriptions.push(this.emitterService.eventBus$.subscribe(event => {
            switch (event) {
                case EmitterServiceEventsEnum.MENU_IS_COLLAPSED:
                    this.notesBarClass = "player-detail__notes--menu-collapsed";
                    break;
                case EmitterServiceEventsEnum.MENU_IS_EXPANDED:
                    this.notesBarClass = "player-detail__notes--menu-expanded";
                    break;
            }
        })
        );
    }

    /**
     * Emits an event through the generic EmitterService to ask for the current state of the lateral menu.
     */
    private askForCurrentMenuState() {
        this.emitterService.emitEvent(EmitterServiceEventsEnum.MENU_CURRENT_STATE_REQUEST);
    }

    private initVars() {
        this.playerDetailTransactionsPermissions = PermissionsCode.PlayerDetailTransactions;
        this.playerDocumentationPermissions = PermissionsCode.PlayerDetailDocumentation;
        this.responsibleGamingPermissions = PermissionsCode.PlayerResponsibleGaming;
        this.playerBonusesPermissions = PermissionsCode.PlayerBonuses;
        this.notesPermissions = {
            view: PermissionsCode.PlayerNotes,
            add: PermissionsCode.PlayerNotesAdd,
            edit_own: PermissionsCode.PlayerNotesEditOwn,
            edit_all: PermissionsCode.PlayerNotesEditAll,
            delete_own: PermissionsCode.PlayerNotesDeleteOwn,
            delete_all: PermissionsCode.PlayerNotesDeleteAll
        };

        this.initNotesInputs();
    }

    private initNotesInputs() {
        this.playerNotes = [];
        this.currentUser = this.authServive.getCurrentUser().username;
        this.notesBarClass = 'player-detail__notes--menu-collapsed';
        this.notesBarIcons = {
            edit: "fa fa-pencil",
            delete: "fa fa-trash",
            save: "fa fa-check",
            cancel: "fa fa-times",
            collapse: "fa fa-angle-double-down",
            expand: "fa fa-angle-double-up"
        };
        this.translate.get(["players.notes.notes", "players.notes.addNote", "common.by"]).subscribe(translations => {
            this.notesBarLiterals = {
                notes: translations["players.notes.notes"],
                addNote: translations["players.notes.addNote"]
            };
            this.notesTimelineLiterals = {
                by: translations["common.by"]
            }
        });
        this.notesActionsAllowed = this.getAllowedNotesActions();
    }

    private getAllowedNotesActions(): NotesBarUserActions {
        return {
            canAdd: this.permissionsService.hasAccess(this.notesPermissions.add),
            canEditOwn: this.permissionsService.hasAccess(this.notesPermissions.edit_own),
            canEditAll: this.permissionsService.hasAccess(this.notesPermissions.edit_all),
            canDeleteOwn: this.permissionsService.hasAccess(this.notesPermissions.delete_own),
            canDeleteAll: this.permissionsService.hasAccess(this.notesPermissions.delete_all)
        };
    }

}
