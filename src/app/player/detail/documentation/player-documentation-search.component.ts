import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ActivatedRoute, Params } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import {
    NotificationTypeEnum,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals,
    TableActionButton,
    TableComponent,
    PermissionsService
} from "mt-web-components";
import { DocumentationExtended, PermissionsCode, PlayerDocument } from "../../model/models";
import { LoaderService } from "../../../shared/loader.service";
import { DocumentationService } from "../player-documentation.service";
import { getDefaultDateFormat, getDefaultDateTimeFormat } from "../../../shared/date-utils";
import { ModalConfirmationComponent } from "../../../shared/components/modal-confirmation.component";
import { downloadFileByDataURI, getMimeTypeByExtension } from "../../../shared/utils";
import { PlayerUploadDocumentationComponent } from "./actions/player-upload-documentation.component";
import { BaseResultsCode } from "../../../shared/model/base-results-code.enum";
import { ResponseHelperService } from "../../../shared/response-helper.service";
import { PlayerViewEnum, PlayerDetailService } from "../../detail/player-detail.service";

@Component({
    selector: 'pas-player-documentation',
    templateUrl: './player-documentation-search.component.html',
    styleUrls: ['./player-documentation-search.component.scss']
})
export class PlayerDocumentationSearchComponent implements OnInit, OnDestroy {
    public accountCode: string;

    //UI attributes
    public errorInSearch: boolean = false;
    public uploadDocumentationPermissions: Array<string> = PermissionsCode.PlayerDetailDocumentationUpload;

    // mt-table configuration
    public tableColumns: TableColumn[];
    public tableRows: DocumentationExtended[];
    public tableLiterals: TableLiterals;
    public tableButtons: TableActionButton[] = [];
    public enableRowSelection: boolean = false;
    private selectedDocumentIDs: Array<number>;
    @ViewChild("table") public table: TableComponent;

    //Vars to store preview documents
    public previewImageFile: SafeUrl;
    public previewPdfFile: SafeUrl;

    private _subscriptions: Subscription[] = [];

    // Templates
    @ViewChild("documentPreviewTemplate") documentPreviewTemplate;

    constructor(public translate: TranslateService,
        private documentationService: DocumentationService,
        private loader: LoaderService,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private responseHelper: ResponseHelperService,
        private sanitizer: DomSanitizer,
        private permissionsService: PermissionsService,
        private playerDetailService: PlayerDetailService) {
    }

    public ngOnInit(): void {

        this.playerDetailService.setActiveView(PlayerViewEnum.PLAYER_DOCUMENTATION);

        this.generateTable();

        this._subscriptions.push(
            this.route.parent.params.subscribe((params: Params) => {
                this.accountCode = params["accountCode"];
                this.searchData(this.accountCode);
            })
        );

        if (this.permissionsService.hasAccess(PermissionsCode.PlayerDetailDocumentation)) {
            this.tableButtons.push({
                id: 1,
                icon: "fa fa-eye",
                text: this.translate.instant("players.documentation.viewDocument"),
                tooltip: this.translate.instant("players.documentation.viewDocument"),
                disabled: true
            });
            this.enableRowSelection = true;
        }
        if (this.permissionsService.hasAccess(PermissionsCode.PlayerDetailDocumentationUpload)) {
            this.tableButtons.push({
                id: 2,
                icon: "fa fa-download",
                text: this.translate.instant("players.documentation.downloadDocument"),
                tooltip: this.translate.instant("players.documentation.downloadDocument"),
                disabled: true
            });
            this.enableRowSelection = true;
        }
        if (this.permissionsService.hasAccess(PermissionsCode.PlayerDetailDocumentationRemove)) {
            this.tableButtons.push({
                id: 3,
                icon: "fa fa-trash",
                text: this.translate.instant("players.documentation.deleteDocument.subtitle"),
                tooltip: this.translate.instant("players.documentation.deleteDocument.subtitle"),
                disabled: true
            });
            this.enableRowSelection = true;
        }
        if (this.permissionsService.hasAccess(PermissionsCode.PlayerDetailDocumentationUpload)) {
            this.tableButtons.push({
                id: 4,
                icon: "fa fa-upload",
                text: this.translate.instant("players.documentation.uploadDocument.subtitle"),
                tooltip: this.translate.instant("players.documentation.uploadDocument.subtitle"),
                disabled: false
            });
            if (this.tableButtons.length > 1) {
                this.tableButtons[this.tableButtons.length - 2].displaySeparatorAfterButton = true;
            }
        }
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * View Document click handler: Download the document from server and show it into a modal window.
     * @param {number} idDocument
     * @param {string} name
     * @param {string} type
     */
    private onViewDocument(idDocument: number, name: string, type: string): void {
        this.loader.show();

        //reset possible previous files of images
        this.previewImageFile = null;
        this.previewPdfFile = null;

        this.documentationService.getPlayerDocument(this.accountCode, idDocument)
            .finally(() => this.loader.hide())
            .subscribe((playerDocument: PlayerDocument) => {
                let mimeType = getMimeTypeByExtension(type);
                if (mimeType) {
                    if (mimeType.indexOf('image') >= 0) {
                        this.previewImageFile = this.sanitizer.bypassSecurityTrustUrl('data:' + mimeType + ';base64,' + playerDocument.content);
                    }
                    else if (mimeType.indexOf('pdf') >= 0) {
                        this.previewPdfFile = this.sanitizer.bypassSecurityTrustResourceUrl('data:' + mimeType + ';base64,' + playerDocument.content);
                    }
                    this.showViewDocumentModal(name);
                }
                else {
                    this.responseHelper.responseHandler(BaseResultsCode.CustomCode, 'players.documentation.viewDocumentNotAvailable', NotificationTypeEnum.Info);
                }
            });
    }

    private documentCanBeShown(type: string): boolean {
        const mimeType: string = getMimeTypeByExtension(type);
        return mimeType && (mimeType.includes("image/") || getMimeTypeByExtension(type) === "application/pdf");
    }

    /**
     * Download Document click handler: Download the document from server with the given name.
     * @param {number} idDocument
     * @param {string} name
     * @param {string} type
     */
    private onDownloadDocument(idDocument: number, name: string, type: string): void {
        let mimeType = getMimeTypeByExtension(type);

        this.loader.show();
        this.documentationService.getPlayerDocument(this.accountCode, idDocument)
            .finally(() => this.loader.hide())
            .subscribe((playerDocument: PlayerDocument) => {
                downloadFileByDataURI('data:' + mimeType + ';base64,' + playerDocument.content, name);
            });
    }

    /**
     * Upload document event handler
     */
    public onUploadDocumentClick(): void {
        this.showUploadDocumentModal();
    }

    /**
     * Delete Document event handler
     * @param {number} idDocument
     * @param {string} name
     */
    private onDeleteDocument(idDocument: number, name: string): void {
        this.showDeleteDocumentModal(idDocument, name);
    }

    public onSelectedRowsChange(event: string[]): void {
        this.selectedDocumentIDs = event && event.map((e: string) => Number.parseInt(e));

        const btnView = this.tableButtons.find(b => b.id === 1);
        const btnDownload = this.tableButtons.find(b => b.id === 2);
        const btnDelete = this.tableButtons.find(b => b.id === 3);
        if (event && event.length === 1) {
            const document = this.tableRows.find(r => r.id == this.selectedDocumentIDs[0]);
            if (btnView) {
                btnView.tooltip = document && !this.documentCanBeShown(document.contentType) ?
                    this.translate.instant("players.documentation.viewDocumentNotAvailable") :
                    this.translate.instant("players.documentation.viewDocument");
                btnView.disabled = !document || !this.documentCanBeShown(document.contentType);
            }
            if (btnDownload)
                btnDownload.disabled = false;
            if (btnDelete)
                btnDelete.disabled = false;
        } else {
            if (btnView)
                btnView.disabled = true;
            if (btnDownload)
                btnDownload.disabled = true;
            if (btnDelete)
                btnDelete.disabled = true;
        }
    }

    public onActionButtonClick(event: any): void {
        if (!event || event.id < 4 && (!this.selectedDocumentIDs || this.selectedDocumentIDs.length !== 1))
            return;

        if (event.id === 4) {
            this.onUploadDocumentClick();
        } else {
            const document = this.tableRows.find(r => r.id == this.selectedDocumentIDs[0]);
            switch (event.id) {
                case 1:
                    this.onViewDocument(document.id, document.documentName, document.contentType);
                    break;
                case 2:
                    this.onDownloadDocument(document.id, document.documentName, document.contentType);
                    break;
                case 3:
                    this.onDeleteDocument(document.id, document.documentName);
                    break;
                default:
                    throw "Unhandled ActionButton event!";
            }
        }
    }

    private clearSelectedDocumentIDs(): void {
        const btnView = this.tableButtons.find(b => b.id === 1);
        const btnDownload = this.tableButtons.find(b => b.id === 2);
        const btnDelete = this.tableButtons.find(b => b.id === 3);
        if (btnView)
            btnView.disabled = true;
        if (btnDownload)
            btnDownload.disabled = true;
        if (btnDelete)
            btnDelete.disabled = true;
        this.selectedDocumentIDs = [];
        this.table && this.table.clearSelectedRows();
    }

    /**
     * Search data
     */
    private searchData(accountCode: string) {
        if (!accountCode)
            return;

        this.errorInSearch = false;
        this.loader.show();

        this.documentationService.getPlayerDocumentation(accountCode)
            .finally(() => this.loader.hide())
            .subscribe((response: Array<DocumentationExtended>) => {
                this.tableRows = this.updateDocumentationData(response);
                this.clearSelectedDocumentIDs();
            }, (error: any) => this.errorInSearch = true);
    }

    /**
     * Generates the table component with player columns: mt-table
     */
    private generateTable(): void {
        this.tableColumns = [{
            type: TableColumnType.DATA,
            id: "documentName",
            label: this.translate.instant("players.documentation.entity.documentName"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }, {
            type: TableColumnType.DATA,
            id: "contentType",
            label: this.translate.instant("players.documentation.entity.contentType"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }, {
            type: TableColumnType.DATA,
            id: "creationDate",
            label: this.translate.instant("players.documentation.entity.creationDate"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }, {
            type: TableColumnType.DATA,
            id: "expirationDate",
            label: this.translate.instant("players.documentation.entity.expirationDate"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant("players.documentation.search.response.noResultsTitle") + " " + this.translate.instant("players.documentation.search.response.noResultsMessage"),
            columnsVisibilityLabel: null,
            totalItemsLabel: null
        };
    }

    /**
     * @return {Array<DocumentationExtended>}
     * @param documentationList
     */
    private updateDocumentationData(documentationList: Array<DocumentationExtended>): Array<DocumentationExtended> {
        documentationList.forEach((document) => {
            document.creationDate = this.formatDateTime(document.creationDate);
            document.expirationDate = this.formatDate(document.expirationDate);
            document.accountCode = this.accountCode;
            document.contentType = document.contentType.toLowerCase();
        });
        return documentationList;
    }

    /**
     * It shows a modal with a document inside.
     * @see documentPreviewTemplate - Custom template to be rendered into the modal.
     * @param {string} name
     */
    private showViewDocumentModal(name: string): void {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
        });
        modalRef.componentInstance.title = name;
        modalRef.componentInstance.showCloseIcon = true;
        modalRef.componentInstance.customTemplate = this.documentPreviewTemplate;
        modalRef.componentInstance.confirmText = this.translate.instant('common.ok');
    }

    /**
     * It shows a modal for upload a new document
     */
    private showUploadDocumentModal(): void {
        const modalRef: NgbModalRef = this.modalService.open(PlayerUploadDocumentationComponent, {
            backdrop: 'static',
            keyboard: false,
        });

        modalRef.componentInstance.accountCode = this.accountCode;
        modalRef.result.then(result => {
            if (result == BaseResultsCode.Ok) {
                //Reload list of documents
                this.searchData(this.accountCode);
            }
        });
    }

    /**
     * It shows a modal asking for delete confirmation
     * @param {number} idDocument
     * @param {string} fileName
     */
    private showDeleteDocumentModal(idDocument: number, fileName: string): void {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant('players.documentation.deleteDocument.title');
        modalRef.componentInstance.message = this.translate.instant('players.documentation.deleteDocument.message');
        modalRef.componentInstance.strongMessage = fileName;
        modalRef.componentInstance.showCloseIcon = true;
        modalRef.componentInstance.confirmText = this.translate.instant('common.ok');
        modalRef.componentInstance.cancelText = this.translate.instant('common.cancel');
        modalRef.result.then(result => {
            (result === "CONFIRM") ? this.deleteDocument(idDocument) : null;
        });
    }

    private formatDate(date): string {
        return date ? getDefaultDateFormat(date) : '-';
    }

    private formatDateTime(date): string {
        return date ? getDefaultDateTimeFormat(date) : '-';
    }

    /**
     * Delete document request API
     * @param {number} idDocument
     */
    private deleteDocument(idDocument: number): void {
        this.loader.show();
        this.documentationService
            .deletePlayerDocument(this.accountCode, idDocument)
            .subscribe(
                (response) => {
                    this.responseHelper.responseHandler(response, 'players.documentation.deleteDocument.response');
                    this.searchData(this.accountCode);
                    //No need to hide loader, it will be hidden after searchData has finished.
                },
                (error) => {
                    console.error('Error deleting player document', error);
                    this.loader.hide();
                }
            );
    }
}
