import { Component, Input, ViewEncapsulation } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { DateInputModel } from "mt-web-components";
import { PlayerDocument } from "../../../model/models";
import { LoaderService } from "../../../../shared/loader.service";
import { ResponseHelperService } from "../../../../shared/response-helper.service";
import { DocumentationService } from "../../player-documentation.service";
import { formatDate, isCompleteDate, isValidDate } from "../../../../shared/date-utils";
import { b64EncodeUnicode } from "../../../../shared/utils";

@Component({
    templateUrl: "./player-upload-documentation.component.html",
    styleUrls: ['./player-upload-documentation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PlayerUploadDocumentationComponent {
    private _accountCode: string;
    expirationDate: string;
    documentList: Array<PlayerDocument>;
    documentListSummary: string = null;
    invalidDateMessage;
    minExpirationDate: Date = new Date(); //Today

    constructor(public activeModal: NgbActiveModal, private documentationService: DocumentationService,
                private responseHelper: ResponseHelperService, private loader: LoaderService, private translate: TranslateService) {
    }


    get accountCode(): string {
        return this._accountCode;
    }

    @Input()
    set accountCode(value: string) {
        this._accountCode = value;
    }

    /**
     * Upload file event handler
     * @param event
     */
    onUploadFileChange(event) {
        let fileList: FileList = event.target.files;


        if (fileList.length > 0) {
            this.documentList = [];

            for (let i = 0; i < fileList.length; i++) {
                let file: File = fileList[i];
                let reader: FileReader = new FileReader();
                if (file) {
                    this.loader.show();
                    // Convert file to Binary String
                    reader.readAsBinaryString(file)
                }

                reader.onloadend = () => {
                    this.loader.hide();
                    let playerDocument: PlayerDocument = {
                        fileName: file.name,
                        expirationDate: this.expirationDate,
                        content: b64EncodeUnicode(reader.result) // Encode the binary-string-file to base64
                    };

                    this.documentList.push(playerDocument);
                    this.documentListSummary = this.documentList.map(item => item.fileName).join(', ');
                };
            }
        } else {
            this.documentList = null;
            this.documentListSummary = null;
        }
    }

    /**
     * Submit form handler. DocumentationService API Request.
     */
    onSubmit() {
        if (this.documentList && this.documentList.length > 0 && this.accountCode) {
            this.loader.show();
            this.documentList.forEach(item => item.expirationDate = this.expirationDate);
            this.documentationService.uploadPlayerDocument(this.accountCode, this.documentList)
                .finally(() => {
                    this.loader.hide();
                })
                .subscribe((response) => {
                    this.responseHelper.responseHandler(response, 'players.documentation.uploadDocument.response');
                    this.cleanFileUploader();
                    this.activeModal.close(response);
                });
        }
    }

    onExpirationDateChanged(date: Date) {
        if (!date) {
            this.cleanExpirationDate();
        } else {
            this.invalidDateMessage = null;
            this.expirationDate = formatDate(date);
        }
    }

    private cleanExpirationDate() {
        this.expirationDate = null;
        this.invalidDateMessage = null;
    }

    /**
     * Clean the "Selected document" after been saved on server.
     */
    private cleanFileUploader() {
        let fileUploader: any = document.getElementById('file-uploader');
        fileUploader ? fileUploader.value = '' : '';
    }
}
