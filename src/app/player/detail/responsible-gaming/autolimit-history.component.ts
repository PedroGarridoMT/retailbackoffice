import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Subscription } from "rxjs/Subscription";
import { TableColumn, TableColumnType, TableColumnVisibilityEnum, TableLiterals, TableActionButton, TableComponent, PermissionsService } from "mt-web-components";
import {
    LimitHistoryExtended,
    PlayerLimitChangeStatusEnum,
    PermissionsCode,
    BaseResultsCode,
    LimitActionOperationEnum,
    PlayerLimitChangeExtended,
    Limit
} from "../../model/models";
import { TranslateService } from "@ngx-translate/core";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { LimitService } from "../../limit.service";
import { LoaderService } from "../../../shared/loader.service";
import { CurrencyService } from "../../../shared/currency.service";
import { ActivatedRoute, Params } from "@angular/router";
import { BaseSearchComponent } from "../../../shared/base-search.component";
import { DefaultSearchRequest } from "../../../shared/model/default-search-request";
import { getDefaultDateTimeFormat } from "../../../shared/date-utils";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ApprovePlayerLimitComponent } from "./actions/approve-player-limit.component";

@Component({
    styleUrls: ['./autolimit-history.component.scss'],
    selector: 'pas-player-autolimits-history',
    templateUrl: './autolimit-history.component.html'
})
export class AutoLimitHistoryComponent extends BaseSearchComponent implements OnInit, OnDestroy {
    accountCode: string;

    //UI attributes
    errorInSearch: boolean = false;

    // mt-table configuration
    table_columns: TableColumn[];
    table_rows: LimitHistoryExtended[];
    tableLiterals: TableLiterals;
    rowSelectionMode: string = "single";
    rowSelectionKey: string = "limitHistoryId";
    enableRowSelection: boolean = false;
    tableButtons: TableActionButton[] = [];
    @ViewChild("table") public table: TableComponent;

    selectedLimitsIds: string[] = [];
    currentLimits: Limit[] = [];

    constructor(private translate: TranslateService,
                private enumsService: PASEnumsService,
                private limitsService: LimitService,
                private loader: LoaderService,
                private currencyService: CurrencyService,
                private modalService: NgbModal,
                private permissionsService: PermissionsService,
                route: ActivatedRoute) {
        super(route);
    }

    ngOnInit(): void {
        this.generateTable();

        if (this.permissionsService.hasAccess(PermissionsCode.PlayerAutoLimitsUpdate)) {
            this.enableRowSelection = true;
            this.tableButtons = [
                {
                    id: 1,
                    icon: "fa fa-check-circle",
                    text: this.translate.instant('players.limits.accept.subtitle'),
                    disabled: true
                },
                {
                    id: 2,
                    icon: "fa fa-times-circle",
                    text: this.translate.instant('players.limits.reject.subtitle'),
                    disabled: true
                }
            ];
        }

        this._subscriptions.push(
            this.route.parent.params.subscribe((params: Params) => {
                this.accountCode = params['accountCode'];
                this._subscriptions.push(
                    this.limitsService.getPlayerLimits(this.accountCode).subscribe(
                        (currentLimits: Limit[]) => {
                            this.currentLimits = currentLimits;
                            this.searchData(this.accountCode);
                        }
                    )
                )
            })
        );
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    public onSelectedRowsChange(event): void {
        this.selectedLimitsIds = event;
        if(this.selectedLimitsIds && this.selectedLimitsIds.length === 1) {
            const selectedLimit = this.table_rows.find(r => r.limitHistoryId === this.selectedLimitsIds[0]);
            const limitChange: PlayerLimitChangeExtended = this.getLimitChange(selectedLimit);

            if (selectedLimit.status === PlayerLimitChangeStatusEnum.Submitted && this.hasSomeLimitToChange(limitChange)) {
                this.tableButtons.forEach(tb => tb.disabled = false);
            } else {
                this.tableButtons.forEach(tb => tb.disabled = true);
            }
        } else {
            this.tableButtons.forEach(tb => tb.disabled = true);
        }
    }

    onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData(this.accountCode);
    }

    onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData(this.accountCode);
    }

    /**
     * Search data
     */
    private searchData(accountCode: string) {
        this.errorInSearch = false;
        this.loader.show();

        this.limitsService.getPlayerLimitsHistory(this.createFilterCriteria({ accountCode: accountCode }))
            .finally(() => this.loader.hide())
            .subscribe(limitsResponse => {
                this.pagingData = {
                    currentPage: this.pagingData.currentPage,
                    pageSize: this.pagingData.pageSize,
                    totalPages: limitsResponse.totalPages,
                    totalItems: limitsResponse.totalItems
                };

                this.table_rows = this.updateLimitResponse(limitsResponse.items);

                this.clearSelectedIds();
            }, () => {
                this.errorInSearch = true;
            });

    }

    /**
     * Create Filter Criteria
     * @param {DefaultSearchRequest} searchFilter
     * @return {DefaultSearchRequest}
     */
    private createFilterCriteria(searchFilter: DefaultSearchRequest): DefaultSearchRequest {
        return Object.assign({}, searchFilter, this.getPaginationData());
    }

    private clearSelectedIds(): void {
        this.selectedLimitsIds = [];
        this.tableButtons.forEach(tb => tb.disabled = true);
        this.table.clearSelectedRows();
    }

    /**
     * Generates the table component with player columns: mt-table
     * @param translations
     */
    private generateTable() {
        this.table_columns = [
            {
                type: TableColumnType.DATA,
                id: "typeTranslated",
                label: this.translate.instant("players.responsibleGaming.currentLimits.type"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "dailyLimit",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.dailyLimit"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "weeklyLimit",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.weeklyLimit"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "monthlyLimit",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.monthlyLimit"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "requestDate",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.requestDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "statusTranslated",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.status"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "closeDate",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.closeDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "validFromDate",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.validFromDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "notesString",
                label: this.translate.instant("players.responsibleGaming.limitsHistory.notes"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('players.responsibleGaming.limitsHistory.noResults'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        }
    }

    /**
     * updateLimitResponse
     * @param {Array<LimitHistoryExtended>} limits
     * @return {Array<LimitHistoryExtended>}
     */
    private updateLimitResponse(limits: Array<LimitHistoryExtended>): Array<LimitHistoryExtended> {
        limits.forEach((limit) => {
            limit.requestDate = this.formatDateTime(limit.requestDate);
            limit.validFromDate = this.formatDateTime(limit.validFromDate);
            limit.closeDate = this.formatDateTime(limit.closeDate);
            limit.statusTranslated = this.formatEnum(limit.status, 'playerLimitChangeStatus');
            limit.typeTranslated = this.formatEnum(limit.limitType, 'limitType');
            limit.notesString = this.formatNotes(limit.notes);

            if (limit.newLimit) {
                limit.dailyLimit = this.formatAmount(limit.newLimit.dailyLimit);
                limit.weeklyLimit = this.formatAmount(limit.newLimit.weeklyLimit);
                limit.monthlyLimit = this.formatAmount(limit.newLimit.monthlyLimit);
            }
        });

        return limits;
    }

    /**
     * Actions handler for each limit in table: Accept / Reject limit actions
     * @param {PlayerLimitChangeExtended} selectedLimit
     */
    onActionButtonClick(event: any) {
        if(!this.selectedLimitsIds || this.selectedLimitsIds.length !== 1 || !event || !event.id) return;

        const selectedLimit = this.table_rows.find(r => r.limitHistoryId === this.selectedLimitsIds[0]);
        const limitChange: PlayerLimitChangeExtended = this.getLimitChange(selectedLimit);

        switch(event.id) {
            case 1:
                limitChange.operation = LimitActionOperationEnum.Accept;
                break;
            case 2:
                limitChange.operation = LimitActionOperationEnum.Reject;
                break;
        }

        this.openUpdateLimitModal(limitChange);
    }

    public getLimitChange(limit: LimitHistoryExtended): PlayerLimitChangeExtended {
        const currentLimit = this.currentLimits.find(item => item.limitType == limit.limitType);
        const limitChange: PlayerLimitChangeExtended = {};
        limitChange.id = Number(limit.limitHistoryId);
        limitChange.name = null;
        limitChange.surname = null;
        limitChange.currentDailyLimit = this.formatAmount(currentLimit.values.dailyLimit);
        limitChange.newDailyLimit = limit.dailyLimit;
        limitChange.currentWeeklyLimit = this.formatAmount(currentLimit.values.weeklyLimit);
        limitChange.newWeeklyLimit = limit.weeklyLimit;
        limitChange.currentMonthlyLimit = this.formatAmount(currentLimit.values.monthlyLimit);
        limitChange.newMonthlyLimit = limit.monthlyLimit;
        limitChange.limitType = limit.limitType;
        limitChange.requestDate = limit.requestDate;
        return limitChange;
    }

    /**
     *
     * @param {PlayerLimitChangeExtended} limit
     * @return {boolean}
     */
    private hasSomeLimitToChange(limit: PlayerLimitChangeExtended): boolean {
        return (
            limit.currentDailyLimit !== limit.newDailyLimit ||
            limit.currentWeeklyLimit !== limit.newWeeklyLimit ||
            limit.currentMonthlyLimit !== limit.newMonthlyLimit
        );
    }

    private openUpdateLimitModal(limit: PlayerLimitChangeExtended): void {
        const modalRef: NgbModalRef = this.modalService.open(ApprovePlayerLimitComponent, {
            backdrop: 'static',
            keyboard: false
        });
        modalRef.componentInstance.limit = limit;

        modalRef.result.then((result: BaseResultsCode) => {
            if (result == BaseResultsCode.Ok) {
                //Reload player limits search
                this.searchData(this.accountCode);
            }
        });
    }

    private formatEnum(value, enumName) {
        return value ? this.enumsService.translateEnumValue(enumName, value) : '-';
    }

    private formatAmount(value): string {
        return value ? this.currencyService.formatAmountWithCurrency(value) : null;
    }

    private formatDateTime(date): string {
        if (date) {
            if (date.indexOf('0001') != -1) return "-"; //Temporary fix to correct null dates sent by backend. They send null dates as 0000-01-01
            else return getDefaultDateTimeFormat(date)
        }
        return '-';
    }

    private formatNotes(notes: Array<string>): string {
        if (!notes || !notes.length) {
            return '-'
        }

        return notes.join(", ");
    }
}
