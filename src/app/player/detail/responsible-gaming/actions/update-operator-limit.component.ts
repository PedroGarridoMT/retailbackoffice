import { Component, Input, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Limit } from "../../../model/models";
import { LoaderService } from "../../../../shared/loader.service";
import { ResponseHelperService } from "../../../../shared/response-helper.service";
import { LimitService } from "../../../limit.service";
import { PASEnumsService } from "../../../../shared/pas-enums.service";
import { LimitEditRequest } from "../../../../shared/model/models";
import { PlayerDetailService } from "../../../../player/detail/player-detail.service";
import { PlayerDetail } from "../../../../player/model/PlayerDetail";
import { CurrencyService } from "../../../../shared/currency.service";
import { TableColumn, TableColumnType, TableColumnVisibilityEnum, TableLiterals } from "mt-web-components";
import { CurrentLimitExtended } from "../../../model/CurrentLimitExtended";
import { Subscription } from "rxjs/Subscription";
import { TranslateService } from "@ngx-translate/core";

@Component({
    styleUrls: ["./update-operator-limit.component.scss"],
    templateUrl: "./update-operator-limit.component.html"
})
export class UpdateOperatorLimitComponent implements OnInit {
    private _limit: Limit;
    newLimit: LimitEditRequest = {} as LimitEditRequest;
    currencyCode: string;
    currentStep: number = 1;
    playerDetail: PlayerDetail;

    // mt-table configuration
    table_columns: TableColumn[];
    table_rows: CurrentLimitExtended[];
    tableLiterals: TableLiterals;

    constructor(public activeModal: NgbActiveModal, private limitService: LimitService,
                private enumsService: PASEnumsService, private responseHelper: ResponseHelperService,
                private loader: LoaderService, private playerDetailService: PlayerDetailService,
                private currencyService: CurrencyService, private translate: TranslateService) {
    }

    ngOnInit(): void {
        this.playerDetail = this.playerDetailService.getStoredPlayer();
        this.currencyCode = this.currencyService.getCode();
        this.table_rows = this.parseLimitData(this.limit);

        this.translate.get([
            'table.noResultsMessage', 'players.responsibleGaming.edit.limitType',
            'players.responsibleGaming.edit.dailyLimit', 'players.responsibleGaming.edit.weeklyLimit',
            'players.responsibleGaming.edit.monthlyLimit'
        ]).subscribe((translations) => {
            this.generateTable(translations);
        });
    }

    @Input()
    set limit(value: Limit) {
        this._limit = value;
    }

    get limit(): Limit {
        return this._limit;
    }

    onNextStep() {
        this.currentStep = 2;
    }

    onPreviousStep() {
        this.currentStep = 1;
    }

    /**
     * onConfirmUpdateLimit
     * approve request param depends on LimitActionOperationEnum value
     */
    onConfirmUpdateLimit() {
        this.loader.show();
        this.limitService.updateOperatorLimit(this.limit.id, this.getOldLimitValuesIfEmptyValues())
            .finally(() => {
                this.loader.hide();
            })
            .subscribe(response => {
                this.responseHelper.responseHandler(response, 'players.responsibleGaming.edit.response');
                this.activeModal.close(response);
            });
    }


    formatAmount(value): string {
        return value ? this.currencyService.formatAmountWithCurrency(value) : null;
    }

    isSameLimit(): boolean {
        if (this.newLimit.dailyLimit != this.limit.values.dailyLimit && this.newLimit.dailyLimit > 0) {
            return false;
        }
        if (this.newLimit.weeklyLimit != this.limit.values.weeklyLimit && this.newLimit.weeklyLimit > 0) {
            return false;
        }
        if (this.newLimit.monthlyLimit != this.limit.values.monthlyLimit && this.newLimit.monthlyLimit > 0) {
            return false;
        }
        return true;
    }

    private formatEnum(value, enumName) {
        return value ? this.enumsService.translateEnumValue(enumName, value) : '-';
    }

    /**
     * generateTable
     * @param {string | any} translations
     */
    private generateTable(translations: string | any) {
        this.table_columns = [
            {
                type: TableColumnType.DATA,
                id: "limitTypeTranslated",
                label: translations['players.responsibleGaming.edit.limitType'],
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "operatorDailyLimit",
                label: translations['players.responsibleGaming.edit.dailyLimit'],
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "operatorWeeklyLimit",
                label: translations['players.responsibleGaming.edit.weeklyLimit'],
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "operatorMonthlyLimit",
                label: translations['players.responsibleGaming.edit.monthlyLimit'],
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ];

        this.tableLiterals = {
            noResultsMessage: translations['table.noResultsMessage'],
            columnsVisibilityLabel: null
        }
    }

    /**
     * Limit to CurrentLimitExtended parser
     *
     * @param {Limit} limit
     * @return {Array<CurrentLimitExtended>}
     */
    private parseLimitData(limit: Limit): Array<CurrentLimitExtended> {
        return [{
            limitTypeTranslated: this.formatEnum(limit.limitType, 'limitType'),
            operatorDailyLimit: this.formatAmount(limit.values ? limit.values.dailyLimit : null),
            operatorWeeklyLimit: this.formatAmount(limit.values ? limit.values.weeklyLimit : null),
            operatorMonthlyLimit: this.formatAmount(limit.values ? limit.values.monthlyLimit : null),
        }];
    }

    /**
     * Returns a newLimit with old limit values if they new limits are empty. (PAS API restriction)
     *
     * @return {LimitEditRequest}
     */
    private getOldLimitValuesIfEmptyValues(): LimitEditRequest {
        let limit = JSON.parse(JSON.stringify(this.newLimit));

        if (!limit.dailyLimit) {
            limit.dailyLimit = this.limit ? this.limit.values.dailyLimit : null;
        }
        if (!limit.weeklyLimit) {
            limit.weeklyLimit = this.limit ? this.limit.values.weeklyLimit : null;
        }
        if (!limit.monthlyLimit) {
            limit.monthlyLimit = this.limit ? this.limit.values.monthlyLimit : null;
        }
        return limit;
    }
}
