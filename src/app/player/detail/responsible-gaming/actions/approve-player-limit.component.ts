import { Component, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { PlayerLimitChangeExtended } from "../../../model/models";
import { LoaderService } from "../../../../shared/loader.service";
import { ResponseHelperService } from "../../../../shared/response-helper.service";
import { LimitService } from "./../../../limit.service";
import { LimitActionOperationEnum } from "../../../model/LimitActionOperationEnum";
import { PASEnumsService } from "../../../../shared/pas-enums.service";

@Component({
    styleUrls: ["./approve-player-limit.component.scss"],
    templateUrl: "./approve-player-limit.component.html"
})
export class ApprovePlayerLimitComponent {
    constructor(public activeModal: NgbActiveModal, private limitService: LimitService,
                private enumsService: PASEnumsService, private responseHelper: ResponseHelperService,
                private loader: LoaderService) {
    }

    selectedReason: string;
    limitOperationEnum = LimitActionOperationEnum;

    private _limit: PlayerLimitChangeExtended;


    @Input()
    set limit(value: PlayerLimitChangeExtended) {
        this._limit = value;
    }

    get limit(): PlayerLimitChangeExtended {
        return this._limit;
    }

    /**
     * onConfirmUpdateLimit
     * approve request param depends on LimitActionOperationEnum value
     */
    onConfirmUpdateLimit() {
        this.loader.show();
        this.limitService
            .updatePlayerLimitChange(
                this.limit.id,
                {
                    approve: this.limit.operation === LimitActionOperationEnum.Accept,
                    notes: this.selectedReason
                }
            )
            .finally(() => {
                this.loader.hide();
            })
            .subscribe(response => {
                let translationKey = this.limit.operation === LimitActionOperationEnum.Accept ?
                    'players.limits.accept.response' : 'players.limits.reject.response';
                this.responseHelper.responseHandler(response, translationKey);
                this.activeModal.close(response);
            });
    }

    /**
     *
     * @param limitType
     * @return {string | any}
     */
    translateLimitType(limitType) {
        return this.enumsService.translateEnumValue('limitType', limitType);
    }

}
