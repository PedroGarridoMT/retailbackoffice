import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { NotificationsService, NotificationTypeEnum } from "mt-web-components";
import {
    CurrentLimitExtended,
    Limit,
    LimitTypeEnum,
    LimitValues,
    PermissionsCode,
    BaseResultsCode
} from "../../model/models";
import { TranslateService } from "@ngx-translate/core";
import { PASEnumsService } from "../../../shared/pas-enums.service";
import { LimitService } from "../../limit.service";
import { LoaderService } from "../../../shared/loader.service";
import { CurrencyService } from "../../../shared/currency.service";
import { Observable } from "rxjs/Observable";
import { ActivatedRoute, Params } from "@angular/router";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { UpdateOperatorLimitComponent } from "./actions/update-operator-limit.component";

@Component({
    selector: 'pas-player-limits-table',
    styleUrls: ['./limits-table.component.scss'],
    templateUrl: './limits-table.component.html'
})
export class LimitsTableComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    accountCode: string;

    //UI attributes
    errorInSearch: boolean = false;
    updateLimitPermissions: Array<string> = PermissionsCode.OperatorLimitsUpdate;

    // Table data
    table_rows: CurrentLimitExtended[] = null;

    //Vars to store current player limits
    operatorLimits: Array<Limit>;
    autoLimits: Array<Limit>;

    private _subscriptions: Subscription[] = [];

    constructor(private translate: TranslateService, private enumsService: PASEnumsService,
                private limitsService: LimitService, private loader: LoaderService, private modalService: NgbModal,
                private currencyService: CurrencyService, private route: ActivatedRoute,
                private notifications: NotificationsService) {
    }

    ngOnInit(): void {
        this._subscriptions.push(
            this.route.parent.params.subscribe((params: Params) => {
                this.accountCode = params['accountCode'];
                this.searchData(this.accountCode);
            })
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * Show update operator limit modal.
     * @param {number} idLimit
     * @param limitType
     * @param {LimitValues} limitValues
     */
    updateOperatorLimit(idLimit: number, limitType: LimitTypeEnum, limitValues: LimitValues) {
        if (idLimit && limitValues) {
            this.showUpdateOperatorLimitModal({ id: idLimit, limitType: limitType, values: limitValues });
        } else {
            this.notifications.add({
                type: NotificationTypeEnum.Warning,
                text: this.translate.instant('players.responsibleGaming.edit.warning.invalidLimit')
            });
        }
    }

    /**
     * Search data
     */
    private searchData(accountCode: string) {
        this.errorInSearch = false;
        this.loader.show();

        return Observable.forkJoin(
                this.limitsService.getOperatorLimits(accountCode),
                this.limitsService.getPlayerLimits(accountCode)
            )
            .finally(() => this.loader.hide())
            .subscribe(res => {
                this.operatorLimits = res[0];
                this.autoLimits = res[1];

                let limitRows: Array<CurrentLimitExtended> = [];

                //Create a table row for each limit type (LimitTypeEnum)
                Object.keys(LimitTypeEnum).forEach(limitType => {
                    limitRows.push(this.createLimitRow(limitType))
                });

                this.table_rows = limitRows;

            }, () => {
                this.operatorLimits = null;
                this.autoLimits = null;
                this.errorInSearch = true;
            });

    }

    /**
     * Given a limit type, creates a CurrentLimitExtended object containing data of the current player and operator
     * limits for this type of limit.
     *
     * @param {LimitTypeEnum | string} limitType
     * @returns {CurrentLimitExtended}
     */
    private createLimitRow(limitType: LimitTypeEnum | string): CurrentLimitExtended {
        let operatorLimit = this.operatorLimits.find(item => item.limitType == limitType);
        let playerLimit = this.autoLimits.find(item => item.limitType == limitType);

        let limitRow: CurrentLimitExtended = {
            limitType: limitType as LimitTypeEnum,
            limitTypeTranslated: this.enumsService.translateEnumValue('limitType', limitType),

            operatorLimitId: operatorLimit ? operatorLimit.id : null,
            operatorDailyLimit: operatorLimit ? this.formatAmount(operatorLimit.values.dailyLimit) : "-",
            operatorWeeklyLimit: operatorLimit ? this.formatAmount(operatorLimit.values.weeklyLimit) : "-",
            operatorMonthlyLimit: operatorLimit ? this.formatAmount(operatorLimit.values.monthlyLimit) : "-",
            operatorLimitValues: operatorLimit ? operatorLimit.values : null,// For being sure that edition will use this values

            playerDailyLimit: playerLimit ? this.formatAmount(playerLimit.values.dailyLimit) : "-",
            playerWeeklyLimit: playerLimit ? this.formatAmount(playerLimit.values.weeklyLimit) : "-",
            playerMonthlyLimit: playerLimit ? this.formatAmount(playerLimit.values.monthlyLimit) : "-"
        };

        return limitRow;
    }

    /**
     * It shows a modal with a UpdateOperatorLimitComponent
     *
     * @param limit
     */
    private showUpdateOperatorLimitModal(limit: Limit): void {
        const modalRef: NgbModalRef = this.modalService.open(UpdateOperatorLimitComponent, {
            backdrop: 'static',
            keyboard: false
        });

        modalRef.componentInstance.limit = limit;

        modalRef.result.then((result: BaseResultsCode) => {
            if (result == BaseResultsCode.Ok) {
                //Reload player limits search
                this.searchData(this.accountCode);
            }
        });
    }

    private formatAmount(value): string {
        return value ? this.currencyService.formatAmountWithCurrency(value) : null;
    }
}
