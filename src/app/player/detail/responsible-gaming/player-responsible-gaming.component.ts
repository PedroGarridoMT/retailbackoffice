import { Component, OnInit } from "@angular/core";
import { PlayerDetailService, PlayerViewEnum } from './../player-detail.service';

@Component({
    selector: 'pas-responsible-gaming',
    templateUrl: './player-responsible-gaming.component.html',
    styleUrls: ['./player-responsible-gaming.component.scss']
})
export class PlayerResponsibleGamingComponent implements OnInit {

    constructor(private playerDetailService: PlayerDetailService) {}

    public ngOnInit(): void {
        this.playerDetailService.setActiveView(PlayerViewEnum.PLAYER_RESPONSIBLE_GAMING);
    }
}
