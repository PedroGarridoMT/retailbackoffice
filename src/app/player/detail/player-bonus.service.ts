import { Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Http, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { ConfigService } from "./../../core/config.service";
import { HelperService } from "./../../shared/helper.service";
import { CancelBonusRequest, PlayerBonus, ApplicableBonus, AddPlayerBonusRequest } from "../model/models";

@Injectable()
export class BonusService {

    constructor(private http: Http, private config: ConfigService) { }

    /**
     * Get a list of currently active and pending bonuses for a specified player. This operation requires the user to have the PlayerActiveAndPendingBonuses permission assigned. If the user does not have the permission a Forbidden (403) response will be returned.
     * @summary Get a list of currently active and pending bonuses for a specified player.
     * @param {string} accountCode Account code.
     */
    public getPlayerActiveAndPendingBonuses(accountCode: string): Observable<Array<PlayerBonus>> {
        return this.http.get(this.config.URLs.player.activeAndPendingBonuses.replace(":accountCode", accountCode))
            .map((bonusesResponse: Response) => {
                return bonusesResponse.json();
            });
    }

    /**
     * Cancel player's active or pending bonus
     * @param {string} accountCode
     * @param {string} bonusKey
     * @param {CancelBonusRequest} cancelBonusRequest
     * @return {Observable<any>}
     *
     * @memberOf PlayerService
      */
    public cancelPlayerActiveOrPendingBonus(accountCode: string, bonusKey: string, cancelBonusRequest: CancelBonusRequest): Observable<any> {
        const urlTemplate = this.config.URLs.player.cancelActiveOrPendingBonus;
        const endpoint = urlTemplate.replace(":accountCode", accountCode).replace(":bonusKey", bonusKey);

        return this.http.delete(endpoint, { body: cancelBonusRequest })
            .map((response: Response) => HelperService.getBaseResultCode(response.status)).catch(HelperService.handleError);
    }

    /**
     * Get a list of applicable bonuses for a soecified player.
     * @param {string} accountCode Account code.
     */
    public getApplicableBonuses(accountCode: string): Observable<Array<ApplicableBonus>> {
        return this.http.get(this.config.URLs.player.applicableBonuses.replace(":accountCode", accountCode))
            .map((bonusesResponse: Response) => {
                return bonusesResponse.json();
            });
    }

    /**
     * Assign bonus to a player
     * @param {string} accountCode Account code.
     * @param {AddPlayerBonusRequest} addPlayerBonusRequest Add player bonus request object.
     */
    public addPlayerBonus(accountCode: string, addPlayerBonusRequest: AddPlayerBonusRequest): Observable<any> {
        return this.http.post(this.config.URLs.player.addBonus.replace(":accountCode", accountCode), addPlayerBonusRequest)
            .map((response: Response) => HelperService.getBaseResultCode(response.status)).catch(HelperService.handleError);
    }
}
