﻿import { Injectable, OnDestroy } from "@angular/core";
import { Http, Response, URLSearchParams } from "@angular/http";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Subscription } from "rxjs/Subscription";
import {
    AccountStatusChangeReasonEnum,
    AccountStatusEnum,
    ApiResponse,
    BaseResultsCode,
    ChangeStatusResponse,
    PatchDocument,
    PlayerDetailsResponse,
    PlayerSearchRequest,
    PlayerSearchResponse,
    PlayerStatusRequest,
    PlayerWallet,
    TitleBarPreviousRouteDetails,
    PlayerFilterCriteria,
    DownloadableFile,
    DefaultSearchRequest,
    BlacklistedPersonsSearchResponse,
    BlacklistItem
} from "./model/models";
import { HelperService } from "../shared/helper.service";
import { ConfigService } from "../core/config.service";
import { AppRoutes } from "../app-routes";
import { sanitizeArrayParam } from '../shared/utils';

@Injectable()
export class PlayerService implements OnDestroy {
    private previousUrl: string = null;

    public static PREVIOUS_ROUTE_KEY: string = 'previousRoute';
    public static LAST_SEARCH_PREFIX: string = 'last-search:';

    private _subscriptions: Subscription[] = [];

    constructor(private http: Http, private config: ConfigService, private router: Router, private route: ActivatedRoute) {

        //Store previous url
        this._subscriptions.push(
            router.events
                .filter(event => event instanceof NavigationEnd)
                .subscribe((e: NavigationEnd) => {
                    this.previousUrl = e.url;
                })
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * Gets an ordered paged list of registered players that comply with the filter criteria
     * @param playerSearchRequest
     * @returns {Observable<PlayerSearchResponse>}
     */
    public getPlayers(playerSearchRequest: PlayerSearchRequest): Observable<PlayerSearchResponse> {
        if (!playerSearchRequest || !playerSearchRequest.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling searchPlayers');
        }

        if (!playerSearchRequest || !playerSearchRequest.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling searchPlayers');
        }

        let params: URLSearchParams = new URLSearchParams();
        params.set('searchRequest', btoa(JSON.stringify(playerSearchRequest))); // PlayerSearchRequest Base64 encoded

        return this.http.get(this.config.URLs.player.search, { params: params })
            .map((playerSearchResponse: Response) => {
                return playerSearchResponse.json();
            });
    }

    /**
     * Gets an ordered paged list of account codes that comply with the filter criteria
     * @param playerSearchRequest
     * @returns {Observable<string[]>}
     */
    public getAccountCodes(playerSearchRequest: PlayerSearchRequest): Observable<string[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('searchRequest', btoa(JSON.stringify(playerSearchRequest))); // PlayerSearchRequest Base64 encoded

        return this.http.get(this.config.URLs.player.accountCodes, { params: params })
            .map((playerSearchResponse: Response) => {
                return playerSearchResponse.json();
            });
    }

    /**
     * Get a CSV report with players that meets filter criteria and visible columns.
     * @param playerFilterCriteria
     * @param columnNames
     * @returns {Observable<DownloadableFile>}
     */
    public getPlayersReport(playerFilterCriteria: PlayerFilterCriteria, columnNames: string[]): Observable<DownloadableFile> {
        let params: URLSearchParams = new URLSearchParams();
        params.set("filters", btoa(JSON.stringify(playerFilterCriteria))); // filters base64 encoded
        params.set("columnNames", columnNames.toString());

        return this.http.get(this.config.URLs.player.playersReport, { params: params })
            .map((response: Response) => response.json());
    }

    /**
     * Get the player detail
     * @param accountCode Account code.
     * @return  Observable<PlayerDetailsResponse>
     */
    public getPlayerDetail(accountCode: string): Observable<PlayerDetailsResponse> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerDetail');
        }

        return this.http
            .get(this.config.URLs.player.details.replace(':accountCode', accountCode), { params: new URLSearchParams() })
            .map((player: Response) => {
                return player.json();
            });
    }

    /**
     * Used to partially modify a player. Currently, this operation can be used to:
     *
     * - Revoke the self-exclusion of a player
     * - Verify the player identity, according to his/her uploaded personal documentation.
     * - Invalidate the player identity, according to his/her uploaded personal documentation.
     * - Edit player details. When editing player details, this operation validates the provided player personal details.
     * Please, review the *Player Validations* section in this documentation for more information.
     * @returns {Observable<BaseResultsCode>}
     */
    public patchPlayer(accountCode: string, playerPatch: Array<PatchDocument>): Observable<BaseResultsCode> {
        if (!accountCode || !playerPatch) {
            throw new Error('A required parameter was null or undefined when calling patchPlayer.');
        }
        return this.http
            .patch(this.config.URLs.player.details.replace(':accountCode', accountCode), playerPatch)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch(HelperService.handleError);
    }

    /**
     * Gets all the wallets for a given player.
     * Gets a list of wallets for a given player.
     * @return  Observable<PlayerWallet[]>
     */
    public getPlayerWallets(accountCode: string): Observable<Array<PlayerWallet>> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerBalance');
        }

        return this.http
            .get(this.config.URLs.player.balance.replace(':accountCode', accountCode))
            .map((playerWallets: Response) => {
                let wallets = playerWallets.json();
                if (wallets && wallets.length > 0) {
                    wallets.forEach((wallet) => {
                        wallet["balance"] = parseFloat(wallet["balance"]);
                    });
                }
                return wallets;
            });
    }

    public setPreviousUrl(previousUrl: string) {
        this.previousUrl = previousUrl;
    }

    public getPreviousUrl(): string {
        return this.previousUrl;
    }

    public updatePlayerStatus(accountCode: string, newStatus: AccountStatusEnum, newStatusReason: AccountStatusChangeReasonEnum): Observable<ApiResponse> {
        if (!accountCode || !newStatus) {
            throw new Error('Required parameter accountCode or newStatus was null or undefined when calling updateStatus.');
        }

        //TODO remove the part after '||' when the back-end is complete.
        //When changing from "In process" to "Activated", no reason should be sent
        let request: PlayerStatusRequest = { status: newStatus, reason: newStatusReason || AccountStatusChangeReasonEnum.Other };
        return this.http.put(this.config.URLs.player.status.replace(':accountCode', accountCode), request)
            .map(response => {
                let changeStatusResponse: ChangeStatusResponse = response.json();
                return {
                    statusCode: HelperService.getBaseResultCode(response.status),
                    value: changeStatusResponse ? changeStatusResponse.value : null
                };
            })
            .catch((error) => {
                return Observable.from([{ statusCode: HelperService.getBaseResultCode(error.status), value: null }]);
            });
    }

    /**
     * Get previous route details
     * @return {TitleBarPreviousRouteDetails}
     */
    public getpreviousRouteDetails(): TitleBarPreviousRouteDetails {
        let previousPagePath: string;
        let previousPageQueryParams: object;
        let previousPageLabel: string = this.route.snapshot.queryParamMap.get(PlayerService.PREVIOUS_ROUTE_KEY) || "players.home.search";

        switch (previousPageLabel) {
            case "players.home.actionSearchLimits":
                previousPagePath = AppRoutes.searchPlayerAutoLimitChanges;
                break;
            case "players.home.search":
                previousPagePath = AppRoutes.playersSearch;
                break;
            case "transactions.home.search":
                previousPagePath = AppRoutes.transactionsSearch;
                break;
            default:
                previousPagePath = AppRoutes.playersSearch;
                break;
        }

        previousPageQueryParams = this.getLastQueryParams(previousPageLabel);

        return {
            previousPagePath,
            previousPageLabel,
            previousPageQueryParams
        };
    }

    /**
     * Save query params to session storage
     * @param {string} key
     * @param {object} filterCriteria
     */
    public setLastQueryParams(key: string, filterCriteria: object): void {
        let urlParams = JSON.parse(JSON.stringify(filterCriteria));
        sessionStorage.setItem(PlayerService.LAST_SEARCH_PREFIX + key, JSON.stringify(urlParams));
    }

    /**
     * Get query params from session storage
     * @param {string} key
     * @return {object}
     */
    public getLastQueryParams(key: string): object {
        return JSON.parse(sessionStorage.getItem(PlayerService.LAST_SEARCH_PREFIX + key));
    }

    /**
     * Gets an ordered paged with a list of blacklisted persons related or linked to the company where they work, that comply with the filter criteria. e.g. A person who works in \"Wanabet\" is not able to bet in \"Wanabet\".
     * @summary Get an ordered paged with a list of black-listed persons
     * @param request
     * @see pageSize Page size (Pagination)
     * @see pageNumber Page number / Current page number  (Pagination)
     * @see sortBy Sort by property. Default value &#39;username&#39;.
     * @see sortDirection Sort direction. &#39;ASC&#39; / &#39;DESC&#39; possible values. Default value &#39;DESC&#39;  (Pagination)
     * @see documentNumber Document number.
     * @see name Player&#39;s name.
     * @see username Player&#39;s username.
     * @see dateFrom Import date lower limit. This argument is a string containing an ISO 8601 date and time, including the time zone (e.g.: 2017-05-25T14:13:27Z).
     * @see dateTo Import date upper limit. This argument is a string containing an ISO 8601 date and time, including the time zone (e.g.: 2017-05-25T14:13:27Z).
     * @see documentType Type of document type user is registered with, e.g. &#39;documentType&#x3D;NationalId&#39;. See possible values in &#39;#/definitions/DocumentTypeEnum&#39; definition.
     */
    public getBlacklistedPlayers(request: DefaultSearchRequest): Observable<BlacklistedPersonsSearchResponse> {
        if (!request) {
            throw new Error('Required request object was null or undefined when calling getBlacklistedPlayers');
        }

        if (!request || !request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling getBlacklistedPlayers');
        }

        if (!request || !request.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling getBlacklistedPlayers');
        }

        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            let paramValue: string = request[key];
            if (key && paramValue !== undefined) {
                params.set(key, sanitizeArrayParam(paramValue));
            }
        }

        return this.http.get(this.config.URLs.player.blacklisted, { params: params })
            .map((response: Response) =>  response.json());
    }

    /**
     * @summary Add persons to blacklist
     * @param { BlacklistItem[] } blacklist
     * @see blacklistedPersons List of persons that need to be added in blacklist
     */
    public addBlacklistedPersons(blacklist: BlacklistItem[]): Observable<BaseResultsCode> {
        if (!blacklist || !blacklist.length) {
            throw new Error('A required parameter was null, undefined or empty when calling addBlacklistedPersons.');
        }
        return this.http
            .post(this.config.URLs.player.blacklisted, blacklist)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch(HelperService.handleError);
    }
}
