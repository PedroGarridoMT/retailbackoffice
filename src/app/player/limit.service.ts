﻿/**
 * Backoffice PAS 3 API
 * # Introduction  This API provides web applications with the necessary features to manage a casino operation. Destined applications are Back Office web applications.  # Authentication and authorization  This API allows only authenticated Back Office users to access the different operations. Thus, in order to be able to call this API it is first necesary to open a user session via the `POST /session` operation. This operation will generate a session token, stored in cookies. Make sure your web application is able to use cookies, because otherwise the API will not be able to authenticate the Back Office users.  Session tokens stored in cookies expire after a configurable period of time. You will be notified about the expiration of your session in the response of the `POST /session` operation. In order to prevent a Back Office user from getting logged out of the Back Office while he/she is actively using the tool you are able to refresh the session token via the `PATCH /session`.  # Cross-Site Request Forgery  This API has active Cross-Site Request Forgery controls. When opening a user session with `POST /session`, a `XSRF-TOKEN` cookie is created. In order to perform successful requests to the API you need to read that cookie and provide its value in an `X-XSRF-TOKEN` HTTP header in every request.
 *
 * OpenAPI spec version: 1.0.0
 *
 */

import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from "@angular/http";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ConfigService } from "../core/config.service";
import {
    BaseResultsCode,
    DefaultSearchRequest,
    Limit,
    LimitEditRequest,
    LimitHistorySearchResponse,
    PlayerLimitApprovalRequest,
    PlayerLimitChangeSearchResponse
} from "./model/models";
import { HelperService } from "../shared/helper.service";
import { sanitizeArrayParam } from "../shared/utils";


@Injectable()
export class LimitService {
    APIResources;

    constructor(private http: Http, private config: ConfigService) {
        this.APIResources = {
            operatorLimits: this.config.getEndpoint('baseUrl') + '/players/limits-operator/{operatorLimitId}',
            operatorPlayerLimits: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/limits-operator',
            playerLimits: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/limits',
            playerLimitsHistory: this.config.getEndpoint('baseUrl') + '/players/{accountCode}/limits-history',
            playerLimitsChanges: this.config.getEndpoint('baseUrl') + '/players/limits/changes',
            playerLimitsChangesDetail: this.config.getEndpoint('baseUrl') + '/players/limits/changes/{limitChangeId}'
        };
    }

    /**
     * Gets the operator limits amounts by the given account code of a player. This operation requires the user to have the OperatorLimits permission assigned. If the user does not have the permission a Forbidden (403) response will be returned.
     * @summary Get the operator limits amounts by the given account code of a player.
     * @param accountCode Account code.
     */
    public getOperatorLimits(accountCode: string): Observable<Array<Limit>> {
        return this.http.get(this.APIResources.operatorPlayerLimits.replace('{accountCode}', accountCode))
            .map((limitsResponse: Response) => {
                return limitsResponse.json();
            });
    }

    /**
     * Gets a list of Limits for a specified player
     * @summary Get a list of Limits for a player
     * @param accountCode Account code.
     */
    public getPlayerLimits(accountCode: string): Observable<Limit[]> {
        if (!accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerLimits.');
        }
        return this.http.get(this.APIResources.playerLimits.replace('{accountCode}', accountCode))
            .map((response: Response) => {
                return response.json();
            });
    }

    /**
     * Get an ordered, paged list of change requests on player limits
     * @summary Get an ordered paged of pending approval limits.
     *
     * @see requestParam pageSize Page size (Pagination)
     * @see requestParam pageNumber Page number / Current page number  (Pagination)
     * @see requestParam sortBy Sort by property. Default value &#39;requestDate&#39;.
     * @see requestParam sortDirection Sort direction. &#39;ASC&#39; / &#39;DESC&#39; possible values. Default value &#39;DESC&#39;  (Pagination)
     * @see requestParam username Username
     * @see requestParam accountCode Account code
     * @see requestParam requestDateFrom Request date from interval. This argument is a string containing an ISO 8601 date and time, including the time zone (e.g.: 2017-05-25T14:13:27Z).
     * @see requestParam requestDateTo Request date to interval. This argument is a string containing an ISO 8601 date and time, including the time zone (e.g.: 2017-05-25T14:13:27Z).
     * @see requestParam status Limit change status.
     * @see requestParam type Limit type
     */
    public getPlayerLimitsChanges(request: DefaultSearchRequest): Observable<PlayerLimitChangeSearchResponse> {
        if (!request) {
            throw new Error('Required request object was null or undefined when calling getPlayerLimitsChanges.');
        }
        if (!request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling getPlayerLimitsChanges.');
        }
        if (!request.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling getPlayerLimitsChanges.');
        }

        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            let paramValue: string = request[key];
            if (key && paramValue !== undefined) {
                params.set(key, sanitizeArrayParam(paramValue));
            }
        }

        return this.http.get(this.APIResources.playerLimitsChanges, { params: params })
            .map((response: Response) => {
                return response.json();
            });
    }

    /**
     * Update the operator limit of a player by the given 'operatorLimitId'. A valid 'operatorLimitId' is a value returned by the 'getOperatorLimits' operation. If some of the fields's values in request is sent null, then the limit will be updated with 0. For that  reason, all the field's values are required. This operation requires the user to have the OperatorLimitsUpdate permission assigned. If the user does not have the permission a Forbidden (403) response will be returned.
     * @summary Update the operator limit of a player by the given operatorLimitId.
     * @param operatorLimitId The identifier of the operator limit to be changed.
     * @param request The Limit Edit request.
     */
    public updateOperatorLimit(operatorLimitId: number, request: LimitEditRequest): Observable<BaseResultsCode> {
        return this.http.put(this.APIResources.operatorLimits.replace('{operatorLimitId}', operatorLimitId), request)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch((error) => {
                return Observable.from([HelperService.getBaseResultCode(error.status)]);
            });
    }

    /**
     * Update the status and notes of an existing change request of player limits.
     * @summary Post for a specified PlayerLimitIdD
     * @param limitChangeId
     * @param request The request.
     */
    public updatePlayerLimitChange(limitChangeId: number, request: PlayerLimitApprovalRequest): Observable<BaseResultsCode> {
        if (!limitChangeId) {
            throw new Error('Required parameter limitChangeId was null or undefined when calling updatePlayerLimitChange.');
        }
        if (!request) {
            throw new Error('Required parameter request was null or undefined when calling updatePlayerLimitChange.');
        }

        return this.http.put(this.APIResources.playerLimitsChangesDetail.replace('{limitChangeId}', limitChangeId), request)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch((error) => {
                return Observable.from([HelperService.getBaseResultCode(error.status)]);
            });
    }


    /**
     * Gets an ordered paged with the available limits history according to the player. This operation requires the user to have the PlayerAutoLimitsHistorySearch permission assigned. If the user does not have the permission a Forbidden (403) response will be returned.
     * @summary Get an ordered paged list of Limits History
     * @see requestParam accountCode Account code.
     * @see requestParam pageSize Page size (Pagination)
     * @see requestParam pageNumber Page number / Current page number  (Pagination)
     * @see requestParam sortBy Sort by property. Default value &#39;requestDate&#39;.
     * @see requestParam sortDirection Sort direction. &#39;ASC&#39; / &#39;DESC&#39; possible values. Default value &#39;DESC&#39;  (Pagination)
     */
    public getPlayerLimitsHistory(request: DefaultSearchRequest): Observable<LimitHistorySearchResponse> {
        if (!request) {
            throw new Error('Required request param was null or undefined when calling getPlayerLimitsHistory.');
        }
        if (!request.accountCode) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerLimitsHistory.');
        }
        if (!request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling getPlayerLimitsHistory.');
        }
        if (!request.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling getPlayerLimitsHistory.');
        }

        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            let paramValue: string = request[key];
            if (key && paramValue !== undefined) {
                params.set(key, sanitizeArrayParam(paramValue));
            }
        }

        return this.http.get(this.APIResources.playerLimitsHistory.replace('{accountCode}', request.accountCode), { params: params })
            .map((response: Response) => {
                return response.json();
            });
    }
}
