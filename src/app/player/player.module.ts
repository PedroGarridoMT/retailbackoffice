import { NgModule } from "@angular/core";
import { PlayersMainComponent } from "./player-main.component";
import { PlayersSearchComponent } from "./search/player-search.component";
import { PlayerBlacklistSearchComponent } from "./blacklist-search/blacklist-search.component";
import { PlayerBlacklistAddComponent } from "./blacklist-add/blacklist-add.component";
import { SharedModule } from "../shared/shared.module";
import { AppRoutingModule } from "../app-routing.module";
import { PlayerService } from "./player.service";
import { PlayerDetailComponent } from "./detail/player-detail.component";
import { PlayerDetailHeaderComponent } from "./detail/header/player-header.component";
import { PlayerPersonalDataComponent } from "./detail/personal-data/player-personal-data.component";
import { PlayerDetailService } from "./detail/player-detail.service";
import { PlayerTransactionsComponent } from "./detail/transactions/player-transactions.component";
import { ModalConfirmationComponent } from "../shared/components/modal-confirmation.component";
import { PlayerStatusChangeComponent } from "./detail/actions/player-status-change.component";
import { ManualAdjustmentComponent } from "./detail/actions/manual-adjustment.component";
import { DocumentationService } from "./detail/player-documentation.service";
import { PlayerDocumentationSearchComponent } from "./detail/documentation/player-documentation-search.component";
import { PlayerUploadDocumentationComponent } from "./detail/documentation/actions/player-upload-documentation.component";
import { PlayerResponsibleGamingComponent } from "./detail/responsible-gaming/player-responsible-gaming.component";
import { PlayerNotesService } from "./detail/player-notes.service";
import { PlayerBonusesComponent } from "./detail/bonuses/player-bonuses.component";
import { PlayerActiveAndPendingBonusesComponent } from "./detail/bonuses/active-and-pending-bonuses.component";
import { PlayerCancelBonusComponent } from "./detail/bonuses/actions/cancel-bonus.component";
import { PlayerAddBonusComponent } from "./detail/actions/add-bonus.component";
import { BonusService } from "./detail/player-bonus.service";
import { LimitService } from "./limit.service";
import { AutoLimitChangesSearchComponent } from "./limit-changes/autolimit-changes-search.component";
import { AutoLimitHistoryComponent } from "./detail/responsible-gaming/autolimit-history.component";
import { LimitsTableComponent } from "./detail/responsible-gaming/limits-table.component";
import { ApprovePlayerLimitComponent } from "./detail/responsible-gaming/actions/approve-player-limit.component";
import { UpdateOperatorLimitComponent } from "./detail/responsible-gaming/actions/update-operator-limit.component";

@NgModule({
    imports: [
        AppRoutingModule,
        SharedModule
    ],
    declarations: [
        PlayersMainComponent,
        PlayersSearchComponent,
        PlayerBlacklistSearchComponent,
        PlayerBlacklistAddComponent,
        PlayerDetailComponent,
        PlayerDetailHeaderComponent,
        PlayerPersonalDataComponent,
        PlayerDocumentationSearchComponent,
        PlayerTransactionsComponent,
        PlayerStatusChangeComponent,
        ManualAdjustmentComponent,
        PlayerUploadDocumentationComponent,
        PlayerResponsibleGamingComponent,
        PlayerBonusesComponent,
        PlayerActiveAndPendingBonusesComponent,
        PlayerCancelBonusComponent,
        PlayerAddBonusComponent,
        AutoLimitChangesSearchComponent,
        LimitsTableComponent,
        AutoLimitHistoryComponent,
        UpdateOperatorLimitComponent,
        ApprovePlayerLimitComponent
    ],
    entryComponents: [
        ModalConfirmationComponent,
        PlayerStatusChangeComponent,
        ManualAdjustmentComponent,
        PlayerUploadDocumentationComponent,
        PlayerCancelBonusComponent,
        PlayerAddBonusComponent,
        ApprovePlayerLimitComponent,
        UpdateOperatorLimitComponent
    ],
    exports: [
        PlayersMainComponent,
        PlayersSearchComponent,
        PlayerDetailComponent,
        PlayerPersonalDataComponent,
        PlayerDocumentationSearchComponent,
        PlayerTransactionsComponent,
        LimitsTableComponent,
        AutoLimitHistoryComponent
    ],
    providers: [
        PlayerService,
        PlayerDetailService,
        DocumentationService,
        PlayerNotesService,
        BonusService,
        LimitService
    ]
})
export class PlayerModule {
}
