import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { AuthenticationService } from './account/shared/authentication.service';

/*
 * App Component
 * Top Level Component
 */
@Component({
    selector: 'pas-app',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./app.component.scss'],
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit, OnDestroy {
    private tabFocusHandler: EventListener;

    constructor(private authService: AuthenticationService) {
    }

    public ngOnInit(): void {
        // Kick off logout timer
        this.authService.startLogoutTimer();
        // On tab focus check for force logout
        this.tabFocusHandler = this.authService.checkForForceLogout.bind(this.authService);
        window.addEventListener("focus", this.tabFocusHandler);
    }

    public ngOnDestroy(): void {
        window.removeEventListener("focus", this.tabFocusHandler);
    }
}
