import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { AppRoutingModule } from "../app-routing.module";
import { LimitsMainComponent } from "./limits-main.component";
import { AccessForbiddenComponent, PageNotFoundComponent, HomeComponent } from "./index";

@NgModule({
    imports: [
        AppRoutingModule,
        SharedModule
    ],
    declarations: [
        PageNotFoundComponent,
        AccessForbiddenComponent,
        HomeComponent
    ],
    exports: [
        PageNotFoundComponent,
        AccessForbiddenComponent,
        HomeComponent
    ],
    providers: [],
})
export class StaticModule {
}
