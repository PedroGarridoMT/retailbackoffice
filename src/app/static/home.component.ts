import { Component } from "@angular/core";
import { AuthenticationService } from "../account/shared/authentication.service";
import {AuthenticationFirebaseService} from "../monitoring/authentication/authentication-firebase.service";

@Component({
    selector: 'pas-home',
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html'
})
export class HomeComponent {

    username: string = "";

    constructor(private authenticationService: AuthenticationService,
                private authFirebaseService: AuthenticationFirebaseService) {
        let user;
        if(this.authenticationService.getCurrentUser() != null){
             user = this.authenticationService.getCurrentUser() ;
        }else if(this.authFirebaseService.user){
             user = this.authFirebaseService.user;
        }

        if (user) {
            this.username = user.username
        }
    }
}
