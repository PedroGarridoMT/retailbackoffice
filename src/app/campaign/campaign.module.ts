import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AppRoutingModule } from "../app-routing.module";
import { SharedModule } from './../shared/shared.module';
import { CampaignCommonService } from "./campaign-common.service";
import { CampaignsMainComponent } from "./campaign-main.component";
import { CampaignSearchService } from "./search/campaign-search.service";
import { CampaignSearchComponent } from "./search/campaign-search.component";
import { CampaignDetailsService } from "./details/campaign-details.service";
import { CampaignDetailsComponent } from "./details/campaign-details.component";
import { CampaignDetailsHeaderComponent } from "./details/campaign-details-header.component";
import { CampaignDetailsDataComponent } from "./details/campaign-details-data.component";
import { ManualCampaignCreateComponent } from './manual-campaign/create/manual-campaign-create.component';
import { ManualCampaignCreateService } from './manual-campaign/create/manual-campaign-create.service';
import { CheckboxRowModule } from './manual-campaign/create/checkbox-row';
import { CampaignCreateComponent } from './create/campaign-create.component';
import { CampaignService } from './campaign.service';

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule,
        SharedModule,
        CheckboxRowModule
    ],
    declarations: [
        CampaignsMainComponent,
        CampaignSearchComponent,
        CampaignDetailsComponent,
        CampaignDetailsHeaderComponent,
        CampaignDetailsDataComponent,
        ManualCampaignCreateComponent,
        CampaignCreateComponent
    ],
    providers: [
        CampaignCommonService,
        CampaignSearchService,
        CampaignDetailsService,
        CampaignService,
        ManualCampaignCreateService
    ]
})
export class CampaignModule { }
