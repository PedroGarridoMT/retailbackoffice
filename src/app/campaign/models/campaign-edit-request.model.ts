export class CampaignEditRequest {
    campaignId: number;
    name: string;
    startDate: string;
    endDate: string;
    scopeStartDate: string;
    daysToExpiration: number;
}
