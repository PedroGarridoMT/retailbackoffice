export class CampaignDetailsResponse {
    public campaignId: number;
    public name: string;
    public startDate: string;
    public endDate: string;
    public daysToExpiration: number;
    public gameSubtypes: string[];
    public cancelOnWithdraw: string;
    public closeConsumedAfterMinutes: number;
    public trigger: string;
    public initialAmount: number;
    public percentOfBonusAmount: number;
    public promotionalCode: string;
    public maxBonusToReceive: number;
    public minDepositRange: number;
    public maxDepositRange: number;
    public percentBonus: number;
    public oddsLimit: number;
    public campaignStatus: string;
    public paymentMethods: string[];
    public depositsScopeStartDate: string;
    public redemptionOnWinnings: boolean;
    public minimumNumberOfBetsPerRedemption: number;
    public onlyFirstBetPerEvent: boolean;
    public excludeRealWalletBets: boolean;
}
