
export class GameTypeInfo {
    public code: string;
    public compatibilityGroup: number;
    public label: string;
    public gameSubtypes: Array<GameTypeInfo>;
    public selected: boolean;
}
