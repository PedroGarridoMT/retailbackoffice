export class CampaignDetailsModel {
    public campaignId: number;
    public campaignName: string;
    public startDate: string;
    public endDate: string;
    public daysToExpiration: number;
    public gameSubtypes: string;
    public cancelOnWithdraw: string;
    public closeConsumedAfterMinutes: number;
    public trigger: string;
    public triggerLabel: string;
    public fixedAmount: number;
    public percentAmount: number;
    public promotionalCode: string;
    public maximumAmountToReceive: number;
    public minimumDepositAmount: number;
    public maximumDepositAmount: number;
    public bonusAmountMultiplier: number;
    public minimumOdds: number;
    public campaignStatus: string;
    public paymentMethods: string;
    public scopeStartDate: string;
    public redemptionOnWinnings: boolean;
    public minimumNumberOfBetsPerRedemption: number;
    public onlyFirstBetPerEvent: boolean;
    public excludeRealWalletBets: boolean;

    constructor(campaignId: number, name: string, startDate: string, endDate: string, daysToExpiration: number, gameSubtypes: string, cancelOnWithdraw: string,
        closeConsumedAfterMinutes: number, trigger: string, triggerLabel: string, fixedAmount: number, percentAmount: number, promotionalCode: string,
        maximumAmountToReceive: number, minimumDepositAmount: number, maximumDepositAmount: number, bonusAmountMultiplier: number,
        minimumOdds: number, campaignStatus: string, paymentMethods: string, scopeStartDate: string, redemptionOnWinnings: boolean,
        minNumberOfBetsPerRedemption: number, onlyFirstBetPerEvent: boolean, excludeRealWalletBets: boolean) {

        this.campaignId = campaignId;
        this.campaignName = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.daysToExpiration = daysToExpiration;
        this.gameSubtypes = gameSubtypes;
        this.cancelOnWithdraw = cancelOnWithdraw;
        this.closeConsumedAfterMinutes = closeConsumedAfterMinutes;
        this.trigger = trigger;
        this.triggerLabel = triggerLabel;
        this.fixedAmount = fixedAmount;
        this.percentAmount = percentAmount;
        this.promotionalCode = promotionalCode;
        this.maximumAmountToReceive = maximumAmountToReceive;
        this.minimumDepositAmount = minimumDepositAmount;
        this.maximumDepositAmount = maximumAmountToReceive;
        this.bonusAmountMultiplier = bonusAmountMultiplier;
        this.minimumOdds = minimumOdds;
        this.campaignStatus = campaignStatus;
        this.paymentMethods = paymentMethods;
        this.scopeStartDate = scopeStartDate;
        this.redemptionOnWinnings = redemptionOnWinnings;
        this.minimumNumberOfBetsPerRedemption = minNumberOfBetsPerRedemption;
        this.onlyFirstBetPerEvent = onlyFirstBetPerEvent;
        this.excludeRealWalletBets = excludeRealWalletBets;
    }
}
