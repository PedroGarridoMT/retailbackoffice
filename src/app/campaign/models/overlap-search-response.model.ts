import { CampaignSearchResult } from "./campaign-search-result.model";

export class OverlapSearchResponse {
    public campaigns: Array<CampaignSearchResult>;
    public blocking: boolean;
}
