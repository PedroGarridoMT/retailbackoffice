export class OverlapSearchRequest {
    public ignoreCampaignId?: number;
    public name: string;
    public startDate?: string;
    public endDate?: string;
    public gameSubTypes: Array<string>;

    constructor(name: string, startDate?: string, endDate?: string, gameSubTypes?: Array<string>, ignoreCampaignId?: number) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.gameSubTypes = gameSubTypes || new Array<string>();
        this.ignoreCampaignId = ignoreCampaignId;
    }
}
