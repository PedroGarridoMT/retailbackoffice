export class SearchRequestModel {
    public pageSize: number;
    public currentPage: number;
    public sortBy: string;
    public sortDirection: string;

    // Filters
    public name: string;
    public startDateFrom: string;
    public startDateTo: string;
    public endDateFrom: string;
    public endDateTo: string;
    public trigger: string;
    public gameSubtypes: Array<string>;

    constructor(pageSize: number,
        currentPage: number,
        sortBy: string,
        sortDirection: string,
        filterName?: string,
        filterStartDateFrom?: string,
        filterStartDateTo?: string,
        filterEndDateFrom?: string,
        filterEndDateTo?: string,
        filterTrigger?: string,
        filterGameSubtypes?: Array<string>) {

        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.sortBy = sortBy;
        this.sortDirection = sortDirection;
        this.name = filterName;
        this.startDateFrom = filterStartDateFrom;
        this.startDateTo = filterStartDateTo;
        this.endDateFrom = filterEndDateFrom;
        this.endDateTo = filterEndDateTo;
        this.trigger = filterTrigger;
        this.gameSubtypes = filterGameSubtypes;
    }
}
