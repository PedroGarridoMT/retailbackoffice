
export class CampaignSearchResult {
    campaignId: number;
    name: string;
    startDate: string;
    endDate: string;
    trigger: string;
    triggerLabel: string;
    gameSubtypes: string[];
    gameSubtypesLabel: string;
    campaignLink: string;
}
