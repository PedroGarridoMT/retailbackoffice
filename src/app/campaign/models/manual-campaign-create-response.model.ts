
export class ManualCampaignCreateResponse {
    clientId: string;
    errorCode: string;
    username: string;
}
