
export class PlayerCsv {
    public id: number;
    public username: string;
    public amount: number;
    public formattedAmount: string;
    public checked: boolean;
}
