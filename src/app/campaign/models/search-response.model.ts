import { CampaignSearchResult } from "./campaign-search-result.model";

export class SearchResponseModel {
    public campaigns: CampaignSearchResult[];
    public totalItems: number;
    public totalPages: number;
}
