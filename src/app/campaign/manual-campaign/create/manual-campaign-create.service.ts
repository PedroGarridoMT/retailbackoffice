import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs";
import { ConfigService } from '../../../core/config.service';
import { formatDate } from '../../../shared/date-utils';

@Injectable()
export class ManualCampaignCreateService {

    constructor(private http: Http, private config: ConfigService) { }

    /**
     * Get all active bonuses available for massive assignation
     */
    public getActiveBonuses(): Observable<any> {
        return this.http.get(this.config.URLs.bonuses.active).map((response: Response) => response.json());
    }

    /**
     * Save massive bonus assignment
     * @param selectedBonusId - Selected Bonus
     * @param campaignName - Name of the campaign
     * @param scheduledDate - Date of activation
     * @param request - Selected players and amounts
     */
    public assignMassiveBonus(selectedBonusId: number, campaignName: string, scheduledDate: Date, request: any): Observable<any> {
        if (!selectedBonusId || !campaignName || !scheduledDate || !request) {
            throw new Error('Required parameter was null or undefined when calling assignMassiveBonus.');
        }

        const payload = {
            "selectedBonusId": selectedBonusId, "campaignName": campaignName
        }
        //uri has to be created this way because Angular is not encoding the "+" sign
        const uri = this.config.URLs.player.massiveBonus + "?scheduledDate=" + formatDate(scheduledDate).replace("+", "%2B")

        return this.http.post(uri, request,
            { params: payload })
            .map((response: Response) => response.json());
    }

}
