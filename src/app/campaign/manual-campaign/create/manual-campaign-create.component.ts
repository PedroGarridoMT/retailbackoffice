import { OnInit, OnDestroy, Component, ViewEncapsulation } from '@angular/core';
import { TitleBarService, TitleBarHandlerComponent, BaseResultsCode } from '../../../shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { downloadFileByDataURI } from '../../../shared/utils';
import { WizardButton, NotificationTypeEnum, WizardLiterals } from 'mt-web-components';
import { PlayerCsv } from '../../models/player-csv.model';
import { TranslateService } from '@ngx-translate/core';
import { ManualCampaignCreateService } from './manual-campaign-create.service';
import { CurrencyService } from '../../../shared/currency.service';
import { CheckboxRow } from './checkbox-row';
import { LoaderService } from '../../../shared/loader.service';
import { Router } from '@angular/router';
import { AppRoutes } from '../../../app-routes';
import { Observable } from 'rxjs';
import { ResponseHelperService } from '../../../shared/response-helper.service';


@Component({
    encapsulation: ViewEncapsulation.None,
    selector: "pas-manual-campaign-create",
    templateUrl: "./manual-campaign-create.component.html",
    styleUrls: ["./manual-campaign-create.component.scss"]
})
export class ManualCampaignCreateComponent extends TitleBarHandlerComponent implements OnInit, OnDestroy {
    private _playersFromCsv: PlayerCsv[] = [];

    public checkboxRows: CheckboxRow[] = [];
    public stepOneForm: FormGroup;
    public availableBonuses: any = [];
    public selectedBonusId: any = null;
    public selectedBonusName: string = null;
    public campaignName: string = null;
    public activationDate: Date = null;
    public activationTime: string = null;
    public inputFile: File = null;
    public inputFileName: string = null;
    public uploadSuccess: boolean = false;
    public uploadFailMsg: string = null;
    public leftButton: WizardButton = {
        text: this.translate.instant("common.cancel"),
        cssClass: 'btn-link'
    };
    public saveButton: WizardButton = {
        text: this.translate.instant("common.save"),
        cssClass: 'btn-forward'
    };
    public wizardLiterals: WizardLiterals = {
        step: this.translate.instant('wizard.step'),
        introduction: this.translate.instant('wizard.introduction'),
        next: this.translate.instant('wizard.next'),
        previous: this.translate.instant('wizard.previous')
    };

    public checkedPlayersCount: number = 0;
    public onSaveButtonClickAsync: any;
    public errorPlayers: string[] = [];

    constructor(public titleBarService: TitleBarService, private formBuilder: FormBuilder,
        private loader: LoaderService, private translate: TranslateService, private router: Router,
        private manualCampaignService: ManualCampaignCreateService, private currencyService: CurrencyService, private responseHelper: ResponseHelperService) {
        super(titleBarService);
    }

    public ngOnInit(): void {

        this.loader.show();
        this.manualCampaignService.getActiveBonuses()
            .finally(() => {
                this.loader.hide();
            })
            .subscribe((res) => {
                this.availableBonuses = res;
            });

        this.stepOneForm = this.formBuilder.group({
            "promotionName": [this.campaignName, [Validators.required]],
            "activationDate": [this.activationDate, [Validators.required]],
            "activationTime": [this.activationTime, null],
            "bonus": [this.selectedBonusId, [Validators.required]],
        });
        this.onSaveButtonClickAsync = this.onSaveButtonClick.bind(this);
    }
    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onBonusChange(value: any): void {
        this.selectedBonusId = value;
        this.selectedBonusName = this.availableBonuses.filter((bonus) => bonus.id == value)[0].name;
    }

    /**
     * Upload file event handler
     * @param event
     */
    public onUploadFileChange(event: any) {
        let fileList: FileList = event.target.files;
        if (fileList && fileList.length > 0) {

            this.inputFile = fileList[0];
            this.inputFileName = fileList[0].name;
            let reader: FileReader = new FileReader();
            if (this.inputFile) {
                this.loader.show();
                // Convert file to Binary String
                reader.readAsBinaryString(this.inputFile)
            }

            reader.onloadend = () => {
                this.uploadSuccess = this.parseAndValidateUploadedFile(reader.result);
                this.loader.hide();
            };
        }
    }

    public onFileRemoveClick() {
        this.inputFile = null;
        this.inputFileName = null;
        this._playersFromCsv.splice(0);
        this.checkboxRows.splice(0);
        this.uploadSuccess = false;
    }

    private parseAndValidateUploadedFile(content: string): boolean {
        this._playersFromCsv.splice(0);
        this.checkboxRows.splice(0);

        const csvRows: string[] = content.split('\n');
        // Remove last empty row if exists
        const rows: string[] = !!csvRows[csvRows.length - 1] ? csvRows : csvRows.slice(0, -1);
        if (!/.*\.csv$/.test(this.inputFileName)) {
            this.uploadFailMsg = this.translate.instant("campaigns.manualCampaigns.create.stepTwo.csvUploadErrorNotCsv");
            return false;
        }
        for (let i = 0; i < rows.length; i++) {
            var row = rows[i].split(';');
            if (row.length != 2) {
                this._playersFromCsv.splice(0);
                this.uploadFailMsg = this.translate.instant("campaigns.manualCampaigns.create.stepTwo.csvUploadErrorNoCols");
                return false;
            }
            if (isNaN(parseFloat(row[1]))) {
                this._playersFromCsv.splice(0);
                this.uploadFailMsg = this.translate.instant("campaigns.manualCampaigns.create.stepTwo.csvUploadErrorNotNumber");
                return false;
            }
            this._playersFromCsv.push({
                id: i,
                username: row[0],
                amount: parseFloat(row[1]),
                formattedAmount: this.currencyService.formatAmountWithCurrency(parseFloat(row[1])),
                checked: true
            });
        }
        if (this._playersFromCsv.length) {
            this.checkboxRows = this._playersFromCsv.map((player) => ({ id: player.id, checkBoxDescription: player.username, rowText: player.formattedAmount, rowChecked: player.checked }));
            this.checkedPlayersCount = this.checkboxRows.filter((checkboxRow) => checkboxRow.rowChecked).length;
        }
        return true;
    }

    public onDownloadDocument() {
        downloadFileByDataURI("assets/samples/example_massive_assignation.csv", "example_massive_assignation.csv");
    }

    public onSaveButtonClick() {
        let messages: any[] = [];
        this._playersFromCsv.filter((player) => player.checked).forEach(player => {
            messages.push({
                UserIdentifier: player.username,
                Amount: player.amount
            });
        });
        if (this.activationTime) {
            this.activationDate.setHours(parseInt(this.activationTime.split(":")[0]));
            this.activationDate.setMinutes(parseInt(this.activationTime.split(":")[1]));
        }

        if (!this.selectedBonusId || !this.campaignName || !this.activationDate || !messages.length) {
            this.responseHelper.responseHandler(BaseResultsCode.CustomCode, "campaigns.manualCampaigns.create.stepFive.saveCampaignError", NotificationTypeEnum.Error)
        } else {
            this.loader.show();
            return this.manualCampaignService.assignMassiveBonus(this.selectedBonusId, this.campaignName, this.activationDate, {
                ClientId: 0,
                Messages: messages
            }).finally(() => {
                this.loader.hide();
            }).catch((err) => {
                this.responseHelper.responseHandler(err.statusCode);
                return Observable.throw(err)
            })
                .map((res) => {
                    if (res.length > 0) {
                        this.errorPlayers = res.filter(player => player.errorCode != "Ok").map((player) => player.username);
                    }
                });
        }
    }

    public onCancelButtonClick() {
        this.router.navigate([AppRoutes.campaignMain]);
    }

    public onRowChecked(event: CheckboxRow) {
        //refresh selection on checkbox rows
        let checkboxRow: CheckboxRow = this.checkboxRows.find((checkboxRow) => checkboxRow.id == event.id);
        checkboxRow.rowChecked = !checkboxRow.rowChecked;
        //refresh selection on players
        let player: PlayerCsv = this._playersFromCsv.find((player) => player.id == event.id);
        player.checked = !player.checked;
        this.checkedPlayersCount = this.checkboxRows.filter((checkboxRow) => checkboxRow.rowChecked).length;
    }

}
