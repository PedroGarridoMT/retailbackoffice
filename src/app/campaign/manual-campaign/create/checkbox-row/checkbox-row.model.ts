export class CheckboxRow {
    id: number;
    checkBoxDescription: string;
    rowText: string;
    rowChecked: boolean;
}
