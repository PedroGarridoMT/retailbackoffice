import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CheckboxRowComponent } from './checkbox-row.component';

const MODULE_DIRECTIVES = [CheckboxRowComponent];

@NgModule({
    declarations: MODULE_DIRECTIVES,
    exports: MODULE_DIRECTIVES,
    imports: [CommonModule]
})
export class CheckboxRowModule {
}
