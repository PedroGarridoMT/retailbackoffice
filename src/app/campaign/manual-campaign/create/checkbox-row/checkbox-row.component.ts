import { Component, Input, Output, EventEmitter, } from '@angular/core';
import { CheckboxRow } from '.';

@Component({
    selector: 'pas-checkbox-row',
    template: `
        <div class="pas-checkbox-row">
            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" (click)="onCheckBoxClick()" type="checkbox" [checked]="row.rowChecked">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">{{row.checkBoxDescription}}</span>
                </label>
            </div>
            <div>
                <span class="custom-control-description">{{row.rowText}}</span>
            </div>
        </div>
    `,
    styleUrls: ["./checkbox-row.component.scss"]
})
export class CheckboxRowComponent {

    @Input() row: CheckboxRow;
    @Output() rowChecked = new EventEmitter<CheckboxRow>();

    constructor() {

    }

    public onCheckBoxClick(){
        this.rowChecked.emit(this.row);
    }
}
