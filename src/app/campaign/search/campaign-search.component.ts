import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    BasicFilterItem,
    ColumnVisibilityEvent,
    FilterToValueMap,
    FilterType,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals,
    TablePagination,
    TableSortInfo,
} from "mt-web-components";
import { TitleBarHandlerComponent, TitleBarParams, TitleBarService } from "../../shared/index";
import { getDateRangeValueFromISODate, getDefaultDateTimeFormat } from "../../shared/date-utils";
import { LoaderService } from "../../shared/loader.service";
import { PASEnumsService } from "../../shared/pas-enums.service";
import { CampaignSearchService } from "./campaign-search.service";
import { CampaignCommonService } from "../campaign-common.service";
import { SearchRequestModel } from "../models/search-request.model";
import { SearchResponseModel } from "../models/search-response.model";
import { CampaignSearchResult } from "../models/campaign-search-result.model";
import { ListItem } from "../../shared/model/list-item.model";

@Component({
    selector: "pas-campaign-search",
    templateUrl: "./campaign-search.component.html",
    styleUrls: ["./campaign-search.component.scss"],
})
export class CampaignSearchComponent extends TitleBarHandlerComponent implements OnInit, OnDestroy {

    private _subscriptions: Subscription[] = [];

    // Title bar attributes
    titleBarParams: TitleBarParams = {
        previousPagePath: "campaigns",
        previousPageLabel: "campaigns.home.title",
        pageTitle: "campaigns.home.search",
        pageActions: null,
    };

    // UI attributes
    loading: boolean = false;
    firstSearchDone: boolean = false;
    errorInSearch: boolean = false;
    previousUrl: string = null;

    // mt-table configuration
    tableColumns: TableColumn[];
    tableRows: CampaignSearchResult[];
    sortProperty: TableSortInfo = { column: "name", order: "ASC" };
    pagingData: TablePagination = { currentPage: 1, pageSize: 5, totalPages: 0, totalItems: 0 };
    tableLiterals: TableLiterals;
    @ViewChild("campaignDetailLinkTemplate") campaignDetailLinkTemplate;

    // mt-basic-filters configuration
    valuesToSearch: FilterToValueMap = {};
    filterBarLiterals: BasicFilterBarLiterals;
    filters: BasicFilterItem[];
    triggers: ListItem[];
    gameSubtypes: ListItem[];
    @ViewChild("filtersBar") filtersBar: BasicFilterBarComponent;

    constructor(titleBarService: TitleBarService,
        private enumsService: PASEnumsService,
        private translate: TranslateService,
        private router: Router,
        private loader: LoaderService,
        private campaignSearchService: CampaignSearchService,
        private campaignCommonService: CampaignCommonService) {

        super(titleBarService);
        this.resetPagingParams();
        this.resetSortingParams();
        this.previousUrl = campaignCommonService.getPreviousUrl();
    }

    public ngOnInit(): void {
        this.init();

        // Notify and pass params to the title bar service, so it can notify subscribers
        this.notifyComponentInitialized(this.titleBarParams);

        // Subscribe to action requests that happens on the title bar
        this._subscriptions.push(
            this.titleBarService.actionRequested$.subscribe(actionId => {
                this.handleTitleBarAction(actionId);
            })
        );
    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    private init() {

        this.enumsService.getTriggersTranslatedList()
            .subscribe(trigerList => this.triggers = trigerList);

        this.enumsService.getGameSubtypesTranslatedList()
            .subscribe(gameSubtypeList => this.gameSubtypes = gameSubtypeList);

        this.generateTable();

        //Reload the last search when coming from a campaign detail page.
        if (this.isPreviousPageCampaignDetail()) {
            let lastSearch: SearchRequestModel = this.loadLastSearch();
            this.generateFilterForm(lastSearch);
            this.launchLastSearch(lastSearch);
        }
        else {
            this.generateFilterForm();
        }
    }

    /**
     * Generates filter form component: mt-basic-filters
     * @param lastSearch
     */
    private generateFilterForm(lastSearch?: SearchRequestModel) {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "name",
                type: FilterType.TEXT,
                label: this.translate.instant("campaigns.entity.name"),
                shownByDefault: true,
                value: lastSearch ? lastSearch.name : null,
            },
            {
                id: "startDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("campaigns.entity.startDate"),
                shownByDefault: true,
                value: lastSearch ? getDateRangeValueFromISODate(lastSearch.startDateFrom, lastSearch.startDateTo) : null,
            },
            {
                id: "endDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("campaigns.entity.endDate"),
                shownByDefault: true,
                value: lastSearch ? getDateRangeValueFromISODate(lastSearch.endDateFrom, lastSearch.endDateTo) : null,
            },
            {
                id: "trigger",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("campaigns.entity.trigger"),
                shownByDefault: true,
                options: this.triggers,
                value: lastSearch ? lastSearch.trigger : null,
            },
            {
                id: "gameSubtypes",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("campaigns.entity.gameSubtypes"),
                shownByDefault: true,
                options: this.gameSubtypes,
                value: lastSearch ? lastSearch.gameSubtypes : null,
            },
        ];
    }

    /**
     * Generates the table component with campaign columns: mt-table
     */
    private generateTable() {
        //@formatter:off
        this.tableColumns = [
            {
                type: TableColumnType.HTML,
                id: "campaignLink",
                label: "",
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false,
            },
            {
                type: TableColumnType.DATA,
                id: "name",
                label: this.translate.instant("campaigns.entity.name"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: true,
            },
            {
                type: TableColumnType.DATA,
                id: "startDate",
                label: this.translate.instant("campaigns.entity.startDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: true,
            },
            {
                type: TableColumnType.DATA,
                id: "endDate",
                label: this.translate.instant("campaigns.entity.endDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: true,
            },
            {
                type: TableColumnType.DATA,
                id: "triggerLabel",
                label: this.translate.instant("campaigns.entity.trigger"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: true,
            },
            {
                type: TableColumnType.DATA,
                id: "gameSubtypesLabel",
                label: this.translate.instant("campaigns.entity.gameSubtypes"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false,
            },
        ];
        //@formatter:on
        this.tableLiterals = {
            noResultsMessage: this.translate.instant('table.noResultsMessage'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        };
    }

    /**
     * Search Campaign Request
     */
    private searchData(predefinedSearchRequest?: SearchRequestModel) {
        this.firstSearchDone = true;
        this.errorInSearch = false;
        this.loader.show();
        this.loading = true;
        let searchRequest: SearchRequestModel;

        if (predefinedSearchRequest) {
            searchRequest = predefinedSearchRequest;
        } else {
            searchRequest = new SearchRequestModel(this.pagingData.pageSize, this.pagingData.currentPage,
                this.sortProperty.column, this.sortProperty.order);
            this.campaignSearchService.mapFiltersToSearchRequestModel(this.valuesToSearch, searchRequest);

            this.storeLastSearch(searchRequest);
        }

        this.campaignSearchService.getCampaigns(searchRequest)
            .finally(() => {
                this.loader.hide();
                this.loading = false;
            })
            .subscribe((searchResponse: SearchResponseModel) => {
                this.tableRows = this.addExtraCampaignInfo(searchResponse.campaigns);
                this.pagingData = {
                    currentPage: this.pagingData.currentPage,
                    pageSize: this.pagingData.pageSize,
                    totalPages: searchResponse.totalPages,
                    totalItems: searchResponse.totalItems,
                };
            }, error => {
                this.errorInSearch = true;
                this.resetPagingParams();
                this.resetSortingParams();
                console.error(error);
            });
    }

    private isPreviousPageCampaignDetail(): boolean {
        if (this.previousUrl) {
            return /\/campaigns\/((?!search).)+$/.test(this.previousUrl);
        }
        return false;
    }

    private launchLastSearch(lastSearch: SearchRequestModel) {
        if (lastSearch) {
            this.pagingData.currentPage = lastSearch.currentPage;
            this.pagingData.pageSize = lastSearch.pageSize;
            this.sortProperty.column = lastSearch.sortBy;
            this.sortProperty.order = lastSearch.sortDirection;
            this.searchData(lastSearch);
            // As the user didn't start the search, we need to store in our local variable the search values
            // loaded from the filters bar for next searches to work properly.
            setTimeout(() => this.valuesToSearch = this.filtersBar.getSearchValues());
        }
    }

    private storeLastSearch(searchCriteria: SearchRequestModel) {
        localStorage.setItem("last-campaign-search", JSON.stringify(searchCriteria));
    }

    private loadLastSearch(): SearchRequestModel {
        let lastSearch = localStorage.getItem("last-campaign-search");
        return lastSearch ? JSON.parse(lastSearch) : null;
    }

    private addExtraCampaignInfo(campaigns: CampaignSearchResult[]): CampaignSearchResult[] {
        campaigns.map((campaign) => {
            campaign.campaignLink = this.campaignDetailLinkTemplate;
            campaign.startDate = campaign.startDate ? getDefaultDateTimeFormat(campaign.startDate) : "";
            campaign.endDate = campaign.endDate ? getDefaultDateTimeFormat(campaign.endDate) : "";
            campaign.triggerLabel = campaign.trigger ? this.triggers.find(t => t.value == campaign.trigger).label : "";
            campaign.gameSubtypesLabel = campaign.gameSubtypes ?
                campaign.gameSubtypes.map(gst => this.gameSubtypes.find(t => t.value == gst).label).join(", ") : "";
        });
        return campaigns;
    }

    private resetPagingParams() {
        this.pagingData = { currentPage: 1, pageSize: 5, totalPages: 0, totalItems: 0 };
    }

    private resetSortingParams() {
        this.sortProperty = { column: "name", order: "ASC" };
    }

    public onSortClick(tableSortInfo: TableSortInfo) {
        this.sortProperty = tableSortInfo;
        this.searchData();
    }

    public onColumnVisibilityChange(columnChanged: ColumnVisibilityEvent) {
        let colIndex = this.tableColumns.findIndex(item => item.id == columnChanged.columnId);
        this.tableColumns[colIndex].visibility = columnChanged.value ?
            TableColumnVisibilityEnum.VISIBLE : TableColumnVisibilityEnum.HIDDEN;
    }

    public onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    public onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    public onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    public onClearBasicFilter() {
        this.valuesToSearch = null;
    }

    public handleTitleBarAction(actionId: string) {
        switch (actionId) {
            default:
                console.log("No actions available yet");
                break;
        }
    }
}
