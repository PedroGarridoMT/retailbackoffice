import { Injectable } from "@angular/core";
import { Http, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs";
import { FilterToValueMap, DateInputModel } from "mt-web-components";
import { getISOString, setTimeInfo } from "../../shared/date-utils";
import { HelperService } from "../../shared/helper.service";
import { ConfigService } from "../../core/config.service";
import { SearchRequestModel } from "../models/search-request.model";
import { SearchResponseModel } from "../models/search-response.model";

@Injectable()
export class CampaignSearchService {

    constructor(private http: Http, private config: ConfigService) { }

    /**
     * Gets an ordered paged list of registered campaigns that comply with the filter criteria
     * @param request
     * @returns {Observable<SearchResponseModel>}
     */
    public getCampaigns(request: SearchRequestModel): Observable<SearchResponseModel> {
        if (!request || !request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling searchCampaigns');
        }
        if (!request || !request.currentPage) {
            throw new Error('Required parameter pageNumber was null or undefined when calling searchCampaigns');
        }
        const params = this.mapUrlSearchParamsFromSearchRequestModel(request);

        return this.http.get(this.config.URLs.campaign.search, { params })
            .map((response: Response) => response.json())
            .catch(HelperService.handleError);
    }

    public mapFiltersToSearchRequestModel(filters: FilterToValueMap, request: SearchRequestModel): void {
        request.name = filters.name;
        request.startDateFrom = (filters.startDate && filters.startDate.from) ?
            getISOString(filters.startDate.from) : null;
        request.startDateTo = (filters.startDate && filters.startDate.to) ?
            getISOString(filters.startDate.to) : null;
        request.endDateFrom = (filters.endDate && filters.endDate.from) ?
            getISOString(filters.endDate.from) : null;
        request.endDateTo = (filters.endDate && filters.endDate.to) ?
            getISOString(filters.endDate.to) : null;
        request.trigger = filters.trigger;
        request.gameSubtypes = filters.gameSubtypes;
    }

    private mapUrlSearchParamsFromSearchRequestModel(request: SearchRequestModel): URLSearchParams {
        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            if (request.hasOwnProperty(key)) {
                if (request[key]) {
                    if (Array.isArray(request[key])) {
                        request[key].forEach(element => params.append(key, element));
                    } else {
                        params.append(key, request[key]);
                    }
                }
            }
        }
        return params;
    }
}
