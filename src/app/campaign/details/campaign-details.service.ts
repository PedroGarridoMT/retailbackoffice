import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { CampaignDetailsResponse } from "./models/campaign-details-response.model";
import { CampaignDetailsModel } from "../models/campaign-details.model";

export enum CampaignVisualizationModeEnum { Visualization, Edition }

export enum CampaignActionsEnum { EDIT, CLONE, EXPORT, CANCEL }

export enum CampaignViewEnum { CAMPAIGN_DATA, CAMPAIGN_PLAYER_PARTICIPATION }

@Injectable()
export class CampaignDetailsService {
    // Private var, to act as a campaign cache.
    private _campaigns = {}; // TODO: Consider different implementation without caching

    // Observable sources
    private campaignDetailsSource = new Subject<CampaignDetailsModel>();
    private visualizationModeSource = new Subject<CampaignVisualizationModeEnum>();
    private activeViewSource = new Subject<CampaignViewEnum>();

    // Observable streams
    public campaignDetails$ = this.campaignDetailsSource.asObservable();
    public visualizationMode$ = this.visualizationModeSource.asObservable();
    public activeView$ = this.activeViewSource.asObservable();

    // Service message commands
    public setCampaign(campaignDetails: CampaignDetailsModel) {
        // Store a copy of the campaign in cache
        this._campaigns[campaignDetails.campaignId] = JSON.parse(JSON.stringify(campaignDetails));
        // And notify subscribers
        this.campaignDetailsSource.next(campaignDetails);
    }

    public setVisualizationMode(mode: CampaignVisualizationModeEnum) {
        this.visualizationModeSource.next(mode);
    }

    public setActiveView(view: CampaignViewEnum) {
        this.activeViewSource.next(view);
    }

    // Helper methods
    public getStoredCampaign(campaignId: number): CampaignDetailsModel {
        return this._campaigns ? this._campaigns[campaignId] : null;
    }

}
