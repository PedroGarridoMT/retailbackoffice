import { Component, EventEmitter, Output, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs/Subscription';
import { PASEnumsService } from "../../shared/pas-enums.service";
import { AuthenticationService } from "../../account/shared/authentication.service";
import { CampaignDetailsService, CampaignVisualizationModeEnum, CampaignViewEnum, CampaignActionsEnum } from "./campaign-details.service";
import { CampaignDetailsModel } from "../models/campaign-details.model";
import { PermissionsCode } from "../../shared";

interface CampaignAction {
    translationKey: string;
    actionId: CampaignActionsEnum;
    cssClasses: string;
}

@Component({
    selector: "pas-campaign-details-header",
    templateUrl: "./campaign-details-header.component.html",
    styleUrls: ["./campaign-details-header.component.scss"]
})
export class CampaignDetailsHeaderComponent implements OnDestroy {
    public visualizationModeEnum = CampaignVisualizationModeEnum;
    public visualizationMode: CampaignVisualizationModeEnum;
    public activeView: CampaignViewEnum;
    private _campaign: CampaignDetailsModel;
    private _subscriptions: Subscription[] = [];

    constructor(private enumsService: PASEnumsService,
        private authenticationService: AuthenticationService,
        private campaignDetailsService: CampaignDetailsService) {

        this._subscriptions.push(
            this.campaignDetailsService.campaignDetails$.subscribe(campaignData => this.campaign = campaignData),
            this.campaignDetailsService.visualizationMode$.subscribe(mode => this.visualizationMode = mode),
            this.campaignDetailsService.activeView$.subscribe(view => this.activeView = view)
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    @Output() actionClick = new EventEmitter<CampaignActionsEnum>();

    public get campaign(): CampaignDetailsModel {
        return this._campaign;
    }

    public set campaign(campaign: CampaignDetailsModel) {
        if (campaign) {
            this._campaign = campaign;
        }
    }

    public getAvailableActions(): CampaignAction[] {
        let actions: CampaignAction[] = [];
        // Edit
        if (this.activeView === CampaignViewEnum.CAMPAIGN_DATA && this.visualizationMode === CampaignVisualizationModeEnum.Visualization
            && this.authenticationService.isUserAuthorizedForAuthCode(PermissionsCode.CampaignEdit)) {
            actions.push({
                translationKey: "common.edit",
                actionId: CampaignActionsEnum.EDIT,
                cssClasses: "fa fa-pencil",
            });
        }
        // Clone
        if (this.activeView === CampaignViewEnum.CAMPAIGN_DATA && this.visualizationMode === CampaignVisualizationModeEnum.Visualization) {
            actions.push({
                translationKey: "common.clone",
                actionId: CampaignActionsEnum.CLONE,
                cssClasses: "fa fa-refresh",
            });
        }
        // Export
        if (this.activeView === CampaignViewEnum.CAMPAIGN_DATA && this.visualizationMode === CampaignVisualizationModeEnum.Visualization) {
            actions.push({
                translationKey: "common.export",
                actionId: CampaignActionsEnum.EXPORT,
                cssClasses: "fa fa-refresh",
            });
        }
        // Cancel
        if (this.activeView === CampaignViewEnum.CAMPAIGN_DATA && this.visualizationMode === CampaignVisualizationModeEnum.Visualization) {
            actions.push({
                translationKey: "common.cancel",
                actionId: CampaignActionsEnum.CANCEL,
                cssClasses: "fa fa-refresh",
            });
        }
        return actions;
    }

    public getStatusTranslated(): string {
        if (this.campaign && this.campaign.campaignId) {
            return this.enumsService.translateEnumValue("campaignStatus", this.campaign.campaignStatus);
        }
        return "";
    }

    public onActionClick(actionId: CampaignActionsEnum) {
        this.actionClick.emit(actionId);
    }
}
