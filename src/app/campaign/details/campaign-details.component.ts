import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { TitleBarHandlerComponent } from "../../shared/title-bar-handler.component";
import { TitleBarParams } from "../../shared/model/title-bar.model";
import { TitleBarService } from "../../shared/title-bar.service";
import { LoaderService } from "../../shared/loader.service";
import { Subscription } from "rxjs";
// import { ConfigService } from "../../core/config.service";
import { CampaignCommonService } from "../campaign-common.service";
import { CampaignDetailsService, CampaignVisualizationModeEnum, CampaignActionsEnum } from "./campaign-details.service";
import { CampaignDetailsModel } from "../models/campaign-details.model";

@Component({
    selector: "pas-campaign-details",
    templateUrl: "./campaign-details.component.html",
    styleUrls: ["./campaign-details.component.scss"]
})
export class CampaignDetailsComponent extends TitleBarHandlerComponent implements OnInit, OnDestroy {

    constructor(titleBarService: TitleBarService,
        private route: ActivatedRoute,
        private loader: LoaderService,
        // private config: ConfigService,
        private campaignDetailsService: CampaignDetailsService,
        private campaignCommonService: CampaignCommonService) {

        super(titleBarService);
    }

    // Title bar attributes
    private titleBarParams: TitleBarParams = {
        previousPagePath: "campaigns/search",
        previousPageLabel: "campaigns.home.search",
        pageTitle: "campaigns.home.details",
        pageActions: null
    };
    private _subscriptions: Subscription[] = [];

    public campaignId: number;

    public ngOnInit(): void {
        this._subscriptions.push(
            this.route.params.subscribe((params: Params) => {
                this.campaignId = params["id"];
                this.loadCampaignDetails();
                this.campaignDetailsService.setVisualizationMode(CampaignVisualizationModeEnum.Visualization);
            })
        );
        // Notify and pass params to the title bar service, so it can notify subscribers
        this.notifyComponentInitialized(this.titleBarParams);
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * Executes Campaign details server request
     */
    private loadCampaignDetails() {
        this.loader.show();

        this.campaignCommonService.getCampaignDetails(this.campaignId)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe((campaign: CampaignDetailsModel) => {
                // Pass data to the campaign details service, so subscribers can get it.
                this.campaignDetailsService.setCampaign(campaign);
            },
            error => {
                console.error(error);
            });
    }

    /**
     * Campaign details actions handler
     * @param actionId
     */
    public onActionClick(actionId: CampaignActionsEnum) {
        switch (actionId) {
            case CampaignActionsEnum.EDIT:
                this.campaignDetailsService.setVisualizationMode(CampaignVisualizationModeEnum.Edition);
                break;
            case CampaignActionsEnum.CLONE:
                console.log("Campaign clone is not yet implemented. Will be available soon...");
                break;
            case CampaignActionsEnum.EXPORT:
                // create request to this.config.API_URL.campaign.detailsExport.replace(/{campaignId}/, this.campaignId.toString());
                console.log("Campaign export is not yet implemented. Will be available soon...");
                break;
            case CampaignActionsEnum.CANCEL:
                console.log("Campaign cancellation is not yet implemented. Will be available soon...");
                break;
        }
    }
}
