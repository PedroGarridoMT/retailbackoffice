import { Subscription } from 'rxjs/Subscription';
import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { getDateAsObject, isValidDate } from "../../shared/date-utils";
import { LoaderService } from "../../shared/loader.service";
import { ModalConfirmationComponent } from "../../shared/components/modal-confirmation.component";
import { CampaignDetailsModel } from "../models/campaign-details.model";
import { CampaignEditRequest } from "../models/campaign-edit-request.model";
import { CampaignCommonService } from "../campaign-common.service";
import { CampaignDetailsService, CampaignViewEnum, CampaignVisualizationModeEnum } from "./campaign-details.service";
import { ResponseHelperService } from "../../shared/response-helper.service";
import { basicDateValidator } from "../../shared/validators";

@Component({
    templateUrl: "./campaign-details-data.component.html",
    styleUrls: ["./campaign-details-data.component.scss"]
})
export class CampaignDetailsDataComponent implements OnInit, OnDestroy {
    public originalCampaign: CampaignDetailsModel = null;
    public visualizationModeEnum = CampaignVisualizationModeEnum; // The enum is made usable from the template
    public visualizationMode: CampaignVisualizationModeEnum;
    public campaignFormGroup: FormGroup;
    private _campaign: CampaignDetailsModel = null;
    private _startDate: { day: number, month: number, year: number };
    private _endDate: { day: number, month: number, year: number };
    private _scopeStartDate: { day: number, month: number, year: number };
    private _subscriptions: Subscription[] = [];

    constructor(private formBuilder: FormBuilder,
                private campaignCommonService: CampaignCommonService,
                private campaignDetailsService: CampaignDetailsService,
                private route: ActivatedRoute,
                private translate: TranslateService,
                private loader: LoaderService,
                private modalService: NgbModal,
                private responseHelper: ResponseHelperService) {
    }

    public ngOnInit(): void {
        this.campaignDetailsService.setActiveView(CampaignViewEnum.CAMPAIGN_DATA);

        // Subscribe to campaign data and visualization mode change events
        this._subscriptions.push(
            this.campaignDetailsService.campaignDetails$.subscribe(campaignData => this.campaign = JSON.parse(JSON.stringify(campaignData))),

            this.campaignDetailsService.visualizationMode$.subscribe((mode) => {
                if (mode == CampaignVisualizationModeEnum.Edition) {
                    this.setVisualizationMode(true);
                }
                else if (mode == CampaignVisualizationModeEnum.Visualization) {
                    this.setVisualizationMode(false);
                }
            })
        );

        // When navigating back to this child view, the parent will not launch an campaign data change event. But the
        // current campaign is stored in the shared campaign details service -> Get it.
        if (!this.campaign) {
            this._subscriptions.push(
                this.route.params.subscribe((params: Params) => {
                    let storedCampaign = this.campaignDetailsService.getStoredCampaign(params["id"]);
                    if (storedCampaign) {
                        this.campaign = storedCampaign;
                    }
                })
            );
        }
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    /**
     * See CanDeactivateGuard in Core module.
     * If the campaign data is being edited, request the user a confirmation to leave the edition.
     */
    public canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
        return this.visualizationMode == CampaignVisualizationModeEnum.Edition ? this.askForConfirmation() : true;
    }

    public get campaign(): CampaignDetailsModel {
        return this._campaign;
    }

    @Input()
    public set campaign(value: CampaignDetailsModel) {
        this._campaign = value;
        if (this._campaign) {
            this.startDate = getDateAsObject(this._campaign.startDate);
            this.endDate = getDateAsObject(this._campaign.endDate);
            this.scopeStartDate = getDateAsObject(this._campaign.scopeStartDate);

            this.buildFormGroup(this._campaign);
            this.originalCampaign = JSON.parse(JSON.stringify(this._campaign));

            // Fields are disabled by default
            this.setVisualizationMode(false);
        }
    }

    public askForConfirmation(): Promise<boolean> {
        const modalRef: NgbModalRef = this.modalService.open(ModalConfirmationComponent, {
            backdrop: "static",
            keyboard: false
        });
        modalRef.componentInstance.title = this.translate.instant("campaigns.details.messageUnsavedChangesTitle");
        modalRef.componentInstance.message = this.translate.instant("campaigns.details.messageUnsavedChanges");
        modalRef.componentInstance.confirmText = this.translate.instant("campaigns.details.messageUnsavedChangesLeave");
        modalRef.componentInstance.cancelText = this.translate.instant("campaigns.details.messageUnsavedChangesStay");

        return modalRef.result.then(
            result => {
                if (result === "CONFIRM") {
                    this.cancelEdition();
                    return true; // Can deactivate => proceed with navigation
                }
                return false;
            }, reason => {
                // This window can not be dismissed. Return false as it is the safest option.
                return false;
            });
    }

    public get startDate(): { day: number; month: number; year: number } {
        if (!this._startDate && this._campaign.startDate) {
            this._startDate = getDateAsObject(this._campaign.startDate);
        }
        return this._startDate;
    }

    public set startDate(value: { day: number; month: number; year: number }) {
        this._startDate = value;
    }

    public get endDate(): { day: number; month: number; year: number } {
        if (!this._endDate && this._campaign.endDate) {
            this._endDate = getDateAsObject(this._campaign.endDate);
        }
        return this._endDate;
    }

    public set endDate(value: { day: number; month: number; year: number }) {
        this._endDate = value;
    }

    public get scopeStartDate(): { day: number; month: number; year: number } {
        if (!this._scopeStartDate && this._campaign.scopeStartDate) {
            this._scopeStartDate = getDateAsObject(this._campaign.scopeStartDate);
        }
        return this._scopeStartDate;
    }

    public set scopeStartDate(value: { day: number; month: number; year: number }) {
        this._scopeStartDate = value;
    }

    /**
     * Updates any date property in campaign entity
     * For example: campaign.birthDate = _birthDate.year + _birthDate.month + _birthDate.day input values
     */
    public onDateInputModelUpdate(date: any, controlKey: string, groupKey?: string) {
        let composedControlKey = groupKey ? this.campaignFormGroup.controls[groupKey]["controls"][controlKey] : this.campaignFormGroup.controls[controlKey];

        if (this.campaign && date) {
            groupKey ? (this.campaign[groupKey][controlKey] = date) : (this.campaign[controlKey] = date);

            if (!isValidDate(date)) {
                composedControlKey.setErrors({ "invalidDateError": true })
            } else {
                composedControlKey.setValue(groupKey ? this.campaign[groupKey][controlKey] : this.campaign[controlKey]);
                composedControlKey.setErrors(null)
            }
        } else {
            composedControlKey.setErrors({ "required": true })
        }
    }

    /**
     * Create FormBuilder with Campaign entity fields
     */
    private buildFormGroup(campaign: CampaignDetailsModel) {
        this.campaignFormGroup = this.formBuilder.group({
            campaignName: [campaign ? campaign.campaignName : "", Validators.required],
            startDate: [campaign ? campaign.startDate : "", [Validators.required, basicDateValidator()]],
            endDate: [campaign ? campaign.endDate : "", [Validators.required, basicDateValidator()]],
            daysToExpiration: [campaign ? campaign.daysToExpiration : "", Validators.required],
            scopeStartDate: [campaign ? campaign.scopeStartDate : ""],
        });
    }

    public getBonusAmountCalculationTranslated(): string {
        return this.campaign.fixedAmount ? this.translate.instant("campaigns.entity.fixedAmount") : this.translate.instant("campaigns.entity.percentAmount");
    }

    /**
     * Sets the campaign details mode (Edition or Visualization)
     * @param editable
     * @see CampaignVisualizationModeEnum
     */
    private setVisualizationMode(editable?: boolean) {
        if (editable) {
            this.visualizationMode = CampaignVisualizationModeEnum.Edition;
            this.enableFormInputs(true);
        } else {
            this.visualizationMode = CampaignVisualizationModeEnum.Visualization;
            this.enableFormInputs(false);
        }
    }

    /**
     * Enable / disable all form inputs (if no "controls" specified)
     * @param enable
     * @param controls
     */
    private enableFormInputs(enable: boolean, controls?: string[]) {
        if ((!this.campaignFormGroup || !this.campaignFormGroup.controls) && !controls) {
            return;
        }

        if (enable) {
            (controls ? controls : Object.keys(this.campaignFormGroup.controls)).forEach((key) => {
                let composedKey = key.split(".");
                composedKey[1] ? this.campaignFormGroup.controls[composedKey[0]]["controls"][composedKey[1]].enable() : this.campaignFormGroup.controls[composedKey[0]].enable();
            })
        } else {
            (controls ? controls : Object.keys(this.campaignFormGroup.controls)).forEach((key) => {
                let composedKey = key.split(".");
                composedKey[1] ? this.campaignFormGroup.controls[composedKey[0]]["controls"][composedKey[1]].disable() : this.campaignFormGroup.controls[composedKey[0]].disable();
            })
        }
    }

    /**
     * Cancel edition mode
     * Restore campaign data
     */
    private cancelEdition() {
        this.campaignDetailsService.setVisualizationMode(CampaignVisualizationModeEnum.Visualization);
        this.campaign = JSON.parse(JSON.stringify(this.originalCampaign));
        console.log(this.campaignFormGroup);
    }

    /**
     * Submit form action
     * @param value
     * @param valid
     */
    public onSubmit({ value, valid }: { value: CampaignDetailsModel, valid: boolean }) {
        console.log(value, valid);
        if (valid) {
            this.loader.show();

            const request = <CampaignEditRequest>{
                campaignId: value.campaignId,
                name: value.campaignName,
                startDate: value.startDate,
                endDate: value.endDate,
                scopeStartDate: value.scopeStartDate,
                daysToExpiration: value.daysToExpiration,
            };

            this.campaignCommonService.updateCampaign(this.campaign.campaignId, request)
                .finally(() => {
                    this.loader.hide();
                })
                .subscribe(response => {
                    console.log(response);
                    this.responseHelper.responseHandler(response, 'campaigns.edit.response');
                });
        }
    }
}
