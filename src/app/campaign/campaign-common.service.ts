import { Router, NavigationEnd } from "@angular/router";
import { Http, Response } from "@angular/http";
import { Injectable, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { HelperService } from "../shared/helper.service";
import { ConfigService } from "../core/config.service";
import { GameTypeInfo } from "./models/game-type-info.model";
import { OverlapSearchRequest } from "./models/overlap-search-request.model";
import { OverlapSearchResponse } from "./models/overlap-search-response.model";
import { CampaignDetailsResponse } from "./models/campaign-details-response.model";
import { CampaignDetailsModel } from "./models/campaign-details.model";
import { CampaignEditRequest } from "./models/campaign-edit-request.model";
import { BaseResultsCode } from "../shared/model/base-results-code.enum";


@Injectable()
export class CampaignCommonService implements OnDestroy {

    private previousUrl: string = null;
    private _subscriptions: Subscription[] = [];

    constructor(private http: Http, private router: Router, private config: ConfigService) {
        // Store previous url
        this._subscriptions.push(
            router.events
                .filter(event => event instanceof NavigationEnd)
                .subscribe((e: NavigationEnd) => {
                    this.previousUrl = e.url;
                })
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    public getPreviousUrl(): string {
        return this.previousUrl;
    }

    public setPreviousUrl(previousUrl: string): void {
        this.previousUrl = previousUrl;
    }

    public getGameTypes(): Observable<Array<GameTypeInfo>> {
        return this.http.get(this.config.URLs.campaign.gameTypes)
            .map((response: Response) => this.fillLabelsForGameTypesAndSubtypes(response.json()))
            .catch(HelperService.handleError);
    }

    public getTriggersModelAll(): Array<any> {
        // TODO: Translate!
        return this.getArrayModelsFromMap(this.getTriggers(true));
    }

    public getTriggersModelForCreation(): Array<any> {
        // TODO: Translate!
        return this.getArrayModelsFromMap(this.getTriggers(true));
    }

    public getTriggerForCode(triggerCode: string): string {
        // TODO: Translate!
        const triggers: Map<string, string> = this.getTriggers();
        return triggers.get(triggerCode);
    }

    // TODO: Translate and remove!
    public getTriggers(supportedForCreation: boolean = false): Map<string, string> {
        const map: Map<string, string> = new Map<string, string>();

        map.set("CashBackAdjustmentActivator", "Manual");
        map.set("RegisterWalletPlayer", "Registration");
        map.set("FirstDepositActivator", "First Deposit");
        map.set("N/A", "N/A");

        if (!supportedForCreation) {
            map.set("FirstDepositActivatorWithRegistrationPromoCode", "First Deposit with Registration Promo Code");
        }

        return map;
    }

    public getPaymentMethodsModelForCreation(): Array<any> {
        // TODO: Translate!
        return this.getArrayModelsFromMap(this.getPaymentMethods());
    }

    // TODO: Translate and remove!
    public getPaymentMethods(): Map<string, string> {
        const map: Map<string, string> = new Map<string, string>();

        map.set("SuertiaCash", "Suertia Cash");
        map.set("Skrill", "Skrill");
        map.set("PayPal", "PayPal");
        map.set("SilkRoad", "Silk Road");
        map.set("Trustly", "Trustly");
        map.set("Paysafecard", "Paysafecard");
        map.set("RFCash", "RF Cash");
        map.set("BravoCard", "Bravo Card");
        map.set("FastPay", "Fast Pay");
        map.set("Teleingreso", "Teleingreso");
        map.set("TelepayCreditCard", "Telepay");
        map.set("CaixaCreditCard", "Caixa");

        return map;
    }

    public getPaymentMethodLabelsForCodes(paymentMethodCodes: Array<string>): Array<string> {
        if (!paymentMethodCodes || paymentMethodCodes.length === 0) {
            return [];
        }
        // TODO: Translate!
        const paymentMethodLabels: Map<string, string> = this.getPaymentMethods();
        return paymentMethodCodes.map((pmCode: string) => {
            return paymentMethodLabels.get(pmCode);
        });
    }

    public getGameSubtypeLabelsForCodes(gameSubtypeCodes: Array<string>): Array<string> {
        const gameSubtypeLabels: Map<string, string> = this.getGameSubtypeLabels();
        return gameSubtypeCodes.map((gstCode: string) => {
            return gameSubtypeLabels.get(gstCode);
        });
    }

    public getCancelOnWithdrawTypesLabelsForCode(code: string): string {
        // TODO: Translate!
        const labels: Map<string, string> = this.cancelOnWithdrawTypesLabels();
        return labels.get(code);
    }

    public getOverlappingCampaigns(request: OverlapSearchRequest): Observable<OverlapSearchResponse> {
        return this.http.get(this.config.URLs.campaign.overlapCheck, { params: request })
            .map((response: Response) => response.json())
            .catch(HelperService.handleError);
    }

    /**
     * PUT campaign with a specified campaignId
     * Updates the campaign related to the specified campaignId.
     * @param campaignId campaign id.
     * @param campaign CampaignEditRequest.
     */
    public updateCampaign(campaignId: number, request: CampaignEditRequest): Observable<BaseResultsCode> {
        if (!campaignId) {
            throw new Error('Required parameter campaignId was null or undefined when calling updateCampaign.');
        }
        if (!request) {
            throw new Error('Required parameter request was null or undefined when calling updateCampaign.');
        }

        return this.http.put(this.config.URLs.campaign.edit(campaignId), request)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch((error) => {
                return Observable.from([HelperService.getBaseResultCode(error.status)]);
            });
    }

    /**
     * Get the campaign details
     * @param campaignId Campaign ID.
     * @return  Observable<CampaignDetailsModel>
     */
    public getCampaignDetails(campaignId: number): Observable<CampaignDetailsModel> {
        if (!campaignId) {
            throw new Error("Required parameter campaignId was null or undefined when calling getCampaignDetails");
        }
        return this.http.get(this.config.URLs.campaign.details(campaignId))
            .map((response: Response) => response.json())
            .map((det: CampaignDetailsResponse) => this.mapCampaignDetailsResponseToCampaignDetailsModel(det))
            .catch(HelperService.handleError);
    }

    private mapCampaignDetailsResponseToCampaignDetailsModel(response: CampaignDetailsResponse): CampaignDetailsModel {
        // TODO: Translate!
        const gameSubtypeLabels: Array<string> = this.getGameSubtypeLabelsForCodes(response.gameSubtypes);
        // TODO: Translate!
        const triggerLabel: string = this.getTriggerForCode(response.trigger);
        // TODO: Translate!
        const paymentMethodLabel: Array<string> = this.getPaymentMethodLabelsForCodes(response.paymentMethods);
        // TODO: Translate!
        const cancelOnWithdrawLabel: string = response.cancelOnWithdraw ? this.getCancelOnWithdrawTypesLabelsForCode(response.cancelOnWithdraw.toLowerCase()) : null;

        const datailsModel: CampaignDetailsModel = new CampaignDetailsModel(response.campaignId, response.name, response.startDate, response.endDate,
            response.daysToExpiration, gameSubtypeLabels.join(", "), cancelOnWithdrawLabel, response.closeConsumedAfterMinutes,
            response.trigger, triggerLabel, response.initialAmount, response.percentOfBonusAmount, response.promotionalCode,
            response.maxBonusToReceive, response.minDepositRange, response.maxDepositRange, response.percentBonus,
            response.oddsLimit, response.campaignStatus, paymentMethodLabel.join(", "), response.depositsScopeStartDate, response.redemptionOnWinnings,
            response.minimumNumberOfBetsPerRedemption, response.onlyFirstBetPerEvent, response.excludeRealWalletBets);

        return datailsModel;
    }

    private getArrayModelsFromMap(map: Map<string, string>): Array<{ code: string, label: string }> {
        const model = new Array<{ code: string, label: string }>();
        map.forEach((value: string, key: string) => model.push({ code: key, label: value }));
        return model;
    }

    private fillLabelsForGameTypesAndSubtypes(gameTypes: Array<GameTypeInfo>): void {
        // TODO: Translate!
        const gameTypeLabels: Map<string, string> = this.getGameTypeLabels();
        // TODO: Translate!
        const gameSubtypeLabels: Map<string, string> = this.getGameSubtypeLabels();
        gameTypes.forEach((gt: GameTypeInfo) => {
            gt.gameSubtypes.forEach((gst: GameTypeInfo) => {
                gst.label = gameSubtypeLabels.get(gst.code);
                gst.compatibilityGroup = gt.compatibilityGroup;
            });
            gt.label = gameTypeLabels.get(gt.code);
        });
    }

    // TODO: Translate and remove!
    private getGameTypeLabels(): Map<string, string> {
        const map: Map<string, string> = new Map<string, string>();

        map.set("CASINO", "Casino");
        map.set("POKER", "Poker");
        map.set("BINGO", "Bingo");
        map.set("BET", "Sport Bet");
        map.set("SKILL", "Skill");
        map.set("LOTTERY", "Lottery");
        map.set("CONCOURSE", "Contest");
        map.set("SLOT", "Slots");

        return map;
    }

    // TODO: Translate and remove!
    private getGameSubtypeLabels(): Map<string, string> {
        const map: Map<string, string> = new Map<string, string>();

        map.set("ROULETTE", "Roulette");
        map.set("BLACKJACK", "Blackjack");
        map.set("SLOT", "Slots");
        map.set("POKER", "Poker");
        map.set("POKERTOUR", "Poker Tournament");
        map.set("POKERCASH", "Poker Cash");
        map.set("BINGO", "Bingo");
        map.set("BET", "Sport Bet");
        map.set("POOLBET", "Pool Bet");
        map.set("BACCARAT", "Baccarat");
        map.set("CONCOURSE", "Contest");
        map.set("COMPLEMENT", "Complement");
        map.set("HRBET", "Horse Race Bet");
        map.set("HRPOOLBET", "Horse Race Pool Bet");
        map.set("NOSPORTBET", "Non Sport Bet");

        return map;
    }

    private cancelOnWithdrawTypesLabels(): Map<string, string> {
        const map: Map<string, string> = new Map<string, string>();

        map.set("true", "Yes");
        map.set("false", "No");
        map.set("onlyActive", "Only if Active");

        return map;
    }

}
