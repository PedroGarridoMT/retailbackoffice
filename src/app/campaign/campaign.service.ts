import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Http } from '@angular/http';
import { Router, NavigationEnd } from '@angular/router';

@Injectable()
export class CampaignService implements OnDestroy {

    private previousUrl: string = null;
    private _subscriptions: Subscription[] = [];

    constructor(private http: Http, private router: Router) {
        // Store previous url
        this._subscriptions.push(
            router.events
                .filter(event => event instanceof NavigationEvent)
                .subscribe((e: NavigationEnd) => {
                    this.previousUrl = e.url;
                })
        );
    }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    public getPreviousUrl(): string {
        return this.previousUrl;
    }

    public setPreviousUrl(previousUrl: string): void {
        this.previousUrl = previousUrl;
    }

}
