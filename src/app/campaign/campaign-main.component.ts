import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import {
    HomeTemplateAction,
    HomeTemplateDashboardItem,
    HomeTemplateSavedSearch,
    PermissionsService
} from "mt-web-components";
import { PermissionsCode } from "../shared/model/permissions-code.model";

@Component({
    templateUrl: "./campaign-main.component.html",
    styleUrls: ["./campaign-main.component.scss"],
})
export class CampaignsMainComponent implements OnInit {
    actions: HomeTemplateAction[] = [];
    searches: HomeTemplateSavedSearch[];
    dashboardItems: HomeTemplateDashboardItem[];

    constructor(private router: Router, private translate: TranslateService, private permissionsService: PermissionsService) {
    }

    ngOnInit() {
        if (this.permissionsService.hasAccess(PermissionsCode.CampaignsSearch)) {
            this.actions.push({
                targetRoute: "/campaigns/search",
                text: this.translate.instant("campaigns.home.search"),
                description: this.translate.instant("campaigns.home.actionSearchDescription"),
            });
        }
        if (this.permissionsService.hasAccess(PermissionsCode.CampaignMassiveAssignation)) {
            this.actions.push({
                targetRoute: "/campaigns/manual-campaign-create",
                text: this.translate.instant("campaigns.manualCampaigns.create.massiveBonusAssignation"),
                description: this.translate.instant("campaigns.manualCampaigns.create.massiveBonusAssignationDescription"),
            });
        }
        if (this.permissionsService.hasAccess(PermissionsCode.CampaignCreate)) {
            this.actions.push({
                targetRoute: "/campaigns/create",
                text: this.translate.instant("campaigns.create.campaignCreate"),
                description: this.translate.instant("campaigns.create.campaignCreateDescription"),
            });
        }
    }

    onSavedSearchClick(savedSearchId: string) {
        console.log("onSavedSearchClick ", savedSearchId);
    }
}
