import { OnInit, OnDestroy, Component, ViewEncapsulation } from '@angular/core';
import { TitleBarService, TitleBarHandlerComponent } from '../../shared';
import { WizardButton, WizardLiterals } from 'mt-web-components';
import { TranslateService } from '@ngx-translate/core';
import { AppRoutes } from '../../app-routes';
import { Router } from '@angular/router';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: "pas-campaign-create",
    templateUrl: "./campaign-create.component.html",
    styleUrls: ["./campaign-create.component.scss"]
})
export class CampaignCreateComponent extends TitleBarHandlerComponent implements OnInit, OnDestroy {
    public leftButton: WizardButton = {
        text: this.translate.instant("common.cancel"),
        cssClass: 'btn-link'
    };
    public saveButton: WizardButton = {
        text: this.translate.instant("common.save"),
        cssClass: 'btn-forward'
    };
    public wizardLiterals: WizardLiterals = {
        step: this.translate.instant('wizard.step'),
        introduction: this.translate.instant('wizard.introduction'),
        next: this.translate.instant('wizard.next'),
        previous: this.translate.instant('wizard.previous')
    };

    constructor(public titleBarService: TitleBarService, private translate: TranslateService, private router: Router) {
        super(titleBarService);
    }

    public ngOnInit(): void {

    }

    public ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public onCancelButtonClick() {
        this.router.navigate([AppRoutes.campaignMain]);
    }

}
