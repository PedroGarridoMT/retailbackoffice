import {PipeTransform} from "@angular/core";
import {LocalizedTransformPipe} from "./localized-pipe";

let localizedPipe: LocalizedTransformPipe<BasePipe>;

describe('Localized pipe - TEST', () => {

    beforeEach(() => {
        localizedPipe = new LocalizedTransformPipe<BasePipe>(new TranslateMockService() as any, BasePipe)
    });

    it('should use base pipe transform with same input parameters', () => {
        let parameters = getBaseParameters();
        TranslateMockService.currentLang = parameters.lang;
        applyTransformation(parameters);

        let expectation =  expect(BasePipe.lastCreated.transform);
        expectation.toHaveBeenCalledWith.apply(expectation, [parameters.value].concat(parameters.args));
    });

    it('should return value produced by the base pipe using current lang', () => {
        let parameters = getBaseParameters();
        TranslateMockService.currentLang = parameters.lang;
        let result = applyTransformation(parameters);

        expect(result).toEqual(basePipeTransformation(parameters));
    });


    it('should return value produced by the base pipe when lang changes', () => {
        let parameters = getBaseParameters();
        TranslateMockService.currentLang = parameters.lang = 'en';
        let result = applyTransformation(parameters);

        expect(result).toEqual(basePipeTransformation(parameters));
    });

});


function getBaseParameters() {
    let parameters = {
        lang: 'es',
        value: 100,
        args: ['a', 'b', 1 ,2 ,3]
    };
    return JSON.parse(JSON.stringify(parameters));
}

function basePipeTransformation(parameters) {
    return BasePipe.lastCreated.transform.apply(BasePipe.lastCreated, [parameters.value].concat(parameters.args));
}


function applyTransformation(parameters) {
    return localizedPipe.transform.apply(localizedPipe, [parameters.value].concat(parameters.args));
}

class TranslateMockService {
    static currentLang;
    get defaultLang (): string {
        return TranslateMockService.currentLang;
    }
}


class BasePipe implements PipeTransform {
    lang: string;
    static lastCreated: BasePipe;

    constructor(lang: string) {
        this.lang = lang;
        spyOn(this, 'transform').and.callThrough();
        BasePipe.lastCreated = this;
    }

    transform(value: any, ...args: any[]) {
        return {
            lang: this.lang,
            value: value,
            args: args
        };
    }
}