import {PipeTransform} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";


interface ClassType<T> {
    new (...args: any[]): T;
}


export class LocalizedTransformPipe<T>  implements PipeTransform {
    private lastLang: string;
    private lastPipe: any;

    constructor(
        private translateService: TranslateService,
        private basePipeClass: ClassType<T>
    ) {}

    transform(value: any, ...args: any[]): any {
        return this.getBasePipe(this.basePipeClass).transform(value, ...args);
    }

    private getBasePipe<T>(classType: ClassType<T>) {
        let currentLang = this.translateService.defaultLang;
        if (currentLang !== this.lastLang || !this.lastPipe) {
            this.lastLang = currentLang;
            this.lastPipe = new classType(currentLang);
        }
        return this.lastPipe;
    }
}