import {PipeTransform} from "@angular/core";


export class LocalizedTransformMockPipe implements PipeTransform {
    transform(value: any, ...args: any[]): any {
        console.log(args);
        return Boolean(value) ? value.toString() : '';
    }
}
