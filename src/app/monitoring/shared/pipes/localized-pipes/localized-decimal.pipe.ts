import {Pipe, PipeTransform} from "@angular/core";
import {DecimalPipe} from "@angular/common";
import {TranslateService} from "@ngx-translate/core";
import {LocalizedTransformPipe} from "./localized-pipe";

@Pipe({ name: 'localizeddecimal', pure: false})
export class LocalizedDecimalPipe
    extends LocalizedTransformPipe<DecimalPipe>
    implements PipeTransform {

    constructor(translateService: TranslateService) {
        super(translateService, DecimalPipe);
    }
}