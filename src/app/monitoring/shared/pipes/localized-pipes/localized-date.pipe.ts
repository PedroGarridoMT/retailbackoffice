import {Pipe, PipeTransform} from "@angular/core";
import {DatePipe} from "@angular/common";
import {TranslateService} from "@ngx-translate/core";
import {LocalizedTransformPipe} from "./localized-pipe";

@Pipe({ name: 'localizeddate', pure: false})
export class LocalizedDatePipe
    extends LocalizedTransformPipe<DatePipe>
    implements PipeTransform {

    constructor(translateService: TranslateService) {
        super(translateService, DatePipe);
    }
}