import {NgModule, Pipe, PipeTransform} from "@angular/core";
import {LocalizedTransformMockPipe} from "./localized-pipes/localized-pipe.mock";

@Pipe({ name: 'localizedcurrency', pure: false})
export class LocalizedCurrencyMockPipe extends LocalizedTransformMockPipe implements PipeTransform {}

@Pipe({ name: 'localizeddate', pure: false})
export class LocalizedDateMockPipe extends LocalizedTransformMockPipe implements PipeTransform{}

@Pipe({ name: 'localizeddecimal', pure: false})
export class LocalizedDecimalMockPipe extends LocalizedTransformMockPipe implements PipeTransform{}

@NgModule({
    declarations: [
        LocalizedCurrencyMockPipe,
        LocalizedDateMockPipe,
        LocalizedDecimalMockPipe
    ],
    exports: [
        LocalizedCurrencyMockPipe,
        LocalizedDateMockPipe,
        LocalizedDecimalMockPipe
    ]
})
export class LocalizedPipesMockModule {}