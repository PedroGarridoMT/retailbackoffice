import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {AliveIconComponent} from "./components/alive-icon.component";
import {TerminalCheckerProvider} from "./providers/terminal-checker";
import {LocalizedPipesModule} from "./pipes/pipes.module";
import {LocalizedCurrencyPipe} from "./pipes/localized-pipes/localized-currency.pipe";
import {LocalizedDecimalPipe} from "./pipes/localized-pipes/localized-decimal.pipe";
import {LocalizedDatePipe} from "./pipes/localized-pipes/localized-date.pipe";

@NgModule({
    imports: [
        SharedModule,
        LocalizedPipesModule
    ],
    declarations:[AliveIconComponent],
    entryComponents:[],
    exports:[
        AliveIconComponent,
        LocalizedCurrencyPipe,
        LocalizedDatePipe,
        LocalizedDecimalPipe
    ],
    providers:[TerminalCheckerProvider]
})
export class MonitoringSharedModule{}
