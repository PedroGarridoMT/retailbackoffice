const terminalAckIntervalMillis = 10000;

export const config = {
    terminalAckIntervalMillis: terminalAckIntervalMillis,
    terminalAckWarningMillis: terminalAckIntervalMillis * 1.5,
    terminalAckErrorMillis: terminalAckIntervalMillis * 3
};
