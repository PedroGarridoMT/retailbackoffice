import {Component, Input} from "@angular/core";
import {TerminalCheckerProvider} from "../providers/terminal-checker";
import {AliveStatus} from "../models/app.models";

enum ConnectionClasses {
    on = 'icon-on',
    warning = 'icon-warning-off',
    off = 'icon-off'
}

@Component({
    "selector": 'mt-alive-icon',
    "template": `
        <i class="fa fa-power-off" [ngClass]="isAliveClass()"
           data-toggle="tooltip" title="{{lastAck | localizeddate:'medium'}}"></i>
    `,
    styles: [`
        .icon-on {
            opacity: 1;
        }

        .icon-warning-off {
            animation: blinker 0.4s linear infinite;
        }

        .icon-off {
            opacity: 0.3;
        }

        @keyframes blinker {
            50% {
                opacity: 0.3;
            }
        }
    `],
})
export class AliveIconComponent {
    @Input() lastAck: any;

    constructor(
        private checker: TerminalCheckerProvider
    ) {
    }

    isAliveClass(): ConnectionClasses {
        let alive = this.checker.aliveStatus({'TerminalAcknowledgement': this.lastAck});
        switch (alive) {
            case AliveStatus.OK:
                return ConnectionClasses.on;
            case AliveStatus.WARNING:
                return ConnectionClasses.warning;
            case AliveStatus.KO:
                return ConnectionClasses.off
        }
        return ConnectionClasses.on;
    }

}
