import { Injectable } from '@angular/core';
import {config} from "../config/config";
import {AliveStatus, ErrorStatus} from "../models/app.models";
import {GeneralStatus, Status, TerminalStatusDB} from "../models/db.models";

/*
  Generated class for the TerminalCheckerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TerminalCheckerProvider {

    public aliveStatus(terminal: TerminalStatusDB): AliveStatus {
        let ackDate = this.parseDate(terminal.TerminalAcknowledgement);
        if(!ackDate) {
            return AliveStatus.KO;
        }
        let tillLastAckMillis = (Date.now() - ackDate.getTime());
        if(tillLastAckMillis>=config.terminalAckErrorMillis) {
            return AliveStatus.KO;
        } else if (tillLastAckMillis>=config.terminalAckWarningMillis) {
            return AliveStatus.WARNING;
        } else {
            return AliveStatus.OK;
        }
    }

    public isLocked(terminal: TerminalStatusDB): boolean {
        return  terminal.Status === Status.LocalLock ||
            terminal.Status === Status.RemoteLock;
    }

    public errorStatus(terminal: TerminalStatusDB): ErrorStatus {
        switch(terminal.TerminalGeneralStatus) {
            case GeneralStatus.OK:
                return ErrorStatus.OK;
            case GeneralStatus.WARNING:
                return ErrorStatus.WARNING;
            case GeneralStatus.ERROR:
                return ErrorStatus.ERROR;
        }
    }

    private parseDate(dateString:  string): Date | null {
        let result = new Date(dateString);
        return isNaN(result.getTime()) ? null : result;
    }
}
