import {Component, OnInit} from "@angular/core";
import {HomeTemplateAction, HomeTemplateDashboardItem, PermissionsService} from "mt-web-components";

import {PermissionsCode} from "../shared/model/models";
import {TranslateService} from "@ngx-translate/core";

@Component({
    template:
            `
        <div class="monitoring-home">
            <mt-home-template [iconClasses]="'fa fa-bar-chart'"
                              [pageTitle]="'monitoring.home.title' | translate"
                              [mainActionsTitle]="'home.mainActions' | translate"
                              [mainActionsContent]="actions"
                              [dashboardTitle]="'home.dashboard' | translate"
                              [dashboardContent]="dashboardItems">
            </mt-home-template>
        </div>`,
    styles: [`
        .monitoring-home {
            padding: 1em;
        }
    `]
})
export class MonitoringMainComponent implements OnInit {
    actions: HomeTemplateAction[] = [];
    dashboardItems: HomeTemplateDashboardItem[];

    constructor(
        private translate: TranslateService,
        private permissionsService: PermissionsService) {
    }

    ngOnInit(): void {
        this.translate
            .get(["monitoring.home.search", "monitoring.home.actionSearchDescription",])
            .subscribe((translations) => {
                if (this.permissionsService.hasAccess(PermissionsCode.Monitoring)) {
                    this.actions.push({
                        targetRoute: "/monitoring/search",
                        text: translations["monitoring.home.search"],
                        description: translations["monitoring.home.actionSearchDescription"],
                        cssSelector: "monitoring-home-search-selector"
                    })
                }
            });
    }
}
