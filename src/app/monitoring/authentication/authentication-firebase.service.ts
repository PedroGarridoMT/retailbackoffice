import {Injectable} from "@angular/core";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {LoginResponseExtended} from "../../account/model/LoginResponseExtended";
import {PermissionsCode} from "../../shared/index";
import {PermissionsService} from "mt-web-components";


@Injectable()
export class AuthenticationFirebaseService{
    user: Observable<firebase.User>;
    private currentUser: any;

    constructor(private firebaseAuth: AngularFireAuth,
                private permissionsService: PermissionsService) {
        this.user = firebaseAuth.authState; //todo review how use it
        this.currentUser = null;
    }

    signup(email: string, password: string) {
        this.firebaseAuth
            .auth
            .createUserWithEmailAndPassword(email, password)
            .then(value => {
                console.log('Success!', value);
            })
            .catch(err => {
                console.log('Something went wrong:',err.message);
            });
    }

    login(email: string, password: string): Promise<any>{
        return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password)
            .then(value => {
                this.currentUser = AuthenticationFirebaseService.mappingUser(value);
                this.saveLoginResponseToLocalStorage();
                return Promise.resolve(this.currentUser)
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    private static mappingUser(responseFirebase:any): LoginResponseExtended{
        let expiresIn = 30 * 60 * 1000; //30 minutes
        let currentDate = new Date();
        let expiredAtTimeStamp = currentDate.getTime() + expiresIn;
        return {
            username: responseFirebase.email,
            expiresIn: expiresIn,
            expiresAt: new Date(expiredAtTimeStamp)
        }
    }

    /**
     * Save to local Storage and updates permissionsService.userPermissions
     * @param loginResponse
     * @returns {null}
     */
    private saveLoginResponseToLocalStorage(): void {
        if (this.currentUser) {
            //Configure PermissionsService with current user permissions(mt-web-components)
            this.permissionsService.userPermissions = PermissionsCode.Monitoring.concat(PermissionsCode.SettingsHome);
            //
            //Store user relevant data.
            localStorage.setItem("user", JSON.stringify(this.currentUser));

            // // Kick of logout timer
            // this.startLogoutTimer(); //TODO IF IT IS NECCESSARY
        }
    }

    logout() {
        this.firebaseAuth.auth.signOut();
        this.currentUser = null; //TODO REVIEW THIS USER
    }

    /**
     * isUserAuthenticated
     * @returns {boolean}
     */
    public isUserAuthenticated(): boolean {
        return this.getCurrentUser() != null;
    }

    public getCurrentUser(): any {
        return this.currentUser;
    }
}
