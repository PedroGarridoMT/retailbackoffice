import {NgModule} from "@angular/core";
import {MonitoringMainComponent} from "./monitoring-main.component";
import {SharedModule} from "../shared/shared.module";
import {AppRoutingModule} from "../app-routing.module";

import {AngularFireModule} from "angularfire2";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AuthenticationFirebaseService} from "./authentication/authentication-firebase.service";
import {AngularFireDatabaseModule} from "angularfire2/database";

import {MonitoringSearchComponent, MonitoringSearchTableComponent, TerminalMappingService} from "./search/index";
import {MonitoringSharedModule} from "./shared/monitoring-shared.module";
import {MonitoringSearchBarComponent} from "./search/monitoring-search-bar.component";


@NgModule({
    imports: [
        AppRoutingModule,
        SharedModule,
        AngularFireModule.initializeApp(process.env.API_KEY),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        MonitoringSharedModule
    ],
    declarations: [
        MonitoringMainComponent,
        MonitoringSearchComponent,
        MonitoringSearchTableComponent,
        MonitoringSearchBarComponent
    ],
    entryComponents: [],
    exports: [
        MonitoringMainComponent,
        MonitoringSearchComponent,
        MonitoringSearchTableComponent,
        MonitoringSearchBarComponent
    ],
    providers: [
        AuthenticationFirebaseService,
        TerminalMappingService
    ]
})
export class MonitoringModule {
}
