export class Terminal {
    public CenterName: string;
    public TerminalId: number;
    public ClientSoftwareVersion: string;
    public EmptyHopperProcess: boolean;
    public FrontStatus: string;
    public RefillInProccess: boolean;
    public Scenario: string;
    public Status: string;
    public TerminalAcknowledgement: any;
    public TerminalGeneralStatus: any;
    public TerminalStart: any;
}

export enum FrontStatus {
    UP = 'UP',
    DOWN = 'DOWN'}

export enum Scenario {
    Dashboard = 'Dashboard' ,
    InGame ='InGame' ,
    InLotteryGame = 'InLotteryGame' ,
    Tester_Basic = 'Tester_Basic' ,
    Tester_Pro ='Tester_Pro'}

export enum Status {
    Nolock = 'Nolock' ,
    UnexpectedError = 'UnexpectedError' ,
    RemoteLock = 'RemoteLock' ,
    LocalLock = 'LocalLock' ,
    LostConnection = 'LostConnection' ,
    OpenMainDoor = 'OpenMainDoor' ,
    HoppersOut = 'HoppersOut' ,
    OpenDownDoor = 'OpenDownDoor' ,
    PrizeRecived = 'PrizeRecived' ,
    ManualPayment = 'ManualPayment' ,
    InitPayment = 'InitPayment' ,
    AskPayment = 'AskPayment' ,
    PaymentServiceError = 'PaymentServiceError'}


export enum GeneralStatus {
    OK = 'OK',
    WARNING = 'WARNING',
    ERROR = 'ERROR'}
