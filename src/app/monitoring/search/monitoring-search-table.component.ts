import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from "@angular/core";
import {TableColumnType, TableColumnVisibilityEnum} from "mt-web-components";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: "mt-monitoring-search-table",
    templateUrl: "./monitoring-search-table.component.html",
    styleUrls: ['./monitoring-search-table.component.scss'],
})
export class MonitoringSearchTableComponent implements OnInit, OnChanges {

    @Input() rows: any[];
    @Input() loading: boolean;

    @ViewChild('terminalGeneralStatusView') terminalGeneralStatusView;
    @ViewChild('terminalAcknowledgeView') terminalAcknowledgeView;
    @ViewChild('terminalStartView') terminalStartView;

    @ViewChild('terminalScenarioView') terminalScenarioView;
    @ViewChild('statusView') statusView;
    @ViewChild('typeView') typeView;

    columns: any[];

    constructor(private translate: TranslateService) {
    }

    ngOnInit(): void {
        this.generateTable();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes && changes.rows.currentValue) {
            this.loadRows();
        }
    }

    private generateTable(): void {
        this.loadColumns();
        this.loadRows();
    }

    private loadColumns(): void {
        this.columns = [
            {
                type: TableColumnType.DATA,
                id: "CenterName",
                label: this.translate.instant("monitoring.entity.centerName"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "TerminalId",
                label: this.translate.instant("monitoring.entity.terminalId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "ClientSoftwareVersion",
                label: this.translate.instant("monitoring.entity.clientVersion"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "typeView",
                label: this.translate.instant("monitoring.entity.clientType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "terminalAcknowledgementView",
                label: this.translate.instant("monitoring.entity.terminalAcknowledgement"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "terminalGeneralStatusView",
                label: this.translate.instant("monitoring.entity.terminalGeneralStatus"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "terminalScenarioView",
                label: this.translate.instant("monitoring.entity.scenario"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "statusView",
                label: this.translate.instant("monitoring.entity.terminalStatus"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.HTML,
                id: "terminalStartView",
                label: this.translate.instant("monitoring.entity.terminalStart"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ];
    }

    private loadRows(): void {
        if (this.rows) {
            this.rows.forEach((item) => {
                item['terminalAcknowledgementView'] = this.terminalAcknowledgeView;
                item['terminalStartView'] = this.terminalStartView;
                item['terminalGeneralStatusView'] = this.terminalGeneralStatusView;
                item['terminalScenarioView'] = this.terminalScenarioView;
                item['statusView'] = this.statusView;
                item['typeView'] = this.typeView;
            });
        }
    }
}
