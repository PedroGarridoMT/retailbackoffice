import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from "@angular/core";
import {BasicFilterBarLiterals, FilterToValueMap, FilterType} from "mt-web-components";
import {TranslateService} from "@ngx-translate/core";
import {TitleBarHandlerComponent, TitleBarParams, TitleBarService} from "../../shared";
import {MonitoringSearchComponent} from "./monitoring-search.component";


@Component({
    selector: 'mt-monitoring-search-bar',
    template: `
        <mt-basic-filters [filters]="filters"
                          [literals]="filterBarLiterals"
                          [allowEmptySearch]="true"
                          (search)="onSearchBasicFilter($event)"
                          (clear)="onClearBasicFilter()">
        </mt-basic-filters>
    `
})
export class MonitoringSearchBarComponent extends TitleBarHandlerComponent implements OnInit, OnChanges {
    @Input() filterList: any;
    @Output() searchFilter = new EventEmitter<FilterToValueMap>();
    public static PAGE_TITLE: string = "monitoring.home.search";
    titleBarParams: TitleBarParams = {
        previousPagePath: 'monitoring',
        previousPageLabel: 'monitoring.home.title',
        pageTitle: MonitoringSearchComponent.PAGE_TITLE,
        pageActions: null
    };

    filters: any;
    filterBarLiterals: BasicFilterBarLiterals;
    valuesToSearch: FilterToValueMap = {};

    constructor(private translate: TranslateService, titleBarService: TitleBarService) {
        super(titleBarService);
    }

    ngOnInit(): void {
        this.loadFilters();
    }

    ngOnChanges(changes): void {
        if (changes && changes.filterList && MonitoringSearchBarComponent.isNewFilterList(changes)) {
            this.loadFilters();
        }
    }


    public onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.searchFilter.emit(searchValues);
    }

    public onClearBasicFilter() {
        this.valuesToSearch = null;
    }

    private getCenterList(): any {
        let centerList = [];
        if (!this.filterList || !this.filterList.centerName) {
            return centerList;
        }
        this.filterList.centerName.forEach((centerName) => {
            centerList.push({value: centerName, label: centerName});
        });
        return centerList;
    }

    private loadFilters(): void {
        this.filters = [
            {
                id: 'centerName',
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("monitoring.entity.centerName"),
                shownByDefault: true,
                options: this.getCenterList()
            },
            {
                id: 'terminalType',
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("monitoring.entity.terminalType"),
                shownByDefault: true,
                options: [
                    {value: 'monitoring.clientType.terminal', label:  this.translate.instant('monitoring.clientType.terminal')},
                    {value: 'monitoring.clientType.viewer', label:  this.translate.instant('monitoring.clientType.viewer')}
                ]
            },
            {
                id: 'terminalStatus',
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("monitoring.entity.terminalGeneralStatus"),
                shownByDefault: true,
                options: [
                    {value: 'OK', label:  this.translate.instant("monitoring.terminalStatus.ok")},
                    // {value: 'WARNING', label:  this.translate.instant("monitoring.terminalStatus.warning")},
                    // {value: 'WARNING_FRONTEND', label:  this.translate.instant("monitoring.terminalStatus.warning_frontend")},
                    {value: 'ERROR', label:  this.translate.instant("monitoring.terminalStatus.error")}
                ]
            }
        ];
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };
    }


    private static isNewFilterList(changes: any): boolean {
        return !(MonitoringSearchBarComponent.equalsTwoObject(changes.filterList.currentValue, changes.filterList.previousValue));
    }

    private static equalsTwoObject(obj1, obj2): boolean {
        return JSON.stringify(obj1) === JSON.stringify(obj2);
    }
}
