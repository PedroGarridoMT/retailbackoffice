import {Injectable} from "@angular/core";

@Injectable()
export class TerminalMappingService {

    public mappingTerminals(response): any {
        let terminalMapping = [];
        response.forEach((item) => {
            let centerName = item.key;
            let terminalList = item.payload.val();
            let terminalsIdsList = Object.keys(terminalList);
            terminalsIdsList.forEach((terminalId) =>
                terminalMapping.push(TerminalMappingService.createTerminalObject(centerName, terminalId, terminalList[terminalId]))
            );
        });

        return terminalMapping;
    }

    public mappingOnlyTerminalsOfCenter(terminalList, centerName): any {
        let terminalMapping = [];
        terminalList.forEach((terminalItem) =>
            terminalMapping.push(TerminalMappingService.createTerminalObject(centerName, terminalItem.key, terminalItem.payload.val()))
        );

        return terminalMapping;
    }

    private static createTerminalObject(centerName: string, terminalId: any, payload: any): any {
        let concatTerminal = Object.assign({}, {CenterName: centerName, TerminalId: terminalId}, payload);
        concatTerminal.Scenario = 'monitoring.scenario.' + concatTerminal.Scenario.toLowerCase();

        concatTerminal.GeneralStatus = TerminalMappingService.getGeneralStatus(concatTerminal.TerminalGeneralStatus, concatTerminal.FrontStatus);
        concatTerminal.Status = concatTerminal.GeneralStatus === 'WARNING_FRONTEND' ?
            'monitoring.status.nofrontend' :
            'monitoring.status.' + concatTerminal.Status.toLowerCase();

        concatTerminal.ClientType = 'monitoring.clientType.' + concatTerminal.ClientType.toLowerCase();
        return concatTerminal;
    }

    public getCenterNameList(response): Array<string> {
        let centerNameList: Array<string> = [];
        response.forEach((item) => centerNameList.push(item.key));
        return centerNameList;
    }

    private static getGeneralStatus(backendStatus: string, frontendStatus: string): string {
        if(backendStatus === 'WARNING' || backendStatus === 'ERROR'){
            return backendStatus;
        }
        return frontendStatus === 'UP' ? 'OK' : 'WARNING_FRONTEND';
    }
}
