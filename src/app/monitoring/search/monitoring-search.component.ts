import {Component, OnDestroy, OnInit} from "@angular/core";
import {TitleBarHandlerComponent, TitleBarParams, TitleBarService} from "../../shared";
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import {Terminal} from "../models/terminal.model";
import {Subscription} from "rxjs/Subscription";
import {TerminalMappingService} from "./terminal-mapping.service";
import {FilterToValueMap} from "mt-web-components";

const QUERY_BASE = 'STATUS/';

@Component({
    selector: 'mt-monitoring-search',
    template: `
        <mt-monitoring-search-bar 
            [filterList]="filterList" 
            (searchFilter)="onSearchBasicFilter($event)">
        </mt-monitoring-search-bar>
        
        <mt-monitoring-search-table 
            [loading]="loading" 
            [rows]="terminalsArray">
        </mt-monitoring-search-table>`
})
export class MonitoringSearchComponent  extends TitleBarHandlerComponent implements OnInit, OnDestroy {
    public static PAGE_TITLE: string = "monitoring.home.search";
    //Title bar attributes
    titleBarParams: TitleBarParams = {
        previousPagePath: 'monitoring',
        previousPageLabel: 'monitoring.home.title',
        pageTitle: MonitoringSearchComponent.PAGE_TITLE,
        pageActions: null
    };
    private _subscriptions: Subscription[] = [];
    private _subsciptionSearch: Subscription;

    items: Observable<any[]>;
    terminalsArray: Array<Terminal>;
    loading: boolean;
    dbService: any;
    filterList: any;

    private filter: any = {};

    constructor(private terminalMapping:TerminalMappingService, titleBarService: TitleBarService, db: AngularFireDatabase) {
        super(titleBarService);
        this.dbService = db;
    }

    ngOnInit(): void {
        this.init();
        // Notify and pass params to the title bar service, so it can notify subscribers
        this.notifyComponentInitialized(this.titleBarParams);

        // Subscribe to action requests that happens on the title bar
        this._subscriptions.push(
            this.titleBarService.actionRequested$.subscribe(actionId => MonitoringSearchComponent.handleTitleBarAction(actionId))
        );
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.titleBarService.componentDestroyed();
    }

    private init(){
        this.loading = true;
        this.search();
    }

    public onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.filter = searchValues;
        this.search();
    }

    private search():void{
        let query = this.filter.centerName ? QUERY_BASE + this.filter.centerName : QUERY_BASE;
        this.items = this.dbService.list(query).snapshotChanges();
        if(this._subsciptionSearch != undefined){ this._subsciptionSearch.unsubscribe()}

        this._subsciptionSearch = this.items.subscribe((response)=>{
            this.loading = false;

            this.updateFilterList(query, response);

            this.terminalsArray = query === QUERY_BASE ?
                    this.terminalMapping.mappingTerminals(response) :
                    this.terminalMapping.mappingOnlyTerminalsOfCenter(response, query.split(QUERY_BASE)[1]);

            if(this.filter.terminalType){
                this.terminalsArray = this.filterTerminal(this.terminalsArray, 'ClientType', this.filter.terminalType);
            }

            if(this.filter.terminalStatus){
                let valuesArray = this.filter.terminalStatus !== 'OK' ? ['WARNING', 'WARNING_FRONTEND', 'ERROR'] : ['OK'];
                this.terminalsArray = this.filterArrayTerminal(this.terminalsArray, 'GeneralStatus', valuesArray);
            }
        });
    }

    private filterTerminal(_terminalsArray, _key, _value): any {
        let filterByType = function(terminal) {
            return (terminal[_key] === _value);
        };
        return _terminalsArray.filter(filterByType, _value);
    }

    private filterArrayTerminal(_terminalsArray, _key, _values): any {
        let filterByType = function(terminal) {
            return (_values.indexOf(terminal[_key]) > -1);
        };
        return _terminalsArray.filter(filterByType, _values);
    }

    private updateFilterList(query:string, response){
        if(query === QUERY_BASE){
            this.filterList = {centerName: this.terminalMapping.getCenterNameList(response)};
        }
    }

    public static handleTitleBarAction(actionId: string) {
        switch (actionId) {
            default:
                console.log("No actions available yet");
                break;
        }
    }

}
