export class AppRoutes {
    public static home: string = "/";
    public static login: string = "/login";
    public static pageNotFound: string = "/page-not-found";
    public static accessForbidden: string = "/forbidden";
    public static changePassword: string = "/change-password";
    public static campaignMain: string = "/campaigns";
    public static campaignWizard: string = "/campaigns/create";
    public static campaignSearch: string = "/campaigns/search";
    public static campaignEdit: string = "/campaigns/{campaignId}/edit";
    public static campaignDetails: string = "/campaigns/{campaignId}";
    public static manualCampaignCreate: string = "/campaigns/manual-campaign-create";

    public static playersSearch: string = "/players/search";
    public static searchPlayerAutoLimitChanges: string = "/players/limits";
    public static searchBlacklistedPlayers: string = "/players/blacklist";
    public static addBlacklistedPlayers: string = "/players/blacklist/add";
    public static playerTransactions(accountCode: string): string[] { return ["players", accountCode, "transactions"]; };

    public static transactionsSearch: string = "/transactions/search";
    public static searchPendingWithdrawals: string = '/transactions/withdrawals';
}
