import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./static/home.component";
import { LoginComponent } from "./account/login/login.component";
import { AuthGuard } from "./core/can-activate-guard.service";
import { CanDeactivateGuard } from "./core/can-deactivate-guard.service";
import { MainTemplateComponent } from "./main-template/main-template.component";
import { CampaignsMainComponent } from "./campaign/campaign-main.component";
import { CampaignSearchComponent } from "./campaign/search/campaign-search.component";
import { CampaignDetailsComponent } from "./campaign/details/campaign-details.component";
import { CampaignDetailsDataComponent } from "./campaign/details/campaign-details-data.component";
import { PlayersMainComponent } from "./player/player-main.component";
import { PlayersSearchComponent } from "./player/search/player-search.component";
import { PlayerBlacklistSearchComponent } from "./player/blacklist-search/blacklist-search.component";
import { PlayerBlacklistAddComponent } from "./player/blacklist-add/blacklist-add.component";
import { PlayerDetailComponent } from "./player/detail/player-detail.component";
import { PlayerPersonalDataComponent } from "./player/detail/personal-data/player-personal-data.component";
import { TransactionsMainComponent } from "./transaction/transaction-main.component";
import { TransactionSearchComponent } from "./transaction/search/transaction-search.component";
import { WithdrawalSearchComponent } from "./transaction/withdrawals/withdrawal-search.component";
import { SettingsMainComponent } from "./settings/settings-main.component";
import { PermissionsGuard } from "./shared/permissions.guard";
import { PermissionsCode } from "./shared/model/models";
import { PageNotFoundComponent } from "./static/page-not-found.component";
import { AccessForbiddenComponent } from "./static/accesss-forbidden.component";
import { AutoLimitChangesSearchComponent } from "./player/limit-changes/autolimit-changes-search.component";
import { PlayerTransactionsComponent } from "./player/detail/transactions/player-transactions.component";
import { PlayerDocumentationSearchComponent } from "./player/detail/documentation/player-documentation-search.component";
import { PlayerResponsibleGamingComponent } from "./player/detail/responsible-gaming/player-responsible-gaming.component";
import { PlayerBonusesComponent } from "./player/detail/bonuses/player-bonuses.component";
import { ManualCampaignCreateComponent } from './campaign/manual-campaign/create/manual-campaign-create.component';
import { CampaignCreateComponent } from './campaign/create/campaign-create.component';
import {MonitoringMainComponent} from "./monitoring/monitoring-main.component";
import {MonitoringSearchComponent} from "./monitoring/search/monitoring-search.component";


const ROUTES: Routes = <Routes>[
    { path: 'login', component: LoginComponent },
    {
        path: '',
        component: MainTemplateComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: HomeComponent, data: { stateName: 'Home' } },
            { path: 'home', component: HomeComponent, data: { stateName: 'Home' } },
            { path: 'campaigns', component: CampaignsMainComponent, data: { stateName: 'Campaigns' } },
            {
                path: "campaigns/search",
                component: CampaignSearchComponent,
                data: { stateName: "Campaigns", permissions: PermissionsCode.CampaignsSearch },
                canActivate: [PermissionsGuard]
            },
            {
                path: "campaigns/create",
                component: CampaignCreateComponent,
                data: { stateName: "Campaigns", permissions: PermissionsCode.CampaignCreate },
                canActivate: [PermissionsGuard]
            },
            {
                path: "campaigns/manual-campaign-create",
                component: ManualCampaignCreateComponent,
                data: { stateName: "Campaigns", permissions: PermissionsCode.CampaignMassiveAssignation },
                canActivate: [PermissionsGuard]
            },
            {
                path: "campaigns/:id",
                component: CampaignDetailsComponent,
                children: [
                    {
                        path: "",
                        component: CampaignDetailsDataComponent,
                        data: { stateName: "Campaigns" },
                        canDeactivate: [CanDeactivateGuard]
                    }
                ]
            },
            {
                path: 'players',
                component: PlayersMainComponent,
                data: {
                    stateName: 'Players',
                    permissions: PermissionsCode.PlayerHome,
                    title: { text: 'pageTitle.players.main' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'players/search',
                component: PlayersSearchComponent,
                data: {
                    stateName: 'Players',
                    permissions: PermissionsCode.PlayerSearch,
                    title: { text: 'pageTitle.players.search' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'players/limits',
                component: AutoLimitChangesSearchComponent,
                data: {
                    stateName: 'Players',
                    permissions: PermissionsCode.PlayerAutoLimitsSearch,
                    title: { text: 'pageTitle.players.limits' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'players/blacklist',
                component: PlayerBlacklistSearchComponent,
                data: {
                    stateName: 'Players',
                    permissions: PermissionsCode.PlayerBlacklistSearch,
                    title: { text: 'pageTitle.players.blacklist' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'players/blacklist/add',
                component: PlayerBlacklistAddComponent,
                data: {
                    stateName: 'Players',
                    permissions: PermissionsCode.PlayerBlacklistAdd,
                    title: { text: 'pageTitle.players.blacklist' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'players/:accountCode',
                component: PlayerDetailComponent,
                data: { stateName: 'Players' },
                canDeactivate: [CanDeactivateGuard],
                children: [
                    {
                        path: 'detail',
                        component: PlayerPersonalDataComponent,
                        data: {
                            stateName: 'Players',
                            permissions: PermissionsCode.PlayerDetailData,
                            title: { text: 'pageTitle.players.detail.data', param: 'accountCode' }
                        },
                        canDeactivate: [CanDeactivateGuard],
                        canActivate: [PermissionsGuard]
                    },
                    {
                        path: "transactions",
                        component: PlayerTransactionsComponent,
                        data: {
                            stateName: "Players",
                            permissions: PermissionsCode.PlayerDetailTransactions,
                            title: { text: 'pageTitle.players.detail.transactions', param: 'accountCode' }
                        },
                        canActivate: [PermissionsGuard]
                    },
                    {
                        path: 'documentation',
                        component: PlayerDocumentationSearchComponent,
                        data: {
                            stateName: 'Players',
                            permissions: PermissionsCode.PlayerDetailDocumentation,
                            title: { text: 'pageTitle.players.detail.documentation', param: 'accountCode' }
                        },
                        canActivate: [PermissionsGuard]
                    },
                    {
                        path: 'responsible-gaming',
                        component: PlayerResponsibleGamingComponent,
                        data: {
                            stateName: 'Players',
                            permissions: PermissionsCode.PlayerResponsibleGaming,
                            title: { text: 'pageTitle.players.detail.responsibleGaming', param: 'accountCode' }
                        },
                        canActivate: [PermissionsGuard]
                    },
                    {
                        path: 'bonuses',
                        component: PlayerBonusesComponent,
                        data: {
                            stateName: 'Players',
                            permissions: PermissionsCode.PlayerBonuses,
                            title: { text: 'pageTitle.players.detail.playerBonuses', param: 'accountCode' }
                        },
                        canActivate: [PermissionsGuard]
                    }
                ]
            },
            {
                path: 'transactions',
                component: TransactionsMainComponent,
                data: {
                    stateName: 'Transactions',
                    permissions: PermissionsCode.TransactionHome,
                    title: { text: 'pageTitle.transactions.main' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'transactions/search',
                component: TransactionSearchComponent,
                data: {
                    stateName: 'Transactions',
                    permissions: PermissionsCode.TransactionSearch,
                    title: { text: 'pageTitle.transactions.search' }
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'transactions/withdrawals',
                component: WithdrawalSearchComponent,
                data: {
                    stateName: 'Transactions',
                    permissions: PermissionsCode.TransactionSearchPendingWithdrawals
                },
                canActivate: [PermissionsGuard]
            },
            {
                path: 'settings', component: SettingsMainComponent,
                data: {
                    stateName: 'Settings',
                    title: { text: 'pageTitle.settings.main' }
                }
            },
            {
                path: 'monitoring', component: MonitoringMainComponent,
                data: {
                    stateName: 'Monitoring',
                    title: {text: 'pageTitle.monitoring.main'}
                },
            },
            {
                path: 'monitoring/search', component: MonitoringSearchComponent,
                data: {
                    stateName: 'Monitoring',
                    permissions: PermissionsCode.Monitoring
                },
                canActivate: [PermissionsGuard]
            },
            { path: 'forbidden', component: AccessForbiddenComponent, data: { stateName: 'Forbidden' } },
            { path: '**', component: PageNotFoundComponent }
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
