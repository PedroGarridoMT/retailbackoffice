import { NO_ERRORS_SCHEMA } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { AppComponent } from "./app.component";
import { FakeTranslateLoader } from "../test-util/fake-translate-loader";
import { ConfigService } from "./core/config.service";
import { AuthenticationService } from "./account/shared/authentication.service";

describe("App", () => {

    let comp: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    let configServiceSpy: jasmine.SpyObj<ConfigService>;
    let authenticationServiceSpy: jasmine.SpyObj<AuthenticationService>;

    // async beforeEach
    beforeEach(async(() => {
        configServiceSpy = jasmine.createSpyObj<ConfigService>("ConfigService", ["getKey"]);
        authenticationServiceSpy = jasmine.createSpyObj<AuthenticationService>("AuthenticationService", ["startLogoutTimer", "checkForForceLogout"]);


        TestBed.configureTestingModule({
            declarations: [AppComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: AuthenticationService, useValue: authenticationServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeTranslateLoader }
                })
            ]
        })
            .compileComponents(); // compile template and css
    }));

    // synchronous beforeEach
    beforeEach(() => {
        // Setup spy before AppComponent constructor call
        configServiceSpy.getKey.and.returnValues([{ "value": "en" }, { "value": "fr" }]);

        fixture = TestBed.createComponent(AppComponent);
        comp = fixture.componentInstance;

        fixture.detectChanges(); // trigger initial data binding
    });

    it("should be initialized", () => {
        expect(fixture).toBeDefined();
        expect(comp).toBeDefined();
    });

    it("should start logout timer", () => {
        expect(authenticationServiceSpy.startLogoutTimer).toHaveBeenCalled();
    });

    it("should check for force logout on tab focus", () => {
        window.dispatchEvent(new Event("focus"));
        expect(authenticationServiceSpy.checkForForceLogout).toHaveBeenCalled();
    });

    it("should remove tab focus event listener on destroy", () => {
        comp.ngOnDestroy();
        window.dispatchEvent(new Event("focus"));
        expect(authenticationServiceSpy.checkForForceLogout).not.toHaveBeenCalled();
    });
});
