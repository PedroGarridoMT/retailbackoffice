﻿import { Injectable } from "@angular/core";
import { Http, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Observable";
import {
    ApiResponse,
    DefaultSearchRequest,
    ManualTransactionRequest,
    TransactionSearchResponse,
    WithdrawalTransactionDetails,
    WithdrawalTransactionSearchResponse,
    DownloadableFile,
    BaseResultsCode,
    PatchDocument,
} from "./model/models";
import { ConfigService } from "../core/config.service";
import { HelperService } from "../shared/helper.service";
import { sanitizeArrayParam, zeroPadding } from "../shared/utils";
import { ExportSepaRequest } from "./model/ExportSepaRequest";
import { MTQueryEncoder } from "../shared/query-encoder";

@Injectable()
export class TransactionService {

    constructor(private http: Http, private config: ConfigService) {
    }

    /**
     * Get an ordered paged history of Transactions
     * Gets an ordered paged history of player transactions according to the accountCode
     * @param request
     * @see requestParam: accountCode Account code.
     * @see requestParam: pageSize Page size (Pagination)
     * @see requestParam: pageNumber Page number / Current page number  (Pagination)
     * @see requestParam: sortBy Sort by property. Default value &#39;transactionDate&#39;. See &#39;#/definitions/TransactionSummary&#39; (Pagination)
     * @see requestParam: sortDirection Sort direction. &#39;ASC&#39; / &#39;DESC&#39; possible values. Default value &#39;DESC&#39;  (Pagination)
     * @see requestParam: username Username responsible for the transaction.
     * @see requestParam: transactionDateFrom Date and time of a transaction lower interval boundary. (UTC format)
     * @see requestParam: transactionDateTo Date and time of a transaction upper interval boundary. (UTC format)
     * @see requestParam: transactionType Type of transaction.
     * @see requestParam: gameName Name of game.
     * @see requestParam: amountFrom Transaction amount lower interval boundary.
     * @see requestParam: amountTo Transaction amount upper interval boundary.
     * @see requestParam: moneyType Indicates money type of the wallet - \&quot;Real\&quot; or \&quot;Bonus\&quot;, see &#39;#/definitions/&#39;#/definitions/MoneyType&#39;
     * @see requestParam: transactionId Transaction ID.
     * @see requestParam: providerTransactionId Provider transaction ID.
     * @see requestParam: couponId Coupon ID (TransferGroupUID)
     */
    public getPlayerTransactions(accountCode: string, request: DefaultSearchRequest): Observable<TransactionSearchResponse> {
        if (!request) {
            throw new Error('Required request object was null or undefined when calling getPlayerTransactions.');
        }
        if (!accountCode || !accountCode.length) {
            throw new Error('Required parameter accountCode was null or undefined when calling getPlayerTransactions.');
        }
        if (!request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling getPlayerTransactions.');
        }
        if (!request.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling getPlayerTransactions.');
        }

        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            let paramValue: string = request[key];
            if (key && paramValue !== undefined) {
                params.set(key, sanitizeArrayParam(paramValue));
            }
        }

        return this.http.get(this.config.URLs.transaction.playerTransactions.replace(':accountCode', accountCode), { params: params })
            .map((transactionSearchResponse: Response) => {
                return transactionSearchResponse.json();
            });
    }

    /**
     * Get a report with a list of transactions
     * Generates a CSV report with a list of player transactions that comply with the filter criteria.
     * @param accountCode Account code of the player.
     * @param filters Object with values entered in filters.
     * @see requestParam: transactionDateFrom Date and time of a transaction lower interval boundary. (UTC format)
     * @see requestParam: transactionDateTo Date and time of a transaction upper interval boundary. (UTC format)
     * @see requestParam: transactionId Transaction ID.
     * @see requestParam: amountFrom Transaction amount lower interval boundary.
     * @see requestParam: amountTo Transaction amount upper interval boundary.
     * @see requestParam: moneyType Indicates money type of the wallet - &quot;Real&quot; or &quot;Bonus&quot;, see &#39;#/definitions/MoneyType&#39;
     * @see requestParam: transactionType Type of transaction.
     * @see requestParam: paymentMethod Payment method.
     * @see requestParam: gameType Types of games.
     * @see requestParam: providerTransactionId Provider transaction ID.
     * @see requestParam: couponId Coupon ID (TransferGroupUID)
     * @param columnNames Array of visible column names.
     */
    public getPlayerTransactionsReport(accountCode: string, filters: any, columnNames: string[]): Observable<DownloadableFile> {
        let params: URLSearchParams = new URLSearchParams();
        for (let key in filters) {
            params.set(key, filters[key]);
        }
        params.set("columnNames", columnNames.toString());

        return this.http.get(this.config.URLs.transaction.playerTransactionsReport.replace(":accountCode", accountCode), { params: params })
            .map((response: Response) => response.json());
    }

    /**
     * Get an ordered paged list of transactions
     * Gets an ordered paged list of transactions that comply with the filter criteria, with the specified page number and page size
     * @param request
     * @see requestParam: pageSize Page size (Pagination)
     * @see requestParam: pageNumber Page number / Current page number  (Pagination)
     * @see requestParam: sortBy Sort by property. Default value &#39;transactionDate&#39;. See &#39;#/definitions/TransactionSummary&#39; (Pagination)
     * @see requestParam: sortDirection Sort direction. &#39;ASC&#39; / &#39;DESC&#39; possible values. Default value &#39;DESC&#39;  (Pagination)
     * @see requestParam: username Username responsible for the transaction.
     * @see requestParam: accountCode Transaction account code.
     * @see requestParam: transactionDateFrom Date and time of a transaction lower interval boundary. (UTC format)
     * @see requestParam: transactionDateTo Date and time of a transaction upper interval boundary. (UTC format)
     * @see requestParam: transactionType Type of transaction.
     * @see requestParam: paymentMethod Payment method.
     * @see requestParam: gameType Types of games.
     * @see requestParam: amountFrom Transaction amount lower interval boundary.
     * @see requestParam: amountTo Transaction amount upper interval boundary.
     * @see requestParam: moneyType Indicates money type of the wallet - \&quot;Real\&quot; or \&quot;Bonus\&quot;, see &#39;#/definitions/&#39;#/definitions/MoneyType&#39;
     * @see requestParam: transactionId Transaction ID.
     * @see requestParam: providerTransactionId Provider transaction ID.
     * @see requestParam: couponId Coupon ID (TransferGroupUID)
     */
    public getTransactions(request: DefaultSearchRequest): Observable<TransactionSearchResponse> {
        if (!request) {
            throw new Error('Required request object was null or undefined when calling getTransactions.');
        }
        if (!request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling getTransactions.');
        }
        if (!request.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling getTransactions.');
        }

        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            let paramValue: string = request[key];
            if (key && paramValue !== undefined) {
                params.set(key, sanitizeArrayParam(paramValue));
            }
        }

        return this.http.get(this.config.URLs.transaction.search, { params: params })
            .map((transactionSearchResponse: Response) => {
                return transactionSearchResponse.json();
            });
    }

    /**
     * Get a report with a list of transactions
     * Generates a CSV report with a list of transactions that comply with the filter criteria.
     * @param filters Object with values entered in filters.
     * @see requestParam: accountCode Account code of the player responsible for the transaction.
     * @see requestParam: username Username of the player responsible for the transaction.
     * @see requestParam: transactionDateFrom Date and time of a transaction lower interval boundary. (UTC format)
     * @see requestParam: transactionDateTo Date and time of a transaction upper interval boundary. (UTC format)
     * @see requestParam: transactionId Transaction ID.
     * @see requestParam: amountFrom Transaction amount lower interval boundary.
     * @see requestParam: amountTo Transaction amount upper interval boundary.
     * @see requestParam: moneyType Indicates money type of the wallet - &quot;Real&quot; or &quot;Bonus&quot;, see &#39;#/definitions/MoneyType&#39;
     * @see requestParam: transactionType Type of transaction.
     * @see requestParam: paymentMethod Payment method.
     * @see requestParam: gameType Types of games.
     * @see requestParam: providerTransactionId Provider transaction ID.
     * @see requestParam: couponId Coupon ID (TransferGroupUID)
     * @param columnNames Array of visible column names.
     */
    public getTransactionsReport(filters: any, columnNames: string[]): Observable<DownloadableFile> {
        let params: URLSearchParams = new URLSearchParams();
        for (let key in filters) {
            params.set(key, filters[key]);
        }
        params.set("columnNames", columnNames.toString());

        return this.http.get(this.config.URLs.transaction.transactionsReport, { params: params })
            .map((response: Response) => response.json());
    }

    /**
     * Get an ordered paged list of withdrawals
     * Gets an ordered paged list of withdrawals that comply with the filter criteria, with the specified page number and page size
     * @param request
     * @see requestParam: pageSize Page size (Pagination)
     * @see requestParam: pageNumber Page number / Current page number  (Pagination)
     * @see requestParam: sortBy Sort by property. Default value &#39;transactionDate&#39;. See &#39;#/definitions/TransactionSummary&#39; (Pagination)
     * @see requestParam: sortDirection Sort direction. &#39;ASC&#39; / &#39;DESC&#39; possible values. Default value &#39;DESC&#39;  (Pagination)
     * @see requestParam: username Username responsible for the withdrawal.
     * @see requestParam: creationDateFrom Date and time of a withdrawal lower interval boundary. (UTC format)
     * @see requestParam: creationDateTo Date and time of a withdrawal upper interval boundary. (UTC format)
     * @see requestParam: closedDateFrom Date and time of a withdrawal lower interval boundary. (UTC format)
     * @see requestParam: closedDateTo Date and time of a withdrawal upper interval boundary. (UTC format)
     * @see requestParam: status Status of a withdrawal.
     * @see requestParam: paymentMethod Payment method of a withdrawal.
     */
    public searchWithdrawals(request: DefaultSearchRequest): Observable<WithdrawalTransactionSearchResponse> {
        if (!request) {
            throw new Error('Required request object was null or undefined when calling searchWithdrawals.');
        }
        if (!request.pageSize) {
            throw new Error('Required parameter pageSize was null or undefined when calling searchWithdrawals.');
        }
        if (!request.pageNumber) {
            throw new Error('Required parameter pageNumber was null or undefined when calling searchWithdrawals.');
        }

        let params: URLSearchParams = new URLSearchParams();
        for (let key in request) {
            let paramValue: string = request[key];
            if (key && paramValue !== undefined) {
                params.set(key, sanitizeArrayParam(paramValue));
            }
        }

        return this.http.get(this.config.URLs.transaction.withdrawals, { params: params })
            .map((withdrawalSearchResponse: Response) => {
                return withdrawalSearchResponse.json();
            });
    }

    /**
     * Post a manual adjustment transaction for a player
     * Create a new manual transaction for a given player.
     * @param accountCode Account code of the player.
     * @param request The manual adjustment payload request.
     */
    public manualAdjustment(accountCode: string, request: ManualTransactionRequest): Observable<ApiResponse> {
        return this.http.post(this.config.URLs.transaction.playerTransactions.replace(':accountCode', accountCode), request)
            .map(response => {
                return HelperService.getApiResponse(response);
            })
            .catch(HelperService.handleError)
    }

    /**
     * Get the withdrawal details
     * @param transactionID Transaction ID.
     * @return  Observable<WithdrawalTransactionDetails>
     */
    public getWithdrawalDetails(transactionID: string): Observable<WithdrawalTransactionDetails> {
        if (!transactionID) {
            throw new Error('Required parameter transactionID was null or undefined when calling getWithdrawalDetails');
        }

        return this.http
            .get(this.config.URLs.transaction.withdrawalDetails.replace(":transactionID", transactionID), { params: new URLSearchParams() })
            .map((withdrawal: Response) => {
                return withdrawal.json();
            });
    }

    /**
     * Patches the witdrawal with approval for level 1, approval for level 2 or rejection.
     * @param transactionID Transaction ID
     * @param patchDocument Used to determine whether the withdrawal is approving or rejecting and which level.
     * @returns {Observable<BaseResultsCode>}
     */
    public patchWithdrawal(transactionID: string, patchDocuments: PatchDocument[]): Observable<BaseResultsCode> {
        if (!transactionID || !patchDocuments || !patchDocuments.length) {
            throw new Error('A required parameter was null or undefined when calling patchWithdrawal.');
        }
        return this.http
            .patch(this.config.URLs.transaction.withdrawalDetails.replace(':transactionID', transactionID), patchDocuments)
            .map(response => {
                return HelperService.getBaseResultCode(response.status);
            })
            .catch(HelperService.handleError);
    }

    /**
     * Export SEPA file for given dates.
     * @param dateFrom Begining of period for export.
     * @param dateTo End of period for export.
     * @returns {Observable<DownloadableFile>}
     */
    public exportSepa(exportSepaRequest: ExportSepaRequest): Observable<DownloadableFile> {
        const params: URLSearchParams = new URLSearchParams("", new MTQueryEncoder());
        params.set("transactionDateFrom", zeroPadding(exportSepaRequest.dateFrom.getFullYear()) + "-" +
            zeroPadding(exportSepaRequest.dateFrom.getMonth() + 1) + "-" + zeroPadding(exportSepaRequest.dateFrom.getDate()) + "T00:00:00Z");
        params.set("transactionDateTo", zeroPadding(exportSepaRequest.dateTo.getFullYear()) + "-" +
            zeroPadding(exportSepaRequest.dateTo.getMonth() + 1) + "-" + zeroPadding(exportSepaRequest.dateTo.getDate()) + "T23:59:59Z");

        return this.http.get(this.config.URLs.transaction.exportSepa, { params })
            .map((response: Response) => response.json());
        //TODO: add error handling when PAS-3550 bug is fixed
        //.catch(HelperService.handleError);
    }

}
