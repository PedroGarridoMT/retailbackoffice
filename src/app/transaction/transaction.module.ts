import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { AppRoutingModule } from "../app-routing.module";
import { TransactionService } from "./transaction.service";
import { TransactionsMainComponent } from "./transaction-main.component";
import { TransactionSearchComponent } from "./search/transaction-search.component";
import { WithdrawalSearchComponent } from "./withdrawals/withdrawal-search.component";
import { ApproveWithdrawalComponent } from "./withdrawals/approve/approve-withdrawal.component";
import { ApproveWithdrawalErrorComponent } from "./withdrawals/approve/approve-withdrawal-error.component";
import { RejectWithdrawalComponent } from "./withdrawals/reject/reject-withdrawal.component";
import { WithdrawalDetailsComponent } from "./withdrawals/details/withdrawal-details.component";
import { ExportSepaComponent } from "./withdrawals/export-sepa/export-sepa.component";

@NgModule({
    imports: [
        AppRoutingModule,
        SharedModule
    ],
    declarations: [
        TransactionsMainComponent,
        TransactionSearchComponent,
        WithdrawalSearchComponent,
        WithdrawalDetailsComponent,
        ApproveWithdrawalComponent,
        ApproveWithdrawalErrorComponent,
        RejectWithdrawalComponent,
        ExportSepaComponent,
    ],
    entryComponents: [
        WithdrawalDetailsComponent,
        ApproveWithdrawalComponent,
        ApproveWithdrawalErrorComponent,
        RejectWithdrawalComponent,
        ExportSepaComponent,
    ],
    exports: [
        TransactionsMainComponent,
        TransactionSearchComponent,
        WithdrawalSearchComponent
    ],
    providers: [
        TransactionService
    ],
})
export class TransactionModule {
}
