import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { RejectWithdrawalComponent } from "./reject-withdrawal.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FakeModalService } from "../../../../test-util/fake-modal.service";
import { WithdrawalTransactionDetails, WithdrawalStatusEnum, PaymentMethodEnum, WithdrawalAction } from "../../model/models";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { CurrencyService } from "../../../shared/currency.service";
import { ModalService } from "../../../shared/services/modal.service";

describe("RejectWithdrawalComponent", () => {

    let ngbActiveModalSpy: jasmine.SpyObj<NgbActiveModal>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;

    let comp: RejectWithdrawalComponent;
    let fixture: ComponentFixture<RejectWithdrawalComponent>;

    ngbActiveModalSpy = jasmine.createSpyObj<NgbActiveModal>("NgbActiveModal", ["close"]);
    currencyServiceSpy = jasmine.createSpyObj("CurrencyService", ["formatAmountWithCurrency"]);

    let fakeModalService: FakeModalService;

    const withdrawals: WithdrawalTransactionDetails[] = [{
        username: "username",
        transactionId: "1",
        creationDate: "2017-01-01",
        amount: 100,
        currency: "COP",
        status: WithdrawalStatusEnum.Success,
        ip: "1.1.1.1",
        closedDate: "2017-01-01",
        paymentMethod: PaymentMethodEnum.Paypal,
        accountCode: ""
    }];

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [RejectWithdrawalComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: NgbActiveModal, useValue: ngbActiveModalSpy },
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal },
                { provide: CurrencyService, useValue: currencyServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        fakeModalService = TestBed.get(NgbModal);

        fixture = TestBed.createComponent(RejectWithdrawalComponent);
        comp = fixture.componentInstance;

    });

    it("should fire reject event on reject click", async () => {
        comp.selectedRows = withdrawals;
        fixture.detectChanges();
        comp.confirmationClick.subscribe((event: WithdrawalAction) => {
            expect(event).toBe(WithdrawalAction.Reject);
        });
        comp.rejectWithdrawals();
    });

});
