import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { Observable } from "rxjs";
import {
    OperationTypeForPaymentMethodEnum,
    TitleBarParams,
    WithdrawalSearchTransactionSummary,
    ApiResponse,
    BaseResultsCode,
    PatchDocument,
    PermissionsCode,
    WaitingForApprovalEnum,
    WithdrawalTransactionDetails,
    WithdrawalAction,
    WithdrawalStatusEnum,
    DownloadableFile
} from "../model/models";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    FilterToValueMap,
    FilterType,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals,
    PermissionsService,
    TableActionButton
} from "mt-web-components";
import { BaseSearchComponent } from "../../shared/base-search.component";
import { AppRoutes } from "../../app-routes";
import { getDefaultDateTimeFormat, getISOString } from "../../shared/date-utils";
import { TransactionService } from "../transaction.service";
import { PASConfigService } from "../../shared/pas-config.service";
import { TitleBarService, PASEnumsService } from "../../shared";
import { CurrencyService } from "../../shared/currency.service";
import { LoaderService } from "../../shared/loader.service";
import { ResponseHelperService } from "../../shared/response-helper.service";
import { ApproveWithdrawalComponent } from "./approve/approve-withdrawal.component";
import { ApproveWithdrawalErrorComponent } from "./approve/approve-withdrawal-error.component";
import { RejectWithdrawalComponent } from "./reject/reject-withdrawal.component";
import { WithdrawalDetailsComponent } from "./details/withdrawal-details.component";
import { getMimeTypeByExtension, downloadFileByDataURI } from "../../shared/utils";
import { ExportSepaComponent } from "./export-sepa/export-sepa.component";
import { ExportSepaRequest } from '../model/ExportSepaRequest';

@Component({
    templateUrl: './withdrawal-search.component.html',
    styleUrls: ['./withdrawal-search.component.scss']
})
export class WithdrawalSearchComponent extends BaseSearchComponent implements OnInit, OnDestroy {
    //Title bar attributes
    titleBarParams: TitleBarParams = {
        previousPagePath: 'transactions',
        previousPageLabel: 'transactions.home.title',
        pageTitle: 'transactions.withdrawals.title',
        pageActions: null
    };

    //UI attributes
    firstSearchDone: boolean = false;
    errorInSearch: boolean = false;
    showWithdrawalDetailsLink: boolean = false;
    filtersLoaded: boolean = false;

    // mt-table configuration
    table_columns: TableColumn[];
    table_rows: WithdrawalSearchTransactionSummary[];
    tableLiterals: TableLiterals;
    tableButtons: TableActionButton[] = [];
    private selectedRows: WithdrawalSearchTransactionSummary[] = [];


    // mt-basic-filters configuration
    filterBarLiterals: BasicFilterBarLiterals;
    filters: any[];
    paymentMethodList: any[];
    withdrawalStatusesList: any[];
    selectedRowIds: string[] = [];
    rowSelectionMode: string = "multiple";
    rowSelectionKey: string = "transactionId";
    enableRowSelection: boolean = false;
    buttons = {
        sepa: {
            id: "sepa",
            visible: false,
            disabled: true
        },
        approve: {
            id: "approve",
            visible: false,
            disabled: true
        },
        reject: {
            id: "reject",
            visible: false,
            disabled: true
        },
        finish: {
            id: "finish",
            visible: false,
            disabled: true
        },
    };

    @ViewChild('filtersBar') filtersBar: BasicFilterBarComponent;
    @ViewChild('withdrawalDetailsLinkTemplate') withdrawalDetailsLinkTemplate;


    constructor(private transactionService: TransactionService,
                private pasConfigService: PASConfigService,
                private translate: TranslateService,
                private titleBarService: TitleBarService,
                private router: Router,
                private enumsService: PASEnumsService,
                private currencyService: CurrencyService,
                private loader: LoaderService,
                route: ActivatedRoute,
                private modalService: NgbModal,
                private responseHelper: ResponseHelperService,
                private permissionsService: PermissionsService) {

        super(route);
    }

    ngOnInit(): void {
        this.init();

        //Notify and pass params to the title bar service, so it can notify subscribers
        this.titleBarService.componentInitialized(this.titleBarParams);

        const actionsButtonsVisible = this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel1) ||
            this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel2);
        this.buttons.approve.visible = actionsButtonsVisible;
        this.buttons.reject.visible = actionsButtonsVisible;
        this.buttons.finish.visible = this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalFinalize);
        this.buttons.sepa.visible = this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalExportSepa);

        this.enableRowSelection = Object.keys(this.buttons).some(key => this.buttons[key].visible);
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.titleBarService.componentDestroyed();
    }

    onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.clearSelectedRows();
        this.searchData();
    }

    onClearBasicFilter() {
        this.valuesToSearch = null;
        this.clearSelectedRows();
        //Clean previous search results and selected rows
        this.table_rows = [];
        this.resetPagingParams();
        //Reset UI
        this.errorInSearch = false;
        this.firstSearchDone = false;
        //Clear URL
        this.router.navigate([AppRoutes.searchPendingWithdrawals]);
    }

    onTableActionButtonClick(action: TableActionButton) {
        switch (action.id) {
            case this.buttons.sepa.id:
                this.onExportSepaClick();
                break;
            case this.buttons.reject.id:
                this.onRejectClick();
                break;
            case this.buttons.approve.id:
                this.onApproveClick();
                break;
            case this.buttons.finish.id:
                this.onFinishClick();
                break;
        }
    }

    private init() {
        this.showWithdrawalDetailsLink = true;

        this.loader.show();
        return Observable.forkJoin(this.pasConfigService.getPaymentMethods(OperationTypeForPaymentMethodEnum.Query))
            .finally(() => this.loader.hide())
            .subscribe(res => {
                this.enumsService.getTranslatedList('enum.paymentMethod.', res[0]).subscribe(paymentMethodsTranslated => {
                    this.paymentMethodList = paymentMethodsTranslated;
                });

                this.enumsService.getWithdrawalStatusEnumTranslatedList().subscribe(withdrawalStatusesTranslated => {
                    this.withdrawalStatusesList = withdrawalStatusesTranslated;
                });

                this.generateTable();
                this.generateFilterForm();
                this.manageUrlParams();
                this.filtersLoaded = true;
            });
    }

    /**
     * If the URL contains a search, prepare the necessary objects to compose the search and then, launch it
     */
    private manageUrlParams() {
        if (this.thereAreQueryParams()) {
            this.prepareDataWithQueryParams();
            this.searchData();
        }
    }

    /**
     * Search Withdrawal Request
     */
    private searchData() {
        this.errorInSearch = false;
        this.loader.show();

        let filterCriteria = this.createWithdrawalFilterCriteria(this.valuesToSearch);

        //Perform search
        this.transactionService
            .searchWithdrawals(filterCriteria)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe(
                withdrawalResponse => {
                    this.updateWithdrawalInfo(withdrawalResponse.items);
                    this.pagingData = {
                        currentPage: this.pagingData.currentPage,
                        pageSize: this.pagingData.pageSize,
                        totalPages: withdrawalResponse.totalPages,
                        totalItems: withdrawalResponse.totalItems
                    };
                },
                error => {
                    this.errorInSearch = true;
                    this.resetPagingParams();
                    console.error(error);
                });

        //Update URL with the new search criteria
        this.router.navigate([AppRoutes.searchPendingWithdrawals], { queryParams: filterCriteria });
    }

    /**
     * Mainly used for build request formatting dates
     * @return {} withdrawalResponse
     * @param withdrawalSearchFilter
     */
    private createWithdrawalFilterCriteria(withdrawalSearchFilter: any): any {
        let withdrawalFilterCriteria: any = withdrawalSearchFilter;
        if (withdrawalSearchFilter.creationDate) {
            let creationDateFrom = withdrawalSearchFilter.creationDate.from;
            withdrawalFilterCriteria.creationDateFrom = creationDateFrom ? getISOString(creationDateFrom) : null;

            let creationDateTo = withdrawalSearchFilter.creationDate.to;
            withdrawalFilterCriteria.creationDateTo = creationDateTo ? getISOString(creationDateTo) : null;

            delete withdrawalFilterCriteria['creationDate'];
        }

        if (withdrawalSearchFilter.closedDate) {
            let closedDateFrom = withdrawalSearchFilter.closedDate.from;
            withdrawalFilterCriteria.closedDateFrom = closedDateFrom ? getISOString(closedDateFrom) : null;

            let closedDateTo = withdrawalSearchFilter.closedDate.to;
            withdrawalFilterCriteria.closedDateTo = closedDateTo ? getISOString(closedDateTo) : null;

            delete withdrawalFilterCriteria['closedDate'];
        }

        //Sorting is not available in backend.
        //return Object.assign({}, withdrawalFilterCriteria, this.getPaginationData(), this.getSortingData());

        return Object.assign({}, withdrawalFilterCriteria, this.getPaginationData());
    }

    /**
     * It formats dates, amounts, enums, etc.
     */
    private updateWithdrawalInfo(withdrawals: WithdrawalSearchTransactionSummary[]): void {
        withdrawals.map((withdrawal: any) => {
            withdrawal.withdrawalLink = this.withdrawalDetailsLinkTemplate;
            withdrawal.formattedCreationDate = withdrawal.creationDate ? getDefaultDateTimeFormat(withdrawal.creationDate) : null;
            withdrawal.formattedClosedDate = withdrawal.closedDate ? getDefaultDateTimeFormat(withdrawal.closedDate) : null;
            withdrawal.translatedPaymentMethod = withdrawal.paymentMethod ? this.enumsService.translateEnumValue('paymentMethod', withdrawal.paymentMethod) : '';
            withdrawal.formattedAmount = this.currencyService.formatAmount(withdrawal.amount);
            withdrawal.translatedStatus = withdrawal.status ? this.enumsService.translateEnumValue('withdrawalStatus', withdrawal.status) : '';
        });
        this.hideEmptyColumns(withdrawals);
    }

    /**
     * Generates filter form component: mt-basic-filters
     */
    private generateFilterForm() {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "status",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.withdrawals.entity.status"),
                shownByDefault: true,
                options: this.withdrawalStatusesList,
                value: this.getMultipleSelectionFromParams("status")
            },
            {
                id: "username",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.withdrawals.entity.username"),
                shownByDefault: true,
                value: this.getValueFromParams("username")
            },
            {
                id: "creationDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("transactions.withdrawals.entity.creationDate"),
                shownByDefault: true,
                showTimeRange: true,
                value: this.getDateRangeFromParams("creationDate")
            },
            {
                id: "closedDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("transactions.withdrawals.entity.closeDate"),
                shownByDefault: true,
                showTimeRange: true,
                value: this.getDateRangeFromParams("closedDate")
            },
            {
                id: "paymentMethod",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.withdrawals.entity.paymentMethod"),
                shownByDefault: true,
                options: this.paymentMethodList,
                value: this.getMultipleSelectionFromParams("paymentMethod")
            }
        ];
    }

    /**
     * Generates the table component with withdrawal columns: mt-table
     */
    private generateTable() {
        this.table_columns = [
            {
                type: TableColumnType.HTML,
                id: "withdrawalLink",
                label: this.translate.instant('transactions.withdrawals.entity.transactionId'),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "username",
                label: this.translate.instant("transactions.withdrawals.entity.username"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedCreationDate",
                label: this.translate.instant("transactions.withdrawals.entity.creationDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA_NUMERIC,
                id: "formattedAmount",
                label: this.translate.instant("transactions.withdrawals.entity.amount"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "currency",
                label: this.translate.instant("transactions.withdrawals.entity.currency"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "translatedStatus",
                label: this.translate.instant("transactions.withdrawals.entity.status"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "paymentAccountInfo",
                label: this.translate.instant("transactions.withdrawals.entity.additionalData.paymentAccountIdentifier"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "formattedClosedDate",
                label: this.translate.instant("transactions.withdrawals.entity.closeDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "translatedPaymentMethod",
                label: this.translate.instant("transactions.withdrawals.entity.paymentMethod"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }
        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('table.noResultsMessage'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        };

        let actions: TableActionButton[] = [];
        if (this.buttons.approve.visible) {
            actions.push({
                "id": this.buttons.approve.id,
                "icon": "fa fa-check-circle",
                "text": this.translate.instant("common.approve"),
                "disabled": this.buttons.approve.disabled
            });
        }
        if (this.buttons.reject.visible) {
            actions.push({
                "id": this.buttons.reject.id,
                "icon": "fa fa-times-circle",
                "text": this.translate.instant("common.reject"),
                "disabled": this.buttons.reject.disabled
            });
        }
        if (this.buttons.finish.visible) {
            actions.push({
                "id": this.buttons.finish.id,
                "icon": "fa fa-check",
                "text": this.translate.instant("common.finalize"),
                "disabled": this.buttons.finish.disabled,
            });
        }
        if (this.buttons.sepa.visible) {
            actions.push({
                "id": this.buttons.sepa.id,
                "icon": "fa fa-download",
                "text": this.translate.instant("transactions.withdrawals.exportSepa"),
                "disabled": false
            });
            if(actions.length > 1) {
                actions[actions.length-2].displaySeparatorAfterButton = true;
            }
        }
        this.tableButtons = actions;

    }

    private updateTableButtons() {
        let approve = this.tableButtons.find(item => item.id == this.buttons.approve.id);
        if (approve) {
            approve.disabled = this.buttons.approve.disabled;
        }
        let reject = this.tableButtons.find(item => item.id == this.buttons.reject.id);
        if (reject) {
            reject.disabled = this.buttons.reject.disabled;
        }
        let finalize = this.tableButtons.find(item => item.id == this.buttons.finish.id);
        if (finalize) {
            finalize.disabled = this.buttons.finish.disabled;
        }
    }

    /**
     * Checks if there are some columns with every null value and hides them
     * @param withdrawals
     */
    private hideEmptyColumns(withdrawals: any[]): void {
        const cols = this.table_columns.splice(0);
        cols.forEach((column) => {
            if (withdrawals.every((row) => row[column.id] == null || row[column.id] === '-')) {
                column.visibility = TableColumnVisibilityEnum.HIDDEN;
            }
            else {
                column.visibility = TableColumnVisibilityEnum.VISIBLE;
            }
        });
        this.table_columns = cols;
        this.table_rows = withdrawals;
        this.firstSearchDone = true;
    }

    public onSelectedRowsChange(selectedKeys: string[]): void {
        this.selectedRowIds = selectedKeys;
        this.selectedRows = this.selectedRows
        //remove unselected rows
            .filter((row) => selectedKeys.some((id) => id === row.transactionId))
            .concat(
                //add selected rows from the current page that do not already exist in selectedRows
                this.table_rows.filter((row) => selectedKeys.some((id) => id === row.transactionId) &&
                    !this.selectedRows.some((selectedRow) => selectedRow.transactionId === row.transactionId))
            );

        const allRowsLevel1Approvable: boolean = this.table_rows.filter((row) => selectedKeys.some((id) => id === row.transactionId))
            .every((row, index) => row.waitingForApproval === WaitingForApprovalEnum.Level1);

        const allRowsLevel2Approvable: boolean = this.table_rows.filter((row) => selectedKeys.some((id) => id === row.transactionId))
            .every((row, index) => row.waitingForApproval === WaitingForApprovalEnum.Level2);

        const allRowsFinalizable = this.table_rows.filter((row) => selectedKeys.some((id) => id === row.transactionId))
            .every((row, index) => row.status === WithdrawalStatusEnum.PendingSettlement);

        this.buttons.approve.disabled = !(selectedKeys.length > 0 && allRowsLevel1Approvable && this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel1)) &&
            !(selectedKeys.length > 0 && allRowsLevel2Approvable && this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel2));

        this.buttons.reject.disabled = !(this.selectedRows.length === 1 &&
            ((this.selectedRows[0].waitingForApproval === WaitingForApprovalEnum.Level1 && this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel1)) ||
                (this.selectedRows[0].waitingForApproval === WaitingForApprovalEnum.Level2 && this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel2))));

        this.buttons.finish.disabled = !((selectedKeys.length > 0 && allRowsFinalizable) &&
            this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalFinalize));


        this.updateTableButtons();
    }

    public onSelectAllChange(data: { checked: boolean, selectedKeys: string[] }): void {
        this.onSelectedRowsChange(data.selectedKeys);
    }

    public clearSelectedRows(): void {
        this.selectedRowIds.splice(0);
        this.selectedRows.splice(0);
        this.buttons.approve.disabled = true;
        this.buttons.reject.disabled = true;
        this.buttons.finish.disabled = true;
        this.updateTableButtons();
    }

    public openDetailsModal(transactionId: string): void {
        this.loader.show();
        this.transactionService.getWithdrawalDetails(transactionId)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe(
                (withdrawal: WithdrawalTransactionDetails) => {
                    const modalRef: NgbModalRef = this.modalService.open(WithdrawalDetailsComponent, {
                        backdrop: "static",
                        keyboard: false
                    });
                    modalRef.componentInstance.withdrawal = withdrawal;
                    modalRef.componentInstance.finishButtonEnabled = withdrawal.status === WithdrawalStatusEnum.PendingSettlement;
                    modalRef.componentInstance.finishButtonVisible = this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalFinalize) && withdrawal.status === WithdrawalStatusEnum.PendingSettlement;
                    modalRef.componentInstance.actionButtonsEnabled =
                        (this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel1) && withdrawal.waitingForApproval === WaitingForApprovalEnum.Level1) ||
                        (this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel2) && withdrawal.waitingForApproval === WaitingForApprovalEnum.Level2);
                    modalRef.componentInstance.actionButtonsVisible = (withdrawal.status === WithdrawalStatusEnum.Working || withdrawal.status === WithdrawalStatusEnum.Submitted) &&
                        (this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel1) || this.permissionsService.hasAccess(PermissionsCode.TransactionWithdrawalApprovalLevel2));

                    modalRef.componentInstance.confirmationClick.subscribe((action: WithdrawalAction) => {
                        this.loader.show();
                        const patchDocuments: PatchDocument[] = [{
                            op: PatchDocument.OpEnum.Add,
                            path: "/decision",
                            value: action
                        }];
                        this.transactionService.patchWithdrawal(transactionId, patchDocuments)
                            .finally(() => {
                                this.loader.hide();
                                modalRef.close();
                            }).subscribe(
                            (response) => {
                                if (action === WithdrawalAction.Approve) {
                                    this.responseHelper.responseHandler(response, "transactions.withdrawals.approve.response");
                                } else if (action === WithdrawalAction.Finish) {
                                    this.responseHelper.responseHandler(response, "transactions.withdrawals.finish.response");
                                } else {
                                    this.responseHelper.responseHandler(response, "transactions.withdrawals.reject.response");
                                }
                                this.clearSelectedRows();
                                this.searchData();
                            },
                            (error: ApiResponse) => {
                                if (action === WithdrawalAction.Approve) {
                                    this.responseHelper.responseHandler(error.statusCode, "transactions.withdrawals.approve.response");
                                } else if (action === WithdrawalAction.Finish) {
                                    this.responseHelper.responseHandler(error.statusCode, "transactions.withdrawals.finish.response");
                                } else {
                                    this.responseHelper.responseHandler(error.statusCode, "transactions.withdrawals.reject.response");
                                }
                            });
                    });
                },
                (error: ApiResponse) => {
                    this.responseHelper.responseHandler(error.statusCode, "transactions.withdrawals.response");
                });
    }

    public onApproveClick() {
        const modalRef: NgbModalRef = this.modalService.open(ApproveWithdrawalComponent, {
            backdrop: "static",
            keyboard: false
        });
        modalRef.componentInstance.selectedRows = this.selectedRows;
        modalRef.componentInstance.confirmationClick.subscribe((action: WithdrawalAction) => {
            if (this.selectedRows.length > 0) {
                this.loader.show();
                let observables: Observable<any>[] = [];
                let errorRows: WithdrawalSearchTransactionSummary[] = [];
                this.selectedRows.forEach((selectedRow) => {
                    observables.push(
                        this.transactionService.patchWithdrawal(selectedRow.transactionId, [{
                            op: PatchDocument.OpEnum.Add,
                            path: "/decision",
                            value: WithdrawalAction.Approve
                        }]).catch((error) => {
                            errorRows.push(selectedRow);
                            return Observable.of(error);
                        })
                    );
                });

                Observable.concat(...observables)
                    .finally(() => {
                        this.loader.hide();
                        modalRef.close();
                        if (errorRows.length === 0) {
                            this.responseHelper.responseHandler(BaseResultsCode.Ok, "transactions.withdrawals.approve.response");
                            this.clearSelectedRows();
                            this.searchData();
                        }
                        else {
                            const modalErrorRef: NgbModalRef = this.modalService.open(ApproveWithdrawalErrorComponent, {
                                backdrop: "static",
                                keyboard: false
                            });
                            modalErrorRef.componentInstance.errorRows = errorRows;
                        }
                    }).subscribe();
            }
        });
    }

    public onRejectClick() {
        const modalRef: NgbModalRef = this.modalService.open(RejectWithdrawalComponent, {
            backdrop: "static",
            keyboard: false
        });
        modalRef.componentInstance.selectedRows = this.selectedRows;
        modalRef.componentInstance.confirmationClick.subscribe((action: WithdrawalAction) => {
            this.loader.show();
            this.transactionService.patchWithdrawal(this.selectedRows[0].transactionId, [{
                op: PatchDocument.OpEnum.Add,
                path: "/decision",
                value: WithdrawalAction.Reject
            }]).finally(() => {
                this.loader.hide();
                modalRef.close();
            }).subscribe(
                (response) => {
                    this.responseHelper.responseHandler(response, "transactions.withdrawals.reject.response");
                    this.clearSelectedRows();
                    this.searchData();
                },
                (error: ApiResponse) => {
                    this.responseHelper.responseHandler(error.statusCode, "transactions.withdrawals.reject.response");
                });
        });
    }

    public onFinishClick() {
        if (this.selectedRows.length > 0) {
            this.loader.show();
            let errorRows: any[] = [];
            let requests: Observable<BaseResultsCode>[] = [];
            this.selectedRows.forEach((selectedRow) => {
                requests.push(
                    this.transactionService.patchWithdrawal(selectedRow.transactionId, [{
                        op: PatchDocument.OpEnum.Add,
                        path: "/decision",
                        value: WithdrawalAction.Finish
                    }])
                        .catch((error) => {
                            errorRows.push(selectedRow);
                            return Observable.of(error);
                        })
                );
            });
            Observable.concat(...requests)
                .finally(() => {
                    this.loader.hide();
                    if (errorRows.length === 0) {
                        this.responseHelper.responseHandler(BaseResultsCode.Ok, "transactions.withdrawals.finish.response");
                        this.clearSelectedRows();
                        this.searchData();
                    }
                    else {
                        if (errorRows[0] && errorRows[0].statusCode) {
                            this.responseHelper.responseHandler(errorRows[0].statusCode, "transactions.withdrawals.finish.response");
                        } else {
                            this.responseHelper.responseHandler(BaseResultsCode.InternalServerError, "transactions.withdrawals.finish.response");
                        }
                    }
                }).subscribe();
        }
    }

    public onExportSepaClick() {
        const modalRef: NgbModalRef = this.modalService.open(ExportSepaComponent, {
            backdrop: "static",
            keyboard: false
        });
        modalRef.componentInstance.confirmationClick.subscribe((exportSepaRequest: ExportSepaRequest) => {
            this.loader.show();
            this.transactionService.exportSepa(exportSepaRequest)
                .finally(() => {
                    this.loader.hide();
                })
                .subscribe((file: DownloadableFile) => {
                    let mimeType = getMimeTypeByExtension("xml");
                    downloadFileByDataURI("data:" + mimeType + ";base64," + file.content, file.fileName);

                    modalRef.close();
                    this.responseHelper.responseHandler(BaseResultsCode.Ok, "transactions.withdrawals.export-sepa.response");
                    this.clearSelectedRows();
                    this.searchData();
                }, (error: ApiResponse) => {
                    console.log("error", error);
                    //TODO: add real error handling when PAS-3550 bug is fixed
                    //this.responseHelper.responseHandler(error.statusCode, "transactions.withdrawals.export-sepa.response");
                    this.responseHelper.responseHandler(BaseResultsCode.NotFound, "transactions.withdrawals.export-sepa.response");
                });
        });
    }

}
