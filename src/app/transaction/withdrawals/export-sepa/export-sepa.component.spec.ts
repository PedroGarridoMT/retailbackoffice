import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { FakeModalService } from "../../../../test-util/fake-modal.service";
import { ExportSepaComponent } from "./export-sepa.component";
import { ExportSepaRequest } from "../../model/ExportSepaRequest";
import { ModalService } from "../../../shared/services/modal.service";

describe("ExportSepaComponent", () => {

    let ngbActiveModalSpy: jasmine.SpyObj<NgbActiveModal>;

    let comp: ExportSepaComponent;
    let fixture: ComponentFixture<ExportSepaComponent>;

    ngbActiveModalSpy = jasmine.createSpyObj<NgbActiveModal>("NgbActiveModal", ["close"]);

    let fakeModalService: FakeModalService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [ExportSepaComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: NgbActiveModal, useValue: ngbActiveModalSpy },
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        fakeModalService = TestBed.get(NgbModal);

        fixture = TestBed.createComponent(ExportSepaComponent);
        comp = fixture.componentInstance;
    });

    it("should fire export event on export click", async () => {
        const requestModel: ExportSepaRequest = {
            dateFrom: new Date("2018-02-01T00:00:00+01:00"),
            dateTo: new Date("2018-02-04T00:00:00+01:00"),
        };
        comp.model = requestModel;
        fixture.detectChanges();
        comp.confirmationClick.subscribe((event: ExportSepaRequest) => {
            expect(event).toBe(requestModel);
        });
        comp.exportButtonClick();
    });

});
