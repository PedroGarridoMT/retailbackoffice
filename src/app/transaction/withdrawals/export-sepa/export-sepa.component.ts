import { NgbActiveModal, NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Component, Output, EventEmitter } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ExportSepaRequest } from "../../model/ExportSepaRequest";

@Component({
    templateUrl: "./export-sepa.component.html",
    styleUrls: ["./export-sepa.component.scss"],
})
export class ExportSepaComponent {
    @Output() public confirmationClick = new EventEmitter<ExportSepaRequest>();
    public isDateFromOpened: boolean = false;
    public isDateToOpened: boolean = false;
    public model: ExportSepaRequest = {
        dateFrom: null,
        dateTo: null,
    };

    constructor(public activeModal: NgbActiveModal,
        private modalService: NgbModal) {
    }

    public exportButtonClick() {
        this.confirmationClick.next(this.model);
    }
}
