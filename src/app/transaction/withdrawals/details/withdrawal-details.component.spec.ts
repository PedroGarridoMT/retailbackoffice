import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { CurrencyService } from '../../../shared/currency.service';
import { PASEnumsService } from '../../../shared/pas-enums.service';
import { WithdrawalDetailsComponent } from './withdrawal-details.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FakeModalService } from '../../../../test-util/fake-modal.service';
import { WithdrawalTransactionDetails, WithdrawalStatusEnum, PaymentMethodEnum, WithdrawalAction } from '../../model/models';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FakeTranslateLoader } from '../../../../test-util/fake-translate-loader';

describe("WithdrawalDetailsComponent", () => {

    let ngbActiveModalSpy: jasmine.SpyObj<NgbActiveModal>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;
    let enumsServiceSpy: jasmine.SpyObj<PASEnumsService>;

    let comp: WithdrawalDetailsComponent;
    let fixture: ComponentFixture<WithdrawalDetailsComponent>;

    ngbActiveModalSpy = jasmine.createSpyObj<NgbActiveModal>("NgbActiveModal", ["close"]);
    currencyServiceSpy = jasmine.createSpyObj<CurrencyService>("CurrencyService", ["formatAmountWithCurrency"]);
    enumsServiceSpy = jasmine.createSpyObj<PASEnumsService>("PASEnumsService", ["translateEnumValue"]);

    let fakeModalService: FakeModalService;

    const withdrawalDetails: WithdrawalTransactionDetails = {
        username: "username",
        transactionId: "1",
        providerTransactionID: "1",
        creationDate: "2017-01-01",
        amount: 100,
        currency: "COP",
        status: WithdrawalStatusEnum.Success,
        ip: "1.1.1.1",
        bankAccount: "bank",
        paymentMethodAccountEmail: "mail@mail.mail",
        payerID: "1",
        closedDate: "2017-01-01",
        paymentMethod: PaymentMethodEnum.Paypal,
        accountCode: ""
    };

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [WithdrawalDetailsComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: NgbActiveModal, useValue: ngbActiveModalSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy },
                { provide: PASEnumsService, useValue: enumsServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        fixture = TestBed.createComponent(WithdrawalDetailsComponent);
        comp = fixture.componentInstance;

    });

    it("should initialize summary data", () => {
        comp.withdrawal = withdrawalDetails;
        fixture.detectChanges();
        expect(comp.summaryData).toBeTruthy;
    });

    it("should fire approve event on approve click", async () => {
        comp.confirmationClick.subscribe((event: WithdrawalAction) => {
            expect(event).toBe(WithdrawalAction.Approve);
        });
        comp.approveWithdrawals();
    });

    it("should fire reject event on reject click", async () => {
        comp.confirmationClick.subscribe((event: WithdrawalAction) => {
            expect(event).toBe(WithdrawalAction.Reject);
        });
        comp.rejectWithdrawals();
    });

    it("should fire finish event on finish click", async () => {
        comp.confirmationClick.subscribe((event: WithdrawalAction) => {
            expect(event).toBe(WithdrawalAction.Finish);
        });
        comp.finishWithdrawals();
    });

});
