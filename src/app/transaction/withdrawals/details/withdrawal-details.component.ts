import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { WithdrawalTransactionDetails, WithdrawalAction } from "../../model/models";
import { PASEnumsService } from "../../../shared/index";
import { CurrencyService } from "../../../shared/currency.service";
import { getLongDateTimeFormat } from "../../../shared/date-utils";

@Component({
    templateUrl: "./withdrawal-details.component.html",
    styleUrls: ["./withdrawal-details.component.scss"]
})
export class WithdrawalDetailsComponent implements OnInit {

    @Input() withdrawal: WithdrawalTransactionDetails;
    @Input() actionButtonsEnabled: boolean;
    @Input() actionButtonsVisible: boolean;
    @Input() finishButtonEnabled: boolean;
    @Input() finishButtonVisible: boolean;
    @Output() public confirmationClick = new EventEmitter<WithdrawalAction>();
    summaryData: any;

    constructor(public activeModal: NgbActiveModal,
        private translate: TranslateService,
        private currencyService: CurrencyService,
        private enumsService: PASEnumsService) {
    }

    ngOnInit(): void {
        this.summaryData = {
            title: "",
            rows: [
                {
                    key: this.translate.instant("transactions.withdrawals.entity.username"),
                    value: this.withdrawal.username,
                    targetRoute: "/players/" + this.withdrawal.accountCode + "/detail",
                    openInNewTab: true
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.transactionId"),
                    value: this.withdrawal.transactionId || "-"
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.creationDate"),
                    value: this.formatColumnDate(this.withdrawal.creationDate)
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.amount"),
                    value: this.currencyService.formatAmountWithCurrency(this.withdrawal.amount) || "-"
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.status"),
                    value: this.withdrawal.status ? this.enumsService.translateEnumValue("withdrawalStatus", this.withdrawal.status) : "-"
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.ip"),
                    value: this.withdrawal.ip || "-"
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.closeDate"),
                    value: this.formatColumnDate(this.withdrawal.closedDate)
                },
                {
                    key: this.translate.instant("transactions.withdrawals.entity.paymentMethod"),
                    value: this.withdrawal.paymentMethod ? this.enumsService.translateEnumValue("paymentMethod", this.withdrawal.paymentMethod) : "-"
                }
            ]
        }

        for (let key in this.withdrawal.additionalData) {
            if (this.withdrawal.additionalData.hasOwnProperty(key)) {
                this.summaryData.rows.push({
                    key: this.translate.instant("transactions.withdrawals.entity.additionalData." + key),
                    value: this.withdrawal.additionalData[key].value || "-"
                });
            }
        }
    }

    private formatColumnDate(date: string): string {
        return date !== null ? getLongDateTimeFormat(date) : "-";
    }

    public approveWithdrawals() {
        this.confirmationClick.next(WithdrawalAction.Approve);
    }

    public rejectWithdrawals() {
        this.confirmationClick.next(WithdrawalAction.Reject);
    }

    public finishWithdrawals() {
        this.confirmationClick.next(WithdrawalAction.Finish);
    }
}
