import { NgbActiveModal, NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TableLiterals, TableColumnType, TableColumnVisibilityEnum } from 'mt-web-components';
import { WithdrawalSearchTransactionSummary } from '../../model/models';
import { CurrencyService } from '../../../shared/currency.service';

@Component({
    templateUrl: "./approve-withdrawal-error.component.html",
    styleUrls: ["./approve-withdrawal-error.component.scss"]
})
export class ApproveWithdrawalErrorComponent implements OnInit {

    @Input() errorRows: WithdrawalSearchTransactionSummary[];
    public tableRows: any[] = [];
    public tableColumns = [
        {
            type: TableColumnType.DATA,
            id: "formattedAmount",
            label: this.translate.instant("transactions.withdrawals.entity.amount"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }, {
            type: TableColumnType.DATA,
            id: "paymentMethod",
            label: this.translate.instant("transactions.withdrawals.entity.paymentMethod"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }, {
            type: TableColumnType.DATA,
            id: "username",
            label: this.translate.instant("transactions.withdrawals.entity.username"),
            visibility: TableColumnVisibilityEnum.VISIBLE,
            sortable: false
        }
    ];
    public tableLiterals: TableLiterals = {
        noResultsMessage: this.translate.instant('table.noResultsMessage'),
        columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
        exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
        totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
        pageLabel: this.translate.instant('table.pageLabel'),
        actionsLabel: this.translate.instant('table.actionsLabel')
    };

    constructor(public activeModal: NgbActiveModal,
        private modalService: NgbModal,
        private translate: TranslateService,
        private currencyService: CurrencyService) {
    }

    ngOnInit(): void {
        this.tableRows = this.errorRows.map((row: any) => {
            row.formattedAmount = this.currencyService.formatAmountWithCurrency(row.amount);
            return row;
        });
    }
}
