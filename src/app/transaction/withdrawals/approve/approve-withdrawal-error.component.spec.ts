import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ApproveWithdrawalErrorComponent } from "./approve-withdrawal-error.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FakeModalService } from "../../../../test-util/fake-modal.service";
import { WithdrawalStatusEnum, PaymentMethodEnum, WithdrawalSearchTransactionSummary } from "../../model/models";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { FakeTranslateLoader } from "../../../../test-util/fake-translate-loader";
import { CurrencyService } from "../../../shared/currency.service";
import { ModalService } from "../../../shared/services/modal.service";

describe("ApproveWithdrawalErrorComponent", () => {

    let ngbActiveModalSpy: jasmine.SpyObj<NgbActiveModal>;
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService>;

    let comp: ApproveWithdrawalErrorComponent;
    let fixture: ComponentFixture<ApproveWithdrawalErrorComponent>;

    ngbActiveModalSpy = jasmine.createSpyObj<NgbActiveModal>("NgbActiveModal", ["close"]);
    currencyServiceSpy = jasmine.createSpyObj("CurrencyService", ["formatAmountWithCurrency"]);

    let fakeModalService: FakeModalService;

    const withdrawals: WithdrawalSearchTransactionSummary[] = [{
        username: "username",
        transactionId: "1",
        creationDate: "2017-01-01",
        amount: 100,
        currency: "COP",
        status: WithdrawalStatusEnum.Success,
        closedDate: "2017-01-01",
        paymentMethod: PaymentMethodEnum.Paypal
    }];

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [ApproveWithdrawalErrorComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                { provide: NgbActiveModal, useValue: ngbActiveModalSpy },
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal },
                { provide: CurrencyService, useValue: currencyServiceSpy }
            ],
            imports: [
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        fakeModalService = TestBed.get(NgbModal);

        fixture = TestBed.createComponent(ApproveWithdrawalErrorComponent);
        comp = fixture.componentInstance;
    });

    it("should initialize component", async () => {
        comp.errorRows = withdrawals;
        fixture.detectChanges();
        expect(comp).toBeTruthy();
    });

});
