import { WithdrawalSearchComponent } from "./withdrawal-search.component";
import { ComponentFixture } from "@angular/core/testing";
import { TransactionService } from "../transaction.service";
import { PASConfigService } from "../../shared/pas-config.service";
import { PASEnumsService } from "../../shared/pas-enums.service";
import { CurrencyService } from "../../shared/currency.service";
import { LoaderService } from "../../shared/loader.service";
import { PermissionsService } from "mt-web-components";
import { TestBed } from "@angular/core/testing";
import { DummyComponent } from "../../../test-util/dummy.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { TitleBarService } from "../../shared/title-bar.service";
import { RouterTestingModule } from "@angular/router/testing";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { FakeTranslateLoader } from "../../../test-util/fake-translate-loader";
import { Observable } from "rxjs/Observable";
import { async } from "@angular/core/testing";
import { inject } from "@angular/core/testing";
import { Router } from "@angular/router";
import { AppRoutes } from "../../app-routes";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FakeModalService } from "../../../test-util/fake-modal.service";
import { ResponseHelperService } from "../../shared/response-helper.service";
import { Subject } from "rxjs";
import { PatchDocument, BaseResultsCode, WithdrawalAction, WithdrawalTransactionSearchResponse, PermissionsCode, PaymentMethodEnum, WaitingForApprovalEnum, WithdrawalStatusEnum } from "../model/models";
import { ModalService } from "../../shared/services/modal.service";

describe("WithdrawalSearchComponent", () => {
    let comp: WithdrawalSearchComponent;
    let fixture: ComponentFixture<WithdrawalSearchComponent>;

    let transactionServiceSpy: jasmine.SpyObj<TransactionService> = jasmine.createSpyObj("TransactionService", ["searchWithdrawals", "getWithdrawalDetails", "patchWithdrawal"]);
    let pasConfigServiceSpy: jasmine.SpyObj<PASConfigService> = jasmine.createSpyObj("PASConfigService", ["getPaymentMethods"]);
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService> = jasmine.createSpyObj("CurrencyService", ["formatAmount"]);
    let loaderServiceSpy: jasmine.SpyObj<LoaderService> = jasmine.createSpyObj("LoaderService", ["show", "hide"]);
    let permissionsServiceSpy: jasmine.SpyObj<PermissionsService> = jasmine.createSpyObj("PermissionsService", ["hasAccess"]);
    let responseHelperServiceSpy: jasmine.SpyObj<ResponseHelperService> = jasmine.createSpyObj("ResponseHelperService", ["responseHandler"]);

    let fakeModalService: FakeModalService;

    const withdrawalResponse: WithdrawalTransactionSearchResponse = {
        totalPages: 1,
        totalItems: null,
        items: [{
            transactionId: "1111",
            username: "username",
            creationDate: "2017-10-01",
            amount: 10000,
            currency: "COP",
            closedDate: "2017-10-01",
            paymentMethod: PaymentMethodEnum.Paypal,
            status: WithdrawalStatusEnum.Submitted,
            waitingForApproval: WaitingForApprovalEnum.Level1
        },
        {
            transactionId: "2222",
            username: "username",
            creationDate: "2017-10-01",
            amount: 10000,
            currency: "COP",
            closedDate: "2017-10-01",
            paymentMethod: PaymentMethodEnum.Paypal,
            status: WithdrawalStatusEnum.Submitted,
            waitingForApproval: WaitingForApprovalEnum.Level1
        },
        {
            transactionId: "3333",
            username: "username",
            creationDate: "2017-10-01",
            amount: 10000,
            currency: "COP",
            closedDate: "2017-10-01",
            paymentMethod: PaymentMethodEnum.Paypal,
            status: WithdrawalStatusEnum.PendingSettlement,
            waitingForApproval: WaitingForApprovalEnum.Level1
        }]
    };

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [WithdrawalSearchComponent, DummyComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                TitleBarService,
                PASEnumsService,
                { provide: TransactionService, useValue: transactionServiceSpy },
                { provide: PASConfigService, useValue: pasConfigServiceSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy },
                { provide: LoaderService, useValue: loaderServiceSpy },
                { provide: PermissionsService, useValue: permissionsServiceSpy },
                { provide: NgbModal, useClass: FakeModalService },
                { provide: ModalService, useExisting: NgbModal },
                { provide: ResponseHelperService, useValue: responseHelperServiceSpy }
            ],
            imports: [
                RouterTestingModule.withRoutes([{ path: "transactions/withdrawals", component: DummyComponent }]),
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        fakeModalService = TestBed.get(NgbModal);

        pasConfigServiceSpy.getPaymentMethods.and.returnValue(Observable.of(["Cash", "BankTransfer", "QA", "Ukash", "Chargeback", "Check", "FakeForContestBets", "Paypal", "Skrill", "Trustly", "SilkRoad", "Paysafecard", "RFCash", "BravoCard", "FastPay", "Teleingreso", "SuertiaCash", "TerminalLobby"]));

        fixture = TestBed.createComponent(WithdrawalSearchComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should initialize table, filters and their literals", async(() => {
        expect(comp).toBeDefined;

        expect(comp.table_columns.length).toBeGreaterThan(0);

        expect(comp.tableLiterals.noResultsMessage.length).toBeGreaterThan(0);
        expect(comp.tableLiterals.columnsVisibilityLabel.length).toBeGreaterThan(0);

        expect(comp.filters.length).toBeGreaterThan(0);

        expect(comp.filterBarLiterals.search.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.clear.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.showAllFilters.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.showDefaultFilters.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.rangeFrom.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.rangeTo.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.emptySearchMessage.length).toBeGreaterThan(0);
    }));

    it("should fill the table with rows after search is performed", async(inject([Router], (router: Router) => {
        const routerNavigateSpy = spyOn(router, "navigate");
        const filterCriteria = {
            username: "username",
            creationDate: { creationDateFrom: "2010-12-31T23:00:00.000Z", creationDateTo: "2016-12-30T23:00:00.000Z" }, pageNumber: 1, pageSize: 5
        };
        transactionServiceSpy.searchWithdrawals.and.callFake(() => { return Observable.of(withdrawalResponse); });
        comp.onSearchBasicFilter(filterCriteria);
        setTimeout(() => {
            expect(comp.table_rows.length).toBeGreaterThan(0);
        }, 550);
    })));

    it("should create query string based on filters", inject([Router], (router: Router) => {
        const routerNavigateSpy = spyOn(router, "navigate");
        const filterCriteria = {
            username: "username",
            closedDate: { closedDateFrom: "2010-12-31T23:00:00.000Z", closedDateTo: "2016-12-30T23:00:00.000Z" }, pageNumber: 1, pageSize: 10
        };
        transactionServiceSpy.searchWithdrawals.and.callFake(() => { return Observable.of(withdrawalResponse); });
        comp.onSearchBasicFilter(filterCriteria);
        expect(comp.valuesToSearch).toEqual(filterCriteria);
        expect(routerNavigateSpy).toHaveBeenCalledWith([AppRoutes.searchPendingWithdrawals], { queryParams: filterCriteria });
    }));

    it("should show error on API error on search", async(() => {
        transactionServiceSpy.searchWithdrawals.and.callFake(() => { return Observable.throw("Fake Error"); });
        comp.onSearchBasicFilter({ username: "username", pageNumber: 1, pageSize: 5 });
        expect(comp.errorInSearch).toBe(true);
    }));

    it("should go to page 2", async(() => {
        transactionServiceSpy.searchWithdrawals.and.callFake(() => { return Observable.of(withdrawalResponse); });
        comp.onPageChanged(2);
        expect(comp.pagingData.currentPage).toBe(2);
    }));

    it("should change page size to 10", async(() => {
        transactionServiceSpy.searchWithdrawals.and.callFake(() => { return Observable.of(withdrawalResponse); });
        comp.onPageSizeChanged(10);
        expect(comp.pagingData.pageSize).toBe(10);
    }));

    it("should clear filter after Clear button click", async(() => {
        transactionServiceSpy.searchWithdrawals.and.callFake(() => { return Observable.of(withdrawalResponse); });
        comp.onClearBasicFilter();
        expect(comp.table_rows.length).toBe(0);
        expect(comp.valuesToSearch).toBeFalsy;
        expect(comp.errorInSearch).toBe(false);
        expect(comp.firstSearchDone).toBe(false);
        expect(comp.pagingData.currentPage).toBe(1);
        expect(comp.pagingData.pageSize).toBe(10);
        expect(comp.pagingData.totalPages).toBe(0);
        expect(comp.pagingData.totalItems).toBe(0);
    }));

    it("should open modal on transactionID click", async(() => {
        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        transactionServiceSpy.getWithdrawalDetails.and.callFake(() => Observable.of(withdrawalResponse.items[0]));
        comp.openDetailsModal("1111");
        expect(transactionServiceSpy.getWithdrawalDetails).toHaveBeenCalledWith("1111");
    }));

    it("should not open modal if there is error with selected transaction", async(() => {
        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        transactionServiceSpy.getWithdrawalDetails.and.callFake(() => Observable.throw({ statusCode: BaseResultsCode.BadRequest }));
        comp.openDetailsModal("1111");
        expect(responseHelperServiceSpy.responseHandler).toHaveBeenCalledWith(BaseResultsCode.BadRequest, "transactions.withdrawals.response");
    }));

    it("should approve withdrawal from details modal", async(() => {
        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        const fakePatchDocuments: PatchDocument[] = [{
            op: PatchDocument.OpEnum.Add,
            path: "/decision",
            value: WithdrawalAction.Approve
        }];
        transactionServiceSpy.getWithdrawalDetails.and.callFake(() =>Observable.of(withdrawalResponse.items[0]));
        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.of(null));
        comp.openDetailsModal("1111");
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Approve);
        expect(transactionServiceSpy.patchWithdrawal).toHaveBeenCalledWith("1111", fakePatchDocuments)
    }));

    it("should reject withdrawal from details modal", async(() => {
        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        const fakePatchDocuments: PatchDocument[] = [{
            op: PatchDocument.OpEnum.Add,
            path: "/decision",
            value: WithdrawalAction.Reject
        }];
        transactionServiceSpy.getWithdrawalDetails.and.callFake(() => Observable.of(withdrawalResponse.items[0]));
        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.throw({ statusCode: BaseResultsCode.BadRequest }));
        comp.openDetailsModal("1111");
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Reject);
        expect(transactionServiceSpy.patchWithdrawal).toHaveBeenCalledWith("1111", fakePatchDocuments)
    }));

    it("should finish withdrawal from details modal", async(() => {
        fakeModalService.componentInstance = {
            confirmationClick: new Subject()
        };
        const fakePatchDocuments: PatchDocument[] = [{
            op: PatchDocument.OpEnum.Add,
            path: "/decision",
            value: WithdrawalAction.Finish
        }];
        transactionServiceSpy.getWithdrawalDetails.and.callFake(() =>Observable.of(withdrawalResponse.items[0]));
        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.of(null));
        comp.openDetailsModal("1111");
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Finish);
        expect(transactionServiceSpy.patchWithdrawal).toHaveBeenCalledWith("1111", fakePatchDocuments)
    }));

    it("should approve multiple withdrawals", async(() => {
        //firstly, restart call counter
        transactionServiceSpy.patchWithdrawal.calls.reset();

        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.of(null));
        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            selectedRows: []
        };
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["1111", "2222"]);
        comp.onApproveClick();
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Approve);
        expect(transactionServiceSpy.patchWithdrawal.calls.count()).toBe(2);
    }));

    it("should finish multiple withdrawals", async(() => {
        //firstly, restart call counter
        transactionServiceSpy.patchWithdrawal.calls.reset();

        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.of(null));
        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            selectedRows: []
        };
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["1111", "2222"]);
        comp.onFinishClick();
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Finish);
        expect(transactionServiceSpy.patchWithdrawal.calls.count()).toBe(2);
    }));

    it("should show dialog with errors after approve fails", async(() => {
        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.throw({ statusCode: BaseResultsCode.BadRequest }));
        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            selectedRows: []
        };
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["1111"]);
        comp.onApproveClick();
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Approve);

        expect(fakeModalService.componentInstance.errorRows.length).toBe(1);
    }));

    it("should reject withdrawal from table", async(() => {
        transactionServiceSpy.patchWithdrawal.and.callFake(() => Observable.of(null));
        fakeModalService.componentInstance = {
            confirmationClick: new Subject(),
            selectedRows: []
        };
        const fakePatchDocuments: PatchDocument[] = [{
            op: PatchDocument.OpEnum.Add,
            path: "/decision",
            value: WithdrawalAction.Reject
        }];
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["1111"]);
        comp.onRejectClick();
        fakeModalService.componentInstance.confirmationClick.next(WithdrawalAction.Reject);
        expect(transactionServiceSpy.patchWithdrawal).toHaveBeenCalledWith("1111", fakePatchDocuments)
    }));

    it("should enable approve button if selected row has the same level permission as the user", async(() => {
        permissionsServiceSpy.hasAccess.and.callFake((permission: PermissionsCode) => {
            return permission == PermissionsCode.TransactionWithdrawalApprovalLevel1;
        });
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["1111"]);
        expect(comp.buttons.approve.disabled).toBe(false);
    }));

    it("should disable approve button if selected row doesn't have the same level permission as the user", async(() => {
        permissionsServiceSpy.hasAccess.and.callFake((permission: PermissionsCode) => {
            return permission == PermissionsCode.TransactionWithdrawalApprovalLevel2;
        });
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["1111"]);
        expect(comp.buttons.approve.disabled).toBe(true);
    }));

    it("should enable finalize button if selected withdrawal has pending status", async(() => {
        permissionsServiceSpy.hasAccess.and.callFake((permission: PermissionsCode) => true);
        comp.table_rows = withdrawalResponse.items;
        comp.onSelectedRowsChange(["3333"]);
        expect(comp.buttons.finish.disabled).toBe(false);
    }));
});
