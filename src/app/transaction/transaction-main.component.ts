import { Component, OnInit } from "@angular/core";
import {
    HomeTemplateAction,
    HomeTemplateDashboardItem,
    HomeTemplateSavedSearch,
    PermissionsService
} from "mt-web-components";
import { TranslateService } from "@ngx-translate/core";
import { PermissionsCode } from "../shared/model/permissions-code.model";
import { AppRoutes } from "../app-routes";

@Component({
    templateUrl: './transaction-main.component.html',
    styleUrls: ['./transaction-main.component.scss']

})
export class TransactionsMainComponent implements OnInit {
    actions: HomeTemplateAction[] = [];
    searches: HomeTemplateSavedSearch[];
    dashboardItems: HomeTemplateDashboardItem[];

    constructor(private translate: TranslateService, private permissionsService: PermissionsService) {
    }

    ngOnInit() {
        this.translate
            .get(['transactions.home.search', 'transactions.home.actionSearchDescription',
                'transactions.home.actionSearchLimits', 'transactions.home.actionSearchLimitsDescription',
                'transactions.home.actionSearchWithdrawals', 'transactions.home.actionSearchWithdrawalsDescription'])
            .subscribe((translations) => {
                if (this.permissionsService.hasAccess(PermissionsCode.TransactionSearch)) {
                    this.actions.push({
                        targetRoute: AppRoutes.transactionsSearch,
                        text: translations['transactions.home.search'],
                        description: translations['transactions.home.actionSearchDescription'],
                        cssSelector: "tx-home-search-transactions-selector"

                    })
                }

                if (this.permissionsService.hasAccess(PermissionsCode.TransactionSearchPendingWithdrawals)) {
                    this.actions.push({
                        targetRoute: AppRoutes.searchPendingWithdrawals,
                        text: translations['transactions.home.actionSearchWithdrawals'],
                        description: translations['transactions.home.actionSearchWithdrawalsDescription']
                    })
                }
            });
    }

    onSavedSearchClick(savedSearchId: string) {
        console.log('onSavedSearchClick ', savedSearchId);
    }
}
