import { TransactionSummary } from "./models";

/**
 * Internal Player entity (Non-swagger auto-generated)
 */
export interface TransactionSummaryExtended extends TransactionSummary {
    /**
     * AccountCode HTML Link column
     */
    accountCodeLink?: string;

    /**
     * Amount value as a formatted string
     */
    amountString?: string;
}
