import { NO_ERRORS_SCHEMA } from "@angular/core";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { async, ComponentFixture, inject, TestBed } from "@angular/core/testing";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TransactionSearchComponent } from "./transaction-search.component";
import { DummyComponent } from "../../../test-util/dummy.component";
import { FakeTranslateLoader } from "../../../test-util/fake-translate-loader";
import { TransactionService } from "../transaction.service";
import { PASConfigService } from "../../shared/pas-config.service";
import { TitleBarService } from "../../shared/title-bar.service";
import { PASEnumsService } from "../../shared/pas-enums.service";
import { CurrencyService } from "../../shared/currency.service";
import { LoaderService } from "../../shared/loader.service";
import { AppRoutes } from "../../app-routes";
import { PermissionsService } from "mt-web-components";
import { Observable } from "rxjs";
import { PlayerService } from "../../player/player.service";
import { ResponseHelperService } from "../../shared/response-helper.service";

describe('TransactionSearchComponent', () => {
    let comp: TransactionSearchComponent;
    let fixture: ComponentFixture<TransactionSearchComponent>;

    let transactionServiceSpy: jasmine.SpyObj<TransactionService> = jasmine.createSpyObj("TransactionService", ["getTransactions"]);
    let pasConfigServiceSpy: jasmine.SpyObj<PASConfigService> = jasmine.createSpyObj("PASConfigService", ["getGameTypes", "getPaymentMethods", "getTransactionTypes"]);
    let pasEnumsServiceSpy: jasmine.SpyObj<PASEnumsService> = jasmine.createSpyObj("PASEnumsService", ["getTranslatedList", "getMoneyTypeTranslatedList"]);
    let currencyServiceSpy: jasmine.SpyObj<CurrencyService> = jasmine.createSpyObj("CurrencyService", ["formatAmountWithCurrency"]);
    let loaderServiceSpy: jasmine.SpyObj<LoaderService> = jasmine.createSpyObj("LoaderService", ["show", "hide"]);
    let permissionsServiceSpy: jasmine.SpyObj<PermissionsService> = jasmine.createSpyObj("PermissionsService", ["hasAccess"]);
    let playerServiceSpy: jasmine.SpyObj<PlayerService> = jasmine.createSpyObj("PlayerService", ["setLastQueryParams"]);
    let responseHelperServiceSpy: jasmine.SpyObj<PlayerService> = jasmine.createSpyObj("ResponseHelperService", ["responseHandler"]);

    const mockTranslations = {
        "transactions.entity.accountCode": "transactions.entity.accountCode", "transactions.entity.username": "transactions.entity.username",
        "transactions.entity.transactionId": "transactions.entity.transactionId", "transactions.entity.transactionDate": "transactions.entity.transactionDate",
        "transactions.entity.amount": "transactions.entity.amount", "transactions.entity.amountFrom": "transactions.entity.amountFrom",
        "transactions.entity.amountTo": "transactions.entity.amountTo", "transactions.entity.moneyType": "transactions.entity.moneyType",
        "transactions.entity.transactionType": "transactions.entity.transactionType", "transactions.entity.paymentMethod": "transactions.entity.paymentMethod",
        "transactions.entity.gameType": "transactions.entity.gameType", "transactions.entity.providerTransactionId": "transactions.entity.providerTransactionId",
        "transactions.entity.couponId": "transactions.entity.couponId", "table.noResultsMessage": "table.noResultsMessage",
        "table.columnsVisibilityLabel": "table.columnsVisibilityLabel", "table.hiddenLabel": "table.hiddenLabel",
        "filtersBar.search": "filtersBar.search", "filtersBar.clear": "filtersBar.clear", "filtersBar.showAllFilters": "filtersBar.showAllFilters",
        "filtersBar.showDefaultFilters": "filtersBar.showDefaultFilters", "filtersBar.rangeFrom": "filtersBar.rangeFrom", "filtersBar.rangeTo": "filtersBar.rangeTo",
        "filtersBar.emptySearchMessage": "filtersBar.emptySearchMessage", "common.yes": "common.yes", "common.no": "common.no"
    };
    const transactionResponse = {
        totalPages: 1,
        totalItems: null,
        items: [{
            transactionId: "1",
            username: "username",
            accountCode: "1",
            transactionDate: "2017-07-12T09:47:26.0970000",
            transactionType: "Withdrawal",
            gameType: "Undefined",
            paymentMethod: "Paypal",
            gameName: "TODO",
            amount: -800.0000,
            moneyType: "Real",
            providerTransactionId: null,
            couponId: null
        }]
    };

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [TransactionSearchComponent, DummyComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [
                TitleBarService,
                PASEnumsService,
                { provide: TransactionService, useValue: transactionServiceSpy },
                { provide: PASConfigService, useValue: pasConfigServiceSpy },
                { provide: CurrencyService, useValue: currencyServiceSpy },
                { provide: LoaderService, useValue: loaderServiceSpy },
                { provide: PermissionsService, useValue: permissionsServiceSpy },
                { provide: PlayerService, useValue: playerServiceSpy },
                { provide: ResponseHelperService, useValue: responseHelperServiceSpy },
            ],
            imports: [
                RouterTestingModule.withRoutes([{ path: 'transactions/search', component: DummyComponent }]),
                TranslateModule.forRoot({ loader: { provide: TranslateLoader, useClass: FakeTranslateLoader } })
            ]
        });

        pasConfigServiceSpy.getGameTypes.and.returnValue(Observable.of(["Sportbook", "Slots", "SlotsTournament", "Casino", "FreeBet"]));
        pasConfigServiceSpy.getPaymentMethods.and.returnValue(Observable.of(["Cash", "BankTransfer", "QA", "Ukash", "Chargeback", "Check", "FakeForContestBets", "Paypal", "Skrill", "Trustly", "SilkRoad", "Paysafecard", "RFCash", "BravoCard", "FastPay", "Teleingreso", "SuertiaCash", "TerminalLobby"]));
        pasConfigServiceSpy.getTransactionTypes.and.returnValue(Observable.of(["Withdrawal", "Deposit", "WithdrawalRejection", "Bet", "Winning", "BetCancelation", "WinningCancelation", "WithdrawalCancelation", "FreeBetGrant", "FreeBetCancelation"]));

        playerServiceSpy.setLastQueryParams.and.returnValue(null);

        fixture = TestBed.createComponent(TransactionSearchComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should initialize table, filters and their literals', async(() => {
        expect(comp).toBeDefined;

        expect(comp.table_columns.length).toBeGreaterThan(0);

        expect(comp.tableLiterals.noResultsMessage.length).toBeGreaterThan(0);
        expect(comp.tableLiterals.columnsVisibilityLabel.length).toBeGreaterThan(0);

        expect(comp.filters.length).toBeGreaterThan(0);

        expect(comp.filterBarLiterals.search.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.clear.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.showAllFilters.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.showDefaultFilters.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.rangeFrom.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.rangeTo.length).toBeGreaterThan(0);
        expect(comp.filterBarLiterals.emptySearchMessage.length).toBeGreaterThan(0);
    }));

    it('should fill the table with rows after search is performed', async(inject([Router], (router: Router) => {
        const routerNavigateSpy = spyOn(router, "navigate");
        const filterCriteria = {
            moneyType: "Real", amount: { amountFrom: 0, amountTo: 1000 },
            transactionDate: { transactionDateFrom: "2010-12-31T23:00:00.000Z", transactionDateTo: "2016-12-30T23:00:00.000Z" }, pageNumber: 1, pageSize: 5
        };
        transactionServiceSpy.getTransactions.and.callFake(() => { return Observable.of(transactionResponse); });
        comp.onSearchBasicFilter(filterCriteria);
        setTimeout(() => {
            expect(comp.table_rows.length).toBeGreaterThan(0);
        }, 550);
    })));

    it('should create query string based on filters', inject([Router], (router: Router) => {
        const routerNavigateSpy = spyOn(router, "navigate");
        const filterCriteria = {
            moneyType: "Real", amount: { amountFrom: 0, amountTo: 1000 },
            transactionDate: { transactionDateFrom: "2010-12-31T23:00:00.000Z", transactionDateTo: "2016-12-30T23:00:00.000Z" }, pageNumber: 1, pageSize: 10
        };
        transactionServiceSpy.getTransactions.and.callFake(() => { return Observable.of(transactionResponse); });
        comp.onSearchBasicFilter(filterCriteria);
        expect(comp.valuesToSearch).toEqual(filterCriteria);
        expect(routerNavigateSpy).toHaveBeenCalledWith([AppRoutes.transactionsSearch], { queryParams: filterCriteria });
    }));

    it('should show error on API error on search', async(() => {
        transactionServiceSpy.getTransactions.and.callFake(() => { return Observable.throw("ERROR"); });
        comp.onSearchBasicFilter({ moneyType: "Real", pageNumber: 1, pageSize: 5 });
        expect(comp.errorInSearch).toBe(true);
    }));

    it('should go to page 2', async(() => {
        transactionServiceSpy.getTransactions.and.callFake(() => { return Observable.of(transactionResponse); });
        comp.onPageChanged(2);
        expect(comp.pagingData.currentPage).toBe(2);
    }));

    it('should change page size to 10', async(() => {
        transactionServiceSpy.getTransactions.and.callFake(() => { return Observable.of(transactionResponse); });
        comp.onPageSizeChanged(10);
        expect(comp.pagingData.pageSize).toBe(10);
    }));

    it('should clear filter after Clear button click', async(() => {
        transactionServiceSpy.getTransactions.and.callFake(() => { return Observable.of(transactionResponse); });
        comp.onClearBasicFilter();
        expect(comp.table_rows.length).toBe(0);
        expect(comp.valuesToSearch).toBeFalsy;
        expect(comp.errorInSearch).toBe(false);
        expect(comp.firstSearchDone).toBe(false);
        expect(comp.pagingData.currentPage).toBe(1);
        expect(comp.pagingData.pageSize).toBe(10);
        expect(comp.pagingData.totalPages).toBe(0);
        expect(comp.pagingData.totalItems).toBe(0);
    }));
});
