import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { TranslateService } from "@ngx-translate/core";
import {
    BasicFilterBarComponent,
    BasicFilterBarLiterals,
    FilterToValueMap,
    FilterType,
    PermissionsService,
    TableColumn,
    TableColumnType,
    TableColumnVisibilityEnum,
    TableLiterals
} from "mt-web-components";
import { TitleBarParams, TitleBarService } from "../../shared/index";
import { getDefaultDateTimeFormat, getISOString } from "../../shared/date-utils";
import { downloadFileByDataURI, getMimeTypeByExtension } from "../../shared/utils";
import { PASEnumsService } from "../../shared/pas-enums.service";
import { CurrencyService } from "../../shared/currency.service";
import { LoaderService } from "../../shared/loader.service";
import { TransactionService } from "../transaction.service";
import { PASConfigService } from "../../shared/pas-config.service";
import {
    OperationTypeForPaymentMethodEnum,
    TransactionSummary,
    TransactionSummaryExtended,
    DownloadableFile,
    DefaultSearchRequest
} from "../model/models";
import { PermissionsCode } from "../../shared/model/permissions-code.model";
import { AppRoutes } from "../../app-routes";
import { BaseSearchComponent } from "../../shared/base-search.component";
import { PlayerService } from "../../player/player.service";
import { ResponseHelperService } from "../../shared/response-helper.service";

@Component({
    selector: 'pas-transaction-search',
    templateUrl: './transaction-search.component.html',
    styleUrls: ['./transaction-search.component.scss']
})
export class TransactionSearchComponent extends BaseSearchComponent implements OnInit, OnDestroy {

    public static PAGE_TITLE: string = "transactions.home.search";

    //Title bar attributes
    titleBarParams: TitleBarParams = {
        previousPagePath: 'transactions',
        previousPageLabel: 'transactions.home.title',
        pageTitle: TransactionSearchComponent.PAGE_TITLE,
        pageActions: null
    };

    playerDetailLinkParams: Object = { [PlayerService.PREVIOUS_ROUTE_KEY]: TransactionSearchComponent.PAGE_TITLE };

    //UI attributes
    firstSearchDone: boolean = false;
    errorInSearch: boolean = false;
    showPlayerDetailLink: boolean = false;
    filtersLoaded: boolean = false;

    // mt-table configuration
    table_columns: TableColumn[];
    table_rows: TransactionSummary[];
    tableLiterals: TableLiterals;
    tableMaxColumnWidth = 200;
    @ViewChild('playerDetailLinkTemplate') playerDetailLinkTemplate;

    // mt-basic-filters configuration
    filterBarLiterals: BasicFilterBarLiterals;
    filters: any[];
    transactionTypeList: any[];
    paymentMethodList: any[];
    gameTypeList: any[];
    moneyTypeList: any[];
    @ViewChild('filtersBar') filtersBar: BasicFilterBarComponent;

    constructor(private transactionService: TransactionService,
        private pasConfigService: PASConfigService,
        private translate: TranslateService,
        private titleBarService: TitleBarService,
        private router: Router,
        private enumsService: PASEnumsService,
        private currencyService: CurrencyService,
        private loader: LoaderService,
        private permissions: PermissionsService,
        private playerService: PlayerService,
        route: ActivatedRoute,
        private responseHelper: ResponseHelperService, ) {

        super(route);
    }

    ngOnInit(): void {
        this.init();

        //Notify and pass params to the title bar service, so it can notify subscribers
        this.titleBarService.componentInitialized(this.titleBarParams);
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.titleBarService.componentDestroyed();
    }

    onPageChanged(pageSelected: number) {
        this.pagingData.currentPage = pageSelected;
        this.searchData();
    }

    onPageSizeChanged(pageSize: number) {
        this.pagingData.pageSize = pageSize;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    onSearchBasicFilter(searchValues: FilterToValueMap) {
        this.valuesToSearch = searchValues;
        this.pagingData.currentPage = 1;
        this.searchData();
    }

    onClearBasicFilter() {
        this.valuesToSearch = null;
        //Clean previous search results
        this.table_rows = [];
        this.resetPagingParams();
        //Reset UI
        this.errorInSearch = false;
        this.firstSearchDone = false;
        //Clear URL
        this.router.navigate([AppRoutes.transactionsSearch]);
    }

    onExportClick(columnNames: string[]) {
        const columns: string[] = columnNames.map(c => c.replace("String", "").replace("Link", ""));
        this.loader.show();
        const filterCriteria = this.createTransactionFilterCriteria(this.valuesToSearch, false);

        this.transactionService.getTransactionsReport(filterCriteria, columns)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe((file: DownloadableFile) => {
                downloadFileByDataURI("data:" + getMimeTypeByExtension("csv") + ";base64," + file.content, file.fileName);
            }, (error) => {
                this.responseHelper.responseHandler(error.statusCode, "common.search.error.exportErrorMessage");
            });
    }

    private init() {
        this.showPlayerDetailLink = this.permissions.hasAccess(PermissionsCode.PlayerDetailData);
        this.loader.show();
        return Observable
            .forkJoin(
                this.pasConfigService.getGameTypes(),
                this.pasConfigService.getPaymentMethods(OperationTypeForPaymentMethodEnum.Query),
                this.pasConfigService.getTransactionTypes())
            .finally(() => this.loader.hide())
            .subscribe(res => {
                this.enumsService.getTranslatedList('enum.gameType.', res[0]).subscribe(gameTypesTranslated => {
                    this.gameTypeList = gameTypesTranslated;
                });

                this.enumsService.getTranslatedList('enum.paymentMethod.', res[1]).subscribe(paymentMethodsTranslated => {
                    this.paymentMethodList = paymentMethodsTranslated;
                });

                this.enumsService.getTranslatedList('enum.transactionType.', res[2]).subscribe(transactionTypeListTranslated => {
                    this.transactionTypeList = transactionTypeListTranslated;
                });

                this.enumsService.getMoneyTypeTranslatedList().subscribe(moneyTypes => {
                    this.moneyTypeList = moneyTypes;
                });

                this.generateTable();
                this.generateFilterForm();
                this.manageUrlParams();
                this.filtersLoaded = true;
            });
    }

    /**
     * If the URL contains a search, prepare the necessary objects to compose the search and then, launch it
     */
    private manageUrlParams() {
        if (this.thereAreQueryParams()) {
            this.prepareDataWithQueryParams();
            this.searchData();
        }
    }

    /**
     * Search Transactions Request
     */
    private searchData() {
        this.errorInSearch = false;
        this.loader.show();

        let filterCriteria = this.createTransactionFilterCriteria(this.valuesToSearch, true);

        //Perform search
        this.transactionService
            .getTransactions(filterCriteria)
            .finally(() => {
                this.loader.hide();
            })
            .subscribe((transactionResponse) => {
                this.updateTransactionInfo(transactionResponse.items);
                this.pagingData = {
                    currentPage: this.pagingData.currentPage,
                    pageSize: this.pagingData.pageSize,
                    totalPages: transactionResponse.totalPages,
                    totalItems: transactionResponse.totalItems
                };
            }, (error) => {
                this.errorInSearch = true;
                this.resetPagingParams();
                console.error(error);
            });

        //Store last search
        this.playerService.setLastQueryParams(TransactionSearchComponent.PAGE_TITLE, filterCriteria);

        //Update URL with the new search criteria
        this.router.navigate([AppRoutes.transactionsSearch], { queryParams: filterCriteria });
    }

    /**
     * Mainly used for build request formatting dates
     * @return {} transactionResponse
     * @param transactionSearchFilter
     */
    private createTransactionFilterCriteria(transactionSearchFilter: any, addPagingAndSortingData: boolean): DefaultSearchRequest {
        let transactionFilterCriteria: any = transactionSearchFilter;
        if (transactionSearchFilter.transactionDate) {
            let transactionDateFrom = transactionSearchFilter.transactionDate.from;
            transactionFilterCriteria.transactionDateFrom = transactionDateFrom ? getISOString(transactionDateFrom) : null;

            let transactionDateTo = transactionSearchFilter.transactionDate.to;
            transactionFilterCriteria.transactionDateTo = transactionDateTo ? getISOString(transactionDateTo) : null;

            delete transactionFilterCriteria['transactionDate'];
        }

        if (transactionSearchFilter.amount) {
            transactionFilterCriteria.amountFrom = transactionSearchFilter.amount.from;
            transactionFilterCriteria.amountTo = transactionSearchFilter.amount.to;

            delete transactionFilterCriteria['amount'];
        }

        if (addPagingAndSortingData) {
            // Sorting is not available in backend.
            // return Object.assign({}, transactionFilterCriteria, this.getPaginationData(), this.getSortingData());
            return Object.assign({}, transactionFilterCriteria, this.getPaginationData());
        } else {
            return transactionFilterCriteria;
        }
    }

    /**
     * It formats dates, amounts, enums, etc.
     */
    private updateTransactionInfo(transactions: any[]): void {
        transactions.map((transaction: TransactionSummaryExtended) => {
            transaction.accountCodeLink = this.playerDetailLinkTemplate;
            transaction.transactionDate = transaction.transactionDate ? getDefaultDateTimeFormat(transaction.transactionDate) : null;
            transaction.transactionType = transaction.transactionType ? this.enumsService.translateEnumValue('transactionType', transaction.transactionType) : '';
            transaction.gameType = transaction.gameType ? this.enumsService.translateEnumValue('gameType', transaction.gameType) : '';
            transaction.paymentMethod = transaction.paymentMethod ? this.enumsService.translateEnumValue('paymentMethod', transaction.paymentMethod) : '';
            transaction.moneyType = transaction.moneyType ? this.enumsService.translateEnumValue('moneyType', transaction.moneyType) : '';
            transaction.amountString = this.currencyService.formatAmountWithCurrency(transaction.amount);
        });
        this.hideEmptyColumns(transactions);
    }

    /**
     * Generates filter form component: mt-basic-filters
     */
    private generateFilterForm() {
        this.filterBarLiterals = {
            search: this.translate.instant("filtersBar.search"),
            clear: this.translate.instant("filtersBar.clear"),
            showAllFilters: this.translate.instant("filtersBar.showAllFilters"),
            showDefaultFilters: this.translate.instant("filtersBar.showDefaultFilters"),
            rangeFrom: this.translate.instant("filtersBar.rangeFrom"),
            rangeTo: this.translate.instant("filtersBar.rangeTo"),
            invalidDate: this.translate.instant("forms.error.invalidDate"),
            invalidRange: this.translate.instant("forms.error.invalidRange"),
            dayCode: this.translate.instant("common.date.dayCode"),
            yearCode: this.translate.instant("common.date.yearCode"),
            emptySearchMessage: this.translate.instant("filtersBar.emptySearchMessage"),
        };

        this.filters = [
            {
                id: "accountCode",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.accountCode"),
                shownByDefault: true,
                value: this.getValueFromParams("accountCode")
            },
            {
                id: "username",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.username"),
                shownByDefault: true,
                value: this.getValueFromParams("username")
            },
            {
                id: "transactionDate",
                type: FilterType.DATE_RANGE,
                label: this.translate.instant("transactions.entity.transactionDate"),
                shownByDefault: true,
                showTimeRange: true,
                value: this.getDateRangeFromParams("transactionDate"),
                maxDate: new Date()
            },
            {
                id: "transactionId",
                type: FilterType.NUMBER,
                label: this.translate.instant("transactions.entity.transactionId"),
                shownByDefault: true,
                value: this.getValueFromParams("transactionId")
            },
            {
                id: "amount",
                type: FilterType.NUMBER_RANGE,
                label: this.translate.instant("transactions.entity.amount"),
                shownByDefault: true,
                value: this.getAmountRangeFromParams("amount")
            },
            {
                id: "moneyType",
                type: FilterType.SINGLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.moneyType"),
                shownByDefault: true,
                options: this.moneyTypeList,
                value: this.getValueFromParams("moneyType")
            },
            {
                id: "transactionType",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.transactionType"),
                shownByDefault: true,
                options: this.transactionTypeList,
                value: this.getMultipleSelectionFromParams("transactionType")
            },
            {
                id: "paymentMethod",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.paymentMethod"),
                shownByDefault: true,
                options: this.paymentMethodList,
                value: this.getMultipleSelectionFromParams("paymentMethod")
            },
            {
                id: "gameType",
                type: FilterType.MULTIPLE_SELECTION_LIST,
                label: this.translate.instant("transactions.entity.gameType"),
                shownByDefault: true,
                options: this.gameTypeList,
                value: this.getMultipleSelectionFromParams("gameType")
            },
            {
                id: "providerTransactionId",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.providerTransactionId"),
                shownByDefault: true,
                value: this.getValueFromParams("providerTransactionId")
            },
            {
                id: "couponId",
                type: FilterType.TEXT,
                label: this.translate.instant("transactions.entity.couponId"),
                shownByDefault: true,
                value: this.getValueFromParams("couponId")
            }
        ];
    }

    /**
     * Generates the table component with transactions columns: mt-table
     */
    private generateTable() {
        this.table_columns = [
            {
                type: TableColumnType.HTML,
                id: "accountCodeLink",
                label: this.translate.instant("transactions.entity.accountCode"),
                visibility: TableColumnVisibilityEnum.ALWAYS_VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "username",
                label: this.translate.instant("transactions.entity.username"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "transactionId",
                label: this.translate.instant("transactions.entity.transactionId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "transactionDate",
                label: this.translate.instant("transactions.entity.transactionDate"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "amountString",
                label: this.translate.instant("transactions.entity.amount"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "moneyType",
                label: this.translate.instant("transactions.entity.moneyType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "transactionType",
                label: this.translate.instant("transactions.entity.transactionType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "paymentMethod",
                label: this.translate.instant("transactions.entity.paymentMethod"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "gameType",
                label: this.translate.instant("transactions.entity.gameType"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "providerTransactionId",
                label: this.translate.instant("transactions.entity.providerTransactionId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            },
            {
                type: TableColumnType.DATA,
                id: "couponId",
                label: this.translate.instant("transactions.entity.couponId"),
                visibility: TableColumnVisibilityEnum.VISIBLE,
                sortable: false
            }

        ];

        this.tableLiterals = {
            noResultsMessage: this.translate.instant('table.noResultsMessage'),
            columnsVisibilityLabel: this.translate.instant('table.columnsVisibilityLabel'),
            exportButtonLabel: this.translate.instant('table.exportButtonLabel'),
            totalItemsLabel: this.translate.instant('table.totalItemsLabel'),
            pageLabel: this.translate.instant('table.pageLabel'),
            actionsLabel: this.translate.instant('table.actionsLabel')
        };
    }

    /**
     * Checks if there are some columns with every null value and hides them
     * @param transactions
     */
    private hideEmptyColumns(transactions: TransactionSummary[]): void {
        const cols = this.table_columns.splice(0);
        cols.forEach((column) => {
            if (transactions.every((row) => row[column.id] == null || row[column.id] == '-')) {
                column.visibility = TableColumnVisibilityEnum.HIDDEN;
            }
            else {
                column.visibility = TableColumnVisibilityEnum.VISIBLE;
            }
        });
        this.table_columns = cols;
        this.table_rows = transactions;
        this.firstSearchDone = true;
    }
}
