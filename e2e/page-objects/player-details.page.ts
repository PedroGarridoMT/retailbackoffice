import { $, $$, browser, ElementFinder } from "protractor";

/**
 * Page object for Player Details.
 */
export class PlayerDetailsPage {
    //Self exclusion
    public static selfExclusionBox: ElementFinder = $(".self-exclusion-data-selector");
    public static selfExclusionDateFrom: ElementFinder = $(".self-exclusion-date-from-selector");
    public static selfExclusionDateTo: ElementFinder = $(".self-exclusion-date-to-selector");
    public static modalSelfExclusion: ElementFinder = $(".modal-self-exclusion-body-selector");
    public static modalSelfExclusionConfirmButton: ElementFinder = $(".modal-self-exclusion-footer-selector .confirm-selector");
    public static modalSelfExclusionCanceltButton: ElementFinder = $(".modal-self-exclusion-footer-selector .cancel-selector");
    // Actions
    public static actionsButton: ElementFinder = $(".actions-button-selector");
    public static editPlayerButton: ElementFinder = $(".edit-selector");
    public static changeStatusButton: ElementFinder = $(".change-status-selector");
    public static manualAdjustmentButton: ElementFinder = $(".manual-adjustment-selector");
    public static revokeSelfExclusionButton: ElementFinder = $(".revoke-selfexclusion-selector");
    public static approveDocButton: ElementFinder = $(".approve-doc-selector");
    public static rejectDocButton: ElementFinder = $(".reject-doc-selector");

    public static openRevokeSelfExclusionModal(): void {
        this.actionsButton.click();
        this.revokeSelfExclusionButton.click();
        //Wait until the modal is open
        browser.driver.wait(function () {
            return $(".modal-self-exclusion-body-selector").isDisplayed().then((value) => {
                return value;
            });
        }, 10000, "The modal to revoke self exclusion does not appear after 10 seconds.");
    }

    public static confirmRevokeSelfExclusion(): void {
        this.modalSelfExclusionConfirmButton.click();
        //Wait until the modal is closed, the loader disappears and the notification is closed
        browser.driver.wait(function () {
            return $(".loader-wrapper").isDisplayed().then((loaderDisplayed) => {
                return $$(".notifications-container .notification").count().then(notifications => {
                    return !loaderDisplayed && notifications == 0
                });
            });
        }, 10000, "The loader or the notification have not disappeared in 10 seconds after revoking self-exclusion.");
    }

}
