import { $, $$, browser, ElementFinder, ElementArrayFinder } from "protractor";

/**
 * Page object for Search players.
 */
export class SearchPlayersPage {
    //Buttons
    public static searchButton: ElementFinder = $(".mt-basic-filters .btn-search");
    public static clearButton: ElementFinder = $(".mt-basic-filters .btn-clear");
    //Filters
    public static usernameFilter: ElementFinder = $(".mt-basic-filters .usernameFilter_selector input");
    public static accountCodeFilter: ElementFinder = $(".mt-basic-filters .accountCodeFilter_selector input");
    //Players table
    public static playerRows: ElementArrayFinder = $$("table.player-search-table tbody tr:not([hidden])"); //A hidden row exists for the "No results" message

    /**
     * Navigates to the players search page
     */
    public static go(): void {
        //Click on menu players
        $(".menu-players-selector").click();
        //Click on the search players link
        $(".players-home-search-players-selector").click();
    }

    /**
     * Set the account code passed in the filters bar and clicks the search button
     * @param accountCode
     */
    public static searchByAccountCode(accountCode: string): void {
        this.accountCodeFilter.sendKeys(accountCode);
        this.searchButton.click();

        browser.driver.wait(function () {
            return $(".loader-wrapper").isDisplayed().then((loaderDisplayed) => {
                return !loaderDisplayed
            });
        }, 10000, "searchByAccountCode has timed out. The loader has not disappeared after 10 seconds");
    }

    /**
     * Search user by username and wait until the loader disappears.
     * @param username
     */
    public static searchPlayerByUsername(username: string): void {
        this.usernameFilter.sendKeys(username);
        this.searchButton.click();

        browser.driver.wait(function () {
            return $(".loader-wrapper").isDisplayed().then((loaderDisplayed) => {
                return !loaderDisplayed
            });
            // return $$("table.player-search-table tbody tr:not([hidden])").count().then((rowsNumber) => {
            //     return rowsNumber > 0;
            // });
        }, 10000, "searchPlayerByUsername has timed out. The loader has not disappeared after 10 seconds");
    }

    /**
     * Clicks on the "view details" button associated to a player in the players table.
     * Then waits up to 10seconds until route changes.
     * @param username
     */
    public static goToPlayerDetailByClickOnTableButton(username: string): void {
        $("table ." + username + "-detail-link-selector").click();

        browser.driver.wait(function () {
            return browser.driver.getCurrentUrl().then(function (url) {
                return (/detail/).test(url);
            });
        }, 10000, "go to player detail function has timed out. Check the BackOffice API is up and running.");
    }

}
