import { $, $$, browser, ElementFinder } from "protractor";

/**
 * Page object for Login page.
 */
export class LoginPage {
    public static loginButton: ElementFinder = $(".login-button-selector");
    public static username: ElementFinder = $(".username-selector");
    public static password: ElementFinder = $(".password-selector");
    /*This element are not in the login page, but since logout/login are related, they're added here to have this functionality together*/
    public static userOptionsButton: ElementFinder = $(".user-options-selector");
    public static logoutButton: ElementFinder = $(".logout-selector");

    public static go(): void {
        browser.get("/login");

        //Wait until we're on the login page
        browser.driver.wait(() => {
            return $(".login").isPresent().then(isLoginFormPresent => {
                return isLoginFormPresent;
            });
        }, 10000, "Could not open the login page after 10 seconds.");
    }

    /**
     * Types username and password and click on the login button. Waits until the current URL does not contain "login".
     * @param username
     * @param password
     */
    public static doLogin(username: string, password: string): void {
        //Assert we're on login page
        expect(browser.getCurrentUrl()).toContain("login", "Try to log in an user but the application was not in the login page");

        //delete all cookies
        browser.driver.manage().deleteAllCookies();

        //Do login
        this.username.sendKeys(username);
        this.password.sendKeys(password);
        this.loginButton.click();

        //Wait until we're not on the login page anymore
        browser.driver.wait(() => {
            return browser.driver.getCurrentUrl().then((url) => {
                return !(/login/).test(url);
            });
        }, 10000, "doLogin function has timed out. Check the BackOffice API is up and running.");
    }

    public static doLogout(): void {
        this.userOptionsButton.click();
        this.logoutButton.click();
    }

}
