import { browser } from "protractor";

describe("BackOffice application", () => {

    beforeEach(() => {
        browser.get("/");
    });

    it("should be up and running and contain IRIS in the title", () => {
        expect(browser.getTitle()).toContain("IRIS");
    });

});
