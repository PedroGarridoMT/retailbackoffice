import { browser, protractor } from "protractor";
import { PlayerUtils } from "../../utils/player-utils";
import { LoginPage } from "../../page-objects/login.page";
import { SearchPlayersPage } from "../../page-objects/player-search.page";

describe("Suite: Search players.", () => {

    //Test suite variables
    let backOfficeUsername = global.testsGlobal.backofficeUser.loginName;
    let backOfficePassword = global.testsGlobal.backofficeUser.password;
    let player = null;

    //Protractor control flow
    let flow = protractor.promise.controlFlow();

    /**
     * Test suite set up:
     * Register a player in PAS through the Operator API.
     */
    // beforeAll(() => {
    //     //Generate player data and register it.
    //     flow.execute(PlayerUtils.generatePlayerData.bind(null, browser.params.countryCode, "e2eplayer", browser.params.postalCode, browser.params.currency))
    //         .then((playerData: any) => {
    //             player = playerData;
    //             //Register player
    //             flow.execute(PlayerUtils.registerPlayer.bind(null, player)).then((accountCode: string) => {
    //                 player.accountCode = accountCode;
    //                 // Activate player if Spain environment
    //                 if (browser.params.countryCode == "ES") {
    //                     flow.execute(PlayerUtils.activatePlayer.bind(null, player.userName));
    //                 }
    //             });
    //         });
    // });
    beforeAll(async () => {
        if (browser.params.debug) {
            console.log("Search players beforeAll started.\n");
        }
        //Generate player data and register it.
        player = await PlayerUtils.generatePlayerDataAsync(browser.params.countryCode, "e2eplayer", browser.params.postalCode, browser.params.currency);
        player.accountCode = await PlayerUtils.registerPlayerAsync(player);
        // Activate player if Spain environment
        if (browser.params.countryCode == "ES") {
            await PlayerUtils.activatePlayerAsync(player.userName);
        }
        if (browser.params.debug) {
            console.log("\nSearch players beforeAll finished.\n");
        }
    });

    describe("Given a player is registered in the system", () => {

        beforeEach(() => {
            LoginPage.go();
            LoginPage.doLogin(backOfficeUsername, backOfficePassword);
        });

        afterEach(() => {
            LoginPage.doLogout();
        });

        it("then the player must be found when searching by his username in BackOffice", () => {
            SearchPlayersPage.go();
            expect(browser.getCurrentUrl()).toContain("/players/search");

            // Search player by username
            SearchPlayersPage.searchPlayerByUsername(player.userName);
            expect(SearchPlayersPage.playerRows.count()).toBe(1);
            expect(SearchPlayersPage.playerRows.first().getText()).toContain(player.userName);
        });

        it("and the player must be found when searching by his account code in BackOffice", () => {
            SearchPlayersPage.go();

            // Search player by account code
            SearchPlayersPage.searchByAccountCode(player.accountCode);
            browser.driver.manage().getCookies();
            expect(SearchPlayersPage.playerRows.count()).toBe(1);
            expect(SearchPlayersPage.playerRows.first().getText()).toContain(player.accountCode);
        });

    });

    // describe("Fake describe", () => {
    //     it("Fake test", () => {
    //         expect(1).toBe(1);
    //     });
    // });

});
