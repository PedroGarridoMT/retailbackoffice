import { browser, protractor } from "protractor";
import { PlayerUtils } from "../../utils/player-utils";
import { LoginPage } from "../../page-objects/login.page";
import { SearchPlayersPage } from "../../page-objects/player-search.page";
import { PlayerDetailsPage } from "../../page-objects/player-details.page";

describe("Suite: Player self-exclusion.", () => {

    //Test suite variables
    let backOfficeUsername = global.testsGlobal.backofficeUser.loginName;
    let backOfficePassword = global.testsGlobal.backofficeUser.password;
    let player = null;
    let playerSelfExclusionEndDate: Date = new Date();
    playerSelfExclusionEndDate.setDate(playerSelfExclusionEndDate.getDate() + 5);//Current date plus 5 days

    //Protractor control flow
    let flow = protractor.promise.controlFlow();

    /**
     * Test suite set up:
     * Create a player in PAS through the Operator API.
     */
    // beforeAll(() => {
    //     //Generate player data
    //     flow.execute(PlayerUtils.generatePlayerData.bind(null, browser.params.countryCode, "e2eplayer", browser.params.postalCode, browser.params.currency))
    //         .then((playerData: any) => {
    //             player = playerData;
    //             //Register player
    //             flow.execute(PlayerUtils.registerPlayer.bind(null, player)).then((accountCode: string) => {
    //                 player.accountCode = accountCode;
    //                 // Activate player if Spain environment
    //                 if (browser.params.countryCode == "ES") {
    //                     flow.execute(PlayerUtils.activatePlayer.bind(null, player.userName));
    //                 }
    //             });
    //             //Create a new session for the player
    //             flow.execute(PlayerUtils.createSession.bind(null, player.userName, player.password)).then((response: any) => {
    //                 //Request self exclusion
    //                 flow.execute(PlayerUtils.createSelfExclusionPeriod.bind(null, response.token, playerSelfExclusionEndDate));
    //             });
    //         });
    // });
    beforeAll(async () => {
        if (browser.params.debug) {
            console.log("Player self-exclusion beforeAll started.\n");
        }
        //Generate player data and register it
        player = await PlayerUtils.generatePlayerDataAsync(browser.params.countryCode, "e2eplayer", browser.params.postalCode, browser.params.currency);
        player.accountCode = await PlayerUtils.registerPlayerAsync(player);
        // Activate player if Spain environment
        if (browser.params.countryCode == "ES") {
            await PlayerUtils.activatePlayerAsync(player.userName);
        }
        //Create a new session for the player
        const sessionResponse = await PlayerUtils.createSessionAsync(player.userName, player.password);
        //Request self exclusion
        await PlayerUtils.createSelfExclusionPeriod(sessionResponse.token, playerSelfExclusionEndDate);
        if (browser.params.debug) {
            console.log("\nPlayer self-exclusion beforeAll finished.\n");
        }
    });

    describe("Given a logged player requests a period of self-exclusion of 5 days", () => {

        beforeEach(() => {
            LoginPage.go();
            LoginPage.doLogin(backOfficeUsername, backOfficePassword);
            SearchPlayersPage.go();
            SearchPlayersPage.searchPlayerByUsername(player.userName);
        });

        afterEach(() => {
            LoginPage.doLogout();
        });

        it("then it should be registered in the system and self-exclusion data available in BackOffice", () => {
            SearchPlayersPage.goToPlayerDetailByClickOnTableButton(player.userName);
            expect(browser.getCurrentUrl()).toContain("/detail");
            expect(PlayerDetailsPage.selfExclusionBox.isDisplayed()).toBe(true);

            let expectedDayFrom = new Date().getDate();
            let expectedDayTo = playerSelfExclusionEndDate.getDate();
            expect(PlayerDetailsPage.selfExclusionDateFrom.getText()).toContain(expectedDayFrom);
            expect(PlayerDetailsPage.selfExclusionDateTo.getText()).toContain(expectedDayTo);
        });

        it("and it should be possible to revoke the self-exclusion period", () => {
            SearchPlayersPage.goToPlayerDetailByClickOnTableButton(player.userName);

            //Open confirmation modal
            PlayerDetailsPage.openRevokeSelfExclusionModal();
            expect(PlayerDetailsPage.modalSelfExclusion.getText()).not.toBeNull();

            //Confirm action
            PlayerDetailsPage.confirmRevokeSelfExclusion();

            // After revoking the self-exclusion, the player is reloaded.
            // At this moment we can check that the self exclusion has been successfully removed. -> The self-exclusion
            // box should not be present
            expect(PlayerDetailsPage.selfExclusionBox.isPresent()).toBe(false);
        });
    });

});
