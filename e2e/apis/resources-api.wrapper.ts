import { browser, promise } from "protractor";
import { RequestResponse } from "request";
import { E2EHttpModule } from "./http.wrapper";

export class ResourcesApiWrapper {

    public static getFiscalResidenceRegions(): promise.Promise<any> {
        return E2EHttpModule.get(browser.params.endpoints.resourcesApiUrl + "/fiscal_residence_regions");
    }

    public static async getFiscalResidenceRegionsAsync(): Promise<any[]> {
        try {
            const response: RequestResponse = await E2EHttpModule.getAsync(browser.params.endpoints.resourcesApiUrl + "/fiscal_residence_regions");
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static getRegions(countryCode: string): promise.Promise<any> {
        return E2EHttpModule.get(browser.params.endpoints.resourcesApiUrl + "/regions?countryCode=" + countryCode);
    }

    public static async getRegionsAsync(countryCode: string): Promise<any> {
        try {
            const response: RequestResponse = await E2EHttpModule.getAsync(browser.params.endpoints.resourcesApiUrl + "/regions?countryCode=" + countryCode);
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static getCities(regionCode: string): promise.Promise<any> {
        return E2EHttpModule.get(browser.params.endpoints.resourcesApiUrl + "/cities?regionCode=" + regionCode);
    }

    public static async getCitiesAsync(regionCode: string): Promise<any> {
        try {
            const response: RequestResponse = await E2EHttpModule.getAsync(browser.params.endpoints.resourcesApiUrl + "/cities?regionCode=" + regionCode);
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    private static logMethodStart(method: string): void {
        if (browser.params.debug) {
            console.log("\nResourcesAPI: " + method);
        }
    }
}
