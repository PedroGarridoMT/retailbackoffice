import { protractor, browser, promise } from "protractor";
import * as moment from "moment";
import { RequestResponse } from "request";
import { E2EHttpModule } from "./http.wrapper";

export class OperatorApiWrapper {

    /*******************************************************************************************************************
     * Registration
     ******************************************************************************************************************/

    public static registerPlayer(player: any): promise.Promise<string> {
        this.logMethodStart("RegisterPlayer");
        const defer = protractor.promise.defer<string>();
        // TODO: Remove trailing slash from url below when fixed at OperatorWebsiteApi
        E2EHttpModule.post(browser.params.endpoints.operatorApiUrl + "/accounts", player, "Bearer " + global.authToken).then(
            response => defer.fulfill(response.body.accountCode),
            error => defer.reject(error)
        );
        return defer.promise;
    }

    public static async registerPlayerAsync(player: any): Promise<string> {
        this.logMethodStart("RegisterPlayer");
        try {
            // TODO: Remove trailing slash from url below when fixed at OperatorWebsiteApi
            const response: RequestResponse = await E2EHttpModule
                .postAsync(browser.params.endpoints.operatorApiUrl + "/accounts", player, "Bearer " + global.authToken);
            return response.body.accountCode;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    /*******************************************************************************************************************
     * Sessions
     ******************************************************************************************************************/

    public static createPlayerSession(username: string, password: string): promise.Promise<any> {
        this.logMethodStart("CreatePlayerSession");
        const defer = protractor.promise.defer();
        const payload = {
            userName: username,
            password: password,
        };
        E2EHttpModule.post(browser.params.endpoints.operatorApiUrl + "/accounts/sessions", payload, "Bearer " + global.authToken).then(
            response => defer.fulfill(response.body),
            error => defer.reject(error));
        return defer.promise;
    }

    public static async createPlayerSessionAsync(username: string, password: string): Promise<any> {
        this.logMethodStart("CreatePlayerSession");
        const payload = {
            userName: username,
            password: password,
        };
        try {
            const response: RequestResponse = await E2EHttpModule
                .postAsync(browser.params.endpoints.operatorApiUrl + "/accounts/sessions", payload, "Bearer " + global.authToken);
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    /*******************************************************************************************************************
     * Self exclusion
     ******************************************************************************************************************/

    public static setSelfExclusionPeriod(playerToken: string, endDate: Date): promise.Promise<any> {
        this.logMethodStart("SetSelfExclusionPeriod");
        let payload: any = null;
        if (endDate) {
            payload = {
                endDate: moment.utc(endDate).toISOString()
            }
        }
        return E2EHttpModule.post(browser.params.endpoints.operatorApiUrl + "/accounts/me/self-exclusions", payload, playerToken);
    }

    public static async setSelfExclusionPeriodAsync(playerToken: string, endDate: Date): Promise<any> {
        this.logMethodStart("SetSelfExclusionPeriod");
        let payload: any = null;
        if (endDate) {
            payload = {
                endDate: moment.utc(endDate).toISOString()
            }
        }
        try {
            const response: RequestResponse = await E2EHttpModule
                .postAsync(browser.params.endpoints.operatorApiUrl + "/accounts/me/self-exclusions", payload, playerToken);
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    private static logMethodStart(method: string): void {
        if (browser.params.debug) {
            console.log("\nOperatorAPI: " + method + "");
        }
    }

}
