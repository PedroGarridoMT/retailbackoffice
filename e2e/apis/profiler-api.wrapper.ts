import { protractor, browser, promise } from "protractor";
import { RequestResponse } from "request";
import { E2EHttpModule } from "./http.wrapper";
import { User } from "../models/models";

export class ProfilerApiWrapper {
    private static defaultUser: string = "Admin";
    private static defaultPass: string = "Admin";
    private static APIResources = {
        get sessions(): string { return browser.params.endpoints.profilerApiUrl + "/sessions"; },
        get users(): string { return browser.params.endpoints.profilerApiUrl + "/users"; },
        get user(): string { return browser.params.endpoints.profilerApiUrl + "/users/{userId}"; },
        get userRoles(): string { return browser.params.endpoints.profilerApiUrl + "/users/{userId}/roles"; },
        get userRole(): string { return browser.params.endpoints.profilerApiUrl + "/users/{userId}/roles/{roleId}"; },
        get roles(): string { return browser.params.endpoints.profilerApiUrl + "/roles"; },
        get role(): string { return browser.params.endpoints.profilerApiUrl + "/roles/{roleId}"; },
        get rolePermissions(): string { return browser.params.endpoints.profilerApiUrl + "/roles/{roleId}/permissions"; },
        get rolePermission(): string { return browser.params.endpoints.profilerApiUrl + "/roles/{roleId}/permissions/{permissionId}"; },
        get permissions(): string { return browser.params.endpoints.profilerApiUrl + "/permissions"; },
        get permission(): string { return browser.params.endpoints.profilerApiUrl + "/permissions/{permissionId}"; },
    };

    public static loginWithDefaultUser(): promise.Promise<any> {
        this.logMethodStart("LoginWithDefaultUser");
        const defer = protractor.promise.defer();
        const credentials = { userName: this.defaultUser, password: this.defaultPass };

        E2EHttpModule.post(this.APIResources.sessions, credentials)
            .then((response) => {
                global.authToken = response.body.jwt;
                defer.fulfill(response.body);
            }, (error) => defer.reject(error));

        return defer.promise;
    }

    public static async loginWithDefaultUserAsync(): Promise<any> {
        this.logMethodStart("LoginWithDefaultUser");
        const credentials = { userName: this.defaultUser, password: this.defaultPass };
        try {
            const loginResponse: RequestResponse = await E2EHttpModule.postAsync(this.APIResources.sessions, credentials);
            global.authToken = loginResponse.body.jwt;
            return loginResponse.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static createUser(user: User): promise.Promise<any> {
        this.logMethodStart("CreateUser");
        const defer = protractor.promise.defer();
        const enableUserPatchRequest = { op: "replace", path: "/enabled", value: true };

        E2EHttpModule.post(this.APIResources.users, user, this.getBearerToken())
            .then((userResponse) => {
                E2EHttpModule.patch(this.APIResources.user.replace("{userId}", userResponse.body.id), enableUserPatchRequest, this.getBearerToken())
                    .then((response) => {
                        defer.fulfill(userResponse.body);
                    }, (error) => defer.reject(error));
            }, (error) => defer.reject(error));

        return defer.promise;
    }

    public static async createUserAsync(user: User): Promise<User> {
        this.logMethodStart("CreateUser");
        const enableUserPatchRequest = { op: "replace", path: "/enabled", value: true };
        try {
            const userResponse1: RequestResponse = await E2EHttpModule
                .postAsync(this.APIResources.users, user, this.getBearerToken());
            const userResponse2: RequestResponse = await E2EHttpModule
                .patchAsync(this.APIResources.user.replace("{userId}", userResponse1.body.id), enableUserPatchRequest, this.getBearerToken());
            return userResponse1.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static addRoleToUser(userId: number, request: any): promise.Promise<any> {
        this.logMethodStart("AddRoleToUser");

        const defer = protractor.promise.defer();

        E2EHttpModule.post(this.APIResources.userRoles.replace("{userId}", userId.toString()), request, this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));

        return defer.promise;
    }

    public static async addRoleToUserAsync(userId: number, roleId: number): Promise<any> {
        this.logMethodStart("AddRoleToUser");
        try {
            const response: RequestResponse = await E2EHttpModule
                .postAsync(this.APIResources.userRoles.replace("{userId}", userId.toString()), { roleId }, this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static deleteUser(userId: number): promise.Promise<any> {
        this.logMethodStart("DeleteUser");
        const defer = protractor.promise.defer();

        E2EHttpModule.deleteQP(this.APIResources.user.replace("{userId}", userId.toString()), this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));

        return defer.promise;
    }

    public static async deleteUserAsync(userId: number): Promise<any> {
        this.logMethodStart("DeleteUser");
        try {
            const response: RequestResponse = await E2EHttpModule
                .deleteAsync(this.APIResources.user.replace("{userId}", userId.toString()), this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static removeRoleFromUser(userId: number, roleId: number): promise.Promise<any> {
        this.logMethodStart("RemoveRoleFromUser");
        const defer = protractor.promise.defer();

        E2EHttpModule.deleteQP(this.APIResources.userRole.replace("{userId}", userId.toString()).replace("{roleId}", roleId.toString()), this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));

        return defer.promise;
    }

    public static async removeRoleFromUserAsync(userId: number, roleId: number): Promise<any> {
        this.logMethodStart("RemoveRoleFromUser");
        try {
            const response: RequestResponse = await E2EHttpModule
                .deleteAsync(this.APIResources.userRole.replace("{userId}", userId.toString()).replace("{roleId}", roleId.toString()), this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static createPermission(name: string): promise.Promise<any> {
        this.logMethodStart("\n CreatePermission " + name);
        const defer = protractor.promise.defer();

        E2EHttpModule.post(this.APIResources.permissions, { name }, this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));

        return defer.promise;
    }

    public static async createPermissionAsync(name: string): Promise<any> {
        this.logMethodStart("\n CreatePermission " + name);
        try {
            const response: RequestResponse = await E2EHttpModule
                .postAsync(this.APIResources.permissions, { name }, this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static getPermissions(): promise.Promise<any> {
        this.logMethodStart("getPermissions");
        const defer = protractor.promise.defer();

        E2EHttpModule.get(this.APIResources.permissions, this.getBearerToken())
            .then((responseBody) => defer.fulfill(responseBody), (error) => defer.reject(error));

        return defer.promise;
    }

    public static async getPermissionsAsync(): Promise<Array<any>> {
        this.logMethodStart("getPermissions");
        try {
            const response: RequestResponse = await E2EHttpModule
                .getAsync(this.APIResources.permissions, this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static createRole(name: string): promise.Promise<any> {
        this.logMethodStart("CreateRole " + name);
        const defer = protractor.promise.defer();
        const enableRolePatchRequest = { op: "replace", path: "/enabled", value: true };

        E2EHttpModule.post(this.APIResources.roles, { name }, this.getBearerToken())
            .then((roleResponse) => {
                E2EHttpModule.patch(this.APIResources.role.replace("{roleId}", roleResponse.body.id), enableRolePatchRequest, this.getBearerToken())
                    .then((response) => defer.fulfill(roleResponse.body), (error) => defer.reject(error));
            }, (error) => defer.reject(error));

        return defer.promise;
    }

    public static async createRoleAsync(name: string): Promise<any> {
        this.logMethodStart("CreateRole " + name);
        const enableRolePatchRequest = { op: "replace", path: "/enabled", value: true };
        try {
            const roleResponse1: RequestResponse = await E2EHttpModule
                .postAsync(this.APIResources.roles, { name }, this.getBearerToken());
            const roleResponse2: RequestResponse = await E2EHttpModule
                .patchAsync(this.APIResources.role.replace("{roleId}", roleResponse1.body.id), enableRolePatchRequest, this.getBearerToken());
            return roleResponse1.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static assignPermissionToRole(roleId: number, permissionId: number): promise.Promise<any> {
        const defer = protractor.promise.defer();
        E2EHttpModule.post(this.APIResources.rolePermissions.replace("{roleId}", roleId.toString()), { permissionId }, this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));
        return defer.promise;
    }

    public static async assignPermissionToRoleAsync(roleId: number, permissionId: number): Promise<any> {
        try {
            const permissionResponse: RequestResponse = await E2EHttpModule
                .postAsync(this.APIResources.rolePermissions.replace("{roleId}", roleId.toString()), { permissionId }, this.getBearerToken());
            return permissionResponse.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static removePermissionFromRole(roleId: number, permissionId: number): promise.Promise<any> {
        const defer = protractor.promise.defer();
        E2EHttpModule.deleteQP(this.APIResources.rolePermission.replace("{roleId}", roleId.toString()).replace("{permissionId}", permissionId.toString()), this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));
        return defer.promise;
    }

    public static async removePermissionFromRoleAsync(roleId: number, permissionId: number): Promise<any> {
        try {
            const response: RequestResponse = await E2EHttpModule
                .deleteAsync(this.APIResources.rolePermission.replace("{roleId}", roleId.toString()).replace("{permissionId}", permissionId.toString()), this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static deleteRole(roleId: number): promise.Promise<any> {
        this.logMethodStart("DeleteRole");
        const defer = protractor.promise.defer();
        E2EHttpModule.deleteQP(this.APIResources.role.replace("{roleId}", roleId.toString()), this.getBearerToken())
            .then((response) => defer.fulfill(response.body), (error) => defer.reject(error));
        return defer.promise;
    }

    public static async deleteRoleAsync(roleId: number): Promise<any> {
        this.logMethodStart("DeleteRole");
        try {
            const response: RequestResponse = await E2EHttpModule
                .deleteAsync(this.APIResources.role.replace("{roleId}", roleId.toString()), this.getBearerToken());
            return response.body;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    private static getBearerToken() {
        return "Bearer " + global.authToken;
    }

    private static logMethodStart(method: string): void {
        if (browser.params.debug) {
            console.log("\nProfilerAPI: " + method);
        }
    }

}
