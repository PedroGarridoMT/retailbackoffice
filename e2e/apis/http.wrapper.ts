/**
 * Since we’re running on Node, we can use its libraries in our tests. Here we're using request, a popular HTTP client.
 * See https://github.com/request/request for further info
 */
import * as request from "request";
import { protractor, browser, promise } from "protractor";

export class E2EHttpModule {

    public static get(url: string, authHeader?: string): promise.Promise<any> {
        let defer = protractor.promise.defer();
        let getParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            json: true,
        };
        if (browser.params.debug) {
            console.log("GET to url: " + getParams.url + " started.");
        }
        request.get(getParams, (error: any, response: request.RequestResponse, body: any) => {
            if (error || response.statusCode >= 400) {
                console.error("GET to url: " + getParams.url + " finished with error.", response.statusMessage);
                if (error) {
                    defer.reject({ error: error, message: error.message });
                } else {
                    defer.reject({ error: response.body, message: response.body.message });
                }
            } else {
                if (browser.params.debug) {
                    console.log("GET to url: " + getParams.url + " finished. Status code: " + response.statusCode);
                }
                defer.fulfill(body);
            }
        });
        return defer.promise;
    }

    public static async getAsync(url: string, authHeader?: string): Promise<any> {
        const getParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            json: true,
        };
        if (browser.params.debug) {
            console.log("GET to url: " + getParams.url + " started.");
        }
        return new Promise<request.RequestResponse>(async (resolve: (value?: request.RequestResponse) => void, reject: (reason?: any) => void) => {
            try {
                await request.get(getParams, (error: any, response: request.RequestResponse, body: any): void => {
                    if (error || response.statusCode >= 400) {
                        console.error("GET to url: " + getParams.url + " finished with error.", response.statusMessage);
                        if (error) {
                            throw error;
                        } else {
                            reject({ error: response.body, message: response.body.message });
                        }
                    } else {
                        if (browser.params.debug) {
                            console.log("GET to url: " + getParams.url + " finished. Status code: " + response.statusCode);
                        }
                        resolve(response);
                    }
                });
            } catch (error) {
                reject({ error: error, message: error.message });
            }
        });
    }

    public static post(url: string, params: any, authHeader?: string): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const postParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            body: params
        };
        if (!this.isBasicAuth(authHeader)) {
            postParams["json"] = true;
        }
        if (browser.params.debug) {
            console.log("POST to url: " + postParams.url + " started.");
        }
        request.post(postParams, (error: any, response: request.RequestResponse) => {
            if (error || response.statusCode >= 400) {
                console.error("POST to url: " + postParams.url + " finished with error.", response.statusMessage);
                if (error) {
                    defer.reject({ error: error, message: error.message });
                } else {
                    defer.reject({ error: response.body, message: response.body.message });
                }
            } else {
                if (browser.params.debug) {
                    console.log("POST to url: " + postParams.url + " finished. Status code: " + response.statusCode);
                }
                defer.fulfill(response);
            }
        });
        return defer.promise;
    }

    public static async postAsync(url: string, params: any, authHeader?: string): Promise<any> {
        const postParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            body: params,
        };
        if (!this.isBasicAuth(authHeader)) {
            postParams["json"] = true;
        }
        if (browser.params.debug) {
            console.log("POST to url: " + postParams.url + " started.");
        }
        return new Promise<request.RequestResponse>(async (resolve: (value?: request.RequestResponse) => void, reject: (reason?: any) => void) => {
            try {
                await request.post(postParams, (error: any, response: request.RequestResponse, body: any): void => {
                    if (error || response.statusCode >= 400) {
                        console.error("POST to url: " + postParams.url + " finished with error.", response.statusMessage);
                        if (error) {
                            throw error;
                        } else {
                            reject({ error: response.body, message: response.body.message });
                        }
                    } else {
                        if (browser.params.debug) {
                            console.log("POST to url: " + postParams.url + " finished. Status code: " + response.statusCode);
                        }
                        resolve(response);
                    }
                });
            } catch (error) {
                reject({ error: error, message: error.message });
            }
        });
    }

    public static put(url: string, params: any, authHeader?: string): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const putParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            body: params
        };
        if (!this.isBasicAuth(authHeader)) {
            putParams["json"] = true;
        }
        if (browser.params.debug) {
            console.log("PUT to url: " + putParams.url + " started.");
        }
        request.put(putParams, (error: any, response: request.RequestResponse, body: any): void => {
            if (error || response.statusCode >= 400) {
                console.error("PUT to url: " + putParams.url + " finished with error.", response.statusMessage);
                if (error) {
                    defer.reject({ error: error, message: error.message });
                } else {
                    defer.reject({ error: response.body, message: response.body.message });
                }
            } else {
                if (browser.params.debug) {
                    console.log("PUT to url: " + putParams.url + " finished. Status code: " + response.statusCode);
                }
                defer.fulfill(response);
            }
        });
        return defer.promise;
    }

    public static async putAsync(url: string, params: any, authHeader?: string): Promise<any> {
        const putParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            body: params,
        };
        if (!this.isBasicAuth(authHeader)) {
            putParams["json"] = true;
        }
        if (browser.params.debug) {
            console.log("PUT to url: " + putParams.url + " started.");
        }
        return new Promise<request.RequestResponse>(async (resolve: (value?: request.RequestResponse) => void, reject: (reason?: any) => void) => {
            try {
                await request.put(putParams, (error: any, response: request.RequestResponse, body: any): void => {
                    if (error || response.statusCode >= 400) {
                        console.error("PUT to url: " + putParams.url + " finished with error.", response.statusMessage);
                        if (error) {
                            throw error;
                        } else {
                            reject({ error: response.body, message: response.body.message });
                        }
                    } else {
                        if (browser.params.debug) {
                            console.log("PUT to url: " + putParams.url + " finished. Status code: " + response.statusCode);
                        }
                        resolve(response);
                    }
                });
            } catch (error) {
                reject({ error: error, message: error.message });
            }
        });
    }

    public static patch(url: string, params: any, authHeader?: string): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const patchParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            body: params
        };
        if (!this.isBasicAuth(authHeader)) {
            patchParams["json"] = true;
        }
        if (browser.params.debug) {
            console.log("PATCH to url: " + patchParams.url + " started.");
        }
        request.patch(patchParams, (error: any, response: request.RequestResponse, body: any): void => {
            if (error || response.statusCode >= 400) {
                console.error("PATCH to url: " + patchParams.url + " finished with error.", response.statusMessage);
                if (error) {
                    defer.reject({ error: error, message: error.message });
                } else {
                    defer.reject({ error: response.body, message: response.body.message });
                }
            } else {
                if (browser.params.debug) {
                    console.log("PATCH to url: " + patchParams.url + " finished. Status code: " + response.statusCode);
                }
                defer.fulfill(response);
            }
        });
        return defer.promise;
    }

    public static async patchAsync(url: string, params: any, authHeader?: string): Promise<any> {
        const patchParams = {
            url: url,
            headers: this.getHeaders(authHeader),
            body: params,
        };
        if (!this.isBasicAuth(authHeader)) {
            patchParams["json"] = true;
        }
        if (browser.params.debug) {
            console.log("PATCH to url: " + patchParams.url + " started.");
        }
        return new Promise<request.RequestResponse>(async (resolve: (value?: request.RequestResponse) => void, reject: (reason?: any) => void) => {
            try {
                await request.patch(patchParams, (error: any, response: request.RequestResponse, body: any): void => {
                    if (error || response.statusCode >= 400) {
                        console.error("PATCH to url: " + patchParams.url + " finished with error.", response.statusMessage);
                        if (error) {
                            throw error;
                        } else {
                            reject({ error: response.body, message: response.body.message });
                        }
                    } else {
                        if (browser.params.debug) {
                            console.log("PATCH to url: " + patchParams.url + " finished. Status code: " + response.statusCode);
                        }
                        resolve(response);
                    }
                });
            } catch (error) {
                reject({ error: error, message: error.message });
            }
        });
    }

    public static deleteQP(url: string, authHeader: string): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const deleteParams = {
            url: url,
            headers: this.getHeaders(authHeader)
        };
        if (browser.params.debug) {
            console.log("DELETE to url: " + deleteParams.url + " started.");
        }
        request.del(deleteParams, (error: any, response: request.RequestResponse) => {
            if (error || response.statusCode >= 400) {
                console.error("DELETE to url: " + deleteParams.url + " finished with error.", response.statusMessage);
                if (error) {
                    defer.reject({ error: error, message: error.message });
                } else {
                    defer.reject({ error: response.body, message: response.body.message });
                }
            } else {
                if (browser.params.debug) {
                    console.log("DELETE to url: " + deleteParams.url + " finished. Status code: " + response.statusCode);
                }
                defer.fulfill(response);
            }
        });
        return defer.promise;
    }

    public static async deleteAsync(url: string, authHeader: string): Promise<any> {
        const deleteParams = {
            url: url,
            headers: this.getHeaders(authHeader),
        };
        if (browser.params.debug) {
            console.log("DELETE to url: " + deleteParams.url + " started.");
        }
        return new Promise<request.RequestResponse>(async (resolve: (value?: request.RequestResponse) => void, reject: (reason?: any) => void) => {
            try {
                await request.del(deleteParams, (error: any, response: request.RequestResponse): void => {
                    if (error || response.statusCode >= 400) {
                        console.error("DELETE to url: " + deleteParams.url + " finished with error.", response.statusMessage);
                        if (error) {
                            throw error;
                        } else {
                            reject({ error: response.body, message: response.body.message });
                        }
                    } else {
                        if (browser.params.debug) {
                            console.log("DELETE to url: " + deleteParams.url + " finished. Status code: " + response.statusCode);
                        }
                        resolve(response);
                    }
                });
            } catch (error) {
                reject({ error: error, message: error.message });
            }
        });
    }

    /**
     * Two sets of headers might be returned depending on the type of authorization header.
     *
     * The authorization header:
     * Bearer xxxxxx -> is for authorized calls to the api, the body is json.
     * Basic xxxxxxx -> for login calls, the body must be sent as x-www-form-urlencoded
     *
     * @param authHeader
     * @returns {any}
     */
    private static getHeaders(authHeader: string): any {
        if (authHeader) {
            if (this.isBasicAuth(authHeader)) {
                return {
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": authHeader,
                };
            }
            else {
                return {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": authHeader
                };
            }
        }

        //Default headers
        return { "Accept": "application/json", "Content-Type": "application/json" };
    }

    private static isBasicAuth(authHeader: string): boolean {
        return authHeader && authHeader.indexOf("Basic") != -1;
    }

    private static isBearerAuth(authHeader: string): boolean {
        return authHeader && authHeader.indexOf("Bearer") != -1;
    }
}
