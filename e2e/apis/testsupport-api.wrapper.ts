import { protractor, browser, promise } from "protractor";
import * as moment from "moment";
import { E2EHttpModule } from "./http.wrapper";
import { RequestResponse } from 'request';

export class TestSupportApiWrapper {

    public static activatePlayer(userName: string): promise.Promise<number> {
        this.logMethodStart("Activating Player: " + userName);
        const defer = protractor.promise.defer<number>();
        const payload = {
            PlayerUserName: userName
        };
        E2EHttpModule.post(browser.params.endpoints.testSupportApiUrl + "/player/active", payload).then(
            (response: RequestResponse) => defer.fulfill(response.statusCode),
            (error) => defer.reject(error)
        );
        return defer.promise;
    }

    public static async activatePlayerAsync(userName: string): Promise<number> {
        this.logMethodStart("Activating Player: " + userName);
        const payload = {
            PlayerUserName: userName,
        };
        try {
            const response: RequestResponse = await E2EHttpModule.postAsync(browser.params.endpoints.testSupportApiUrl + "/player/active", payload);
            return response.statusCode;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    public static logMethodStart(method: string): void {
        if (browser.params.debug) {
            console.log("\nTestSupportAPI: " + method);
        }
    }

}
