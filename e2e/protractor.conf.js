/**
 * @author: @RFrancoDigital
 */
require("ts-node/register");
const SpecReporter = require("jasmine-spec-reporter").SpecReporter;
const UserUtils = require("./utils/user-utils").UserUtils;
const environmentConfig = require("./environment.config.json");
const params = environmentConfig.environment[process.env.NODE_ENV].keys;
params.endpoints = environmentConfig.environment[process.env.NODE_ENV].endpoints;

exports.config = {
    // Set the base url for the e2e tests. The back office web application should be running on this URL.
    baseUrl: params.endpoints.appUrl,

    // load diffrent params depending on environment
    params,

    // Spec patterns are relative to the location of this config.
    specs: [
        "test-suites/**/*.e2e.ts",
    ],
    exclude: [],

    framework: "jasmine2",

    allScriptsTimeout: 110000,

    jasmineNodeOpts: {
        showTiming: true,
        showColors: true,
        isVerbose: false,
        includeStackTrace: true,
        defaultTimeoutInterval: 400000,
        //Remove protractor dot reporter. The SpecReporter is used
        print: function () { }
    },

    directConnect: true,

    capabilities: {
        "browserName": "chrome",
        "chromeOptions": { "args": ["show-fps-counter=true"] }
    },

    // onPrepare: () => {
    //     global.testsGlobal = {};
    //     browser.ignoreSynchronization = false;
    //     //Delete all cookies
    //     browser.driver.manage().deleteAllCookies();
    //     //Add tests reporter.
    //     jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    //     console.log("--> TESTS SET UP <--");
    //     //Create user with full access to back office features.
    //     //This user will be stored in the global object and used in all test suites.
    //     const userData = UserUtils.generateBackOfficeUser();
    //     return UserUtils.createUserWithFullAccess(userData)
    //         .then((boUser) => {
    //             testsGlobal.backofficeUser = boUser;
    //             testsGlobal.backofficeUser["password"] = userData.password;
    //             console.log("\n--> SET UP DONE, STARTING THE TESTS <-- \n");
    //         }, (error) => console.error("OnPrepare: error creating backoffice user\n", error));
    // },

    onPrepare: async () => {
        testsGlobal = {};
        browser.ignoreSynchronization = false;
        //Delete all cookies
        browser.driver.manage().deleteAllCookies();
        //Add tests reporter.
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
        console.log("\n--> TESTS SET UP <--");
        //Create user with full access to back office features. It will be stored in the global object and used in all test suites.
        const userData = UserUtils.generateBackOfficeUser();
        try {
            const boUser = await UserUtils.createUserWithFullAccessAsync(userData);
            testsGlobal.backofficeUser = boUser;
            testsGlobal.backofficeUser.password = userData.password;
            console.log("\n--> SET UP DONE, STARTING THE TESTS <-- \n");
        } catch (error) {
            console.error("OnPrepare: Error creating backoffice user.\n", error);
            throw error;
        }
    },

    // onComplete: () => {
    //     console.log("--> TESTS TEAR DOWN <--");
    //     return UserUtils.deleteUserWithFullAccess(testsGlobal.backofficeUser)
    //         .then((resp) => {
    //             console.log("\n--> TEAR DOWN DONE <--");
    //         }, (error) => console.error("OnComplete: User or role could not be cleaned. Please review data on profiler web.", error));
    // },

    onComplete: async () => {
        console.log("\n--> TESTS TEAR DOWN <--");
        try {
            await UserUtils.deleteUserWithFullAccessAsync(testsGlobal.backofficeUser);
            console.log("\n--> TEAR DOWN DONE <--\n");
        } catch (error) {
            console.error("OnComplete: User or role could not be cleaned. Please review data on profiler web.\n", error);
            throw error;
        }
    },

    /**
     * Angular 2 configuration
     *
     * useAllAngular2AppRoots: tells Protractor to wait for any angular2 apps on the page instead of just
     * the one matching `rootEl`
     */
    useAllAngular2AppRoots: true
};
