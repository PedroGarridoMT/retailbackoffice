# E2E Tests


### Quick start
**Make sure you have Node version >= 5.0 and NPM >= 3**

First of all, start PAS BO Web app
```bash
npm start
```

Change directory to the end to end tests folder
```bash
cd pasbackoffice/e2e
```
Install dependencies with npm
```bash
npm install
```

Execute e2e tests
```bash
npm run protractor:dev-es
```


# Table of Contents
* [File Structure](#file-structure)
* [Getting Started](#getting-started)
    * [Dependencies](#dependencies)
    * [Installing test dependencies](#installing)
    * [Running the tests](#running-the-tests)
* [PAS modules tested](#pas-modules-tested)
    * [Endpoints configuration](#endpoint-config)


## File Structure
Here's how it looks:
```
 pasbackoffice/
 │
 └──e2e/
    │
    ├──apis/                           * external apis configuration and wrappers
    │   ├──http.wrapper.ts             * http wrapper library for http calls to external apis. Using "request" library of node
    │   ├──resources-api.wrapper.ts    * contains methods of the resources API used in tests
    │   ├──profiler-api.wrapper.ts     * contains methods of the profiler API used in tests
    │   └──operator-api.wrapper.ts     * contains methods of the operator API used in tests
    │
    ├──utils/                          * contains features to be used through all the tests suites
    │    ├──backoffice-permissions.ts  * a list of all the permissions of the back office application
    │    ├──player-utils.ts            * player utilities: generate random player, register a player in the platform, etc
    │    └──user-utils.ts              * back office user utilities: manages permissions, roles and users
    │
    ├──page-objects                    * contains all the page objects needed in our tests
    │    ├──login.page.ts              * page object for the login page of the back office application
    │    ├──player-detail.page.ts      * page object for the player detail page of the back office application
    │    ├──player-search.page.ts      * page object for the player search page of the back office application
    │    └──....page.ts                * create as many page objects as needed in this folder
    │
    ├──test-suites
    │    ├──players                    * player test suites
    │    │   ├──search-player.e2e.ts   * search players tests
    │    │   └──....e2e.ts             * other players tests suites
    │    │
    │    └──transactions               * transaction test suites
    │
    ├──environment.config.json         * config file for setting environment variables and urls of the external apis
    ├──package.json                    * what npm uses to manage it's dependencies
    ├──protractor.conf.js              * protractor configuration file
    └──tsconfig.json                   * typescript config used by tsc

```

# Getting Started
## Dependencies
What you need to run the end to end tests:
* Install `node` and `npm`. Ensure you're running the latest versions ()Node `v6.x.x` and NPM `3.x.x`).
* Since tests are executed in chrome, make sure it is installed on your computer and you're running the latest version (at the time of writing this document `v61.x.x`)


## Installing test dependencies
* `cd pasbackoffice/e2e` for opening the tests folder
* `npm install` to install all dependencies


## Running the tests
After you have installed all dependencies you can now run the tests specifying the desired environment, `npm run protractor:Environment`. 

`Environment` possible values:

* `live-es`
* `live-co`
* `dev-es`
* `dev-co`
* `qa`

For example, for running tests in spanish dev environment; execute:
```bash
npm run protractor:dev-es
```
Protractor will run tests against the back office application, interacting with it as a user would.
Therefore, ensure the back office application is up and running before you launch the e2e tests.

Protractor reads the URL of the back office instance which is going to be tested in the file `environment.config.json`, variable `appUrl`

# PAS modules being tested
The aim of this end-to-end tests is to test the whole PAS system, from the UI interface to the database.

The "pieces" of PAS which are being tested in the current project are:

* Integrated in the back office web application
    * Back office API

* Modules not managed by the back office web application
    * [Resources API](https://bitbucket.org/mediatechsolutions/mtcs/src/b00d91dcc0bb65531f40675e20ebedb67f478f64/PAS2/Api/ResourcesApi/?at=master) `To get resources used in the system like countries, cities, person titles, etc.`
    * [Operator website API](https://bitbucket.org/mediatechsolutions/mtcs/src/b00d91dcc0bb65531f40675e20ebedb67f478f64/PAS2/Api/OperatorWebsiteApi/?at=master) `To perform player actions like: register, modify limits, request self-exclusions...`
    * Profiler service  `For back office users management: Permissions, roles and users`
    * Authentication service `To authenticate back office users`
    * Databases (PAS and Profiler)()

> If you need more info about these modules, please follow the links.

## Endpoints configuration

All of these "pieces" must be up and running before launching the tests.
* The endpoint of the back office API is configured within the back office web application, in the file
    * `/assets/properties.json` -> variable `baseUrl`
* The endpoints of the APIs not managed in the back office web application must be configured in the test folder itself.
    * File `/environment.config.json`
