
export interface User {
    id: number;
    loginName: string;
    password?: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    language: string;
    company: string;
    enabled?: boolean;
    roles?: any[];
}
