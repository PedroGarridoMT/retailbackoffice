import { protractor, browser, promise } from "protractor";
import { BackOfficePermissions } from "./backoffice-permissions";
import { ProfilerApiWrapper } from "../apis/profiler-api.wrapper";
import { User } from "../models/models";

export class UserUtils {

    private static fullAccessRole = {
        name: "E2E-FullPermissions",
        id: null, //To be filled when the role is created in the database
    };
    private static flow = promise.controlFlow(); //Used to control flow in sync methods

    public static generateBackOfficeUser(): User {
        return {
            company: "RFranco Digital",
            emailAddress: "e2e@tests.com",
            firstName: "e2e",
            language: "test",
            lastName: "user",
            loginName: "E2E-user",
            password: "M3d1.tech",
            id: -1, //To be filled when the user is created in the database
        };
    }

    /**
     * Create a user ready to be used in the back office application, it contains permissions to every action that might
     * be performed on the back office application. (Role FullPermissionsE2E)
     * STEP 1 - Login with the default user of the profiler.
     *          * ASSUMPTIONS: The default user exists and have enough rights to perform CRUD in the system.
     * STEP 2 - Create back office permissions if they're not created already.
     * STEP 3 - Create FullPermissionsE2E role
     * STEP 4 - Assign permissions to the role
     * STEP 5 - Create a back office user in the profiler API.
     * STEP 6 - Assign the FullPermissionsE2E to the new user
     */
    public static createUserWithFullAccess(userRequest: any): promise.Promise<any> {
        const defer = protractor.promise.defer();

        //Login in profiler
        ProfilerApiWrapper.loginWithDefaultUser().then((loginResponse: any) => {

            //Create permissions if necessary
            this.createBackOfficePermissions().then((permsResponse: any) => {

                //Create role with full permissions
                this.createFullAccessRole().then((roleResponse: any) => {

                    //Create back office user
                    ProfilerApiWrapper.createUser(userRequest).then((userResponse: any) => {

                        //Assign role to user
                        ProfilerApiWrapper.addRoleToUser(userResponse.id, { roleId: roleResponse.id })
                            .then(() => {
                                //Resolve promise (containing the user)
                                defer.fulfill(userResponse);
                            }, (assignRoleError: any) => {
                                console.log("createUserWithFullAccess - Add role to user FAIL");
                                defer.reject(assignRoleError);
                            })
                    }, (createUserError: any) => {
                        console.error("createUserWithFullAccess - Create user FAIL");
                        defer.reject(createUserError);
                    });
                }, (rolesError: any) => {
                    console.error("createUserWithFullAccess - Create role FAIL");
                    defer.reject(rolesError);
                });
            }, (permsError: any) => {
                console.error("createUserWithFullAccess - Create permissions FAIL");
                defer.reject(permsError);
            });
        }, (loginError: any) => {
            console.error("createUserWithFullAccess - Default user login FAIL");
            defer.reject(loginError);
        });

        return defer.promise;
    }

    public static async createUserWithFullAccessAsync(boUser: User): Promise<User> {
        let createdUser: User;
        try {
            //Login in profiler
            const loginResponse = await ProfilerApiWrapper.loginWithDefaultUserAsync();
        } catch (loginError) {
            console.error("createUserWithFullAccess - Default user login FAIL", loginError);
            throw loginError;
        }
        try {
            //Create permissions if necessary
            const permsResponse = await this.createBackOfficePermissionsAsync();
        } catch (permsError) {
            console.error("createUserWithFullAccess - Create permissions FAIL", permsError);
            throw permsError;
        }
        try {
            //Create role with full permissions
            const roleResponse = await this.createFullAccessRoleAsync();
        } catch (rolesError) {
            console.error("createUserWithFullAccess - Create role FAIL", rolesError);
            throw rolesError;
        }
        try {
            //Create back office user
            createdUser = await ProfilerApiWrapper.createUserAsync(boUser);
        } catch (createUserError) {
            console.error("createUserWithFullAccess - Create user FAIL", createUserError);
            throw createUserError;
        }
        try {
            //Assign role to user
            await ProfilerApiWrapper.addRoleToUserAsync(createdUser.id, this.fullAccessRole.id);
            return createdUser;
        } catch (assignRoleError) {
            console.error("createUserWithFullAccess - Add role to user FAIL", assignRoleError);
            throw assignRoleError;
        }
    }

    /**
     * Deletes the user passed and the FullPermissionsE2E role.
     * STEP 1 - Remove the role FullPermissionsE2E from the user
     * STEP 2 - Deletes the user
     * STEP 3 - Removes all the permissions from the role FullPermissionsE2E
     * STEP 4 - Deletes the role FullPermissionsE2E
     */
    public static deleteUserWithFullAccess(boUser: any): promise.Promise<any> {
        const defer = protractor.promise.defer();

        //Remove FullPermissionsE2E role from user
        ProfilerApiWrapper.removeRoleFromUser(boUser.id, this.fullAccessRole.id).then((removeRoleResp: any) => {
            //Delete user
            ProfilerApiWrapper.deleteUser(boUser.id).then((deleteUserResp: any) => {
                //Remove permissions from FullPermissionsE2E role
                this.deletePermissionsFromRole().then((deletePermResp: any) => {
                    //Delete role
                    ProfilerApiWrapper.deleteRole(this.fullAccessRole.id)
                        .then((deleteRoleResp: any) => {
                            defer.fulfill("User deleted");
                        }, (deleteRoleErr: any) => {
                            defer.reject(deleteRoleErr);
                        });
                }, (deletePermError: any) => defer.reject(deletePermError));
            }, (deleteUserError): any => defer.reject(deleteUserError));
        }, (removeRoleError: any) => defer.reject(removeRoleError));

        return defer.promise;
    }

    public static async deleteUserWithFullAccessAsync(boUser: User): Promise<void> {
        try {
            //Remove FullPermissionsE2E role from user
            await ProfilerApiWrapper.removeRoleFromUserAsync(boUser.id, this.fullAccessRole.id);
        } catch (error) {
            console.error("deleteUserWithFullAccess - Removing role from user FAIL", error);
            throw error;
        }
        try {
            //Delete user
            await ProfilerApiWrapper.deleteUserAsync(boUser.id);
        } catch (error) {
            console.error("deleteUserWithFullAccess - Deleting user FAIL", error);
            throw error;
        }
        try {
            //Remove permissions from FullPermissionsE2E role
            await this.deletePermissionsFromRoleAsync();
        } catch (error) {
            console.error("deleteUserWithFullAccess - Removing permissions from role FAIL", error);
            throw error;
        }
        try {
            //Delete role
            await ProfilerApiWrapper.deleteRoleAsync(this.fullAccessRole.id);
        } catch (error) {
            console.error("deleteUserWithFullAccess - Removing permissions from role FAIL", error);
            throw error;
        }
    }

    private static createBackOfficePermissions(): promise.Promise<string> {
        const defer = protractor.promise.defer<string>();

        let systemPermissions: Array<any> = [];
        const neededPermissions: Array<string> = BackOfficePermissions.getBackOfficePermissions();
        const promises = [];

        ProfilerApiWrapper.getPermissions().then(perms => {
            systemPermissions = perms;

            neededPermissions.forEach((permission: string) => {
                //Check if the needed permission is already on the system, if not, create it.
                if (!systemPermissions.some((sysPerm: any) => sysPerm.name === permission)) {
                    promises.push(this.flow.execute(ProfilerApiWrapper.createPermission.bind(null, permission.trim())));
                }
            });

            if (promises.length > 0 && browser.params.debug) {
                console.log("\nCreating permissions: ");
            }

            protractor.promise.all(promises)
                .then(responses => defer.fulfill("All permissions created"))
                .catch(error => defer.reject("Error creating permissions"));
        });

        return defer.promise;
    }

    private static async createBackOfficePermissionsAsync(): Promise<string> {
        const neededPermissions: Array<string> = BackOfficePermissions.getBackOfficePermissions();
        let systemPermissions: Array<any>;
        try {
            systemPermissions = await ProfilerApiWrapper.getPermissionsAsync();
        } catch (error) {
            throw "Error getting permissions";
        }
        try {
            for (let permission of neededPermissions) {
                if (!systemPermissions.some((sysPerm: any) => sysPerm.name === permission)) {
                    await ProfilerApiWrapper.createPermissionAsync(permission);
                }
            }
            return "All permissions created";
        } catch (error) {
            throw "Error creating permissions";
        }
    }

    private static createFullAccessRole(): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const boPermissions: string[] = BackOfficePermissions.getBackOfficePermissions();
        const promises = [];

        ProfilerApiWrapper.createRole(this.fullAccessRole.name).then((newRole: any) => {

            if (browser.params.debug) {
                console.log("\nCreateFullAccessRole - Assign permissions to role:");
            }
            //Store role id
            this.fullAccessRole.id = newRole.id;
            this.updateRolePermissions(true)
                .then((response) => {
                    defer.fulfill(newRole);
                    console.log("\nDone assigning permissions to role.");
                })
                .catch((err) => defer.reject("Could not create full access role."));
        }, err => defer.reject("Could not create full access role."));

        return defer.promise;
    }

    private static async createFullAccessRoleAsync(): Promise<string> {
        const boPermissions: string[] = BackOfficePermissions.getBackOfficePermissions();
        const systemPermissions = await ProfilerApiWrapper.getPermissionsAsync();
        try {
            const newRole: any = await ProfilerApiWrapper.createRoleAsync(this.fullAccessRole.name);
            this.fullAccessRole.id = newRole.id;
        } catch (error) {
            throw "Could not create full access role.";
        }
        if (browser.params.debug) {
            console.log("\nAssign permissions to role:");
        }
        try {
            for (let permission of systemPermissions.filter(sysPerm => boPermissions.some(boPerm => sysPerm.name === boPerm))) {
                await ProfilerApiWrapper.assignPermissionToRoleAsync(this.fullAccessRole.id, permission.id);
            }
        } catch (error) {
            throw "Could not assign permissions to full access role.";
        }
        if (browser.params.debug) {
            console.log("\nDone assigning permissions to role.");
        }
        return "Full access role created successfully.";
    }

    private static deletePermissionsFromRole(): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const boPermissions = BackOfficePermissions.getBackOfficePermissions();
        const promises = [];
        if (browser.params.debug) {
            console.log("\nDelete permissions from role: ");
        }
        this.updateRolePermissions(false)
            .then((responses) => defer.fulfill("Permissions deleted successfully"))
            .catch(err => defer.reject(err));
        return defer.promise;
    }

    private static async deletePermissionsFromRoleAsync(): Promise<void> {
        const boPermissions = BackOfficePermissions.getBackOfficePermissions();
        const systemPermissions = await ProfilerApiWrapper.getPermissionsAsync();
        if (browser.params.debug) {
            console.log("\nDelete permissions from role: ");
        }
        try {
            for (let permission of systemPermissions.filter(sysPerm => boPermissions.some(boPerm => sysPerm.name === boPerm))) {
                await ProfilerApiWrapper.removePermissionFromRoleAsync(this.fullAccessRole.id, permission.id);
            }
        } catch (error) {
            throw "Could not remove permissions from full access role";
        }
        if (browser.params.debug) {
            console.log("\nDone removing permissions from role");
        }
    }

    /**
     * if addPermissionToRole true then assignPermissionToRole will be requested
     * if addPermissionToRole false then removePermissionFromRole will be requested
     *
     * @param {boolean} addPermissionToRole
     * @return {string}
     */
    private static updateRolePermissions(addPermissionToRole: boolean): promise.Promise<any> {
        const defer = protractor.promise.defer();
        const boPermissions = BackOfficePermissions.getBackOfficePermissions();
        const promises = [];

        ProfilerApiWrapper.getPermissions().then(systemPermissions => {
            systemPermissions.forEach((systemPermission) => {
                boPermissions.forEach((boPermissionName) => {
                    if (systemPermission.name === boPermissionName) {
                        promises.push(this.flow.execute(
                            (addPermissionToRole ? ProfilerApiWrapper.assignPermissionToRole : ProfilerApiWrapper.removePermissionFromRole)
                                .bind(ProfilerApiWrapper, this.fullAccessRole.id, systemPermission.id))
                        );
                    }
                });
            });
            protractor.promise.all(promises)
                .then((response) => defer.fulfill("Permissions updated successfully"))
                .catch((error) => defer.reject(error));
        });

        return defer.promise;
    }

}
