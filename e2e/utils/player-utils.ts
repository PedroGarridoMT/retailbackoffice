import * as moment from "moment";
import * as casual from "casual";
import { protractor, promise } from "protractor";
import { OperatorApiWrapper } from "../apis/operator-api.wrapper";
import { TestSupportApiWrapper } from "../apis/testsupport-api.wrapper";
import { ResourcesApiWrapper } from "../apis/resources-api.wrapper";

export class PlayerUtils {

    public static registerPlayer(player: any): promise.Promise<string> {
        return OperatorApiWrapper.registerPlayer(player);
    }

    public static async registerPlayerAsync(player: any): Promise<string> {
        return await OperatorApiWrapper.registerPlayerAsync(player);
    }

    /**
     *
     * @param {string} username
     * @param {string} password
     * @returns {{token: string}}
     */
    public static createSession(username: string, password: string): promise.Promise<any> {
        return OperatorApiWrapper.createPlayerSession(username, password);
    }

    public static async createSessionAsync(username: string, password: string): Promise<any> {
        return await OperatorApiWrapper.createPlayerSessionAsync(username, password);
    }

    /**
     *
     * @param {string} token
     * @param {Date} endDate
     * @returns {promise}
     */
    public static createSelfExclusionPeriod(token: string, endDate: Date): promise.Promise<any> {
        return OperatorApiWrapper.setSelfExclusionPeriod(token, endDate);
    }

    public static async createSelfExclusionPeriodAsync(token: string, endDate: Date): Promise<any> {
        return await OperatorApiWrapper.setSelfExclusionPeriodAsync(token, endDate);
    }

    public static activatePlayer(userName): promise.Promise<number> {
        return TestSupportApiWrapper.activatePlayer(userName);
    }

    public static async activatePlayerAsync(userName): Promise<number> {
        return await TestSupportApiWrapper.activatePlayerAsync(userName);
    }

    public static generatePlayerData(countryCode: string, seed: string, postalCode: string, currency: string): promise.Promise<any> {
        const defer = protractor.promise.defer();
        let player = null;
        ResourcesApiWrapper.getFiscalResidenceRegions().then(
            fiscalResidenceList => {
                ResourcesApiWrapper.getRegions(countryCode).then(
                    regionList => {
                        ResourcesApiWrapper.getCities(regionList[0].code).then(
                            (cityList) => {
                                const casualCasted: any = <any>casual;
                                player = casualCasted.PlayerData(seed, countryCode, fiscalResidenceList[0], regionList[0], cityList[0], postalCode, currency);
                                defer.fulfill(player);
                            }, (error) => defer.reject("Error getting cities"));
                    }, (error) => defer.reject("Error getting regions"));
            }, (error) => defer.reject("Error getting fiscal residences"));
        return defer.promise;
    }

    public static async generatePlayerDataAsync(countryCode: string, seed: string, postalCode: string, currency: string): Promise<any> {
        let fiscalResidenceList: any[];
        let regionList: any[];
        let cityList: any[];
        try {
            fiscalResidenceList = await ResourcesApiWrapper.getFiscalResidenceRegionsAsync();
        } catch (error) {
            console.error("Error getting fiscal residences");
            throw error;
        }
        try {
            regionList = await ResourcesApiWrapper.getRegionsAsync(countryCode);
        } catch (error) {
            console.error("Error getting regions");
            throw error;
        }
        try {
            cityList = await ResourcesApiWrapper.getCitiesAsync(regionList[0].code);
        } catch (error) {
            console.error("Error getting cities");
            throw error;
        }
        const player = (<any>casual).PlayerData(seed, countryCode, fiscalResidenceList[0], regionList[0], cityList[0], postalCode, currency);
        return player;
    }

}

function generateDni(num: number): string {
    return num + "TRWAGMYFPDXBNJZSQVHLCKE".charAt(num % 23);
}

casual.define("PlayerData", (stringSeed: string, countryCode: string, fiscalResidenceRegion: any, region: any, city: any, postalCode: string, currency: string): any => {
    const username = stringSeed + Date.now();
    const playerAge = casual.random_element([20, 25, 30, 35, 40, 45, 50, 55, 60]);
    const fname = casual.first_name;
    const lname = casual.last_name;

    return {
        userName: username,
        firstName: fname,
        lastName: lname,
        lastName2: casual.last_name,
        email: fname + "_" + lname + "@e2e.com",
        password: "Qwerty1!",
        personTitle: casual.random_element(["MR", "MRS"]),
        gender: casual.random_element(["M", "F"]),
        birthDate: moment().subtract(playerAge, "years").toISOString(),
        cityOfBirth: city.code,
        countryOfBirth: countryCode,//"ES or CO",
        documentType: "ID",
        documentNumber: generateDni(parseInt(casual.numerify("########"))),
        nationality: countryCode, //"ES or CO",
        fiscalResidenceCountry: countryCode,//"ES or CO",
        fiscalResidenceRegion: fiscalResidenceRegion.code,
        country: countryCode,//"ES or CO",
        language: "SPA",
        address: casual.street,
        addrNumber: casual.building_number,
        addrPostCode: postalCode,
        regionCode: region.code,
        regionText: region.description,
        cityCode: city.code,
        cityText: city.description,
        mobilePhoneCountryCode: "57",
        mobile: casual.numerify("#########"),
        securityQuestion: casual.word,
        securityQuestionAnswer: casual.sentence,
        acceptsTerms: true,
        acceptsReceivingEmails: casual.boolean,
        currency: currency,
    };
});
