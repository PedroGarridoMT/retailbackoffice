
export class BackOfficePermissions {

    private static permissionsArray: Array<string> = [
        "CampaignHome", "CampaignSearch",

        "PlayerHome", "PlayerSearch", "PlayerBalance", "PlayerDetailData", "PlayerDetailTransactions", "PlayerNotes",
        "PlayerDetailDocumentation", "PlayerDetailDocumentationRemove", "PlayerDetailDocumentationUpload",
        "PlayerChangeStatus", "PlayerEdit", "PlayerManualAdjustment", "PlayerRevokeSelfExclusion", "PlayerVerifyIdentity",
        "PlayerRejectIdentity", "PlayerAutoLimitsSearch", "PlayerAutoLimitsUpdate", "PlayerAutoLimitsHistorySearch",

        "OperatorLimits", "OperatorLimitsUpdate",

        "TransactionHome", "TransactionSearch", "TransactionSearchPendingWithdrawals",

        "SettingsHome",
    ];

    public static getBackOfficePermissions(): Array<string> {
        return this.permissionsArray;
    }

}
