let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {
    // Custom Types
    let idNumber = casual.numerify('########' + casual.letter.toUpperCase());
    let accountStatusCodes = ['Undefined', 'New', 'Confirmed', 'Activated', 'Blocked', 'Closed', 'Frozen', 'SecurityBlocked', 'Deactivated', 'Blacklisted'];
    let accountStatusReasonCodes = ['Undefined', 'NoCasino', 'NoSpain', 'NoMadrid', 'Forbidden', 'FraudDetected', 'WrongPassword', 'LostPassword', 'DocumentConfirmationExpired', 'Inactivity', 'UserRequest', 'Other', 'DocumentSendingTimeExpired', 'Minors', 'TemporarySuspendedOnUserRequest', 'InclusionInGeneralRegistry', 'IdentityFraud', 'ClosedDueToInactivityOrUserRequest', 'ViolationOfTOC', 'PaymentMethodsFraud', 'BonusAbuse', 'Others'];

    let generatePlayerWithStatus = function (accountStatusList) {
        let randomAccountStatus = accountStatusList && accountStatusList.length > 0 ? accountStatusList[Math.floor(Math.random() * accountStatusList.length)] : 'Activated';

        return casual.PlayerSummary(null, null, randomAccountStatus)
    };

    casual.define('PlayerSummary', function (accountCode, username, status) {
        return {
            playerId: casual.numerify('########'),
            accountCode: accountCode || casual.numerify('########'),
            username: username || casual.username,
            name1: casual.first_name,
            name2: casual.first_name,
            surname1: casual.last_name,
            surname2: casual.last_name,
            creationDate: casual.date(format = 'YYYY-MM-DD'),
            accountStatus: status || casual.random_element(accountStatusCodes),
            accountStatusReason: casual.random_element(accountStatusReasonCodes),
            verifiedIdentity: casual.boolean,
            selfExcluded: casual.boolean,
            receivePromotions: casual.boolean,
            provenanceCode: casual.random_element(['provenanceCode1', 'provenanceCode2', null]),
            promotionalCode: casual.random_element(['promCode1', 'promCode2', null])
        };
    });

    casual.define('PlayerDetailsResponse', function (accountCode, username, status) {
        return {
            playerId: casual.numerify('########'),
            accountCode: accountCode || casual.numerify('########'),
            username: username || casual.username,
            name1: casual.first_name,
            name2: casual.first_name,
            surname1: casual.last_name,
            surname2: casual.last_name,
            identityDocument: casual.IdentityDocument,
            creationDate: casual.date(format = 'YYYY-MM-DD'),
            birthDate: casual.date(format = 'YYYY-MM-DD'),
            gender: casual.random_element(['Male', 'Female']),
            nationality: 'CO',  //casual.country_code,
            fiscalResidence: casual.address,
            fiscalRegion: casual.city,
            residence: casual.ResidenceInfo,
            mobilePhoneNumber: casual.phone,
            homePhoneNumber: casual.phone,
            language: casual.random_element(['EN', 'ES']),
            accountStatus: status || casual.random_element(accountStatusCodes),
            accountStatusReason: casual.random_element(accountStatusReasonCodes),
            verifiedIdentity: casual.boolean,
            selfExcluded: casual.boolean,
            selfExclusionInfo: casual.SelfExclusionInfo,
            email: casual.email,
            securityQuestionAnswer: casual.sentence,
            receivePromotions: casual.boolean,
            provenanceCode: casual.random_element(['provenanceCode1', 'provenanceCode2', null]),
            promotionalCode: casual.random_element(['promCode1', 'promCode2', null]),
            registrationBonusCode: casual.random_element(['registrationBonusCode1', 'registrationBonusCode2', null]),
            referrer: casual.company_name
        };
    });

    casual.define('PlayerSearchResponse', function (pageSize, pageNumber, accountCode, username, accountStatusList) {
        let totalItems = (accountCode || username) ? 1 : 3 * pageSize;
        let status = accountStatusList ? accountStatusList[0] : casual.random_element(accountStatusCodes);

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: (accountCode || username) ?
                generatorHelper.generateArrayOf(1, casual.PlayerSummary(accountCode, username, status)) :
                generatorHelper.generateArrayOf(pageSize, generatePlayerWithStatus.bind(null, accountStatusList))
        };
    });

    casual.define('PlayerNotesResponse', function () {
        let numberOfNotes = casual.integer(from = 0, to = 15);
        return generatorHelper.generateArrayOf(numberOfNotes, () => { return casual.PlayerNote });
    });

    casual.define('PlayerNote', function () {
        return {
            id: Date.now(),
            author: casual.first_name + "_" + casual.last_name,
            text: casual.text,
            creationDate: casual.date(format = 'YYYY-MM-DD') + "T" + casual.time(format = 'HH:mm') + "Z"
        }
    });

    casual.define('SelfExclusionInfo', function () {
        return {
            from: casual.date(format = 'YYYY-MM-DD'),
            to: casual.date(format = 'YYYY-MM-DD'),
            reason: casual.sentence
        }
    });

    casual.define('PlayerWallet', function (walletType, balance) {
        if (balance == undefined) {
            balance = casual.numerify('####');
        }
        return {
            walletId: casual.numerify('######'),
            name: casual.words(n = 4),
            moneyType: walletType || casual.random_element(['Real', 'Bonus']),
            balance: balance
        }
    });

    casual.define('IdentityDocument', function () {
        return {
            type: 'NationalId', //casual.random_element(['ID', 'RES']),
            number: idNumber,
            issueDate: casual.date(format = 'YYYY-MM-DD'),
            issueCityCode: casual.random_element(generatorHelper.getCityCodeList()), // city_code
            expirationDate: casual.date(format = 'YYYY-MM-DD'),
            birthCityCode: casual.random_element(generatorHelper.getCityCodeList()), // city_code
            birthCountryCode: casual.country_code
        }
    });

    casual.define('ResidenceInfo', function () {
        return {
            countryCode: 'CO', //casual.country_code,
            regionCode: casual.random_element(generatorHelper.getRegionCodeList()), // region_code
            zipCode: casual.zip(),
            cityCode: casual.random_element(generatorHelper.getCityCodeList()), // city_code
            address: casual.address,
            addressNumber: casual.numerify('##')
        }
    });

    casual.define('ChangeStatusResponse', function (value) {
        return {
            value: value || casual.random_element(['ActivationEmailSent', 'StatusChanged'])
        }
    });
};
