let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {

    /**
     * Transaction Entity
     */
    casual.define('TransactionSummary', function (accountCode) {
        return {
            transactionId: casual.uuid,
            username: casual.username,
            accountCode: accountCode || casual.uuid,
            transactionDate: casual.date(format = 'YYYY-MM-DD'),
            transactionType: casual.random_element(['BonusGrant', 'BonusCancelation', 'PointsGrant', 'PointsExchange', 'Deposit', 'DepositCancelation', 'Withdrawal', 'WithdrawalCancelation', 'WithdrawalRejection', 'Bet', 'BetCancelation', 'Winning']),
            gameType: casual.random_element(['Casino', 'CasinoJackpot', 'PokerJackpot', 'SkillGames', 'Slots']),
            paymentMethod: casual.random_element(['Skrill', 'Paypal', 'ScratchCard', 'Cash', 'BankCard', 'BankTransfer', 'BankCardAtCashier', 'QA', 'ATM']),
            gameName: casual.random_element(['Blackjack', 'Multiplayer Blackjack', 'Big Bang', 'Spin a win', 'Fruit Shop']),
            amount: casual.double(from = 1, to = 1000),
            moneyType: casual.random_element(['Real', 'Bonus']),
            providerTransactionId: casual.uuid,
            couponId: casual.uuid,
        };
    });


    /**
     * Transaction Search Response
     */
    casual.define('TransactionSearchResponse', function (pageSize, pageNumber) {
        let totalItems = 3 * pageSize;

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: generatorHelper.generateArrayOf(pageSize, casual.TransactionSummary)
        };
    });

    /**
     * Withdrawal Entity
     */
    casual.define('WithdrawalSearchTransactionSummary', function (pageSize, pageNumber) {
        let totalItems = 6 * pageSize;
        return {
            transactionId: casual.uuid,
            username: casual.username,
            creationDate: casual.date(format = 'YYYY-MM-DD'),
            amount: casual.double(from = 1, to = 100000),
            currency: "COP",
            status: casual.random_element(['APPROVED', 'DONE', 'FAILED', 'WORKING', 'INTERNALERROR', 'PENDING', 'REJECTED', 'SUBMITTED', 'SUCCESS']),
            emailPaymentMethodAccount: casual.email,
            closedDate: casual.date(format = 'YYYY-MM-DD'),
            paymentMethod: "Paypal"
        };
    });

    /**
     * Withdrawal Response
     */
    casual.define('WithdrawalTransactionSearchResponse', function (pageSize, pageNumber) {
        let totalItems = 3 * pageSize;

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: generatorHelper.generateArrayOf(pageSize, casual.WithdrawalSearch)
        };
    });

    /**
     * Withdrawal Details Response
     */
    casual.define('WithdrawalTransactionDetails', function (transactionId) {
        return {
            username: casual.username,
            transactionId: transactionId,
            providerTransactionID: casual.integer(from = 0, to = 1000),
            accountCode: casual.uuid,
            creationDate: casual.date(format = 'YYYY-MM-DD'),
            amount: casual.integer(from = 0, to = 1000) + 'COP',
            status: casual.random_element(['Submitted', 'Working', 'Approved', 'Rejected', 'Done', 'Pending', 'Success', 'Failed', 'InternalFailed']),
            ip: casual.ip,
            bankAccount: casual.card_type,
            paymentMethodAccountEmail: casual.email,
            payerID: casual.integer(from = 0, to = 1000),
            closedDate: casual.date(format = 'YYYY-MM-DD'),
            paymentMethod: 'PayPal',
            waitingForApproval: casual.random_element(['None', 'Level1', 'Level2'])
        };
    });

};
