let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {

    /**
     * Resource Entity
     */
    casual.define('Resource', function (code, description) {
        return {
            code: code || casual.state_abbr,
            description: description || casual.city
        };
    });

};
