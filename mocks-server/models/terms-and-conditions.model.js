exports = module.exports = function (casual) {

    /**
     * TermsAndConditionsAccepted History
     */
    casual.define('TermsAndConditionsAccepted', function (accountCode) {
        return {
            termsAndConditionsAcceptedId: casual.uuid,
            username: casual.username,
            accountCode: accountCode || casual.uuid,
            acceptanceDate: casual.date(format = 'YYYY-MM-DD')
        };
    });
};
