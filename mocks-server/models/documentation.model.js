let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {
    let extensions = ['.jpg', '.png', '.pdf'];

    /**
     * Player Documentation entity
     */
    casual.define('Documentation', function (extension) {
        return {
            id: casual.uuid,
            documentName: casual.word + (extension || casual.random_element(extensions)),
            contentType: extension || casual.random_element(extensions),
            creationDate: casual.date(format = 'YYYY-MM-DD'),
            expirationDate: casual.date(format = 'YYYY-MM-DD')
        };
    });

    /**
     * Player Document entity
     */
    casual.define('PlayerDocument', function (content, extension) {
        return {
            fileName: casual.word + (extension || casual.random_element(extensions)),
            expirationDate: casual.date(format = 'YYYY-MM-DD'),
            content: content || generatorHelper.getImageBase64()
        };
    });
};
