let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {
    // Custom Types
    let idNumber = casual.numerify('########' + casual.letter.toUpperCase());

    /**
     * BannedPlayer
     */
    casual.define('BannedPlayer', function (accountCode) {
        return {
            bannedPlayerId: casual.uuid,
            accountCode: accountCode || casual.uuid,
            name1: casual.first_name,
            name2: casual.first_name,
            surname1: casual.last_name,
            surname2: casual.last_name,
            IDType: casual.random_element(['Passport', 'ID']),
            IDNumber: idNumber
        };
    });

    /**
     * BannedPlayerSearchResponse
     */
    casual.define('BannedPlayerSearchResponse', function (pageSize, pageNumber, accountCode) {
        let totalItems = 3 * pageSize;

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: generatorHelper.generateArrayOf(pageSize, casual.BannedPlayer)
        };
    });
};
