let generatorHelper = require("../helpers/generator.helper");

exports = module.exports = function (casual) {

    casual.define("PlayerBonus", function (accountCode) {

        const isRollover = casual.random_element([true, false]);
        const isBet = casual.random_element([true, false]);
        const moneyRedemptionProgressCurrentAmount = casual.integer(from = 0, to = 1000);
        const InitialNumberOfBetsToRollover = casual.integer(from = 5, to = 20);

        return {
            "bonusKey": casual.catch_phrase.toUpperCase().replace(/\s/g, ""),
            "displayName": casual.catch_phrase,
            "status": casual.random_element(["Active", "Pending"]),
            "initialBonusAmountAwarded": 0,
            "bonusBalance": casual.integer(from = 0, to = 50),
            "dateAwarded": casual.date(format = "YYYY-MM-DD"),
            "dateActivated": casual.date(format = "YYYY-MM-DD"),
            "dateClosed": casual.random_element([null, casual.date(format = "YYYY-MM-DD")]),
            "daysToExpiration": casual.integer(from = 0, to = 365),
            "moneyRedemptionCurrentValue": isRollover ? casual.integer(from = 0, to = moneyRedemptionProgressCurrentAmount) : null,
            "moneyRedemptionTargetValue": isRollover ? moneyRedemptionProgressCurrentAmount : null,
            "numberOfBetsRedemptionCurrentValue": isBet ? casual.integer(from = 0, to = InitialNumberOfBetsToRollover) : null,
            "numberOfBetsRedemptionTargetValue": isBet ? InitialNumberOfBetsToRollover : null
        };
    });

    casual.define("Bonus", function (id) {
        return {
            "id": casual.integer(from = 0, to = 10),
            "name": casual.catch_phrase.toUpperCase()
        };
    });

    casual.define("MassiveBonusResponseMessage", function () {
        return {
            "Amount": casual.integer(from = 5, to = 1000000),
            "UserIdentifier": casual.username
        };
    });

    casual.define("MassiveBonusResponse", function (username) {
        return {
            "clientId": casual.integer(from = 1, to = 3),
            "errorCode": casual.random_element(["UndefinedError", "DuplicatedUserOrInvalidSessionUID ", "InvalidUserIdentification", "Ok", "MultipleErrors"]),
            "username": casual.username
        };
    });

}
