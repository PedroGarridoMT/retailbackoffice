let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {

    /**
     * SelfExcludedPlayer
     */
    casual.define('SelfExclusionHistory', function (accountCode) {
        return {
            selfExclusionHistoryId: casual.uuid,
            accountCode: accountCode || casual.uuid,
            username: casual.username,
            requestDate: casual.date(format = 'YYYY-MM-DD'),
            startDate: casual.date(format = 'YYYY-MM-DD'),
            endDate: casual.date(format = 'YYYY-MM-DD'),
            closeDate: casual.date(format = 'YYYY-MM-DD'),
            selfExcluded: casual.boolean,
            operator: casual.boolean
        };
    });


    /**
     * SelfExclusionHistorySearchResponse
     */
    casual.define('SelfExclusionHistorySearchResponse', function (pageSize, pageNumber, accountCode) {
        let totalItems = 3 * pageSize;

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: generatorHelper.generateArrayOf(pageSize, casual.SelfExclusionHistory(accountCode))
        };
    });
};
