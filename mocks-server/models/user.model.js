exports = module.exports = function (casual) {

    let sessionPermissions = ['PlayerHome', 'PlayerSearch', 'PlayerDetail', 'PlayerDetailData', 'PlayerDetailDocumentation', 'PlayerDetailDocumentationRemove', 'PlayerDetailTransactions', 'PlayerBalance', 'PlayerDetailHistory', 'PlayerChangeStatus', 'PlayerEdit', 'PlayerVerifyIdentity', 'PlayerRejectIdentity', 'PlayerManualAdjustment', 'PlayerRevokeSelfExclusion', 'TransactionHome', 'TransactionSearch', 'SettingsHome', 'CampaignHome'];

    casual.define('LoginResponse', function (permissions) {
        return {
            username: casual.username,
            expiresIn: casual.integer(from = 3600, to = 60000), //number
            permissions: permissions || sessionPermissions,
        };
    });

    casual.define('LoginResponseError', function (error) {
        return {
            loginResult: error || "GenericError"
        };
    });

};
