let casual = require('casual');
let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {
    /**
     * BlacklistedPerson
     */
    casual.define('BlacklistedPerson', function (accountCode) {
        return {
            blacklistedPersonId:  casual.numerify('########'),
            documentType: casual.random_element(['Passport', 'NationalId']),
            documentNumber: casual.numerify('########' + casual.letter.toUpperCase()),
            username: casual.username.toLowerCase(),
            name:  casual.first_name + ' ' + casual.last_name,
            importDate: casual.date(format = 'YYYY-MM-DD')
        };
    });

    /**
     * BlacklistedPersonsSearchResponse
     */
    casual.define('BlacklistedPersonsSearchResponse', function (pageSize, pageNumber, accountCode) {
        let totalItems = 3 * pageSize;

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: generatorHelper.generateArrayOf(pageSize, casual.BlacklistedPerson)
        };
    });
};
