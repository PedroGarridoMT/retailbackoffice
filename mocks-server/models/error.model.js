exports = module.exports = function (casual) {

    casual.define('Error', function (errorCode, errorDescription) {
        return {
            errorCode: errorCode || casual.random_element(['GeneralError', 'OtherError']),
            errorDescription: errorDescription || casual.sentence
        };
    });

    casual.define('ErrorMessage', function (errorDescription) {
        return {
            message: errorDescription || casual.sentence
        };
    });
};
