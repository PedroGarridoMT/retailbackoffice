exports = module.exports = function (casual) {

    casual.define('GenericResponse', function (statusCode, isSuccess) {
        return {
            statusCode: statusCode || casual.random_element(['OK', 'OK_RESPONSE']),
            isSuccess: isSuccess || casual.boolean
        };
    });
};