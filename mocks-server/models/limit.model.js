let generatorHelper = require('../helpers/generator.helper');

exports = module.exports = function (casual) {

    let playerLimitChangeStatusEnum = ["Accepted", "Rejected", "Executed", "Submitted"];

    let generateRandomLimit = function (statusList, type) {
        let randomStatus = statusList && statusList.length > 0 ? statusList[Math.floor(Math.random() * statusList.length)] : 'Submitted';
        return casual.PlayerLimitChange(null, null, randomStatus, type)
    };

    /**
     * Player Limit
     */
    casual.define('Limit', function (type) {
        return {
            id: casual.uuid,
            limitType: type || casual.random_element(['Deposit']),
            values: casual.LimitValues
        };
    });

    casual.define('LimitValues', function(){
        return {
            dailyLimit: casual.double(from = 100000, to = 200000),
            weeklyLimit: casual.double(from = 200000, to = 300000),
            monthlyLimit: casual.double(from = 300000, to = 400000)
        }
    });

    /**
     * Player History Limit
     */
    casual.define('LimitHistory', function (limitHistoryId, status) {
        return {
            limitHistoryId: limitHistoryId || casual.uuid,
            newLimit: casual.LimitValues,
            requestDate: casual.date(format = 'YYYY-MM-DD'),
            closeDate: casual.date(format = 'YYYY-MM-DD'),
            status: status || casual.random_element(playerLimitChangeStatusEnum),
            validFromDate: casual.date(format = 'YYYY-MM-DD'),
            notes: casual.array_of_words(2),
        };
    });

    /**
     * PlayerLimitChange
     */
    casual.define('PlayerLimitChange', function (username, accountCode, status, type) {
        return {
            id: casual.uuid,
            accountCode: accountCode || casual.numerify('########'),
            username: username || casual.username,
            name: casual.first_name + ' ' + casual.first_name,
            surname: casual.last_name + ' ' + casual.last_name,
            requestedValues: casual.LimitValues,
            limitType: type || casual.random_element(['Bet', 'Deposit', 'Loss']),
            status: status || casual.random_element(playerLimitChangeStatusEnum),
            requestDate: casual.date(format = "YYYY-MM-DD"),
            closedDate: casual.date(format = "YYYY-MM-DD"),
            validFromDate: casual.date(format = "YYYY-MM-DD"),
            notes: casual.array_of_words(2),
        };
    });

    /**
     * LimitHistory Search Response
     */
    casual.define('LimitHistorySearchResponse', function (pageSize, pageNumber) {
        let totalItems = 3 * pageSize;

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: generatorHelper.generateArrayOf(pageSize, casual.LimitHistory)
        };
    });

    /**
     * PlayerLimitChangeSearchResponse
     */
    casual.define('PlayerLimitChangeSearchResponse', function (pageSize, pageNumber, username, accountCode, statusList, type) {
        let totalItems = 3 * pageSize;
        let status = statusList ? statusList[0] : casual.random_element(playerLimitChangeStatusEnum);

        return {
            totalPages: Math.ceil(totalItems / pageSize) || casual.integer(from = 0, to = 100),
            totalItems: totalItems,
            items: (accountCode || username) ?
                generatorHelper.generateArrayOf(1, casual.PlayerLimitChange(accountCode, username, status, type)) :
                generatorHelper.generateArrayOf(pageSize, generateRandomLimit.bind(null, statusList, type))
        };
    });
};
