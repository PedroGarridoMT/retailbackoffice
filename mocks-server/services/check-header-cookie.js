/**
 * Checks cookies header
 * @param req
 * @param res
 * @param next
 */
module.exports = function checkHeaderCookie(req, res, next) {
    if (req.method.toUpperCase() === 'OPTIONS') {
        next();
        return;
    }

    var xsrfTokenHeader = req.headers['x-xsrf-token'];
    if (xsrfTokenHeader) {
        next();
    }
    else {
        // Any Cookie header is provider -> 401
        return res.status(401).send({
            isSuccess: false,
            message: 'Cookie header and X-XSRF-TOKEN header should be provided!'
        });
    }
};
