/**
 * Checks Bearer token
 *
 * @param req
 * @param res
 * @param next
 */
module.exports = function checkAuthenticationToken(req, res, next) {
    if (req.method.toUpperCase() === 'OPTIONS') {
        next();
        return;
    }

    var token = req.headers['authorization'];
    if (token) {
        var tokenParts = token.split(' ');
        if (tokenParts.length < 2 || tokenParts[0] != 'Bearer') {
            // if the token has an invalid format, return a 401 error
            return res.status(401).json({isSuccess: false, message: 'Failed to authenticate token.'});
        }
        else {
            next();
        }
    }
    else {
        // if there is no token return an 401 error
        return res.status(401).send({isSuccess: false, message: 'No token provided'});
    }
};
