//Our responses stack, behaves as a FIFO stack
var responsesStack = [];

var printStack = function () {
    console.log("----------------------------------------------");
    if (responsesStack.length == 0) {
        console.log("|                 Empty                      |");
    }
    else {
        for (var i = 0; i < responsesStack.length; i++) {
            console.log("  " + i + ": '" + responsesStack[i].resource + "', '" + responsesStack[i].method + "', '" +
                responsesStack[i].response + "'");
        }
    }
    console.log("----------------------------------------------\n");
};

// Create new comment in your database and return its id
exports.addResponse = function (resource, method, response) {
    responsesStack.push({resource: resource, method: method, response: response});

    setTimeout(function () {
        console.log("\x1b[34m[Responses Stack]\x1b[0m New item added");
        printStack();
    }, 400);
};

exports.consumeResponse = function (resource, method) {

    var index, response = null;
    index = responsesStack.findIndex(function (item) {
        return (item.resource.toUpperCase() == resource.toUpperCase())
            && (item.method.toUpperCase() == method.toUpperCase())
    });

    if (index > -1) {
        var responseArray = responsesStack.splice(index, 1);
        response = responseArray[0];
        setTimeout(function () {
            console.log("\x1b[34m[Responses Stack]\x1b[0m Item consumed (" + response.resource + ", " + response.method + ", " + response.response + ")");
            printStack();
        }, 400);
    }

    return response;
};


exports.checkConfiguredResponsesWithNoLogic = function (resource, method) {
    let configured = this.consumeResponse(resource, method);
    if (configured) {
        console.log("There is a configured response but no logic to handle it.  Returning default response...");
    }
};