var responsesManager = require("../services/responses-manager");

//POST - Insert a new configured response into the stack of responses
exports.add = function (req, res) {
    if (!req.body) {
        console.warn('Body is empty: No response will be added to the stack');
    }
    else if (!req.body["resource"]) {
        console.warn('Resource is empty: No response will be added to the stack');
    }
    else if (!req.body["method"]) {
        console.warn('Method is empty: No response will be added to the stack');
    }
    else if (!req.body["response"]) {
        console.warn('Response is empty: No response will be added to the stack');
    }
    else {
        responsesManager.addResponse(req.body["resource"], req.body["method"], req.body["response"]);
    }

    res.status(200).json({});
};
