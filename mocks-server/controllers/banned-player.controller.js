let casual = require('casual');
let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');

/**
 * Get an ordered paged with a list of banned players
 * Example: GET http://localhost:9268/v1/players/banned?pageSize=3&pageNumber=4&sortBy=a&sortDirection=asc&accountCode=666a
 * @param req
 * @param res
 */
exports.getBannedPlayers = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }
    res.status(200).json(casual.BannedPlayerSearchResponse(req.query.pageSize, req.query.pageNumber, req.params.accountCode));
};

/**
 * Get a report with a list of banned players
 * Example: GET http://localhost:9268/v1/players/banned/report?accountCode=666&name1=paco
 * @param req
 * @param res
 */
exports.getBannedPlayersReport = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.BannedPlayer(req.params.accountCode)));
};

