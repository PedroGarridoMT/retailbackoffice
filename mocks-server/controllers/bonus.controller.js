let casual = require('casual');
let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');

/**
 * Return a list of currently active and pending bonuses for a specified player.
 * @param req
 * @param res
 */
exports.getPlayerBonuses = function (req, res) {
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.PlayerBonus));
};

/**
 * Cancel player's active or pending bonus
 * @param req
 * @param res
 */
exports.cancelPlayerBonus = function (req, res) {

    let configured = responsesManager.consumeResponse('player-bonus', 'DELETE');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "bad-request":
                return res.status(400).json(casual.Error('Bad request', 'The request is not well-formed or it is missing some required parameter'));
            case "unauthorized":
                return res.status(401).json(casual.Error('Unauthorized request', 'The token is not valid'));
            case "forbidden":
                return res.status(403).json(casual.Error('Forbidden', ''));
            case "not-found":
                return res.status(404).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "unprocessable":
                return res.status(422).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    return res.status(200).json(casual.GenericResponse('OK', true));
};

/**
 * Return a list of currently active and pending bonuses for all players.
 * @param req
 * @param res
 */
exports.getActiveBonuses = function (req, res) {
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.Bonus));
};

/**
 * Assign massive bonus to players.
 * @param req
 * @param res
 */
exports.assignMassiveBonus = function (req, res) {
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.MassiveBonusResponse));
};
