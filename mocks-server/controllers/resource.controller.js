let casual = require('casual');
let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');

let getCasualResources = function () {
    return generatorHelper.getResourceList().map(resourceCode => {
        return casual.Resource(resourceCode);
    });
};

let getCitiesByRegion = function (regionCode) {
    let cityList = [];
    generatorHelper.getCities().map(city => {
        if (regionCode === city.regionCode) {
            cityList.push(city);
        }
    });
    return cityList;
};

let getRegionByCity = function (cityCode) {
    let existingCity = generatorHelper.getCities().find(city => city.code == cityCode);

    if (existingCity) {
        return [casual.Resource(existingCity.regionCode, 'someRegion')];
    }
    else {
        return [casual.Resource('11', 'BOGOTÁ, D.C.')];
    }
};

let getAllFiscalRegions = function () {
    return [
        { code: "01", description: "Andalucía" },
        { code: "02", description: "Aragón" },
        { code: "03", description: "Asturias" },
        { code: "04", description: "Canarias" },
        { code: "05", description: "Cantabria" },
        { code: "06", description: "Castilla - La Mancha" },
        { code: "07", description: "Castilla y León" },
        { code: "08", description: "Cataluña" },
        { code: "09", description: "Extremadura" },
        { code: "10", description: "Galicia" },
        { code: "11", description: "Illes Balears" },
        { code: "12", description: "La Rioja" },
        { code: "13", description: "Madrid" },
        { code: "14", description: "Murcia" },
        { code: "15", description: "Valencia" },
        { code: "16", description: "Diputación Foral de Navarra" },
        { code: "17", description: "Diputación Foral de Álava" },
        { code: "18", description: "Diputación Foral de Guipúzcoa" },
        { code: "19", description: "Diputación Foral de Vizcaya" },
        { code: "20", description: "Ceuta" },
        { code: "21", description: "Melilla" },
        { code: "22", description: "No residentes" }
    ];
};

/**
 * Get the available cities for player residence in a given region.
 * Retrieves the list of available cities for player residence (e.g. Barcelona) in a given region. These cities are used during player registration.
 *
 * regionCode String The resource code of the region for which the list of cities has to be retrieved.
 * returns List
 **/
module.exports.getCities = function getCities(req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('cities', 'GET');

    if (!req.query.regionCode) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing regionCode param'));
    }

    res.status(200).json(getCitiesByRegion(req.query.regionCode));
};

/**
 * Get the available regions for player residence in a given country.
 * Retrieves the list of available player regions for player residence (e.g. Catalonia) in a given country. Each returned region is identified by its ISO 3166-2 alpha-2 code without the country code.
 *
 * countryCode String The ISO 3166-1 alpha-2 code of the country for which the list of regions has to be retrieved.
 * cityCode Optional parameter for getting a regionCode for a given cityCode.
 * returns List
 **/
module.exports.getRegions = function getRegions(req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('regions', 'GET');

    if (!req.query.countryCode) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing countryCode param'));
    }

    if (req.query.cityCode) {
        return res.status(200).json(getRegionByCity(req.query.cityCode)); // That should work as a regionCode
    }

    res.status(200).json(generatorHelper.getRegions());
};

/**
 * Get the list of available fiscal regions
 **/
module.exports.getFiscalRegions = function getFiscalRegions(req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('fiscal_residence_regions', 'GET');
    return res.status(200).json(getAllFiscalRegions());
}
