var casual = require('casual');
var responsesManager = require("../services/responses-manager");

//POST - Do login
exports.login = function (req, res) {
    var configured = responsesManager.consumeResponse('user', 'POST');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "invalid-credentials":
                return res.status(401).json(casual.LoginResponseError("InvalidCredentials"));
            case "login-attempts-exceeded":
                return res.status(422).json(casual.LoginResponseError("LoginAttemptsExceeded"));
            case "first-login":
                return res.status(422).json(casual.LoginResponse("PasswordNotSetFirstLogin"));
            case "password-expired":
                return res.status(422).json(casual.LoginResponse("PasswordExpired"));
        }
    }

    if (req.body["username"] === "admin" && req.body["password"] === "admin") {
        return res.json(casual.LoginResponse());
    }
    else {
        return res.status(401).json(casual.LoginResponseError("InvalidCredentials"));
    }
};

exports.changePassword = function (req, res) {
    var currentPassword = req.body["currentPassword"];
    var newPassword = req.body["newPassword"];
    var newPasswordRetyped = req.body["newPasswordRetyped"];

    if (!currentPassword && !newPassword && !newPasswordRetyped) {
        return res.status(412).json({isSuccess: false, statusCode: 'Bad request'});
    }
    if (!currentPassword) {
        return res.status(412).json({isSuccess: false, statusCode: 'CurrentPasswordIsMandatory'});
    }
    if (!newPassword) {
        return res.status(412).json({isSuccess: false, statusCode: 'NewPasswordIsMandatory'});
    }
    if (!newPasswordRetyped) {
        return res.status(412).json({isSuccess: false, statusCode: 'NewPasswordRetypedIsMandatory'});
    }
    if (newPassword.length < 6) {
        return res.status(412).json({isSuccess: false, statusCode: 'NewPasswordNotLongEnough'});
    }
    if (newPassword !== newPasswordRetyped) {
        return res.status(412).json({isSuccess: false, statusCode: 'NewPasswordMismatch'});
    }
    if (newPassword === currentPassword) {
        return res.status(412).json({isSuccess: false, statusCode: 'NewAndCurrentPasswordMustMismatch'});
    }

    res.status(200).json({isSuccess: true, statusCode: 'SuccessfullyChanged'});
};
