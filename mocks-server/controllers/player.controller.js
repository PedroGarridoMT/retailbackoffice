const casual = require("casual");
const responsesManager = require("../services/responses-manager");
const generatorHelper = require("../helpers/generator.helper");
const accountStatusCodes = ["Undefined", "New", "Confirmed", "Activated", "Blocked", "Closed", "Frozen", "SecurityBlocked", "Deactivated", "Blacklisted"];
const accountStatusReasonCodes = ["Undefined", "NoCasino", "NoSpain", "NoMadrid", "Forbidden", "FraudDetected", "WrongPassword", "LostPassword",
    "DocumentConfirmationExpired", "Inactivity", "UserRequest", "Other", "DocumentSendingTimeExpired", "Minors", "TemporarySuspendedOnUserRequest",
    "InclusionInGeneralRegistry", "IdentityFraud", "ClosedDueToInactivityOrUserRequest", "ViolationOfTOC", "PaymentMethodsFraud", "BonusAbuse", "Others"];
const playerColumnMap = new Map([
    ["playerId", "Player ID"],
    ["username", "Username"],
    ["accountCode", "Account code"],
    ["fullName", "Name"],
    ["fullSurname", "Surname"],
    ["creationDate", "Creation date"],
    ["accountStatus", "Account status"],
    ["accountStatusReason", "Account status reason"],
    ["verifiedIdentityString", "Verified identity"],
    ["selfExcludedString", "Self excluded"],
    ["receivePromotionsString", "Receive promotions"],
    ["provenanceCode", "Provenance code"],
    ["promotionalCode", "Promotional code"],
    ["balanceString", "Balance"],
]);

function generatePlayerMockForExport() {
    return {
        playerId: casual.numerify("########"),
        username: casual.username,
        accountCode: casual.numerify("########"),
        fullName: casual.first_name,
        fullSurname: casual.last_name,
        creationDate: casual.date(format = "YYYY-MM-DD"),
        accountStatus: casual.random_element(accountStatusCodes),
        accountStatusReason: casual.random_element(accountStatusReasonCodes),
        verifiedIdentityString: casual.random_element(["yes", "no"]),
        selfExcludedString: casual.random_element(["yes", "no"]),
        receivePromotionsString: casual.random_element(["yes", "no"]),
        provenanceCode: casual.random_element(["provenanceCode1", "provenanceCode2", null]),
        promotionalCode: casual.random_element(["promCode1", "promCode2", null]),
        balanceString: casual.numerify("####,##"),
    };
}

/**
 * GET - Return an ordered paged list of registered players that comply with the filter criteria
 * Example: GET http://localhost:9268/v1/players?searchRequest=ew0KICAicGFnZVNpemUiOiA1LA0KICAicGFnZU51bWJlciI6IDEsDQogICJzb3J0QnkiOiAidHJhbnNhY3Rpb25EYXRlIiwNCiAgInNvcnREaXJlY3Rpb24iOiAiREVTQyIsDQogICJmaWx0ZXJDcml0ZXJpYSI6IHsNCiAgICAiYWNjb3VudENvZGUiOiAiYWZkMTAxYjUtNzUxMi00YjM5LTk5ZTItYThhZGNjNzQ3YzMzIiwNCiAgICAidXNlcm5hbWUiOiAiRG9yaXNfUHJvaGFza2EiLA0KICAgICJuYW1lMSI6ICJaaXRhIiwNCiAgICAibmFtZTIiOiAiQXN0cmlkIiwNCiAgICAic3VybmFtZTEiOiAiU3Rva2VzIiwNCiAgICAic3VybmFtZTIiOiAiUnVzc2VsIiwNCiAgICAiSURUeXBlIjogIlBhc3Nwb3J0IiwNCiAgICAiSUROdW1iZXIiOiAiMjM3Njk2OTFRIiwNCiAgICAiSURJc3N1ZURhdGUiOiAiMTk3OS0wNy0wOCIsDQogICAgIklESXNzdWVQbGFjZSI6ICJNYWxheXNpYSIsDQogICAgIklERXhwaXJ5RGF0ZSI6ICIyMDA4LTA5LTI1IiwNCiAgICAiY3JlYXRpb25EYXRlRnJvbSI6ICIxOTcyLTAzLTA3IiwNCiAgICAiY3JlYXRpb25EYXRlVG8iOiAiMTk3Mi0wMy0wNyIsDQogICAgImJpcnRoRGF0ZUZyb20iOiAiMTk3Ny0wOC0yMiIsDQogICAgImJpcnRoRGF0ZVRvIjogIjE5NzctMDgtMjIiLA0KICAgICJiaXJ0aFBsYWNlIjogIlNpZXJyYSBMZW9uZSIsDQogICAgImdlbmRlciI6ICJmZW1hbGUiLA0KICAgICJuYXRpb25hbGl0eSI6ICJMdXhlbWJvdXJnIiwNCiAgICAidGF4UmVzaWRlbmNlIjogIjI0OTkgSHVkc29uIEZhbGwgTGFrZSBKZXNzaWthLCBBWiA3MDMzNy03OTM5IiwNCiAgICAidGF4UmVnaW9uIjogIldhbHNoaGF2ZW4iLA0KICAgICJjb3VudHJ5IjogIkxpZWNodGVuc3RlaW4iLA0KICAgICJwcm92aW5jZSI6ICJOb3J0aCBEYWtvdGEiLA0KICAgICJjaXR5IjogIldlc3QgQWJpZ2FsZWJvcm91Z2giLA0KICAgICJ6aXBDb2RlIjogIjc2Nzc5IiwNCiAgICAicmVzaWRlbmNlRGVwYXJ0bWVudCI6ICJDb2xvcmFkbyIsDQogICAgImhvbWVDaXR5IjogIkVhc3QgQ29sbGVlbiIsDQogICAgInBob25lTnVtYmVyMSI6ICIxMDQtMTgwLTc0NzEiLA0KICAgICJwaG9uZU51bWJlcjIiOiAiNjM3LTM0NC02ODE2IiwNCiAgICAiYWNjb3VudFN0YXR1cyI6ICJhY3RpdmUiLA0KICAgICJ2ZXJpZmllZElkZW50aXR5IjogZmFsc2UsDQogICAgInNlbGZFeGNsdWRlZCI6IGZhbHNlLA0KICAgICJiYWxhbmNlRnJvbSI6IDE1MCwNCiAgICAiYmFsYW5jZVRvIjogMjAwLA0KICAgICJsYXN0VHJhbnNhY3Rpb25EYXRlRnJvbSI6ICIyMDEyLTExLTI2IiwNCiAgICAibGFzdFRyYW5zYWN0aW9uRGF0ZVRvIjogIjIwMTItMTEtMjYiLA0KICAgICJlbWFpbCI6ICJWaXZpYW5lX0hhcnZleUB5YWhvby5jb20iLA0KICAgICJJUEFkZHJlc3MiOiAiMTk2LjQxLjI0NC43MCIsDQogICAgInJlY2VpdmVQcm9tb3Rpb25zIjogdHJ1ZSwNCiAgICAicHJvdmVuYW5jZUNvZGUiOiAicHJvdmVuYW5jZUNvZGUxIiwNCiAgICAicHJvbW90aW9uYWxDb2RlIjogInByb21Db2RlMiIsDQogICAgInJlZ2lzdHJhdGlvbkJvbnVzQ29kZSI6IG51bGwsDQogICAgInJlZmVycmVyIjogIlBvbGxpY2ggYW5kIFNvbnMiLA0KICAgICJtZW1iZXIiOiAiT25kcmlja2EgSW5jIg0KICB9DQp9
 * @param req
 * @param res
 */
exports.getPlayers = function (req, res) {
    var configured = responsesManager.consumeResponse('players', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "no-results":
                return res.json(casual.PlayerSearchResponse(0, 0));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }

    let decodedRequest;
    if (req.query.searchRequest) {
        try {
            decodedRequest = JSON.parse(Buffer.from(req.query.searchRequest, 'base64').toString());
        } catch (err) {
            return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
        }

        if (!decodedRequest.pageSize || !decodedRequest.pageNumber) {
            return res.status(400).json(casual.Error('GeneralError', 'Missing pagination params'));
        }
    } else {
        return res.status(400).json(casual.Error('GeneralError', 'Invalid searchRequest param'));
    }
    res.status(200).json(casual.PlayerSearchResponse(decodedRequest.pageSize, decodedRequest.pageNumber, decodedRequest.filterCriteria.accountCode, decodedRequest.filterCriteria.username, decodedRequest.filterCriteria.accountStatus));
};

/**
 * GET Player Detail
 * Example: GET http://localhost:9268/v1/players/44
 * @param req
 * @param res
 */
exports.getPlayerDetail = function (req, res) {
    let configured = responsesManager.consumeResponse('player-detail', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "self-excluded":
                return res.json(casual.PlayerDetailsResponse(req.params.accountCode, 'self-excluded'));
            case "blocked":
                return res.json(casual.PlayerDetailsResponse(req.params.accountCode, 'blocked'));
        }
    }
    res.status(200).json(casual.PlayerDetailsResponse(req.params.accountCode));
};

/**
 * Get a CSV report with a list of players that meets filter criteria and visible columns.
 * Example: GET http://localhost:9268/v1/players/report?filters=eyJhY2NvdW50U3RhdHVzIjpbIkFjdGl2YXRlZCJdfQ==&columnNames=accountCode,username,fullName,fullSurname,creationDate,accountStatus,balanceString
 * @param req
 * @param res
 */
exports.getPlayersReport = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic("players/report", "GET");
    if (!req.query.filters) {
        return res.status(400).json(casual.Error("GeneralError", "Missing params"));
    }
    try {
        const filters = JSON.parse(Buffer.from(req.query.filters, "base64").toString());
        let columnNames = [];
        if (req.query.columnNames && req.query.columnNames.length) {
            columnNames = req.query.columnNames.split(",");
        } else {
            for (var propertyName in entity = generatePlayerMockForExport()) {
                if (entity.hasOwnProperty(propertyName)) {
                    columnNames.push(propertyName);
                }
            }
        }
        const headerRow = columnNames.map((colName) => playerColumnMap.has(colName) ? playerColumnMap.get(colName) : colName);
        const rows = new Array(headerRow);
        rows.push(...generatorHelper.generateMatrixOfEntities(generatePlayerMockForExport, 15, columnNames, false));
        const fileName = "ExportPlayers_" + (new Date()).toJSON().slice(0, 10) + ".csv";
        const response = generatorHelper.getBase64CsvFromMatrix(rows, fileName);
        return res.status(200).json(response);
    }
    catch (err) {
        return res.status(400).json(casual.Error("GeneralError", err));
    }
}

/**
 * PUT - Update a register already exists
 * Example: PUT http://localhost:9268/v1/players/41578105
 * @param req
 * @param res
 */
exports.updatePlayer = function (req, res) {
    let configured = responsesManager.consumeResponse('player-detail', 'PUT');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    res.status(200).json(casual.GenericResponse('OK', true));
};

/**
 * Applies partial modifications to a given player.
 * @param req
 * @param res
 */
exports.patchPlayer = function (req, res) {
    handleGenericResponses('player-detail', 'PATCH', res);
};

/**
 * Gets all the balances (wallets / Campaigns) for a given player.
 * Example: GET http://localhost:9268/v1/players/12/wallets
 * @param req
 * @param res
 */
exports.getPlayerWallets = function (req, res) {
    let configured = responsesManager.consumeResponse('player-balance', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "zero-balance":
                return res.status(200).json([casual.PlayerWallet('Real', 0)]);
            case "no-results":
                return res.json(null);
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }

    // Create a unique real money wallet
    let realMoneyWallet = casual.PlayerWallet('Real');

    // Let's create [0 - 4] bonus money wallets
    let bonusWallets = casual.integer(from = 0, to = 3);
    let bonusMoneyWallets = [];
    if (bonusWallets > 0) {
        for (let i = 0; i < bonusWallets; i++) {
            bonusMoneyWallets.push(casual.PlayerWallet('Bonus'))
        }
    }

    // Return all generated wallets
    res.status(200).json([realMoneyWallet, ...bonusMoneyWallets]);

};

/**
 * Updates the status of a given player.
 * @param req
 * @param res
 */
exports.updatePlayerStatus = function (req, res) {
    handleGenericResponses('player-status', 'PUT', res);
};

/**
 * Get a list of notes associated to a player, given his account code.
 * @param req
 * @param res
 */
exports.getPlayerNotes = function (req, res) {
    let configured = responsesManager.consumeResponse('player-notes', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    res.status(200).json(casual.PlayerNotesResponse);
};

exports.addPlayerNote = function (req, res) {
    let configured = responsesManager.consumeResponse('player-notes', 'POST');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "bad-request":
                return res.status(400).json(casual.Error('Bad request', 'The request is not well-formed or it is missing some required parameter'));
            case "unprocessable-entity":
                return res.status(422).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }

    // Default LoginResponse (No configuredRoleResponse applied): It returns all permissions
    return res.status(200).json(casual.GenericResponse('OK', true));
};

exports.deletePlayerNote = function (req, res) {
    let configured = responsesManager.consumeResponse('player-notes', 'DELETE');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "bad-request":
                return res.status(400).json(casual.Error('Bad request', 'The request is not well-formed or it is missing some required parameter'));
            case "unauthorized":
                return res.status(401).json(casual.Error('Unauthorized request', 'The token is not valid'));
            case "forbidden":
                return res.status(403).json(casual.Error('Forbidden', ''));
            case "not-found":
                return res.status(404).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    return res.status(200).json(casual.GenericResponse('OK', true));
};

exports.patchPlayerNote = function (req, res) {
    let configured = responsesManager.consumeResponse('player-notes', 'PATCH');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "bad-request":
                return res.status(400).json(casual.Error('Bad request', 'The request is not well-formed or it is missing some required parameter'));
            case "unauthorized":
                return res.status(401).json(casual.Error('Unauthorized request.', 'The token is not valid'));
            case "forbidden":
                return res.status(403).json(casual.Error('Forbidden', ''));
            case "unprocessable":
                return res.status(422).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    return res.status(200).json(casual.GenericResponse('OK', true));
};

/**
 * Get an ordered paged with a list of black-listed persons
 * @param req
 * @param res
 */
exports.getBlacklistedPersons = function (req, res) {
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }
    res.status(200).json(casual.BlacklistedPersonsSearchResponse(req.query.pageSize, req.query.pageNumber, req.params.accountCode));
};

/**
 * Add persons to blacklist
 * @param req
 * @param res
 */
exports.addBlacklistedPersons = function (req, res) {
    let configured = responsesManager.consumeResponse('players-blacklist', 'POST');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "bad-request":
                return res.status(400).json(casual.Error('Bad request', 'The request is not well-formed or it is missing some required parameter'));
            case "unauthorized":
                return res.status(401).json(casual.Error('Unauthorized request', 'The token is not valid'));
            case "forbidden":
                return res.status(403).json(casual.Error('Forbidden', ''));
            case "not-found":
                return res.status(404).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    return res.status(200).json(casual.GenericResponse('OK', true));
};


let handleGenericResponses = function (resource, method, res) {
    let configured = responsesManager.consumeResponse(resource, method);
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "email":
                return res.status(200).json(casual.ChangeStatusResponse('ActivationEmailSent'));
            case "bad-request":
                return res.status(400).json(casual.Error('Bad request', 'The request is not well-formed or it is missing some required parameter'));
            case "unprocessable-entity":
                return res.status(422).json(casual.Error('Unprocessable entity', 'The request was well-formed but was unable to be followed due to semantic errors'));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
};
