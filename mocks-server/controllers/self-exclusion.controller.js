let casual = require('casual');
let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');

/**
 * Get an ordered paged of history of self-excluded status for a player
 * Example: GET http://localhost:9268/v1/players/44/self-exclusion-history?pageSize=3&pageNumber=4&sortBy=asd
 * @param req
 * @param res
 */
exports.getPlayerSelfExclusionHistory = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }
    res.status(200).json(casual.SelfExclusionHistorySearchResponse(req.query.pageSize, req.query.pageNumber, req.params.accountCode));
};

/**
 * Get an ordered paged of history of self-excluded status for all players
 * Example: GET http://localhost:9268/v1/self-exclusion-history?pageSize=3&pageNumber=4&sortBy=date&sortDirection=ASC&accountCode=iedmdmf2
 * @param req
 * @param res
 */
exports.getSelfExclusionHistory = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }
    res.status(200).json(casual.SelfExclusionHistorySearchResponse(req.query.pageSize, req.query.pageNumber));
};

/**
 * Get a report with a list of self-excluded players
 * Example: GET http://localhost:9268/v1/self-exclusion-history/report?accountCode=4434&username=paco
 * @param req
 * @param res
 */
exports.getSelfExclusionHistoryReport = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.SelfExclusionHistory));
};
