let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');
let casual = require('casual');


/**
 * Get the available game types.
 * Retrieves the list of available game Types. The possible values are: '#/definitions/GameTypeEnum'.
 *
 * returns List
 **/
module.exports.getGameTypes = function (req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('gameTypes', 'GET');
    res.status(200).json(generatorHelper.getGameTypes());
};

/**
 * Get the available payment methods.
 * Retrieves the list of available payment methods. The possible values are: '#/definitions/PaymentMethodEnum'
 *
 * returns List
 **/
module.exports.getPaymentMethods = function (req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('paymentMethod', 'GET');
    //Operation type is mandatory.
    if (req.query["operationType"]) {

        //Transaction type is optional
        if (req.query["transactionType"]) {
            if (req.query["transactionType"] == 'Deposit') {
                return res.status(200).json(generatorHelper.getPaymentMethods('Deposit'));
            }
            else if (req.query["transactionType"] == 'Withdrawal') {
                return res.status(200).json(generatorHelper.getPaymentMethods('Withdrawal'));
            }
        }
        return res.status(200).json(generatorHelper.getPaymentMethods());
    }
    else {
        return res.status(400).json(casual.Error('BadRequest', 'Missing operationType param'))
    }
};

/**
 * Get the available payment methods.
 * Retrieves the list of available payment methods. The possible values are: '#/definitions/PaymentMethodEnum'
 *
 * returns List
 **/
exports.getStatusPermissions = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('statusPermissions', 'GET');
    res.status(200).json(generatorHelper.getPermissionsByStatus());
};

/**
 * Get the available transaction types.
 * Retrieves the list of available transaction Types. The possible values are: '#/definitions/TransactionTypeEnum'
 *
 * returns List
 **/
module.exports.getTransactionTypes = function (req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('transactionType', 'GET');
    res.status(200).json(generatorHelper.getTransactionTypes());
};

/**
 * Get the available transaction types.
 * Retrieves the list of available transaction Types. The possible values are: '#/definitions/TransactionTypeEnum'
 *
 * returns List
 **/
module.exports.getEnvironmentVariables = function (req, res, next) {
    responsesManager.checkConfiguredResponsesWithNoLogic('environmentVariables', 'GET');
    res.status(200).json(generatorHelper.getEnvironmentVariables());
};
