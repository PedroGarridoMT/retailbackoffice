var casual = require('casual');
var responsesManager = require("../services/responses-manager");

let adminMTPermissions = ['PlayerHome', 'PlayerSearch', 'PlayerDetailData', 'PlayerBalance', 'PlayerDetailHistory', 'PlayerChangeStatus', 'PlayerEdit', 'PlayerVerifyIdentity', 'PlayerRejectIdentity', 'PlayerManualAdjustment', 'PlayerRevokeSelfExclusion', 'TransactionHome', 'TransactionSearch', 'SettingsHome', 'CampaignHome'];
let adminPermissions = ['PlayerHome', 'PlayerSearch', 'PlayerDetailData', 'PlayerBalance', 'PlayerDetailHistory', 'PlayerChangeStatus', 'PlayerEdit', 'PlayerVerifyIdentity', 'PlayerRejectIdentity', 'PlayerManualAdjustment', 'PlayerRevokeSelfExclusion', 'TransactionHome', 'TransactionSearch', 'SettingsHome', 'CampaignHome'];
let financialPermissions = ['TransactionHome', 'TransactionSearch', 'CampaignHome'];
let legalPermissions = ['PlayerHome', 'PlayerSearch', 'PlayerDetailData', 'PlayerBalance', 'PlayerDetailHistory', 'TransactionHome', 'TransactionSearch', 'CampaignHome'];
let marketingPermissions = ['CampaignHome'];
let wannabetPermissions = ['PlayerHome', 'PlayerSearch', 'PlayerDetailData', 'PlayerBalance'];
let certificationPermissions = ['PlayerHome', 'PlayerSearch'];

/**
 * Login
 * @param req
 * @param res
 */
exports.loginUser = function (req, res) {
    let configuredErrorResponse = responsesManager.consumeResponse('session', 'POST');
    if (configuredErrorResponse) {
        switch (configuredErrorResponse["response"].toLowerCase()) {
            case "invalid-credentials":
                return res.status(401).json(casual.ErrorMessage("InvalidCredentials"));
            case "login-attempts-exceeded":
                return res.status(422).json(casual.Error("LoginAttemptsExceeded"));
            case "first-login":
                return res.status(422).json(casual.Error("PasswordNotSetFirstLogin"));
            case "password-expired":
                return res.status(422).json(casual.Error("PasswordExpired"));
        }
    }

    if (req.body["username"] === "Admin" && req.body["password"] === "Admin") {
        let configuredRoleResponse = responsesManager.consumeResponse('session-role', 'POST');
        if (configuredRoleResponse) {
            switch (configuredRoleResponse["response"].toLowerCase()) {
                case "admin-mt":
                    return res.status(200).json(casual.LoginResponse(adminMTPermissions));
                case "admin":
                    return res.status(200).json(casual.LoginResponse(adminPermissions));
                case "financial":
                    return res.status(200).json(casual.LoginResponse(financialPermissions));
                case "legal":
                    return res.status(200).json(casual.LoginResponse(legalPermissions));
                case "marketing":
                    return res.status(200).json(casual.LoginResponse(marketingPermissions));
                case "wannabet":
                    return res.status(200).json(casual.LoginResponse(wannabetPermissions));
                case "certification":
                    return res.status(200).json(casual.LoginResponse(certificationPermissions));
                case "none":
                    return res.status(200).json(casual.LoginResponse([]));
            }
        }

        // Default LoginResponse (No configuredRoleResponse applied): It returns all permissions
        return res.status(200).json(casual.LoginResponse());
    }
    else {
        return res.status(401).json(casual.ErrorMessage("Invalid Credentials"));
    }
};

/**
 * Logout
 * @param req
 * @param res
 */
exports.logoutUser = function (req, res) {
    var configured = responsesManager.consumeResponse('session', 'DELETE');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "invalid-credentials":
                return res.status(401).json(casual.Error("InvalidCredentials"));
        }
    }

    return res.status(200).json();
};

/**
 * refreshToken / patchSession
 * @param req
 * @param res
 */
exports.refreshToken = function (req, res) {
    var configured = responsesManager.consumeResponse('session', 'PATCH');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "invalid-credentials":
                return res.status(401).json(casual.Error("InvalidCredentials"));
        }
    }

    return res.status(200).json(casual.LoginResponse());
};
