let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');
let casual = require('casual');

module.exports.deletePlayerDocument = function (args, res, next) {
    /**
     * Remove some existing player personal document, given the player account code and the document ID.
     * Remove some existing player personal document, given the player account code and the document ID.  This operation requires the user to have the `PlayerDetailDocumentationRemove` permission assigned. If the user does not have the permission a `Forbidden (403)` response will be returned.
     *
     * accountCode String Account code.
     * documentId Integer The identifier of player document.
     * no response value expected for this operation
     **/
    var configured = responsesManager.consumeResponse('player-document', 'DELETE');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
            case "not-found":
                return res.status(404).json(casual.Error('Server error', 'Resource not found'));
        }
    }
    res.status(200).json();
};

/**
 * Get the document file, given the player account code and the document ID.
 * Get the document file, given the player account code and the document ID.  This operation requires the user to have the `PlayerDetailDocumentation` permission assigned. If the user does not have the permission a `Forbidden (403)` response will be returned.
 *
 * accountCode String Account code.
 * documentId Integer The identifier of player document.
 * returns PlayerDocument
 **/
module.exports.getPlayerDocument = function (args, res) {
    var configured = responsesManager.consumeResponse('player-document', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
            case "not-found":
                return res.status(404).json(casual.Error('Server error', 'Resource not found'));
            case "png-document":
                return res.status(200).json(casual.PlayerDocument(generatorHelper.getPngBase64(), '.png'));
            case "pdf-document":
                return res.status(200).json(casual.PlayerDocument(generatorHelper.getPdfBase64(), '.pdf'));
        }
    }
    res.status(200).json(casual.PlayerDocument(null, null));
};

/**
 * Get a list of the uploaded documents, given the account code of the player.
 * Get a list of the uploaded documents, given the account code of the player.  This operation requires the user to have the `PlayerDetailDocumentation` permission assigned. If the user does not have the permission a `Forbidden (403)` response will be returned.
 *
 * accountCode String Account code.
 * returns List
 */
module.exports.getPlayerDocumentation = function getPlayerDocumentation(req, res) {
    var configured = responsesManager.consumeResponse('player-documentation', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "no-results":
                return res.json(generatorHelper.generateArrayOf(0, casual.Documentation));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
            case "jpg-docs":
                return res.status(200).json(generatorHelper.generateArrayOf(10, generateDocumentationEntity.bind(null, '.jpg')));
            case "png-docs":
                return res.status(200).json(generatorHelper.generateArrayOf(10, generateDocumentationEntity.bind(null, '.png')));
            case "pdf-docs":
                return res.status(200).json(generatorHelper.generateArrayOf(10, generateDocumentationEntity.bind(null, '.pdf')));
        }
    }
    res.status(200).json(generatorHelper.generateArrayOf(10, generateDocumentationEntity.bind(null, null)));
};

/**
 * Post a new player document.
 * Upload a player document.
 *
 * accountCode String Account code of the player.
 * formData File The player document file to upload. The corresponding HTTP request will include multipart/form-data payload.
 * no response value expected for this operation
 **/
module.exports.uploadPlayerDocument = function (args, res) {
    var configured = responsesManager.consumeResponse('player-document', 'POST');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
            case "not-found":
                return res.status(404).json(casual.Error('Server error', 'Resource not found'));
        }
    }
    res.status(200).json();
};

let generateDocumentationEntity = function (extension) {
    return casual.Documentation(extension)
};
