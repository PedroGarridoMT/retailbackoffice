let casual = require('casual');
let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');

/**
 * GET - Return array of Limit
 * Example: GET localhost:9268/v1/players/666/limits-operator
 *
 * @param req
 * @param res
 */
exports.getOperatorLimits = function getLimits(req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('limits', 'GET');
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.Limit));
};

/**
 * Get a list of Limits for a player
 * Gets a list of Limits for a specified player
 * Example: GET localhost:9268/v1/players/666/limits
 *
 * @param req
 * @param res
 */
exports.getPlayerLimits = function getPlayerLimits(req, res) {

    var configured = responsesManager.consumeResponse('players-limits', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "no-results":
                return res.json(generatorHelper.generateArrayOf(0, casual.Limit));
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    res.status(200).json(generatorHelper.generateArrayOf(3, casual.Limit));
};

/**
 * Get an ordered paged list of Limits History
 * Example: GET http://localhost:9268/v1/players/334/limits-history?pageSize=4&pageNumber=4
 * @param req
 * @param res
 */
exports.getPlayerLimitsHistory = function getPlayerLimitsHistory(req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }

    res.status(200).json(casual.LimitHistorySearchResponse(req.query.pageSize, req.query.pageNumber));
};

/**
 * PUT for a specified PlayerLimitIdD
 *  the status and notes of an existing change request of player limits.
 *
 * PUT http://localhost:92687v1/players/limits/changes/666
 * @param req
 * @param res
 */
exports.updatePlayerLimitChange = function approveLimit(req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic("player-limits", "PUT");
    if (!req.params.limitChangeId || !req.body.notes || req.body.approve === undefined) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }
    res.status(200).json({ "statusCode": "Ok", "isSuccess": true });
};

/**
 * Get an ordered, paged list of change requests on player limits
 * Example: http://localhost:9268/v1/players/limits/changes?username=paco&accountCode=344&requestDate=29-03-1988&pageSize=3&pageNumber=4
 * @param req
 * @param res
 */
exports.getPlayerLimitsChanges = function getPlayerLimitsChanges(req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic("limits", "GET");
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error("GeneralError", "Missing params"));
    }

    res.status(200).json(casual.PlayerLimitChangeSearchResponse(req.query.pageSize, req.query.pageNumber,
        req.query.username, req.query.accountCode, (req.query.status ? req.query.status.split(',') : null), req.query.type));
};
