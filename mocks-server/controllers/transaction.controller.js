const casual = require("casual");
const responsesManager = require("../services/responses-manager");
const generatorHelper = require("../helpers/generator.helper");
const transactionTypeCodes = ["BonusGrant", "BonusCancelation", "PointsGrant", "PointsExchange", "Deposit", "DepositCancelation", "Withdrawal",
    "WithdrawalCancelation", "WithdrawalRejection", "Bet", "BetCancelation", "Winning"];
const gameTypeCodes = ["Casino", "CasinoJackpot", "PokerJackpot", "SkillGames", "Slots"];
const gameNameCodes = ["Blackjack", "Multiplayer Blackjack", "Big Bang", "Spin a win", "Fruit Shop"];
const paymentMethodCodes = ["Skrill", "Paypal", "ScratchCard", "Cash", "BankCard", "BankTransfer", "BankCardAtCashier", "QA", "ATM"];
const moneyTypeCodes = ["Real", "Bonus"];
const transactionColumnMap = new Map([
    ["transactionId", "Transaction ID"],
    ["username", "Username"],
    ["accountCode", "Account code"],
    ["transactionDate", "Transaction date"],
    ["transactionType", "Transaction type"],
    ["gameType", "Game type"],
    ["gameName", "Game name"],
    ["paymentMethod", "Payment method"],
    ["amountString", "Amount"],
    ["moneyType", "Money type"],
    ["providerTransactionId", "Provider Transaction ID"],
    ["couponId", "Coupon ID"],
]);

function generatePlayerTransactionMockForExport(accountCode, username) {
    return {
        transactionId: casual.uuid,
        username: username || casual.username,
        accountCode: accountCode || casual.uuid,
        transactionDate: casual.date(format = "YYYY-MM-DD"),
        transactionType: casual.random_element(transactionTypeCodes),
        gameType: casual.random_element(gameTypeCodes),
        gameName: casual.random_element(gameNameCodes),
        paymentMethod: casual.random_element(paymentMethodCodes),
        amountString: casual.numerify("####,##"),
        moneyType: casual.random_element(moneyTypeCodes),
        providerTransactionId: casual.uuid,
        couponId: casual.uuid,
    };
}

function generateTransactionMockForExport() {
    return {
        transactionId: casual.uuid,
        username: casual.username,
        accountCode: casual.uuid,
        transactionDate: casual.date(format = "YYYY-MM-DD"),
        transactionType: casual.random_element(transactionTypeCodes),
        gameType: casual.random_element(gameTypeCodes),
        gameName: casual.random_element(gameNameCodes),
        paymentMethod: casual.random_element(paymentMethodCodes),
        amountString: casual.numerify("####,##"),
        moneyType: casual.random_element(moneyTypeCodes),
        providerTransactionId: casual.uuid,
        couponId: casual.uuid,
    };
}

/**
 * GET - Return a history of player transactions according to the accountCode
 * Example: GET http://localhost:9268/v1/players/666/transactions?pageSize=3&pageNumber=54&sortBy=frf&sortDirection=ASC
 * @param req
 * @param res
 */
exports.getPlayerTransactions = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('player-transactions', 'GET');
    if (!req.params.accountCode) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing params'));
    }
    res.status(200).json(casual.TransactionSearchResponse(req.query.pageSize, req.query.pageNumber));
};

/**
 * Get a CSV report with a list of player transactions that meets filter criteria and visible columns.
 * Example: GET http://localhost:9268/v1/players/93251ae292f24006/transactions/report?transactionType=Deposit&columnNames=transactionId,transactionDate,amountString,moneyType,transactionType,paymentMethod
 * @param req
 * @param res
 */
exports.getPlayerTransactionsReport = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic("player-transactions/report", "GET");
    if (!req.params.accountCode) {
        return res.status(400).json(casual.Error("GeneralError", "Missing params"));
    }
    try {
        let columnNames = [];
        if (req.query.columnNames && req.query.columnNames.length) {
            columnNames = req.query.columnNames.split(",");
        } else {
            for (var propertyName in entity = generatePlayerTransactionMockForExport(req.params.accountCode, "MockUsername")) {
                if (entity.hasOwnProperty(propertyName)) {
                    columnNames.push(propertyName);
                }
            }
        }
        const headerRow = columnNames.map((colName) => transactionColumnMap.has(colName) ? transactionColumnMap.get(colName) : colName);
        const rows = new Array(headerRow);
        rows.push(...generatorHelper.generateMatrixOfEntities(generatePlayerTransactionMockForExport.bind(this, req.params.accountCode, "MockUsername"), 15, columnNames, false));
        const fileName = "ExportTransactions_MockUsername_" + (new Date()).toJSON().slice(0, 10) + ".csv";
        const response = generatorHelper.getBase64CsvFromMatrix(rows, fileName);
        return res.status(200).json(response);
    }
    catch (err) {
        return res.status(400).json(casual.Error("GeneralError", err));
    }
}

/**
 * GET - Return all transactions
 * Example: GET http://localhost:9268/v1/transactions?pageSize=4&pageNumber=4&sortBy=ACS&sortDirection=ASC
 * @param req
 * @param res
 */
exports.getTransactions = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('transactions', 'GET');
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing pagination params'));
    }
    res.status(200).json(casual.TransactionSearchResponse(req.query.pageSize, req.query.pageNumber));
};

/**
 * Get a CSV report with a list of transactions that meets filter criteria and visible columns.
 * Example: GET http://localhost:9268/v1/transactions/report?transactionType=Deposit&columnNames=transactionId,transactionDate,amountString,moneyType,transactionType,paymentMethod
 * @param req
 * @param res
 */
exports.getTransactionsReport = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic("transactions/report", "GET");
    try {
        let columnNames = [];
        if (req.query.columnNames && req.query.columnNames.length) {
            columnNames = req.query.columnNames.split(",");
        } else {
            for (var propertyName in entity = generateTransactionMockForExport()) {
                if (entity.hasOwnProperty(propertyName)) {
                    columnNames.push(propertyName);
                }
            }
        }
        const headerRow = columnNames.map((colName) => transactionColumnMap.has(colName) ? transactionColumnMap.get(colName) : colName);
        const rows = new Array(headerRow);
        rows.push(...generatorHelper.generateMatrixOfEntities(generateTransactionMockForExport, 15, columnNames, false));
        const fileName = "ExportTransactions_" + (new Date()).toJSON().slice(0, 10) + ".csv";
        const response = generatorHelper.getBase64CsvFromMatrix(rows, fileName);
        return res.status(200).json(response);
    }
    catch (err) {
        return res.status(400).json(casual.Error("GeneralError", err));
    }
};

/**
 * GET - Search withdrawals by input parameters
 * Example: GET http://localhost:9268/v1/transactions/withdrawals?pageSize=4&pageNumber=4&sortBy=ACS&sortDirection=ASC
 * @param req
 * @param res
 */
exports.searchWithdrawals = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('withdrawals', 'GET');
    if (!req.query.pageSize || !req.query.pageNumber) {
        return res.status(400).json(casual.Error('GeneralError', 'Missing pagination params'));
    }
    res.status(200).json(casual.WithdrawalTransactionSearchResponse(req.query.pageSize, req.query.pageNumber));
};

/**
 * Post a manual adjustment transaction for a player
 * Create a new manual transaction for a given player.
 *
 * accountCode String Account code of the player.
 * request ManualTransactionRequest The manual adjustment payload request.
 * returns GenericResponse
 **/
exports.manualAdjustment = function (req, res) {
    let configured = responsesManager.consumeResponse('player-transactions', 'POST');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    res.status(200).json(casual.GenericResponse('OK', true));
};

/**
 * GET Withdrawal Details
 * Example: GET http://localhost:9268/v1/transactions/withdrawals/44
 * @param req
 * @param res
 */
exports.getWithdrawalDetails = function (req, res) {
    let configured = responsesManager.consumeResponse('withdrawal-details', 'GET');
    if (configured) {
        switch (configured["response"].toLowerCase()) {
            case "server-error":
                return res.status(500).json(casual.Error('Server error', 'Mocked error received from the server'));
        }
    }
    res.status(200).json(casual.WithdrawalTransactionDetails(req.params.transactionId));
};

/**
 * Patches the witdrawal with approval for level 1, approval for level 2 or rejection.
 * @param req
 * @param res
 */
exports.patchWithdrawal = function (req, res) {
    res.status(200).json(casual.GenericResponse('OK', true));
};

/**
 * Get a SEPA report with a list of withdrawals between two dates.
 * Example: GET http://localhost:9268/v1/transactions/sepa?transactionDateFrom=2016-05-25T14:13:27Z&transactionDateTo=2018-01-26T23:00:00Z
 * @param req
 * @param res
 */
exports.exportSepaFile = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic("transactions/sepa", "GET");
    try {
        if (!req.query.transactionDateFrom || !req.query.transactionDateTo) {
            return res.status(400).json(casual.Error("GeneralError", "Missing params"));
        }
        const fileName = (new Date()).toJSON().slice(0, 10) + ".tra.sepa";
        const response = generatorHelper.getBase64DownloadableFile(generatorHelper.getSepaXml(), fileName);
        return res.status(200).json(response);
    }
    catch (err) {
        return res.status(400).json(casual.Error("GeneralError", err));
    }
};
