let casual = require('casual');
let responsesManager = require("../services/responses-manager");
let generatorHelper = require('../helpers/generator.helper');

/**
 * Get a history of players with a terms and conditions acceptance date
 * Example: GET http://localhost:9268/v1/players/terms-and-conditions/acceptance?username=name&accountCode=asdf
 * @param req
 * @param res
 */
exports.getTermsAndConditionsAccepted = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.TermsAndConditionsAccepted(req.params.accountCode)));
};

/**
 * Get a report with a list of players with a terms and conditions acceptance date
 * Example: GET http://localhost:9268/v1/players/terms-and-conditions/acceptance/report?username=df&accountCode=666
 * @param req
 * @param res
 */
exports.getTermsAndConditionsAcceptedReport = function (req, res) {
    responsesManager.checkConfiguredResponsesWithNoLogic('players', 'GET');
    res.status(200).json(generatorHelper.generateArrayOf(5, casual.TermsAndConditionsAccepted(req.params.accountCode)));
};

