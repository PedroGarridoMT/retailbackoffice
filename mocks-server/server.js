let express = require("express");
let bodyParser = require("body-parser");
let methodOverride = require("method-override");
let morgan = require('morgan');
let casual = require('casual');
let checkHeaderCookie = require('./services/check-header-cookie');
let app = express();
let port = 9268;


// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(morgan('dev'));
app.use(morgan(function (tokens, req, res) {
    //Don't log mock/responses bodies because they will be printed on the responses stack
    if (req.url.indexOf('/responses') > -1) {
        return null;
    }
    if (Object.keys(req.body || {}).length) {
        return JSON.stringify(req.body, null, '   ').split('\n').forEach(function (line) {
            console.log(line)
        });
    }
    return null;
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Origin", "http://local.retail.mediatechsolutions.es:56320");
    res.header("Access-Control-Allow-Origin", "http://local.integration.mediatechsolutions.es:56320");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-XSRF-TOKEN");
    next();
});


// Import models
require('./models/user.model')(casual);
require('./models/player.model')(casual);
require('./models/transaction.model')(casual);
require('./models/limit.model')(casual);
require('./models/self-exclusion-history.model')(casual);
require('./models/terms-and-conditions.model')(casual);
require('./models/banned-player.model')(casual);
require('./models/resource.model')(casual);
require('./models/error.model')(casual);
require('./models/generic-response.model')(casual);
require('./models/documentation.model')(casual);
require('./models/bonus.model')(casual);
require('./models/blacklisted-person.model')(casual);


// Import controllers
let responsesController = require('./controllers/responses.controller');
let userController = require('./controllers/user.controller');
let sessionController = require('./controllers/session.controller');
let playerController = require('./controllers/player.controller');
let transactionController = require('./controllers/transaction.controller');
let limitController = require('./controllers/limit.controller');
let termsAndConditionsController = require('./controllers/terms-and-conditions.controller');
let selfExcludedController = require('./controllers/self-exclusion.controller');
let bannedPlayerController = require('./controllers/banned-player.controller');
let resourcesController = require('./controllers/resource.controller');
let configController = require('./controllers/config.controller');
let documentationController = require('./controllers/documentation.controller');
let bonusController = require('./controllers/bonus.controller');


// Non auth required - routes
let unprotectedRouter = express.Router();
unprotectedRouter.post('/mock/responses', responsesController.add);
unprotectedRouter.post('/authentication-service/login', userController.login);
unprotectedRouter.post('/v1/session', sessionController.loginUser);
unprotectedRouter.delete('/v1/session', sessionController.logoutUser);
unprotectedRouter.patch('/v1/session', sessionController.refreshToken);
app.use(unprotectedRouter);


// Protected API routes
let apiRouter = express.Router();
//apiRouter.use(checkHeaderCookie);

apiRouter.put('/authentication-service/account/password', userController.changePassword);

// Banned Players
apiRouter.get('/v1/players/banned', bannedPlayerController.getBannedPlayers);
apiRouter.get('/v1/players/banned/report', bannedPlayerController.getBannedPlayersReport);

// Players
apiRouter.get('/v1/players', playerController.getPlayers);
apiRouter.get('/v1/players/report', playerController.getPlayersReport);
apiRouter.post('/v1/players/black-listed', playerController.addBlacklistedPersons);
apiRouter.get('/v1/players/black-listed', playerController.getBlacklistedPersons);
apiRouter.get('/v1/players/:accountCode', playerController.getPlayerDetail);
apiRouter.put('/v1/players/:accountCode', playerController.updatePlayer);
apiRouter.patch('/v1/players/:accountCode', playerController.patchPlayer);
apiRouter.get('/v1/players/:accountCode/wallets', playerController.getPlayerWallets);
apiRouter.put('/v1/players/:accountCode/status', playerController.updatePlayerStatus);

//Notes
apiRouter.get('/v1/players/:accountCode/notes', playerController.getPlayerNotes);
apiRouter.post('/v1/players/:accountCode/notes', playerController.addPlayerNote);
apiRouter.delete('/v1/players/:accountCode/notes/:noteId', playerController.deletePlayerNote);
apiRouter.patch('/v1/players/:accountCode/notes/:noteId', playerController.patchPlayerNote);

// Transactions
apiRouter.get('/v1/players/:accountCode/transactions', transactionController.getPlayerTransactions);
apiRouter.get('/v1/players/:accountCode/transactions/report', transactionController.getPlayerTransactionsReport);
apiRouter.post('/v1/players/:accountCode/transactions', transactionController.manualAdjustment);
apiRouter.get('/v1/transactions', transactionController.getTransactions);
apiRouter.get('/v1/transactions/report', transactionController.getTransactionsReport);
apiRouter.get('/v1/transactions/withdrawals', transactionController.searchWithdrawals);
apiRouter.get('/v1/transactions/withdrawals/:transactionID', transactionController.getWithdrawalDetails);
apiRouter.patch('/v1/transactions/withdrawals/:transactionID', transactionController.patchWithdrawal);
apiRouter.get('/v1/transactions/sepa', transactionController.exportSepaFile);

// Limits
apiRouter.get('/v1/players/:accountCode/limits-operator', limitController.getOperatorLimits);
apiRouter.get('/v1/players/limits/changes', limitController.getPlayerLimitsChanges);
apiRouter.put('/v1/players/limits/changes/:limitChangeId', limitController.updatePlayerLimitChange);
apiRouter.get('/v1/players/:accountCode/limits', limitController.getPlayerLimits);
apiRouter.get('/v1/players/:accountCode/limits-history', limitController.getPlayerLimitsHistory);

// Documentation
apiRouter.get('/v1/players/:accountCode/documentation', documentationController.getPlayerDocumentation);
apiRouter.post('/v1/players/:accountCode/documentation', documentationController.uploadPlayerDocument);
apiRouter.get('/v1/players/:accountCode/documentation/:documentId', documentationController.getPlayerDocument);
apiRouter.delete('/v1/players/:accountCode/documentation/:documentId', documentationController.deletePlayerDocument);

// Bonuses
apiRouter.get('/v1/bounus/active', bonusController.getActiveBonuses);
apiRouter.get('/v1/players/:accountCode/bonuses', bonusController.getPlayerBonuses);
apiRouter.post('/v1/players/massiveBonus', bonusController.assignMassiveBonus);
apiRouter.delete('/v1/players/:accountCode/bonuses/:bonusName', bonusController.cancelPlayerBonus);

// Self Exclusion History
apiRouter.get('/v1/players/:accountCode/self-exclusion-history', selfExcludedController.getPlayerSelfExclusionHistory);
apiRouter.get('/v1/self-exclusion-history', selfExcludedController.getSelfExclusionHistory);
apiRouter.get('/v1/self-exclusion-history/report', selfExcludedController.getSelfExclusionHistoryReport);

// Terms And conditions
apiRouter.get('/v1/players/terms-and-conditions/acceptance', termsAndConditionsController.getTermsAndConditionsAccepted);
apiRouter.get('/v1/players/terms-and-conditions/acceptance/report', termsAndConditionsController.getTermsAndConditionsAcceptedReport);

// Resources / Catalog
apiRouter.get('/v1/resources/cities', resourcesController.getCities);
apiRouter.get('/v1/resources/regions', resourcesController.getRegions);
apiRouter.get('/v1/resources/fiscal_residence_regions', resourcesController.getFiscalRegions);

// PAS Config
apiRouter.get('/v1/config/transaction-types', configController.getTransactionTypes);
apiRouter.get('/v1/config/game-types', configController.getGameTypes);
apiRouter.get('/v1/config/payment-methods', configController.getPaymentMethods);
apiRouter.get('/v1/config/status-permissions', configController.getStatusPermissions);
apiRouter.get('/v1/config/environment', configController.getEnvironmentVariables);

app.use(apiRouter);


app.listen(port, function () {
    printSplash();
});


let printSplash = function () {
    console.log("\n\n\n" +
        "        .___  ___.   ______     ______  __  ___      _______.\r\n        |   \\\/   |  \/  __  \\   \/      " +
        "||  |\/  \/     \/       |\r\n        |  \\  \/  | |  |  |  | |  ,----\'|  \'  \/     |   (----`\r\n        " +
        "|  |\\\/|  | |  |  |  | |  |     |    <       \\   \\    \r\n        |  |  |  | |  `--\'  | |  `----.|  .  " +
        "\\  .----)   |   \r\n        |__|  |__|  \\______\/   \\______||__|\\__\\ |_______\/    \r\n" +
        "                                                     \r\n     _______. _______ .______     ____    ____  " +
        "_______ .______      \r\n    \/       ||   ____||   _  \\    \\   \\  \/   \/ |   ____||   _  \\     \r\n   " +
        "|   (----`|  |__   |  |_)  |    \\   \\\/   \/  |  |__   |  |_)  |    \r\n    \\   \\    |   __|  |      \/" +
        "      \\      \/   |   __|  |      \/     \r\n.----)   |   |  |____ |  |\\  \\----.  \\    \/    |  |____ |" +
        "  |\\  \\----.\r\n|_______\/    |_______|| _| `._____|   \\__\/     |_______|| _| `._____|\r\n" +
        "                                                                     \r\n" +
        "\n");

    console.log("Running  on http://localhost:" + port);
};
