import groovy.transform.Field

@Field def lastStatus = null

def warnIfStatusChange(current) {
  if (lastStatus == null) {
    lastStatus = current
    return
  }

  if (lastStatus == current) return
  if (current == null) return
  echo '*********************************************************'
  echo "WARNING: Status changed from ${lastStatus} to ${current}."
  echo '*********************************************************'
  lastStatus = current
}

pipeline {
  agent {
    node {
      label 'pas-java-builder'
      customWorkspace "/tmp/jenkins/${env.JOB_NAME}"
    }
  }

  environment {
    PATH_TO_BUILD = "."
    //PROJECT_NAME = "${env.PATH_TO_BUILD}".split('/')[-1].toLowerCase()
    PROJECT_NAME = 'pasbackoffice'
    REPOSITORY_NAME = scm.getUserRemoteConfigs()[0].getUrl().split('/')[-1].replace('.git', '')
    nexus_zip_repository='https://nexus.betserver.es/repository/misc/'
  }

  options {
    disableConcurrentBuilds()
    timeout(time: 45, unit:'MINUTES')
    timestamps()
    buildDiscarder(logRotator(numToKeepStr: '20'))
    ansiColor('xterm')
  }

  stages {
    stage('Setup') {
      steps {
        script {
          if (currentBuild.result == null) {
            currentBuild.result == 'SUCCESS'
          }
        }
        script{warnIfStatusChange(currentBuild.result)}

        echo "Calculate the version number"
        script {
          def version = readJSON file:'version.json'
          if (env.BRANCH_NAME == 'master') {
            env.VERSION="${version.major}.${version.minor}.${env.BUILD_NUMBER}"
          } else {
            env.VERSION="${version.major}.${version.minor}.${BUILD_NUMBER}-${env.BRANCH_NAME.replace('-', '')}"
          }

        }

        echo "Setting description"
        script {
          try {
            def commit = currentBuild.changeSets[0].items[0]
            currentBuild.setDescription("${env.VERSION} (${commit.commitId.take(8)}) - ${commit.author}<br/>")
          } catch (err) {
            println err
            currentBuild.setDescription("${env.VERSION}")
          }
        }

        echo "Setting versions"
        script {
          dir(env.PATH_TO_BUILD) {
            def pkg = readJSON file:'package.json'
            pkg.version = env.VERSION
            writeJSON file:'package.json', json:pkg, pretty: 2
            archiveArtifacts 'package.json'
          }
        }
        

        echo "Remove caches"
        script {
          try {
            sh 'rm -rf $HOME/.gradle'
          } catch (err) {
            println "WARNING: $err"
          }
        }

        echo "Retrieve certificates"
        script {
          try {
            sh '''
              url='sonar.test.int.mediatechsolutions.es'
              binUrl="https://bitbucket.org/daniel_aguado_mt/java_installcert/downloads/java_installcert-1.0.jar"
              file="/opt/InstallCert.jar"

              if [ -f "$file" ]
              then
                  echo "Certificates installed. Skipped"
              else
                  echo "Installing certificate from $url"
                  sudo curl -sLo $file $binUrl
                  sudo java -jar $file $url:443
              fi
            '''
          } catch (err) {
              echo err.message
          }
        }
      }
    }

    stage('Dependencies') {
      steps {
        dir(env.PATH_TO_BUILD) {
          sh(script: 'npm install')
          sh(script: 'npm list > versions.txt || true')
          archiveArtifacts 'versions.txt'
        }
      }
    }

    stage('Build') {
      steps {
        dir(env.PATH_TO_BUILD) {
          echo "Building..."
          sh(script: 'npm run build:aot')
        }
      }
    }

    stage('Unit Tests') {
      steps {
        dir(env.PATH_TO_BUILD) {
          echo "Testing..."
          sh(script: 'npm run server:mocks:ci')
          sh(script: "xvfb-run --server-args='-screen 0, 1280x1024x24' node_modules/karma/bin/karma start")
        }
        script{warnIfStatusChange(currentBuild.result)}
      }
    }

    stage('Quality') {
      steps {
        dir(env.PATH_TO_BUILD) {
          sh(script: 'npm run lint:ci')
        }
        script{warnIfStatusChange(currentBuild.result)}

        script {
           try {
            sh '''
              binUrl="https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.0.3.778-linux.zip"
              zip_file="/tmp/sonar.zip"
              unzipped_path="/tmp/sonar-scanner-3.0.3.778-linux"
              final_path="/tmp/sonar-scanner"

              if [ -f "$final_path" ]
              then
                  echo "Certificates installed. Skipped"
              else
                  curl -sLo $zip_file $binUrl
                  unzip $zip_file -d /tmp
                  mv $unzipped_path $final_path
              fi
            '''
          } catch (err) {
              echo err.message
          }
        }
        script{warnIfStatusChange(currentBuild.result)}

        dir(env.PATH_TO_BUILD) {
          withSonarQubeEnv('sonarqube') {
            sh "/tmp/sonar-scanner/bin/sonar-scanner -Dsonar.host.url=${env.SONAR_HOST_URL} -Dsonar.login=${SONAR_AUTH_TOKEN} -Dsonar.projectName=${env.PROJECT_NAME} -Dsonar.projectKey=${env.PROJECT_NAME} -Dsonar.projectVersion=${env.VERSION} -Dsonar.branch=${env.BRANCH_NAME} -Dsonar.sources=. -Dsonar.javascript.exclusions=**/node_modules/**,**/bower_components/**,**/build/**"
          }
        }
        script{warnIfStatusChange(currentBuild.result)}
      }
    }

    stage('Security') {
      steps {
        dir(env.PATH_TO_BUILD) {
          step([
              $class: 'DependencyCheckBuilder',
              datadir: '/tmp/owaspdb',
              hintsFile: '',
              includeCsvReports: false,
              includeHtmlReports: true,
              includeJsonReports: false,
              isAutoupdateDisabled: false,
              outdir: 'owasp',
              scanpath: 'package.json',
              skipOnScmChange: false,
              skipOnUpstreamChange: false,
              suppressionFile: '',
              zipExtensions: ''
          ])
        }
      }
    }

    stage('Publish') {
      when {
        expression {
          (env.BRANCH_NAME == 'master')
        }
      }
      steps {
        dir(env.PATH_TO_BUILD) {
          script {
            echo "Upload binary"
            sh 'gradle continuous_integration_publish'
          }
        }
      }
    }

    stage('Triggers') {
      when {
        expression {
            (currentBuild.result == 'SUCCESS' && "${env.BRANCH_NAME}" == "master" && !"${env.PATH_TO_BUILD}".contains("lib"))
        }
      }
      steps {
        script {
          echo "No triggers"
          //  build (
          //    job: it,
          //    parameters: [string(name: 'branch', value: "${env.BRANCH_NAME}")],
          //    wait: false,
          //    quietPeriod: 5,
          //  )
        }
      }
    }
  }

  post {
    always {
      script{warnIfStatusChange(currentBuild.result)}

      script {
        try {
          def props = readProperties file: "${env.PATH_TO_BUILD}/.scannerwork/report-task.txt"
          def dashboardUrl= props['dashboardUrl']
          currentBuild.setDescription(currentBuild.description + "<br/><b>SonarQube report:</b> <a href=\"${dashboardUrl}\">${dashboardUrl}</a>")
        } catch (err) {
          println("Found error when try to read sonarqube results file")
          println(err)
        }
      }

      script{warnIfStatusChange(currentBuild.result)}

      step([$class: 'DependencyCheckPublisher'])

      script{warnIfStatusChange(currentBuild.result)}

      step([
        $class: 'CoberturaPublisher',
        autoUpdateHealth: false,
        autoUpdateStability: false,
        coberturaReportFile: "coverage/cobertura.txt",
        failNoReports: false,
        failUnhealthy: false,
        failUnstable: false,
        maxNumberOfBuilds: 0,
        onlyStable: false,
        sourceEncoding: 'ASCII',
        zoomCoverageChart: false
      ])

      script{warnIfStatusChange(currentBuild.result)}

      step([
        $class: 'XUnitPublisher',
        testTimeMargin: '3000',
        thresholdMode: 1,
        thresholds: [[
          $class: 'FailedThreshold',
          failureNewThreshold: '',
          failureThreshold: '',
          unstableNewThreshold: '',
          unstableThreshold: ''
        ],
        [
          $class: 'SkippedThreshold',
          failureNewThreshold: '',
          failureThreshold: '',
          unstableNewThreshold: '',
          unstableThreshold: ''
        ]],
        tools: [[
          $class: 'JUnitType',
          deleteOutputFiles: true,
          failIfNotNew: true,
          pattern: "test_results/**/*.xml",
          skipNoTestFiles: false,
          stopProcessingIfError: true
        ]]
      ])

      script{warnIfStatusChange(currentBuild.result)}

      warnings(
        canComputeNew: false,
        canResolveRelativePaths: false,
        canRunOnFailed: true,
        categoriesPattern: '',
        defaultEncoding: '',
        excludePattern: '',
        healthy: '',
        includePattern: '',
        messagesPattern: '',
        parserConfigurations: [[
          parserName: 'JSLint',
          pattern: 'tslint_output.xml'
        ]],
        unHealthy: ''
      )

      script{warnIfStatusChange(currentBuild.result)}

      publishHTML([
        allowMissing: true,
        alwaysLinkToLastBuild: true,
        keepAll: false,
        reportDir: "owasp",
        reportFiles: 'dependency-check-report.html',
        reportName: 'Vulnerabilities Report',
        reportTitles: ''
      ])

      script{warnIfStatusChange(currentBuild.result)}

      publishHTML([
        allowMissing: true,
        alwaysLinkToLastBuild: true,
        keepAll: false,
        reportDir: 'coverage/html',
        reportFiles: 'index.html',
        reportName: 'Coverage',
        reportTitles: ''
      ])

      script{warnIfStatusChange(currentBuild.result)}

      script {
        slackSend(
          channel: "#ci_${env.REPOSITORY_NAME}".take(23),
          color: (currentBuild.result == 'SUCCESS' ? 'good' : (currentBuild.result == 'FAILURE' ? 'danger' : '#FFFF00')),
          message: "${currentBuild.result} --> Job ${env.JOB_NAME} - #${env.BUILD_NUMBER} (${currentBuild.durationString}) (<${env.BUILD_URL}|Open>)"
        )
      }

      script{warnIfStatusChange(currentBuild.result)}

      script {
         bitbucketStatusNotify(
          buildState: (currentBuild.result == 'FAILURE' ? 'FAILED' : 'SUCCESSFUL'),
        )
      }

      script{warnIfStatusChange(currentBuild.result)}

      script {
        if (currentBuild.result != 'SUCCESS') {
          emailext(
            attachLog: false,
            compressLog: false,
            recipientProviders: [[$class: 'DevelopersRecipientProvider']],
            subject: "Jenkins job ${env.JOB_NAME}",
            body: '''${SCRIPT, template="mediatech.template"}'''
          )
        }
      }
    }
  }
}
